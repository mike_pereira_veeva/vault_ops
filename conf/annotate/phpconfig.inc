<?php

  // Configuration file for A.nnotate server.
  // 
  // This sample uses static linked linux binaries in the bin/ 
  // folder for pdf to image and text conversions.
  //  
  // For more configuration options, or to use your 
  // own version of xpdf and poppler, see phpconfig-linux.inc
  //
  // Edit the settings below for a local install and copy to:
  // 'phpconfig.inc' in the php/ directory.
  //


//
// *** Required: Site of installed machine.
//
$nnotatesite = "vv1.veevavault.com";
// e.g. $nnotatesite = "yourcompany.com";
// $nnotatesite = "a.nnotate.com";
// $nnotatesite = "192.168.1.123";


//
// *** Required: Full path to your installation.
// (do not include a trailing "/"):
// examples:
// $nnotatepath = "http://yourcompany.com/your-dir";
// $nnotatepath = "http://ec2-1-2-3-4.compute-1.amazonaws.com/annotate";
//
$nnotatepath = "https://vv1.veevavault.com/annotate/annotate408";


//
// *** Required: replace this with your license code.
//
$licensecode = "7140-5436-1615-9049";


//
// *** Required: Set the list of admin users - they will have access to
// php/serverAdmin.php when logged in.
// Example: for more than one admin user, use:
//  $adminusers = array("user1@yourcompany.com", "user2@yourcompany.com");
//
$adminusers = array("derek.allwardt@veevasystems.com");

//
// To enable API access to this installation.
//
$enableapi = 1;


// *** Required - edit the annotatebindir to 
// point at the static executables.
//
// See phpconfig-linux.inc if using standard xpdf or poppler.
//
$annotatebindir="/usr/local/apache-tomcat/6.0.32/webapps/annotate/annotate408/bin/";

$pdftoimage   = $annotatebindir."pdftoimage"; 
$wordscommand = $annotatebindir."pdftotext -enc UTF-8 -raw";
$infocommand  = $annotatebindir."pdfinfo";


// *** If installing on Quercus java servlets (not Apache) ***
// Set the document and notes storage folders.
// If you change this, you must also edit your 
// web server config to serve (e.g.) 
//   http://yoursite.com/annotate/docs/
// from the given folder - or make a softlink
// and check the web server is following them...
//
$docsurl = $nnotatepath."/docs/docs";

$docsdir    = "/data2/annotate/data/public/docs/";
$privatedir = "/data2/annotate/data/private/";


// Path to java (for pdf export, pdf outline extraction)
$javaexe = "/usr/bin/java";


// May 2011 beta; hide some features which will be ready for the
// full release (see embed-guide.html for full list of hide codes)
$defaultHide = "nlb-fht-rtb-pre-wsb";

$enablePrivateWorkspaces = 1; // For api-uploaded documents.
// $noSampleDocument = 1;

// *** Important: If you will be installing to a server which
// does not have internet access, you will need to uncomment
// both lines below:
//
// $noNewAccountEmail = 1;
// $mailsender = "none";   

require_once("customRepo.php");

// --- omitting the optional closing php tag on purpose in config files --- 
