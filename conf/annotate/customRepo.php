<?php

  //
  // Sample functions which provide the mapping from 
  // repository ID to folders and URLs to fetch documents.
  //
  // Copy to 'customRepo.php' (or your chosen name)
  // and add the line below to the end of phpconfig.inc:
  //
  //   require_once('customRepo.php')
  //


// The base of the storage path for all repositories (used below)
$customrepobase       = "/data2/annotate/data/public/";
$customrepobaseprivate       = "/data2/annotate/data/private/";

// A script or static URL which will fetch a document from a 
// repository. A path like '2011-01-01/abc/page-1.js' (or .png)
// will be appended to this URL to fetch the given item.
//$customrepodocsurl   = "/repo/php/fetch.php?repoid=__ID__&doc=";

// It may be faster to use a static path if you can serve this
// data staticly rather than through a script, as many web servers 
// include optimizations for static files:
$customrepodocsurl  = "/annotate/annotate408/docs/repo-__ID__/";


// These functions implement the mapping from 
// repoid to paths and URLs. More complex mappings
// (e.g. where an id like 123456 maps to a path like 12/34/56)
// should be coded here.
function getDocsDirForRepo($repoid) {
  global $customrepobase;
  return $customrepobase."repo-".$repoid."/";
}
function getPrivateDirForRepo($repoid) {
  global $customrepobaseprivate;
  return $customrepobaseprivate."repo-".$repoid."/";
}
function getDocsUrlForRepo($repoid) {
  global $customrepodocsurl;
  return str_replace("__ID__", $repoid, $customrepodocsurl);
}

// Check if repoid GET or POSTed to an annotate php call.
function maybeGetRepoID() {
  $repoid = "";
  if (isset($_GET["repoid"]))  { $repoid = $_GET['repoid']; }
  if (isset($_POST["repoid"])) { $repoid = $_POST['repoid']; }
  return $repoid;
}

function setRepoPaths($repoid) {
  global $docsdir, $docsurl, $privatedir;
  $privatedir = getPrivateDirForRepo($repoid);
  $docsdir    = getDocsDirForRepo($repoid);
  $docsurl    = getDocsUrlForRepo($repoid);
}
$repoid = maybeGetRepoID();
if ($repoid) { 
  setRepoPaths($repoid); 
 }


// --- closing tag omitted on purpose for config file
