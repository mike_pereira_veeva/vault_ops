-- MySQL dump 10.13  Distrib 5.1.56, for unknown-linux-gnu (x86_64)
--
-- Host: localhost    Database: instance_1
-- ------------------------------------------------------
-- Server version	5.1.56-rel12.7-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `instance_1`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `instance_1` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `instance_1`;

--
-- Table structure for table `ACT_GE_BYTEARRAY`
--

DROP TABLE IF EXISTS `ACT_GE_BYTEARRAY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_GE_BYTEARRAY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BYTES_` longblob,
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_BYTEARR_DEPL` (`DEPLOYMENT_ID_`),
  CONSTRAINT `ACT_FK_BYTEARR_DEPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `ACT_RE_DEPLOYMENT` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_GE_BYTEARRAY`
--

LOCK TABLES `ACT_GE_BYTEARRAY` WRITE;
/*!40000 ALTER TABLE `ACT_GE_BYTEARRAY` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_GE_BYTEARRAY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_GE_PROPERTY`
--

DROP TABLE IF EXISTS `ACT_GE_PROPERTY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_GE_PROPERTY` (
  `NAME_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `VALUE_` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `REV_` int(11) DEFAULT NULL,
  PRIMARY KEY (`NAME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_GE_PROPERTY`
--

LOCK TABLES `ACT_GE_PROPERTY` WRITE;
/*!40000 ALTER TABLE `ACT_GE_PROPERTY` DISABLE KEYS */;
INSERT INTO `ACT_GE_PROPERTY` VALUES ('historyLevel','2',1),('next.dbid','201',3),('schema.history','create(5.5)',1),('schema.version','5.5',1);
/*!40000 ALTER TABLE `ACT_GE_PROPERTY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_ACTINST`
--

DROP TABLE IF EXISTS `ACT_HI_ACTINST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_ACTINST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8_bin NOT NULL,
  `ACT_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ACT_TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime NOT NULL,
  `END_TIME_` datetime DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_ACT_INST_START` (`START_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_END` (`END_TIME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_ACTINST`
--

LOCK TABLES `ACT_HI_ACTINST` WRITE;
/*!40000 ALTER TABLE `ACT_HI_ACTINST` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_ACTINST` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_ATTACHMENT`
--

DROP TABLE IF EXISTS `ACT_HI_ATTACHMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_ATTACHMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `URL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CONTENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_ATTACHMENT`
--

LOCK TABLES `ACT_HI_ATTACHMENT` WRITE;
/*!40000 ALTER TABLE `ACT_HI_ATTACHMENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_ATTACHMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_COMMENT`
--

DROP TABLE IF EXISTS `ACT_HI_COMMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_COMMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TIME_` datetime NOT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACTION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MESSAGE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `FULL_MSG_` longblob,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_COMMENT`
--

LOCK TABLES `ACT_HI_COMMENT` WRITE;
/*!40000 ALTER TABLE `ACT_HI_COMMENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_COMMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_DETAIL`
--

DROP TABLE IF EXISTS `ACT_HI_DETAIL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_DETAIL` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACT_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TIME_` datetime NOT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_DETAIL_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_ACT_INST` (`ACT_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_TIME` (`TIME_`),
  KEY `ACT_IDX_HI_DETAIL_NAME` (`NAME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_DETAIL`
--

LOCK TABLES `ACT_HI_DETAIL` WRITE;
/*!40000 ALTER TABLE `ACT_HI_DETAIL` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_DETAIL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_PROCINST`
--

DROP TABLE IF EXISTS `ACT_HI_PROCINST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_PROCINST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `START_TIME_` datetime NOT NULL,
  `END_TIME_` datetime DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  `START_USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `START_ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `END_ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `PROC_INST_ID_` (`PROC_INST_ID_`),
  UNIQUE KEY `ACT_UNIQ_HI_BUS_KEY` (`PROC_DEF_ID_`,`BUSINESS_KEY_`),
  KEY `ACT_IDX_HI_PRO_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_PRO_I_BUSKEY` (`BUSINESS_KEY_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_PROCINST`
--

LOCK TABLES `ACT_HI_PROCINST` WRITE;
/*!40000 ALTER TABLE `ACT_HI_PROCINST` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_PROCINST` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_TASKINST`
--

DROP TABLE IF EXISTS `ACT_HI_TASKINST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_TASKINST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `OWNER_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime NOT NULL,
  `END_TIME_` datetime DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  `DELETE_REASON_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PRIORITY_` int(11) DEFAULT NULL,
  `DUE_DATE_` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_TASKINST`
--

LOCK TABLES `ACT_HI_TASKINST` WRITE;
/*!40000 ALTER TABLE `ACT_HI_TASKINST` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_TASKINST` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_ID_GROUP`
--

DROP TABLE IF EXISTS `ACT_ID_GROUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_ID_GROUP` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_ID_GROUP`
--

LOCK TABLES `ACT_ID_GROUP` WRITE;
/*!40000 ALTER TABLE `ACT_ID_GROUP` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_ID_GROUP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_ID_INFO`
--

DROP TABLE IF EXISTS `ACT_ID_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_ID_INFO` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `USER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `VALUE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PASSWORD_` longblob,
  `PARENT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_ID_INFO`
--

LOCK TABLES `ACT_ID_INFO` WRITE;
/*!40000 ALTER TABLE `ACT_ID_INFO` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_ID_INFO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_ID_MEMBERSHIP`
--

DROP TABLE IF EXISTS `ACT_ID_MEMBERSHIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_ID_MEMBERSHIP` (
  `USER_ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `GROUP_ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`USER_ID_`,`GROUP_ID_`),
  KEY `ACT_FK_MEMB_GROUP` (`GROUP_ID_`),
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `ACT_ID_GROUP` (`ID_`),
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `ACT_ID_USER` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_ID_MEMBERSHIP`
--

LOCK TABLES `ACT_ID_MEMBERSHIP` WRITE;
/*!40000 ALTER TABLE `ACT_ID_MEMBERSHIP` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_ID_MEMBERSHIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_ID_USER`
--

DROP TABLE IF EXISTS `ACT_ID_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_ID_USER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `FIRST_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LAST_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PWD_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PICTURE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_ID_USER`
--

LOCK TABLES `ACT_ID_USER` WRITE;
/*!40000 ALTER TABLE `ACT_ID_USER` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_ID_USER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RE_DEPLOYMENT`
--

DROP TABLE IF EXISTS `ACT_RE_DEPLOYMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RE_DEPLOYMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DEPLOY_TIME_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RE_DEPLOYMENT`
--

LOCK TABLES `ACT_RE_DEPLOYMENT` WRITE;
/*!40000 ALTER TABLE `ACT_RE_DEPLOYMENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RE_DEPLOYMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RE_PROCDEF`
--

DROP TABLE IF EXISTS `ACT_RE_PROCDEF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RE_PROCDEF` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `VERSION_` int(11) DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RE_PROCDEF`
--

LOCK TABLES `ACT_RE_PROCDEF` WRITE;
/*!40000 ALTER TABLE `ACT_RE_PROCDEF` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RE_PROCDEF` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RU_EXECUTION`
--

DROP TABLE IF EXISTS `ACT_RU_EXECUTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RU_EXECUTION` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BUSINESS_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `SUPER_EXEC_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IS_ACTIVE_` tinyint(4) DEFAULT NULL,
  `IS_CONCURRENT_` tinyint(4) DEFAULT NULL,
  `IS_SCOPE_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_UNIQ_RU_BUS_KEY` (`PROC_DEF_ID_`,`BUSINESS_KEY_`),
  KEY `ACT_IDX_EXEC_BUSKEY` (`BUSINESS_KEY_`),
  KEY `ACT_FK_EXE_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_EXE_PARENT` (`PARENT_ID_`),
  KEY `ACT_FK_EXE_SUPER` (`SUPER_EXEC_`),
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`),
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RU_EXECUTION`
--

LOCK TABLES `ACT_RU_EXECUTION` WRITE;
/*!40000 ALTER TABLE `ACT_RU_EXECUTION` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RU_EXECUTION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RU_IDENTITYLINK`
--

DROP TABLE IF EXISTS `ACT_RU_IDENTITYLINK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RU_IDENTITYLINK` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `GROUP_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_IDENT_LNK_USER` (`USER_ID_`),
  KEY `ACT_IDX_IDENT_LNK_GROUP` (`GROUP_ID_`),
  KEY `ACT_FK_TSKASS_TASK` (`TASK_ID_`),
  CONSTRAINT `ACT_FK_TSKASS_TASK` FOREIGN KEY (`TASK_ID_`) REFERENCES `ACT_RU_TASK` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RU_IDENTITYLINK`
--

LOCK TABLES `ACT_RU_IDENTITYLINK` WRITE;
/*!40000 ALTER TABLE `ACT_RU_IDENTITYLINK` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RU_IDENTITYLINK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RU_JOB`
--

DROP TABLE IF EXISTS `ACT_RU_JOB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RU_JOB` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LOCK_OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RETRIES_` int(11) DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_JOB_EXCEPTION` (`EXCEPTION_STACK_ID_`),
  CONSTRAINT `ACT_FK_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RU_JOB`
--

LOCK TABLES `ACT_RU_JOB` WRITE;
/*!40000 ALTER TABLE `ACT_RU_JOB` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RU_JOB` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RU_TASK`
--

DROP TABLE IF EXISTS `ACT_RU_TASK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RU_TASK` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `OWNER_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DELEGATION_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PRIORITY_` int(11) DEFAULT NULL,
  `CREATE_TIME_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DUE_DATE_` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_TASK_CREATE` (`CREATE_TIME_`),
  KEY `ACT_FK_TASK_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_TASK_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_TASK_PROCDEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RU_TASK`
--

LOCK TABLES `ACT_RU_TASK` WRITE;
/*!40000 ALTER TABLE `ACT_RU_TASK` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RU_TASK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RU_VARIABLE`
--

DROP TABLE IF EXISTS `ACT_RU_VARIABLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RU_VARIABLE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_VAR_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_VAR_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_VAR_BYTEARRAY` (`BYTEARRAY_ID_`),
  CONSTRAINT `ACT_FK_VAR_BYTEARRAY` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RU_VARIABLE`
--

LOCK TABLES `ACT_RU_VARIABLE` WRITE;
/*!40000 ALTER TABLE `ACT_RU_VARIABLE` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RU_VARIABLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_template`
--

DROP TABLE IF EXISTS `email_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_bin NOT NULL,
  `value` longtext COLLATE utf8_bin,
  `version` int(11) DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `master_template` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_template`
--

LOCK TABLES `email_template` WRITE;
/*!40000 ALTER TABLE `email_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_access_token`
--

DROP TABLE IF EXISTS `veeva_access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_token` varchar(255) COLLATE utf8_bin NOT NULL,
  `access_identifier` varchar(255) COLLATE utf8_bin NOT NULL,
  `contact_user_id` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `status` varchar(255) COLLATE utf8_bin NOT NULL,
  `target_user_id` bigint(20) NOT NULL,
  `target_user_email` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `allow_rendition_download` bit(1) DEFAULT b'0',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_access_token`
--

LOCK TABLES `veeva_access_token` WRITE;
/*!40000 ALTER TABLE `veeva_access_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_access_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_admin_audit`
--

DROP TABLE IF EXISTS `veeva_admin_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_admin_audit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event` varchar(255) COLLATE utf8_bin NOT NULL,
  `event_time` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `new_value` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `object_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `object_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `old_value` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `property` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `vmc_user_on_behalf_of` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_admin_audit`
--

LOCK TABLES `veeva_admin_audit` WRITE;
/*!40000 ALTER TABLE `veeva_admin_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_admin_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_auto_number`
--

DROP TABLE IF EXISTS `veeva_auto_number`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_auto_number` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `pattern` varchar(100) COLLATE utf8_bin NOT NULL,
  `last_used_value` int(11) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `search_pattern` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `search_pattern` (`instance_id`,`search_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_auto_number`
--

LOCK TABLES `veeva_auto_number` WRITE;
/*!40000 ALTER TABLE `veeva_auto_number` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_auto_number` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_auto_number_format`
--

DROP TABLE IF EXISTS `veeva_auto_number_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_auto_number_format` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `pattern` varchar(100) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `format_key` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `format_key` (`instance_id`,`format_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_auto_number_format`
--

LOCK TABLES `veeva_auto_number_format` WRITE;
/*!40000 ALTER TABLE `veeva_auto_number_format` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_auto_number_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_catalog`
--

DROP TABLE IF EXISTS `veeva_catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_catalog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `catalog_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `public_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `active` bit(1) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `version` int(11) DEFAULT NULL,
  `system` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `catalog_key` (`instance_id`,`catalog_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_catalog`
--

LOCK TABLES `veeva_catalog` WRITE;
/*!40000 ALTER TABLE `veeva_catalog` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_catalog_value`
--

DROP TABLE IF EXISTS `veeva_catalog_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_catalog_value` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `catalog_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `catalog_value_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `active` bit(1) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `version` int(11) DEFAULT NULL,
  `public_key` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `catalog_value_key` (`instance_id`,`catalog_key`,`catalog_value_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_catalog_value`
--

LOCK TABLES `veeva_catalog_value` WRITE;
/*!40000 ALTER TABLE `veeva_catalog_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_catalog_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_compound_document_node`
--

DROP TABLE IF EXISTS `veeva_compound_document_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_compound_document_node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `node_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `node_type` varchar(25) COLLATE utf8_bin NOT NULL,
  `parent_node_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `position_in_parent` int(11) NOT NULL,
  `section_number` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `section_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `referenced_document_id` bigint(20) DEFAULT NULL,
  `referenced_major_version` int(11) DEFAULT NULL,
  `referenced_minor_version` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `node_key` (`instance_id`,`document_id`,`major_version`,`minor_version`,`node_key`),
  KEY `doc_id` (`instance_id`,`document_id`,`major_version`,`minor_version`),
  KEY `ref_doc_id` (`instance_id`,`referenced_document_id`),
  KEY `parent_node` (`instance_id`,`document_id`,`major_version`,`minor_version`,`parent_node_key`,`position_in_parent`),
  KEY `ref_doc_version` (`instance_id`,`referenced_document_id`,`referenced_major_version`,`referenced_minor_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_compound_document_node`
--

LOCK TABLES `veeva_compound_document_node` WRITE;
/*!40000 ALTER TABLE `veeva_compound_document_node` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_compound_document_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_compound_document_template`
--

DROP TABLE IF EXISTS `veeva_compound_document_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_compound_document_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `public_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `disabled` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`template_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_compound_document_template`
--

LOCK TABLES `veeva_compound_document_template` WRITE;
/*!40000 ALTER TABLE `veeva_compound_document_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_compound_document_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_compound_document_template_base`
--

DROP TABLE IF EXISTS `veeva_compound_document_template_base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_compound_document_template_base` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `doc_type_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `doc_sub_type_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `doc_classification_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `standard` tinyint(1) DEFAULT '0',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_key` (`instance_id`,`template_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_compound_document_template_base`
--

LOCK TABLES `veeva_compound_document_template_base` WRITE;
/*!40000 ALTER TABLE `veeva_compound_document_template_base` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_compound_document_template_base` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_compound_document_template_node`
--

DROP TABLE IF EXISTS `veeva_compound_document_template_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_compound_document_template_node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `parent_node_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `node_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `node_type` varchar(25) COLLATE utf8_bin NOT NULL,
  `section_number` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `position_in_parent` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `root` tinyint(1) DEFAULT '0',
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `node_key` (`instance_id`,`template_key`,`node_key`),
  KEY `parent_node` (`instance_id`,`template_key`,`parent_node_key`,`position_in_parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_compound_document_template_node`
--

LOCK TABLES `veeva_compound_document_template_node` WRITE;
/*!40000 ALTER TABLE `veeva_compound_document_template_node` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_compound_document_template_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_country`
--

DROP TABLE IF EXISTS `veeva_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `country_key` bigint(20) NOT NULL,
  `active` bit(1) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `country_key` (`instance_id`,`country_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_country`
--

LOCK TABLES `veeva_country` WRITE;
/*!40000 ALTER TABLE `veeva_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_dashboard`
--

DROP TABLE IF EXISTS `veeva_dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_dashboard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `layout_key` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_dashboard`
--

LOCK TABLES `veeva_dashboard` WRITE;
/*!40000 ALTER TABLE `veeva_dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_dashboard_component`
--

DROP TABLE IF EXISTS `veeva_dashboard_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_dashboard_component` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `data_source_key` bigint(20) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_dashboard_component`
--

LOCK TABLES `veeva_dashboard_component` WRITE;
/*!40000 ALTER TABLE `veeva_dashboard_component` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_dashboard_component` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_dashboard_data_source`
--

DROP TABLE IF EXISTS `veeva_dashboard_data_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_dashboard_data_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `report_key` bigint(20) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_dashboard_data_source`
--

LOCK TABLES `veeva_dashboard_data_source` WRITE;
/*!40000 ALTER TABLE `veeva_dashboard_data_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_dashboard_data_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_dashboard_layout`
--

DROP TABLE IF EXISTS `veeva_dashboard_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_dashboard_layout` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `component_key` bigint(20) DEFAULT NULL,
  `layout` longtext COLLATE utf8_bin,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_dashboard_layout`
--

LOCK TABLES `veeva_dashboard_layout` WRITE;
/*!40000 ALTER TABLE `veeva_dashboard_layout` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_dashboard_layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_dashboard_sharing`
--

DROP TABLE IF EXISTS `veeva_dashboard_sharing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_dashboard_sharing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `dashboard_key` bigint(20) NOT NULL,
  `share_user_id` bigint(20) NOT NULL,
  `share_type` varchar(20) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `share_user_type` varchar(20) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_dashboard_sharing`
--

LOCK TABLES `veeva_dashboard_sharing` WRITE;
/*!40000 ALTER TABLE `veeva_dashboard_sharing` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_dashboard_sharing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc`
--

DROP TABLE IF EXISTS `veeva_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `currently_checked_out_by` bigint(20) DEFAULT NULL,
  `currently_checked_out_date` datetime DEFAULT NULL,
  `document_number` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `deleted` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'N',
  `compound_document` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `document_number` (`instance_id`,`document_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc`
--

LOCK TABLES `veeva_doc` WRITE;
/*!40000 ALTER TABLE `veeva_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_attribute`
--

DROP TABLE IF EXISTS `veeva_doc_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_attribute` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `category_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `category_type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `position_in_section` int(11) NOT NULL,
  `section_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `system_attribute` bit(1) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `is_shared` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`attribute_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`),
  KEY `section_key` (`instance_id`,`section_key`,`position_in_section`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_attribute`
--

LOCK TABLES `veeva_doc_attribute` WRITE;
/*!40000 ALTER TABLE `veeva_doc_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_attribute_category_key`
--

DROP TABLE IF EXISTS `veeva_doc_attribute_category_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_attribute_category_key` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `attribute_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `category_key` varchar(1024) COLLATE utf8_bin NOT NULL,
  `category_type` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_attribute_category_key`
--

LOCK TABLES `veeva_doc_attribute_category_key` WRITE;
/*!40000 ALTER TABLE `veeva_doc_attribute_category_key` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_attribute_category_key` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_attribute_dependency_rule`
--

DROP TABLE IF EXISTS `veeva_doc_attribute_dependency_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_attribute_dependency_rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `rule_key` varchar(50) COLLATE utf8_bin NOT NULL,
  `category_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `controlling_attribute_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `controlling_attribute_value` varchar(250) COLLATE utf8_bin NOT NULL,
  `controlled_attribute_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `rule_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `rule_key` (`instance_id`,`rule_key`),
  KEY `controlling_attribute` (`instance_id`,`controlling_attribute_key`),
  KEY `controlled_attribute` (`instance_id`,`controlled_attribute_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_attribute_dependency_rule`
--

LOCK TABLES `veeva_doc_attribute_dependency_rule` WRITE;
/*!40000 ALTER TABLE `veeva_doc_attribute_dependency_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_attribute_dependency_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_audit`
--

DROP TABLE IF EXISTS `veeva_doc_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_audit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `doc_id` bigint(20) NOT NULL,
  `event` varchar(255) COLLATE utf8_bin NOT NULL,
  `event_time` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `new_value` varchar(1500) COLLATE utf8_bin DEFAULT NULL,
  `old_value` varchar(1500) COLLATE utf8_bin DEFAULT NULL,
  `property` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `signature_meaning` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `task_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `workflow_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `vmc_user_on_behalf_of` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `DOC_ID_PROPERTY_IDX` (`doc_id`,`property`),
  KEY `event` (`instance_id`,`event`,`event_time`),
  KEY `event_user` (`instance_id`,`user_id`,`event`,`event_time`),
  KEY `event_time` (`doc_id`,`event`,`event_time`),
  KEY `event_time2` (`event_time`,`doc_id`,`event`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_audit`
--

LOCK TABLES `veeva_doc_audit` WRITE;
/*!40000 ALTER TABLE `veeva_doc_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_comment`
--

DROP TABLE IF EXISTS `veeva_doc_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `comment_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `reply_to` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `comment_text` longtext COLLATE utf8_bin NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `comment_key` (`instance_id`,`comment_key`),
  KEY `reply_to` (`instance_id`,`reply_to`),
  KEY `document_id` (`instance_id`,`document_id`,`major_version`,`minor_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_comment`
--

LOCK TABLES `veeva_doc_comment` WRITE;
/*!40000 ALTER TABLE `veeva_doc_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_favorites`
--

DROP TABLE IF EXISTS `veeva_doc_favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_favorites` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `document_id` bigint(20) NOT NULL,
  `when_added` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`instance_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_favorites`
--

LOCK TABLES `veeva_doc_favorites` WRITE;
/*!40000 ALTER TABLE `veeva_doc_favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_object_reference`
--

DROP TABLE IF EXISTS `veeva_doc_object_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_object_reference` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `referenced_object_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `referenced_object_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `referenced_parent_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `referencing_object_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `referencing_object_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `referencing_parent_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `referenced_object` (`instance_id`,`referenced_object_type`,`referenced_object_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_object_reference`
--

LOCK TABLES `veeva_doc_object_reference` WRITE;
/*!40000 ALTER TABLE `veeva_doc_object_reference` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_object_reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_property_level_security`
--

DROP TABLE IF EXISTS `veeva_doc_property_level_security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_property_level_security` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `rule_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `rule_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `doc_property_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `deleted` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `user_group_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `user_group_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rule_key` (`instance_id`,`rule_key`),
  KEY `doc_property` (`instance_id`,`doc_property_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_property_level_security`
--

LOCK TABLES `veeva_doc_property_level_security` WRITE;
/*!40000 ALTER TABLE `veeva_doc_property_level_security` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_property_level_security` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_recent`
--

DROP TABLE IF EXISTS `veeva_doc_recent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_recent` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `document_id` bigint(20) NOT NULL,
  `action` varchar(100) COLLATE utf8_bin NOT NULL,
  `when_added` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`instance_id`,`user_id`),
  KEY `document_id` (`instance_id`,`user_id`,`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_recent`
--

LOCK TABLES `veeva_doc_recent` WRITE;
/*!40000 ALTER TABLE `veeva_doc_recent` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_recent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_rel`
--

DROP TABLE IF EXISTS `veeva_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_rel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `relationship_type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `source_document_id` bigint(20) NOT NULL,
  `target_document_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `source_major_version` int(11) DEFAULT NULL,
  `source_minor_version` int(11) DEFAULT NULL,
  `target_major_version` int(11) DEFAULT NULL,
  `target_minor_version` int(11) DEFAULT NULL,
  `source_third_party_linkage` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `target_third_party_linkage` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `third_party_linkage` (`target_third_party_linkage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_rel`
--

LOCK TABLES `veeva_doc_rel` WRITE;
/*!40000 ALTER TABLE `veeva_doc_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_rel_type`
--

DROP TABLE IF EXISTS `veeva_doc_rel_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_rel_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `relationship_type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `deleted` bit(1) DEFAULT b'0',
  `disabled` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`relationship_type_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_rel_type`
--

LOCK TABLES `veeva_doc_rel_type` WRITE;
/*!40000 ALTER TABLE `veeva_doc_rel_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_rel_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_rendition`
--

DROP TABLE IF EXISTS `veeva_doc_rendition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_rendition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `rendition_type` varchar(200) COLLATE utf8_bin NOT NULL,
  `rendition_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `content_size` bigint(20) DEFAULT NULL,
  `mime_type` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `upload_date` datetime DEFAULT NULL,
  `upload_user` bigint(20) DEFAULT NULL,
  `file_name` varchar(1000) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `document_id` (`instance_id`,`document_id`,`major_version`,`minor_version`),
  KEY `rendition_type` (`instance_id`,`rendition_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_rendition`
--

LOCK TABLES `veeva_doc_rendition` WRITE;
/*!40000 ALTER TABLE `veeva_doc_rendition` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_rendition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_rendition_type`
--

DROP TABLE IF EXISTS `veeva_doc_rendition_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_rendition_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `rendition_type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `deleted` bit(1) DEFAULT b'0',
  `disabled` bit(1) DEFAULT b'0',
  `removable_from_doc` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`rendition_type_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_rendition_type`
--

LOCK TABLES `veeva_doc_rendition_type` WRITE;
/*!40000 ALTER TABLE `veeva_doc_rendition_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_rendition_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_section`
--

DROP TABLE IF EXISTS `veeva_doc_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_section` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `properties_can_be_added` bit(1) NOT NULL,
  `section_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `section_order` int(11) NOT NULL,
  `system_defined` bit(1) NOT NULL,
  `user_modifiable` bit(1) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `country_flag_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`section_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_section`
--

LOCK TABLES `veeva_doc_section` WRITE;
/*!40000 ALTER TABLE `veeva_doc_section` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_status_role_permission`
--

DROP TABLE IF EXISTS `veeva_doc_status_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_status_role_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `document_status` varchar(255) COLLATE utf8_bin NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle` varchar(255) COLLATE utf8_bin NOT NULL,
  `permission` varchar(255) COLLATE utf8_bin NOT NULL,
  `role_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`instance_id`,`lifecycle`,`document_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_status_role_permission`
--

LOCK TABLES `veeva_doc_status_role_permission` WRITE;
/*!40000 ALTER TABLE `veeva_doc_status_role_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_status_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_template`
--

DROP TABLE IF EXISTS `veeva_doc_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `doc_type_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `doc_sub_type_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `doc_classification_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `template_mime_type` varchar(200) COLLATE utf8_bin NOT NULL,
  `template_file_path` varchar(500) COLLATE utf8_bin NOT NULL,
  `template_file_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `template_size` bigint(20) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `file_upload_date` datetime DEFAULT NULL,
  `file_upload_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_key` (`instance_id`,`template_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_template`
--

LOCK TABLES `veeva_doc_template` WRITE;
/*!40000 ALTER TABLE `veeva_doc_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_type`
--

DROP TABLE IF EXISTS `veeva_doc_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `system` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`type_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_type`
--

LOCK TABLES `veeva_doc_type` WRITE;
/*!40000 ALTER TABLE `veeva_doc_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_type_filter`
--

DROP TABLE IF EXISTS `veeva_doc_type_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_type_filter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `sub_type_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `classification_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `attribute_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_type` (`instance_id`,`type_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_type_filter`
--

LOCK TABLES `veeva_doc_type_filter` WRITE;
/*!40000 ALTER TABLE `veeva_doc_type_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_type_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_type_lifecycle`
--

DROP TABLE IF EXISTS `veeva_doc_type_lifecycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_type_lifecycle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `sub_type_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `classification_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `lifecycle_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_type` (`instance_id`,`type_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_type_lifecycle`
--

LOCK TABLES `veeva_doc_type_lifecycle` WRITE;
/*!40000 ALTER TABLE `veeva_doc_type_lifecycle` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_type_lifecycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_type_rendition_type`
--

DROP TABLE IF EXISTS `veeva_doc_type_rendition_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_type_rendition_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `sub_type_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `classification_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `rendition_type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_type` (`instance_id`,`type_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_type_rendition_type`
--

LOCK TABLES `veeva_doc_type_rendition_type` WRITE;
/*!40000 ALTER TABLE `veeva_doc_type_rendition_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_type_rendition_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_type_role`
--

DROP TABLE IF EXISTS `veeva_doc_type_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_type_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `sub_type_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `classification_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `role` varchar(20) COLLATE utf8_bin NOT NULL,
  `role_type` varchar(20) COLLATE utf8_bin NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_type` (`instance_id`,`type_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_type_role`
--

LOCK TABLES `veeva_doc_type_role` WRITE;
/*!40000 ALTER TABLE `veeva_doc_type_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_type_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_user_role`
--

DROP TABLE IF EXISTS `veeva_doc_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `instance_user_id` bigint(20) NOT NULL,
  `role_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_type` (`instance_id`,`user_type`,`instance_user_id`),
  KEY `document_id` (`instance_id`,`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_user_role`
--

LOCK TABLES `veeva_doc_user_role` WRITE;
/*!40000 ALTER TABLE `veeva_doc_user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_version`
--

DROP TABLE IF EXISTS `veeva_doc_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_version` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `annotate_doc_key` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `classification` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `content_file_name` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `content_mime_type` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `content_size` bigint(20) DEFAULT NULL,
  `content_thumbnail_file_name` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `description` varchar(1500) COLLATE utf8_bin DEFAULT NULL,
  `document_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `latest_version_for_doc` bit(1) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` varchar(255) COLLATE utf8_bin NOT NULL,
  `title` varchar(1500) COLLATE utf8_bin DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `steady_state` bit(1) NOT NULL DEFAULT b'0',
  `checkout_file_name` varchar(1500) COLLATE utf8_bin DEFAULT NULL,
  `document_type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `document_sub_type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `deleted` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'N',
  `lifecycle` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `product` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `country` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `study` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `location` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `site` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `owning_facility` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `impacted_facilities` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `owning_department` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `impacted_departments` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `clm_content` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `version_id` (`instance_id`,`document_id`,`major_version`,`minor_version`),
  KEY `doc_id` (`instance_id`,`document_id`),
  KEY `doc_type` (`instance_id`,`document_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_version`
--

LOCK TABLES `veeva_doc_version` WRITE;
/*!40000 ALTER TABLE `veeva_doc_version` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_version_standard_catalog_values`
--

DROP TABLE IF EXISTS `veeva_doc_version_standard_catalog_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_version_standard_catalog_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `doc_property_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `catalog_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `catalog_value_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `version_id` (`instance_id`,`document_id`,`major_version`,`minor_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_version_standard_catalog_values`
--

LOCK TABLES `veeva_doc_version_standard_catalog_values` WRITE;
/*!40000 ALTER TABLE `veeva_doc_version_standard_catalog_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_version_standard_catalog_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_execution_cache_entry`
--

DROP TABLE IF EXISTS `veeva_execution_cache_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_execution_cache_entry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `executed_by` bigint(20) NOT NULL,
  `type` varchar(50) COLLATE utf8_bin NOT NULL,
  `key` bigint(20) NOT NULL,
  `dirty` bit(1) DEFAULT b'0',
  `executed_date` datetime NOT NULL,
  `value` longtext COLLATE utf8_bin,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_execution_cache_entry`
--

LOCK TABLES `veeva_execution_cache_entry` WRITE;
/*!40000 ALTER TABLE `veeva_execution_cache_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_execution_cache_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_file_type_setting`
--

DROP TABLE IF EXISTS `veeva_file_type_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_file_type_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `file_type_setting_key` bigint(20) NOT NULL,
  `checkout_operation` varchar(100) COLLATE utf8_bin NOT NULL,
  `deleted` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `file_type_setting_key` (`instance_id`,`file_type_setting_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_file_type_setting`
--

LOCK TABLES `veeva_file_type_setting` WRITE;
/*!40000 ALTER TABLE `veeva_file_type_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_file_type_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_group`
--

DROP TABLE IF EXISTS `veeva_flow_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `process_groups` (`process_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_group`
--

LOCK TABLES `veeva_flow_group` WRITE;
/*!40000 ALTER TABLE `veeva_flow_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_group_membership`
--

DROP TABLE IF EXISTS `veeva_flow_group_membership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_group_membership` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `group_member_type` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT 'user',
  PRIMARY KEY (`id`),
  KEY `user_groups` (`user_id`),
  KEY `flow_group_membership_idx` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_group_membership`
--

LOCK TABLES `veeva_flow_group_membership` WRITE;
/*!40000 ALTER TABLE `veeva_flow_group_membership` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_group_membership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_process_definition`
--

DROP TABLE IF EXISTS `veeva_flow_process_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_process_definition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_version` int(11) NOT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT 'active',
  `lifecycle_key` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `flow_def_status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_process_definition`
--

LOCK TABLES `veeva_flow_process_definition` WRITE;
/*!40000 ALTER TABLE `veeva_flow_process_definition` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_process_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_process_history`
--

DROP TABLE IF EXISTS `veeva_flow_process_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_process_history` (
  `history_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `process_definition_id` bigint(20) NOT NULL,
  `status` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `document_id` bigint(20) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_process_history`
--

LOCK TABLES `veeva_flow_process_history` WRITE;
/*!40000 ALTER TABLE `veeva_flow_process_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_process_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_process_instance`
--

DROP TABLE IF EXISTS `veeva_flow_process_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_process_instance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `process_definition_id` bigint(20) NOT NULL,
  `status` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `document_id` bigint(20) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_process_instance`
--

LOCK TABLES `veeva_flow_process_instance` WRITE;
/*!40000 ALTER TABLE `veeva_flow_process_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_process_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_task`
--

DROP TABLE IF EXISTS `veeva_flow_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assignee` bigint(20) DEFAULT NULL,
  `assignee_source_group` varchar(255) COLLATE utf8_bin NOT NULL,
  `assign_date` datetime DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `status` varchar(20) COLLATE utf8_bin NOT NULL,
  `task_meta_data_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `process_tasks` (`process_instance_id`),
  KEY `flow_task_assignee_idx` (`assignee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_task`
--

LOCK TABLES `veeva_flow_task` WRITE;
/*!40000 ALTER TABLE `veeva_flow_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_task_history`
--

DROP TABLE IF EXISTS `veeva_flow_task_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_task_history` (
  `history_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assignee` bigint(20) DEFAULT NULL,
  `assignee_source_group` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `status` varchar(20) COLLATE utf8_bin NOT NULL,
  `task_meta_data_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `assign_date` datetime DEFAULT NULL,
  PRIMARY KEY (`history_id`),
  KEY `process_tasks_hist` (`process_instance_id`),
  KEY `flow_task_assignee_hist_idx` (`assignee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_task_history`
--

LOCK TABLES `veeva_flow_task_history` WRITE;
/*!40000 ALTER TABLE `veeva_flow_task_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_task_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_timer`
--

DROP TABLE IF EXISTS `veeva_flow_timer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_timer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `status` varchar(20) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_timer`
--

LOCK TABLES `veeva_flow_timer` WRITE;
/*!40000 ALTER TABLE `veeva_flow_timer` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_timer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_variable`
--

DROP TABLE IF EXISTS `veeva_flow_variable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_variable` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `value` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `value_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `step_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `process_variables` (`process_instance_id`),
  KEY `process_variable_name_value` (`name`,`value`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_variable`
--

LOCK TABLES `veeva_flow_variable` WRITE;
/*!40000 ALTER TABLE `veeva_flow_variable` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_variable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_variable_history`
--

DROP TABLE IF EXISTS `veeva_flow_variable_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_variable_history` (
  `history_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `value` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `value_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `step_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`history_id`),
  KEY `process_var_hist` (`process_instance_id`),
  KEY `process_var_name_value_hist` (`name`,`value`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_variable_history`
--

LOCK TABLES `veeva_flow_variable_history` WRITE;
/*!40000 ALTER TABLE `veeva_flow_variable_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_variable_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_group`
--

DROP TABLE IF EXISTS `veeva_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `group_description` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `group_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `system_group` bit(1) DEFAULT NULL,
  `user_type_id` bigint(20) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `group_key` bigint(20) NOT NULL,
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  `active` bit(1) NOT NULL DEFAULT b'1',
  `rolodex_slot` varchar(3) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_key` (`instance_id`,`group_key`),
  KEY `rolodex_slot` (`instance_id`,`rolodex_slot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_group`
--

LOCK TABLES `veeva_group` WRITE;
/*!40000 ALTER TABLE `veeva_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_group_member`
--

DROP TABLE IF EXISTS `veeva_group_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_group_member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `group_member_id` bigint(20) NOT NULL,
  `group_member_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_member` (`instance_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_group_member`
--

LOCK TABLES `veeva_group_member` WRITE;
/*!40000 ALTER TABLE `veeva_group_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_group_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_instance_user`
--

DROP TABLE IF EXISTS `veeva_instance_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_instance_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `receive_emails_for_notifications_and_tasks` bit(1) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_type_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `image_file_name` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`instance_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_instance_user`
--

LOCK TABLES `veeva_instance_user` WRITE;
/*!40000 ALTER TABLE `veeva_instance_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_instance_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_lifecycle`
--

DROP TABLE IF EXISTS `veeva_lifecycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_lifecycle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `starting_state_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `superseded_state_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `steady_state_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`lifecycle_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_lifecycle`
--

LOCK TABLES `veeva_lifecycle` WRITE;
/*!40000 ALTER TABLE `veeva_lifecycle` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_lifecycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_lifecycle_role`
--

DROP TABLE IF EXISTS `veeva_lifecycle_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_lifecycle_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `role_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `system_role` bit(1) NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `description` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`lifecycle_key`,`role_key`),
  UNIQUE KEY `public_key` (`instance_id`,`lifecycle_key`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_lifecycle_role`
--

LOCK TABLES `veeva_lifecycle_role` WRITE;
/*!40000 ALTER TABLE `veeva_lifecycle_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_lifecycle_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_lifecycle_role_default_user`
--

DROP TABLE IF EXISTS `veeva_lifecycle_role_default_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_lifecycle_role_default_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `role_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `rule_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `user_type` varchar(20) COLLATE utf8_bin NOT NULL,
  `user_group_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `default_user_group` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `default_user` (`instance_id`,`lifecycle_key`,`role_key`,`rule_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_lifecycle_role_default_user`
--

LOCK TABLES `veeva_lifecycle_role_default_user` WRITE;
/*!40000 ALTER TABLE `veeva_lifecycle_role_default_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_lifecycle_role_default_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_lifecycle_role_rule`
--

DROP TABLE IF EXISTS `veeva_lifecycle_role_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_lifecycle_role_rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `role_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `rule_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `allow_all_users_and_groups` bit(1) DEFAULT b'1',
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rule` (`instance_id`,`lifecycle_key`,`role_key`,`rule_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_lifecycle_role_rule`
--

LOCK TABLES `veeva_lifecycle_role_rule` WRITE;
/*!40000 ALTER TABLE `veeva_lifecycle_role_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_lifecycle_role_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_lifecycle_state`
--

DROP TABLE IF EXISTS `veeva_lifecycle_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_lifecycle_state` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `state_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `system` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`lifecycle_key`,`state_key`),
  UNIQUE KEY `public_key` (`instance_id`,`lifecycle_key`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_lifecycle_state`
--

LOCK TABLES `veeva_lifecycle_state` WRITE;
/*!40000 ALTER TABLE `veeva_lifecycle_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_lifecycle_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_notification`
--

DROP TABLE IF EXISTS `veeva_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `message` longtext COLLATE utf8_bin NOT NULL,
  `notification` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `recipient_id` bigint(20) NOT NULL,
  `sender_id` bigint(20) NOT NULL,
  `status` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `task_id` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `template_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `workflow_id` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `major_version` int(11) DEFAULT NULL,
  `minor_version` int(11) DEFAULT NULL,
  `comment` varchar(5000) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recipient_id` (`instance_id`,`recipient_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_notification`
--

LOCK TABLES `veeva_notification` WRITE;
/*!40000 ALTER TABLE `veeva_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_notification_template`
--

DROP TABLE IF EXISTS `veeva_notification_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_notification_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `message_template` longtext COLLATE utf8_bin NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `notification_template` varchar(1023) COLLATE utf8_bin NOT NULL,
  `subject_template` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_notification_template`
--

LOCK TABLES `veeva_notification_template` WRITE;
/*!40000 ALTER TABLE `veeva_notification_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_notification_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_overlay`
--

DROP TABLE IF EXISTS `veeva_overlay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_overlay` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `overlay_key` varchar(255) NOT NULL,
  `xml` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  `created_by` bigint(20) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) unsigned NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `overlay_key` (`instance_id`,`overlay_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_overlay`
--

LOCK TABLES `veeva_overlay` WRITE;
/*!40000 ALTER TABLE `veeva_overlay` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_overlay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_pdf_template`
--

DROP TABLE IF EXISTS `veeva_pdf_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_pdf_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `template_file_path` varchar(500) COLLATE utf8_bin NOT NULL,
  `template_file_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `template_size` bigint(20) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `file_upload_date` datetime DEFAULT NULL,
  `file_upload_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_key` (`instance_id`,`template_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_pdf_template`
--

LOCK TABLES `veeva_pdf_template` WRITE;
/*!40000 ALTER TABLE `veeva_pdf_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_pdf_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_picklist`
--

DROP TABLE IF EXISTS `veeva_picklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_picklist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `picklist_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `catalog_picklist` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`picklist_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_picklist`
--

LOCK TABLES `veeva_picklist` WRITE;
/*!40000 ALTER TABLE `veeva_picklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_picklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_product`
--

DROP TABLE IF EXISTS `veeva_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `deleted` bit(1) DEFAULT b'0',
  `product_key` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_key` (`instance_id`,`product_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_product`
--

LOCK TABLES `veeva_product` WRITE;
/*!40000 ALTER TABLE `veeva_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_report_format`
--

DROP TABLE IF EXISTS `veeva_report_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_report_format` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `query_id` bigint(20) NOT NULL,
  `shared` bit(1) NOT NULL DEFAULT b'0',
  `parent_report_id` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `report_format_key` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `format_key` (`instance_id`,`report_format_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_report_format`
--

LOCK TABLES `veeva_report_format` WRITE;
/*!40000 ALTER TABLE `veeva_report_format` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_report_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_report_query`
--

DROP TABLE IF EXISTS `veeva_report_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_report_query` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `report_type` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `report_object_type` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `report_query_key` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `query_key` (`instance_id`,`report_query_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_report_query`
--

LOCK TABLES `veeva_report_query` WRITE;
/*!40000 ALTER TABLE `veeva_report_query` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_report_query` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_report_sharing`
--

DROP TABLE IF EXISTS `veeva_report_sharing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_report_sharing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `report_id` bigint(20) NOT NULL,
  `share_user_id` bigint(20) NOT NULL,
  `share_type` varchar(20) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `share_user_type` varchar(20) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_report_sharing`
--

LOCK TABLES `veeva_report_sharing` WRITE;
/*!40000 ALTER TABLE `veeva_report_sharing` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_report_sharing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_report_usage`
--

DROP TABLE IF EXISTS `veeva_report_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_report_usage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `report_id` bigint(20) NOT NULL,
  `usage_type` varchar(20) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_report_usage`
--

LOCK TABLES `veeva_report_usage` WRITE;
/*!40000 ALTER TABLE `veeva_report_usage` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_report_usage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_search_relevance_settings`
--

DROP TABLE IF EXISTS `veeva_search_relevance_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_search_relevance_settings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `setting_key` varchar(50) COLLATE utf8_bin NOT NULL,
  `setting_value` varchar(25) COLLATE utf8_bin NOT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `setting_key` (`instance_id`,`setting_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_search_relevance_settings`
--

LOCK TABLES `veeva_search_relevance_settings` WRITE;
/*!40000 ALTER TABLE `veeva_search_relevance_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_search_relevance_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_signature`
--

DROP TABLE IF EXISTS `veeva_signature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_signature` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `doc_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `event_time` datetime NOT NULL,
  `manifest` tinyint(1) DEFAULT '0',
  `task_name` varchar(255) DEFAULT NULL,
  `workflow_name` varchar(255) DEFAULT NULL,
  `signature_meaning` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_idx` (`instance_id`,`doc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_signature`
--

LOCK TABLES `veeva_signature` WRITE;
/*!40000 ALTER TABLE `veeva_signature` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_signature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_signature_template`
--

DROP TABLE IF EXISTS `veeva_signature_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_signature_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(100) NOT NULL,
  `public_key` varchar(100) NOT NULL,
  `xml` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  `created_by` bigint(20) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) unsigned NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_key` (`instance_id`,`template_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_signature_template`
--

LOCK TABLES `veeva_signature_template` WRITE;
/*!40000 ALTER TABLE `veeva_signature_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_signature_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_user_license_type`
--

DROP TABLE IF EXISTS `veeva_user_license_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_user_license_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `vault_license_id` bigint(20) NOT NULL,
  `user_license_type_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `licensed_user_count` int(11) NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_user_license_type`
--

LOCK TABLES `veeva_user_license_type` WRITE;
/*!40000 ALTER TABLE `veeva_user_license_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_user_license_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_user_preferences`
--

DROP TABLE IF EXISTS `veeva_user_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_user_preferences` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_preference_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `user_preference_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `user_preference_xml` longtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_preference` (`instance_id`,`user_id`,`user_preference_type`,`user_preference_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_user_preferences`
--

LOCK TABLES `veeva_user_preferences` WRITE;
/*!40000 ALTER TABLE `veeva_user_preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_user_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_user_type`
--

DROP TABLE IF EXISTS `veeva_user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_user_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `system_privileges` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `user_type_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_user_type`
--

LOCK TABLES `veeva_user_type` WRITE;
/*!40000 ALTER TABLE `veeva_user_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_user_type_definition`
--

DROP TABLE IF EXISTS `veeva_user_type_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_user_type_definition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `user_type_id` bigint(20) NOT NULL,
  `user_license_type_id` bigint(20) NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_user_type_definition`
--

LOCK TABLES `veeva_user_type_definition` WRITE;
/*!40000 ALTER TABLE `veeva_user_type_definition` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_user_type_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_vault`
--

DROP TABLE IF EXISTS `veeva_vault`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_vault` (
  `id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `database_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `doc_auto_number` varchar(250) COLLATE utf8_bin NOT NULL,
  `instance_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `promo_mats_app` bit(1) DEFAULT NULL,
  `solr_core_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `default_last_used_autonumber` int(11) NOT NULL DEFAULT '0',
  `domain_id` bigint(20) DEFAULT NULL,
  `advanced_checkin_checkout` bit(1) DEFAULT NULL,
  `admin_access_classic_mode` bit(1) DEFAULT b'1',
  `admin_role_access_all` bit(1) DEFAULT b'0',
  `admin_add_viewer_role` bit(1) DEFAULT b'0',
  `admin_send_to_external` bit(1) DEFAULT b'0',
  `admin_allow_rendition_download` bit(1) DEFAULT b'0',
  `admin_allow_source_download` bit(1) DEFAULT b'1',
  `default_timezone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `tmf_app` bit(1) DEFAULT NULL,
  `quality_app` bit(1) DEFAULT NULL,
  `r_d_app` bit(1) DEFAULT NULL,
  `med_info_app` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_vault`
--

LOCK TABLES `veeva_vault` WRITE;
/*!40000 ALTER TABLE `veeva_vault` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_vault` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_vault_configuration`
--

DROP TABLE IF EXISTS `veeva_vault_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_vault_configuration` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) unsigned NOT NULL,
  `created_by` bigint(20) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) unsigned NOT NULL,
  `modified_date` datetime NOT NULL,
  `edition` varchar(50) COLLATE utf8_bin NOT NULL,
  `application` varchar(50) COLLATE utf8_bin NOT NULL,
  `feature_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `group_by` varchar(255) COLLATE utf8_bin NOT NULL,
  `data_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `value` varchar(2000) COLLATE utf8_bin NOT NULL,
  `default_enabled_value` varchar(2000) COLLATE utf8_bin NOT NULL,
  `scope` int(10) unsigned NOT NULL DEFAULT '0',
  `visible_in_ui` bit(1) NOT NULL,
  `enable_action` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `disable_action` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `run_once_on_enable` bit(1) NOT NULL DEFAULT b'0',
  `enable_action_executed` bit(1) NOT NULL DEFAULT b'0',
  `confirmation_message` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `help_text` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `feature_idx` (`instance_id`,`edition`,`application`,`feature_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Contains feature information about a vault instance';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_vault_configuration`
--

LOCK TABLES `veeva_vault_configuration` WRITE;
/*!40000 ALTER TABLE `veeva_vault_configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_vault_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_vault_license`
--

DROP TABLE IF EXISTS `veeva_vault_license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_vault_license` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `license_model` varchar(255) COLLATE utf8_bin NOT NULL,
  `external_domain` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `licensed_document_count` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_vault_license`
--

LOCK TABLES `veeva_vault_license` WRITE;
/*!40000 ALTER TABLE `veeva_vault_license` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_vault_license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_workflow_task`
--

DROP TABLE IF EXISTS `veeva_workflow_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_workflow_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_workflow_task`
--

LOCK TABLES `veeva_workflow_task` WRITE;
/*!40000 ALTER TABLE `veeva_workflow_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_workflow_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'instance_1'
--
/*!50003 DROP FUNCTION IF EXISTS `get_next_value` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_bin */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`vba`@`localhost`*/ /*!50003 FUNCTION `get_next_value`(inst_id bigint(20), autonumber_key bigint(20)) RETURNS int(11)
    DETERMINISTIC
    SQL SECURITY INVOKER
begin
  declare current_val integer;

  update veeva_auto_number
     set last_used_value = last_used_value + 1
   	 where instance_id = inst_id
   	   and id = autonumber_key
       and true_function((@current_val := veeva_auto_number.last_used_value + 1) is not null);

  return @current_val;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `true_function` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_bin */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`vba`@`localhost`*/ /*!50003 FUNCTION `true_function`(p_param int) RETURNS int(11)
    DETERMINISTIC
    SQL SECURITY INVOKER
return true */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `provision_vault` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_bin */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`v_provisioner`@`localhost`*/ /*!50003 PROCEDURE `provision_vault`( IN instance_id int , OUT return_code int)
BEGIN  
       DECLARE v_col1                   varchar(64);                     
       declare tbl               varchar(64);
       declare vault                    varchar(64);
       declare fk_name          varchar(255);
       declare column_names             varchar(255);
       declare referenced_tbl    varchar(64);
       declare referenced_column_names  varchar(255);
       declare del_rule              varchar(32);
       declare upd_rule              varchar(32);
       DECLARE no_more_rows boolean;

       declare cursor1 cursor for              
       select table_name from  information_schema.tables where table_schema = 'instance_1';

       declare cursor2 cursor for              
SELECT 
   fks.TABLE_NAME
  ,fks.constraint_name
  ,group_concat(fks.COLUMN_NAME ORDER BY ordinal_position) 
  ,fks.REFERENCED_TABLE_NAME
  ,group_concat(fks.REFERENCED_COLUMN_NAME ORDER BY  position_in_unique_constraint )
  ,delete_rule,update_rule
FROM 
      information_schema.KEY_COLUMN_USAGE fks
JOIN  information_schema.referential_constraints  rule
  ON  rule.table_name = fks.table_name AND rule.constraint_schema = fks.table_schema AND rule.constraint_name = fks.constraint_name
WHERE
    fks.referenced_table_name IS NOT NULL
AND table_schema = 'instance_1'
GROUP BY  1,2,4;

           
       declare continue handler for not found   
           set no_more_rows := TRUE;            

       declare exit handler  for SQLEXCEPTION
           set  return_code=1;

      set return_code := 0;

      set vault := concat("instance_", instance_id); 

      SET @s = CONCAT('CREATE DATABASE ', vault);
      PREPARE stmt FROM @s;
      EXECUTE stmt;

      set no_more_rows := FALSE;
      open cursor1;                           

        LOOP1: loop                             
            fetch cursor1                       
            into  v_col1                        
            ;



            if no_more_rows then                
                close cursor1;                  
                leave LOOP1;                    
            end if;

            
            
            SET @s = CONCAT('CREATE table ' , vault , '.', v_col1 ,  ' like instance_1.', v_col1); 

            PREPARE stmt FROM @s;
            EXECUTE stmt;

        end loop LOOP1;


            SET @s = CONCAT('INSERT INTO ' , vault , '.ACT_GE_PROPERTY SELECT * FROM   instance_1.ACT_GE_PROPERTY'); 

            PREPARE stmt FROM @s;
            EXECUTE stmt;     
	    


        set no_more_rows := FALSE;
        open cursor2;                           
        LOOP2: loop                             
            fetch cursor2                       
            into
            tbl
            ,fk_name
            ,column_names               
            ,referenced_tbl      
            ,referenced_column_names    
            ,del_rule                
            ,upd_rule                
            ;

            if no_more_rows then                
                close cursor2;                  
                leave LOOP2;                    
            end if;

            SET @s = CONCAT( 'Alter table ' , vault , '.', tbl, ' add foreign key  ' , fk_name , ' ('  , column_names  , ') REFERENCES ' ,referenced_tbl, '(' 
  ,referenced_column_names  , ')  on DELETE ',del_rule, ' on UPDATE ' , upd_rule);



            PREPARE stmt FROM @s;
            EXECUTE stmt;

        end loop LOOP2;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-01-09 18:59:06
