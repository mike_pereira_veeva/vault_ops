-- MySQL dump 10.13  Distrib 5.1.56, for unknown-linux-gnu (x86_64)
--
-- Host: localhost    Database: instance_1
-- ------------------------------------------------------
-- Server version	5.1.56-rel12.7-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ACT_GE_BYTEARRAY`
--

DROP TABLE IF EXISTS `ACT_GE_BYTEARRAY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_GE_BYTEARRAY` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BYTES_` longblob,
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_BYTEARR_DEPL` (`DEPLOYMENT_ID_`),
  CONSTRAINT `ACT_FK_BYTEARR_DEPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `ACT_RE_DEPLOYMENT` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_GE_BYTEARRAY`
--

LOCK TABLES `ACT_GE_BYTEARRAY` WRITE;
/*!40000 ALTER TABLE `ACT_GE_BYTEARRAY` DISABLE KEYS */;
INSERT INTO `ACT_GE_BYTEARRAY` VALUES ('10',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/PrepareForReview.sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa.png','1','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0�\0\0\0_\0\0\0�\r��\0\0�IDATx���\rp���m����@�t��H(��e�Q���FL$V��R�BSIQ\Z���JZQRM1\"Ey\Z1� WK!@����8�C���b5%\'���}6�99	���K���gr�������������0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�G��kѢ;*�L)|s���^��D\'S�U�)�!C�]|�ȍC�ˑ\0\0���O�s�ر��ɒ��2���߿CD�P�n���eRU5S�\r|4+k���&��\0\0q��v��A�Ζ��Y�\"�E�.�]��Mo��\'mG\0\0�y:��V�AͻjiY%��������\0\0 &�kX[	����{�}b�e\0\0��7��5��5L0�]UU��855u+G$\0\0��5�7�p�`�+33�}s��sT\0\0���1z�0a,�U^^�d�*�J\0\0�GP���j`y�v�<V�%���,(ȐyW���\\��^{���.i�\0\0����:��gf�2eII�,6k�5�d���uY�,_4�@��8��.	pT\0\0/u9��\\�!��������\'�/ ��f�//=�@�B�O+\0\0��08��t�Ts�|P�@v��&���\0�5�����I�e���Na\0\0$k|�l��?|G>�^#�jxD���W�o|H�]�ܚ\'�cܑL\0\0IW�?A�~A�Y-mo�0k�����+d��E�9� \0\0H�0�E�IfW>,+�/���Ԋ����jk~^�U\\$�����?�M�#\0�d�G�h��/o��-��?�+��JV�Ud����v�d�d�<��^���A\0\0�<a0�#�7�dp�����d�\"��)�%2�/\"7�\"r�k\";����+�˒��%�e��0\0\0�!nٵU�M\\ �w~)O�)o�;�L��)�r�ʠL|��Yo��UA�a��Җ:�a\0\0$Cl��9X7G_Z\'�f����ȍ\n�/֘A���\\���\\U�=,2�z�|�i��#�`G\0\0���a���I��e�#��5\"ׯʵϚA�6(��)^&2g��2~�r�dK��3�`G\0\0��,�7�\Z&S�Η�����A��Q7�5�\r�_�#��.�����\0\0  ֱ@�9�(wּ*���ˈ��r���r~�\Z^�RF=\'7ܻYZ?�n�K�#\0�$\n�a\0\0)� \0\0 R�A\0pG�Y3�Zmֻf��\'A�Y��zɬ\n{>\0�A� \0$I�*6��^�%fM4+׬t���3ǬB����|fMf�A� \0��V~�ҕ�I�ҀWoV��RO�f5��ͬ��s�0H���J\"(ϲU�Y������3�^��\0a�0�;�lu($��p8ͬ<�����5�{a�3څڂ�\Z�l����O����F�˛�q\0	�\0��B;�9C`���R����!�0*٪�n9./7�^.-�\0a�0�;)		q���V�ӡ�k		��f+\rlڥ���1�^~.�@$�4[9���z��.S����]Ʈe+]pE�7�{C\0 $;gh��Bt���b{��<�0���\0a�0 ����sy�yF��爳�&�B�6�8��A�0H�ܜ��UQ����K��:���ܑ��ҁ�s8N���8.!$��Z�UG�]���D�.c�3����:�\Z�7�&�\n�\'�sȳKO4��A� ��=W�p�W��Y����H2��\rY�����Q�a��$������J��>%nO�����q9f���|�	����%�A� ��9W�똞���u|�ig+�&��\r����x�t�q���i��������:M߱c����C��o�����dI�)r����{�8#U�;�[R[{�	��0 ���m�����>}��䏊ǍIQT��$�h��e��M�4}ڴbY��Έ��������\ZQ�liYe���3-Z)��י��A 	��l2گ����?��r�h��yfo�ݻ���;�i������c�?x�A.���VӖ9m1t��_|P���0��(��=�SKdC�r��O\Z�ꪭ�Ç���߅[G�;�˯����.�O� �(��[�e�k��<���@Í0�5s�u�wt�.\\x��r�Ϭד&��~��{��\Z�a,��0��2��.[�kH��7�t�<�h�I[�4�^}�Xk=B�nu��~�ҹ��n~� �(���A�&���h��\\n�ψ�޽+��\Z5j�����?�r��g�]G7����daP�����~��i�Jj��G�ڥ���.�:vL?vlw�φ��0 J�ꄻ��q���%��0輑D�!tv�v��.����A�	.��]}�]��QNCױcz�g��O�su3�H���ԍ�ݽ�,m�������\'����w�t����\r���v킈�`�u,--����?�iz��	�\0�$�@�x61�� �F���٭�-xή�Hàv��]��Uڅ{:a0�:�ߡwE��ԕ�	�\0�(��M��ϻ��uP�W\'s�\0�\r�^�;דּ��Wt\Z@7Q�j�:���$y��[ŭl�3��;��r�\0�\'�5��*:�5a@��9��>ýaft8��s��+�*ǬF��.Gj���\\�\r�{�AG���lu($�E\Z5:��[��Vz{�~���;r���=	�\0��V��\\m!<�.c���Eе��P�vps��p���9\0a�0�*\r	��Zo*����|�C�!��V�b�YS�!��Z�<`D�E�0H�3�V���9\rwuvN�V��.d�\nε?����x���aO�U���\'�͑��Y>FG��fp� @$�3[I�l5�^q���\'����,�t������Q�<ߵWT+w\r�A� \0�8[N1�<[��ٷ\Z�=�`�1���#k3g���E\0� a\0�V\0��A\0\0\0� a\0\0�0H\0\0 \0\0��A\0\0\0� a\0\0�0H\0\0 \0\0��A\0\0\0� a\0\0�0H\0\0 \0\0��A\0\0@$�\0\0a�\"\0�n�gϴ�߿� �ֶ��.	pT\0\0�~�>�b�X���x��%M�\0\0�3cƌZSYYF��**\Z�K�8*\0�g�����=��@`�,��?%%���.��\0\0���\Z���z�,�UX8�)sW4p4\0�X���8�Hc�2�Y��{j����ǡ\0\0be�����!zSRR�3�!� \0\0�y LK;����1�F�\ZA�k�\0A\0\0ē�=��9=������ƦM�`B����c��a�f�F��a\0\0�F�Ui�6��F��1��J��qu��\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0���\0�	�(�E/1\0\0\0\0IEND�B`�'),('102',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Approval Workflow - V2.bpmn20.xml','101','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns:activiti=\"http://activiti.org/bpmn\" xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:signavio=\"http://www.signavio.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-39062ac0-8ac8-43bf-b32f-639d483f2094\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\">\n   <process name=\"MLR Approval\" id=\"startMlrApproval\" isExecutable=\"false\">\n   \n      <extensionElements>\n        <activiti:executionListener delegateExpression=\"${workflowListener}\" event=\"end\" />\n      </extensionElements>\n     \n      <startEvent id=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B\" name=\"Start approval workflow\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <outgoing>sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB</outgoing>\n      </startEvent>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'inMlrApproval\')}\" completionQuantity=\"1\" id=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: READY FOR MLR to IN APPROVAL\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB</incoming>\n         <outgoing>sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD</outgoing>\n         <outgoing>sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172</outgoing>\n      </serviceTask>\n      <userTask activiti:assignee=\"${assignee}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\" implementation=\"webService\" isForCompensation=\"false\" name=\"Approve\" startQuantity=\"1\">\n         <documentation>PM-MLRApproval-Approve</documentation>	\n         <extensionElements>\n         	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD</incoming>\n         <outgoing>sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE</outgoing>\n         <multiInstanceLoopCharacteristics activiti:collection=\"${groupService.getCandidateTaskUsers(execution,\'approver\')}\" activiti:elementVariable=\"assignee\" behavior=\"All\" id=\"sid-d5e84c0f-762a-4cf7-92de-bde3c31f3a5a\" isSequential=\"false\"/>\n      </userTask>\n      <userTask activiti:assignee=\"${groupService.getTaskAssignee(execution,\'coordinator\')}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\" implementation=\"webService\" isForCompensation=\"false\" name=\"Assess outcome\" startQuantity=\"1\">\n      	<documentation>PM-MLRApproval-CompleteApproval</documentation>\n         <extensionElements>\n           	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE</incoming>\n         <outgoing>sid-4D72DB74-7882-494E-A373-460453FDCED0</outgoing>\n      </userTask>\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" name=\"Any changes required?\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <incoming>sid-4D72DB74-7882-494E-A373-460453FDCED0</incoming>\n         <outgoing>sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA</outgoing>\n         <outgoing>sid-79C7E93B-C8EB-488F-8BBD-F323077A925E</outgoing>\n      </exclusiveGateway>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'draft\')}\" completionQuantity=\"1\" id=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN APPROVAL to DRAFT\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA</incoming>\n         <outgoing>sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'approvedWithChanges\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Approval workflow completed, changes needed\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'approvedForProduction\')}\" completionQuantity=\"1\" id=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN APPROVAL to APPROVED\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-79C7E93B-C8EB-488F-8BBD-F323077A925E</incoming>\n         <outgoing>sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'approved\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Approval workflow completed, no changes needed\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'approvalWorkflowStarted\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Approval workflow started\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172</incoming>\n      </serviceTask>\n      <sequenceFlow id=\"sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB\" name=\"\" sourceRef=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B\" targetRef=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\"/>\n      <sequenceFlow id=\"sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD\" name=\"\" sourceRef=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" targetRef=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\"/>\n      <sequenceFlow id=\"sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE\" name=\"\" sourceRef=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\" targetRef=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\"/>\n      <sequenceFlow id=\"sid-4D72DB74-7882-494E-A373-460453FDCED0\" name=\"\" sourceRef=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\" targetRef=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\"/>\n      <sequenceFlow id=\"sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172\" name=\"\" sourceRef=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" targetRef=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805\"/>\n      <sequenceFlow id=\"sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3\" name=\"\" sourceRef=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\" targetRef=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C\"/>\n      <sequenceFlow id=\"sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE\" name=\"\" sourceRef=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\" targetRef=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436\"/>\n      <sequenceFlow id=\"sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA\" name=\"yes\" sourceRef=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" targetRef=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\">\n         <conditionExpression id=\"sid-82a14cc3-9c6f-441d-9b20-f9b90e0f42c8\" xsi:type=\"tFormalExpression\">${documentService.any(execution, \'finalOutcome\', \'Not approved for production\')}</conditionExpression>\n      </sequenceFlow>\n      <sequenceFlow id=\"sid-79C7E93B-C8EB-488F-8BBD-F323077A925E\" name=\"no\" sourceRef=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" targetRef=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\">\n         <conditionExpression id=\"sid-d2ecff9b-876c-4dd1-95af-49a17646e205\" xsi:type=\"tFormalExpression\">${documentService.all(execution, \'finalOutcome\', \'Approved for production\')}</conditionExpression>\n      </sequenceFlow>\n   </process>\n   <bpmndi:BPMNDiagram id=\"sid-39fbb042-c68b-432b-8516-9f9dd5a5c37b\">\n      <bpmndi:BPMNPlane bpmnElement=\"startMlrApproval\" id=\"sid-b872979f-4d08-4cd9-b6b5-fc9ec1e165c2\">\n         <bpmndi:BPMNShape bpmnElement=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B\" id=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B_gui\">\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"180.0\" y=\"360.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" id=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"303.0\" y=\"450.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\" id=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"390.0\" y=\"660.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\" id=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"560.0\" y=\"450.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" id=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2_gui\" isMarkerVisible=\"true\">\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"750.0\" y=\"470.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\" id=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"720.0\" y=\"320.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C\" id=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"720.0\" y=\"195.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\" id=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"922.5\" y=\"450.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436\" id=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"922.5\" y=\"195.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805\" id=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"303.0\" y=\"195.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-79C7E93B-C8EB-488F-8BBD-F323077A925E\" id=\"sid-79C7E93B-C8EB-488F-8BBD-F323077A925E_gui\">\n            <omgdi:waypoint x=\"790.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"922.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172\" id=\"sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172_gui\">\n            <omgdi:waypoint x=\"353.0\" y=\"450.0\"/>\n            <omgdi:waypoint x=\"353.0\" y=\"275.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-4D72DB74-7882-494E-A373-460453FDCED0\" id=\"sid-4D72DB74-7882-494E-A373-460453FDCED0_gui\">\n            <omgdi:waypoint x=\"660.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"750.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE\" id=\"sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE_gui\">\n            <omgdi:waypoint x=\"972.0\" y=\"450.0\"/>\n            <omgdi:waypoint x=\"972.0\" y=\"275.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA\" id=\"sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA_gui\">\n            <omgdi:waypoint x=\"770.0\" y=\"470.0\"/>\n            <omgdi:waypoint x=\"770.0\" y=\"400.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB\" id=\"sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB_gui\">\n            <omgdi:waypoint x=\"210.0\" y=\"375.0\"/>\n            <omgdi:waypoint x=\"256.5\" y=\"375.0\"/>\n            <omgdi:waypoint x=\"256.5\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"303.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE\" id=\"sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE_gui\">\n            <omgdi:waypoint x=\"490.0\" y=\"700.0\"/>\n            <omgdi:waypoint x=\"535.0\" y=\"700.0\"/>\n            <omgdi:waypoint x=\"535.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"560.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3\" id=\"sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3_gui\">\n            <omgdi:waypoint x=\"770.0\" y=\"320.0\"/>\n            <omgdi:waypoint x=\"770.0\" y=\"275.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD\" id=\"sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD_gui\">\n            <omgdi:waypoint x=\"403.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"440.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"440.0\" y=\"660.0\"/>\n         </bpmndi:BPMNEdge>\n      </bpmndi:BPMNPlane>\n   </bpmndi:BPMNDiagram>\n</definitions>\n'),('103',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Submit for QC - V2.bpmn20.xml','101','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns:activiti=\"http://activiti.org/bpmn\"  xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:signavio=\"http://www.signavio.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-b2bce920-71f1-4aae-8167-84d32c7fd8c4\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\">\n   <process name=\"Submit for MLR\" id=\"submitForMlr\" isExecutable=\"false\">\n   \n     <extensionElements>\n        <activiti:executionListener delegateExpression=\"${workflowListener}\" event=\"end\" />\n     </extensionElements>\n   \n      <startEvent id=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04\" name=\"Submit piece for QC\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <outgoing>sid-D8055B00-59CB-4604-A217-C0C44E4F218D</outgoing>\n      </startEvent>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'pendingQc\')}\" completionQuantity=\"1\" id=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: DRAFT to PENDING QC\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-D8055B00-59CB-4604-A217-C0C44E4F218D</incoming>\n         <outgoing>sid-B60C9535-C150-4B52-B391-2A45A1DF2A06</outgoing>\n      </serviceTask>\n      <userTask activiti:assignee=\"${groupService.getTaskAssignee(execution,\'coordinator\')}\" activiti:dueDate=\"${documentService.getDate(execution,\'requestedDate\')}\" completionQuantity=\"1\" id=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\" implementation=\"webService\" isForCompensation=\"false\" name=\"Verify piece meets QC criteria\" startQuantity=\"1\">\n         <documentation>PM-SubmitMLR-QualityControl</documentation>	\n         <extensionElements>\n           	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-B60C9535-C150-4B52-B391-2A45A1DF2A06</incoming>\n         <outgoing>sid-F226E847-2506-4D6D-A759-D787243EAD7C</outgoing>\n      </userTask>\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" name=\"Does it meet criteria?\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <incoming>sid-F226E847-2506-4D6D-A759-D787243EAD7C</incoming>\n         <outgoing>sid-FC7B38D5-7383-417D-8157-DAFADD605E4B</outgoing>\n         <outgoing>sid-85967F7A-4BB4-4860-9C86-671BC8892D6D</outgoing>\n      </exclusiveGateway>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'readyForMlr\')}\" completionQuantity=\"1\" id=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: PENDING QC to READY FOR MLR \" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-FC7B38D5-7383-417D-8157-DAFADD605E4B</incoming>\n         <outgoing>sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'readyForMLR\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Piece is ready for MLR.\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'draft\')}\" completionQuantity=\"1\" id=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: PENDING QC to DRAFT\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-85967F7A-4BB4-4860-9C86-671BC8892D6D</incoming>\n         <outgoing>sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'notReadyForMLR\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Piece is not ready for MLR.\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415</incoming>\n      </serviceTask>\n      \n      \n    <serviceTask id=\"servicetask6\" name=\"Send Notification: Coordinator reminder\" activiti:expression=\"#{documentService.sendNotification(execution,\'readyForMLRReminder\',\'coordinator\')}\">\n    </serviceTask>\n    <sequenceFlow id=\"flow8\" name=\"\" sourceRef=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\" targetRef=\"servicetask6\"></sequenceFlow>      \n      \n      \n      <sequenceFlow id=\"sid-F226E847-2506-4D6D-A759-D787243EAD7C\" name=\"\" sourceRef=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\" targetRef=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\"/>\n      <sequenceFlow id=\"sid-D8055B00-59CB-4604-A217-C0C44E4F218D\" name=\"\" sourceRef=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04\" targetRef=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\"/>\n      <sequenceFlow id=\"sid-B60C9535-C150-4B52-B391-2A45A1DF2A06\" name=\"\" sourceRef=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\" targetRef=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\"/>\n      <sequenceFlow id=\"sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415\" name=\"\" sourceRef=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\" targetRef=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45\"/>\n      <sequenceFlow id=\"sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF\" name=\"\" sourceRef=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\" targetRef=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\"/>\n      <sequenceFlow id=\"sid-FC7B38D5-7383-417D-8157-DAFADD605E4B\" name=\"Yes\" sourceRef=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" targetRef=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\">\n         <conditionExpression id=\"sid-87f8fded-4359-4ffb-8cd7-c8dd1fc4d4eb\" xsi:type=\"tFormalExpression\">${documentService.all(execution, \'outcome\', \'Ready for MLR\')}</conditionExpression>\n      </sequenceFlow>\n      <sequenceFlow id=\"sid-85967F7A-4BB4-4860-9C86-671BC8892D6D\" name=\"No\" sourceRef=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" targetRef=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\">\n         <conditionExpression id=\"sid-925e18ad-e84a-44de-951b-b9e34abd3c48\" xsi:type=\"tFormalExpression\">${documentService.any(execution, \'outcome\', \'Not ready for MLR\')}</conditionExpression>\n      </sequenceFlow>\n   </process>\n   <bpmndi:BPMNDiagram id=\"sid-6543adbe-9e4b-4ca5-8393-86755e55b4d2\">\n      <bpmndi:BPMNPlane bpmnElement=\"submitForMlr\" id=\"sid-5c9125a0-8c36-41bc-9956-eb6dc41661ec\">\n         <bpmndi:BPMNShape bpmnElement=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04\" id=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04_gui\">\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"103.0\" y=\"129.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\" id=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"180.0\" y=\"225.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\" id=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"300.0\" y=\"414.5\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" id=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7_gui\" isMarkerVisible=\"true\">\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"500.0\" y=\"434.5\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\" id=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"686.0\" y=\"414.5\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\" id=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"686.0\" y=\"104.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\" id=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"470.0\" y=\"255.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45\" id=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"470.0\" y=\"104.0\"/>\n         </bpmndi:BPMNShape>\n         \n      <bpmndi:BPMNShape bpmnElement=\"servicetask6\" id=\"BPMNShape_servicetask6\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"900\" y=\"97\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"flow8\" id=\"BPMNEdge_flow8\">\n        <omgdi:waypoint x=\"825\" y=\"124\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"900\" y=\"124\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>      \n      \n         <bpmndi:BPMNEdge bpmnElement=\"sid-FC7B38D5-7383-417D-8157-DAFADD605E4B\" id=\"sid-FC7B38D5-7383-417D-8157-DAFADD605E4B_gui\">\n            <omgdi:waypoint x=\"540.0\" y=\"455.0\"/>\n            <omgdi:waypoint x=\"686.0\" y=\"454.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-F226E847-2506-4D6D-A759-D787243EAD7C\" id=\"sid-F226E847-2506-4D6D-A759-D787243EAD7C_gui\">\n            <omgdi:waypoint x=\"400.0\" y=\"454.0\"/>\n            <omgdi:waypoint x=\"500.0\" y=\"454.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-D8055B00-59CB-4604-A217-C0C44E4F218D\" id=\"sid-D8055B00-59CB-4604-A217-C0C44E4F218D_gui\">\n            <omgdi:waypoint x=\"133.0\" y=\"144.0\"/>\n            <omgdi:waypoint x=\"156.5\" y=\"144.0\"/>\n            <omgdi:waypoint x=\"156.5\" y=\"265.0\"/>\n            <omgdi:waypoint x=\"180.0\" y=\"265.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-B60C9535-C150-4B52-B391-2A45A1DF2A06\" id=\"sid-B60C9535-C150-4B52-B391-2A45A1DF2A06_gui\">\n            <omgdi:waypoint x=\"280.0\" y=\"265.0\"/>\n            <omgdi:waypoint x=\"350.0\" y=\"265.0\"/>\n            <omgdi:waypoint x=\"350.0\" y=\"414.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-85967F7A-4BB4-4860-9C86-671BC8892D6D\" id=\"sid-85967F7A-4BB4-4860-9C86-671BC8892D6D_gui\">\n            <omgdi:waypoint x=\"520.0\" y=\"434.0\"/>\n            <omgdi:waypoint x=\"520.0\" y=\"335.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF\" id=\"sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF_gui\">\n            <omgdi:waypoint x=\"736.0\" y=\"414.0\"/>\n            <omgdi:waypoint x=\"736.0\" y=\"184.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415\" id=\"sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415_gui\">\n            <omgdi:waypoint x=\"520.0\" y=\"255.0\"/>\n            <omgdi:waypoint x=\"520.0\" y=\"184.0\"/>\n         </bpmndi:BPMNEdge>\n      </bpmndi:BPMNPlane>\n   </bpmndi:BPMNDiagram>\n</definitions>\n'),('104',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/Vault-Test-Workflow.bpmn20.xml','101','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" \r\n             xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" \r\n             xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" \r\n             xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" \r\n             xmlns:signavio=\"http://www.signavio.com\" \r\n             xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-7bef7376-9427-41db-8394-3de98e53008e\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\" \r\n             xmlns:activiti=\"http://activiti.org/bpmn\">\r\n   <process name=\"TestWorkflow\" id=\"sid-3806d577-ddd6-479c-85f6-11805b79addd\" isExecutable=\"false\">\r\n      <startEvent id=\"sid-64325C52-DE2E-4295-9079-C8C25306742D\" name=\"Start\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <outgoing>sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A</outgoing>\r\n      </startEvent>\r\n      <userTask activiti:candidateUsers=\"${groupService.getCandidateTaskUsers(execution,\'reviewer\')}\" completionQuantity=\"1\" id=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\" implementation=\"webService\" isForCompensation=\"false\" name=\"Verify Piece\" startQuantity=\"1\">\r\n         <documentation>PM-SubmitMLR-QualityControl</documentation>	\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\r\n         </extensionElements>\r\n         <incoming>sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A</incoming>\r\n         <outgoing>sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56</outgoing>\r\n      </userTask>\r\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" name=\"\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56</incoming>\r\n         <outgoing>sid-60F7520F-7792-475F-9E43-B59408B7D40E</outgoing>\r\n         <outgoing>sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F</outgoing>\r\n      </exclusiveGateway>\r\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'inMlrReview\')}\" completionQuantity=\"1\" id=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\" implementation=\"webService\" isForCompensation=\"false\" name=\"Update Doc Status (ReadyForReview)\" startQuantity=\"1\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\r\n         </extensionElements>\r\n         <incoming>sid-60F7520F-7792-475F-9E43-B59408B7D40E</incoming>\r\n         <outgoing>sid-E5617A55-5B8C-4599-AA65-BB847858AED7</outgoing>\r\n      </serviceTask>\r\n      <endEvent id=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB\" name=\"End (good)&#10;\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-E5617A55-5B8C-4599-AA65-BB847858AED7</incoming>\r\n      </endEvent>\r\n      <endEvent id=\"sid-5E2E046B-9380-4584-A037-D196E92AB302\" name=\"End (bad)\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F</incoming>\r\n      </endEvent>\r\n      <sequenceFlow id=\"sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A\" name=\"\" sourceRef=\"sid-64325C52-DE2E-4295-9079-C8C25306742D\" targetRef=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\"/>\r\n      <sequenceFlow id=\"sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56\" name=\"\" sourceRef=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\" targetRef=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\"/>\r\n      <sequenceFlow id=\"sid-E5617A55-5B8C-4599-AA65-BB847858AED7\" name=\"\" sourceRef=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\" targetRef=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB\"/>\r\n      <sequenceFlow id=\"sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F\" name=\"Failed\" sourceRef=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" targetRef=\"sid-5E2E046B-9380-4584-A037-D196E92AB302\">\r\n         <conditionExpression id=\"sid-a1188d86-b470-4645-b51a-33c5d64d5727\" xsi:type=\"tFormalExpression\">${pieceVerified == false}</conditionExpression>\r\n      </sequenceFlow>\r\n      <sequenceFlow id=\"sid-60F7520F-7792-475F-9E43-B59408B7D40E\" isImmediate=\"false\" name=\"Passed\" sourceRef=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" targetRef=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\">\r\n         <conditionExpression id=\"sid-61fb3b25-e618-4f43-b0b8-dec5d94df430\" xsi:type=\"tFormalExpression\">${pieceVerified == true}</conditionExpression>\r\n      </sequenceFlow>\r\n   </process>\r\n   <bpmndi:BPMNDiagram id=\"sid-c14dc061-b8a6-41f5-841c-45a0544d01eb\">\r\n      <bpmndi:BPMNPlane bpmnElement=\"sid-3806d577-ddd6-479c-85f6-11805b79addd\" id=\"sid-5d36db64-e793-4274-a948-b95a943640c3\">\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-64325C52-DE2E-4295-9079-C8C25306742D\" id=\"sid-64325C52-DE2E-4295-9079-C8C25306742D_gui\">\r\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"30.0\" y=\"103.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\" id=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191_gui\">\r\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"120.0\" y=\"78.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" id=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D_gui\" isMarkerVisible=\"true\">\r\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"300.0\" y=\"98.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\" id=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E_gui\">\r\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"270.0\" y=\"222.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB\" id=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB_gui\">\r\n            <omgdc:Bounds height=\"28.0\" width=\"28.0\" x=\"511.0\" y=\"248.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-5E2E046B-9380-4584-A037-D196E92AB302\" id=\"sid-5E2E046B-9380-4584-A037-D196E92AB302_gui\">\r\n            <omgdc:Bounds height=\"28.0\" width=\"28.0\" x=\"510.0\" y=\"104.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-60F7520F-7792-475F-9E43-B59408B7D40E\" id=\"sid-60F7520F-7792-475F-9E43-B59408B7D40E_gui\">\r\n            <omgdi:waypoint x=\"320.0\" y=\"138.0\"/>\r\n            <omgdi:waypoint x=\"320.0\" y=\"222.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F\" id=\"sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F_gui\">\r\n            <omgdi:waypoint x=\"340.0\" y=\"118.0\"/>\r\n            <omgdi:waypoint x=\"510.0\" y=\"118.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56\" id=\"sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56_gui\">\r\n            <omgdi:waypoint x=\"220.0\" y=\"118.0\"/>\r\n            <omgdi:waypoint x=\"300.0\" y=\"118.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-E5617A55-5B8C-4599-AA65-BB847858AED7\" id=\"sid-E5617A55-5B8C-4599-AA65-BB847858AED7_gui\">\r\n            <omgdi:waypoint x=\"370.0\" y=\"262.0\"/>\r\n            <omgdi:waypoint x=\"511.0\" y=\"262.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A\" id=\"sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A_gui\">\r\n            <omgdi:waypoint x=\"60.0\" y=\"118.0\"/>\r\n            <omgdi:waypoint x=\"120.0\" y=\"118.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n      </bpmndi:BPMNPlane>\r\n   </bpmndi:BPMNDiagram>\r\n</definitions>\r\n'),('105',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/PrepareForReview.bpmn20.xml','101','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" \r\n			xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" \r\n			xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" \r\n			xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" \r\n			xmlns:signavio=\"http://www.signavio.com\" \r\n			xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \r\n			exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" \r\n			expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-8f89b30b-9210-479a-946b-4c3f75405b76\" \r\n			targetNamespace=\"http://www.signavio.com/bpmn20\" \r\n			typeLanguage=\"http://www.w3.org/2001/XMLSchema\" \r\n			xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\"\r\n			xmlns:activiti=\"http://activiti.org/bpmn\" >\r\n   <process name=\"PrepareForReview\" id=\"sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa\" isExecutable=\"false\">\r\n      <startEvent id=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240\" name=\"Start\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <outgoing>sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F</outgoing>\r\n      </startEvent>\r\n      <userTask activiti:candidateUsers=\"${groupService.getCandidateTaskUsers(execution,\'reviewer\')}\" completionQuantity=\"1\" id=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\" implementation=\"webService\" isForCompensation=\"false\" name=\"Verify Piece&#10;\" startQuantity=\"1\" >\r\n         <documentation>PM-SubmitMLR-QualityControl</documentation>	\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\r\n         </extensionElements>\r\n         <incoming>sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F</incoming>\r\n         <outgoing>sid-12195530-42AD-4E34-BDC9-740FBF187574</outgoing>\r\n      </userTask>\r\n      <endEvent id=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B\" name=\"End&#10;\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-12195530-42AD-4E34-BDC9-740FBF187574</incoming>\r\n      </endEvent>\r\n      <sequenceFlow id=\"sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F\" name=\"\" sourceRef=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240\" targetRef=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\"/>\r\n      <sequenceFlow id=\"sid-12195530-42AD-4E34-BDC9-740FBF187574\" name=\"\" sourceRef=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\" targetRef=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B\"/>\r\n   </process>\r\n   <bpmndi:BPMNDiagram id=\"sid-af2d216f-2a59-433a-bb05-69b2533a21cf\">\r\n      <bpmndi:BPMNPlane bpmnElement=\"sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa\" id=\"sid-d4f7ba96-f1cc-45d4-a894-c7ba60deb401\">\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240\" id=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240_gui\">\r\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"120.0\" y=\"85.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\" id=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464_gui\">\r\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"357.0\" y=\"60.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B\" id=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B_gui\">\r\n            <omgdc:Bounds height=\"28.0\" width=\"28.0\" x=\"720.0\" y=\"86.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F\" id=\"sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F_gui\">\r\n            <omgdi:waypoint x=\"150.0\" y=\"100.0\"/>\r\n            <omgdi:waypoint x=\"357.0\" y=\"100.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-12195530-42AD-4E34-BDC9-740FBF187574\" id=\"sid-12195530-42AD-4E34-BDC9-740FBF187574_gui\">\r\n            <omgdi:waypoint x=\"457.0\" y=\"100.0\"/>\r\n            <omgdi:waypoint x=\"720.0\" y=\"100.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n      </bpmndi:BPMNPlane>\r\n   </bpmndi:BPMNDiagram>\r\n</definitions>\r\n'),('106',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/MLR Review - V2.bpmn20.xml','101','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns:activiti=\"http://activiti.org/bpmn\" xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:signavio=\"http://www.signavio.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-6c9c2dee-be44-4ebb-8302-e85cf48d4d44\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\">\n   <process name=\"MLR Review\" id=\"startMlrReview\" isExecutable=\"false\">\n   \n     <extensionElements>\n        <activiti:executionListener delegateExpression=\"${workflowListener}\" event=\"end\" />\n     </extensionElements>\n     \n      <startEvent id=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C\" name=\"Start review workflow \">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <outgoing>sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4</outgoing>\n      </startEvent>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'inMlrReview\')}\" completionQuantity=\"1\" id=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change (READY FOR MLR to IN REVIEW)\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4</incoming>\n         <outgoing>sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E</outgoing>\n         <outgoing>sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39</outgoing>\n      </serviceTask>\n      <userTask activiti:assignee=\"${assignee}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\" implementation=\"webService\" isForCompensation=\"false\" name=\"Review and provide annotations\" startQuantity=\"1\">\n         <documentation>PM-MLRReview-ReviewAnnotate</documentation>	\n         <extensionElements>\n			<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E</incoming>\n         <outgoing>sid-FFDFD243-271D-458F-8DF4-854C3668FED8</outgoing>\n         <multiInstanceLoopCharacteristics activiti:collection=\"${groupService.getCandidateTaskUsers(execution,\'reviewer\')}\"  activiti:elementVariable=\"assignee\" behavior=\"All\" id=\"sid-83c385a5-2e28-45aa-a49d-8105f3aedc2d\" isSequential=\"false\"/>\n      </userTask>\n      <userTask activiti:assignee=\"${groupService.getTaskAssignee(execution,\'coordinator\')}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\" implementation=\"webService\" isForCompensation=\"false\" name=\"Assess outcome\" startQuantity=\"1\">\n      	 <documentation>PM-MLRReview-CompleteReview</documentation>\n         <extensionElements>\n           	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-FFDFD243-271D-458F-8DF4-854C3668FED8</incoming>\n         <outgoing>sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174</outgoing>\n      </userTask>\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" name=\"Are changes required?\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <incoming>sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174</incoming>\n         <outgoing>sid-9218A35A-08ED-4C84-93AA-C568CADBD664</outgoing>\n         <outgoing>sid-1498D019-EFAC-4F53-9400-715E685CA2F4</outgoing>\n      </exclusiveGateway>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'readyForMlr\')}\" completionQuantity=\"1\" id=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN REVIEW to READY FOR MLR\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-9218A35A-08ED-4C84-93AA-C568CADBD664</incoming>\n         <outgoing>sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'mlrReviewedApproved\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Review completed, changes not required\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'draft\')}\" completionQuantity=\"1\" id=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN REVIEW to DRAFT\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-1498D019-EFAC-4F53-9400-715E685CA2F4</incoming>\n         <outgoing>sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'mlrReviewedWithChanges\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Review completed, changes required\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'inMlrReview\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Piece is in review\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39</incoming>\n      </serviceTask>\n      <sequenceFlow id=\"sid-FFDFD243-271D-458F-8DF4-854C3668FED8\" name=\"\" sourceRef=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\" targetRef=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\"/>\n      <sequenceFlow id=\"sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174\" name=\"\" sourceRef=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\" targetRef=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\"/>\n      <sequenceFlow id=\"sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4\" name=\"\" sourceRef=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C\" targetRef=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\"/>\n      <sequenceFlow id=\"sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E\" name=\"\" sourceRef=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" targetRef=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\"/>\n      <sequenceFlow id=\"sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39\" name=\"\" sourceRef=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" targetRef=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235\"/>\n      <sequenceFlow id=\"sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E\" name=\"\" sourceRef=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\" targetRef=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF\"/>\n      <sequenceFlow id=\"sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6\" name=\"\" sourceRef=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\" targetRef=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714\"/>\n      <sequenceFlow id=\"sid-9218A35A-08ED-4C84-93AA-C568CADBD664\" name=\"no\" sourceRef=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" targetRef=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\">\n         <conditionExpression id=\"sid-ffc0c9dc-bfc1-4cb5-b41f-6cd53e3707cb\" xsi:type=\"tFormalExpression\">${documentService.all(execution, \'finalOutcome\', \'Changes not required\')}</conditionExpression>\n      </sequenceFlow>\n      <sequenceFlow id=\"sid-1498D019-EFAC-4F53-9400-715E685CA2F4\" name=\"yes\" sourceRef=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" targetRef=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\">\n         <conditionExpression id=\"sid-933c53b2-38a4-407f-863e-999540f85fd0\" xsi:type=\"tFormalExpression\">${documentService.any(execution, \'finalOutcome\', \'Changes required\')}</conditionExpression>\n      </sequenceFlow>\n   </process>\n   <bpmndi:BPMNDiagram id=\"sid-8458580e-bbc3-4cbb-b62e-ab1badc59ee5\">\n      <bpmndi:BPMNPlane bpmnElement=\"startMlrReview\" id=\"sid-e350d9f4-f8f1-48a5-b091-f75355ef3655\">\n         <bpmndi:BPMNShape bpmnElement=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C\" id=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C_gui\">\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"210.0\" y=\"555.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" id=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"285.0\" y=\"645.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\" id=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"456.0\" y=\"768.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\" id=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"634.0\" y=\"532.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" id=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8_gui\" isMarkerVisible=\"true\">\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"821.0\" y=\"552.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\" id=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"1015.0\" y=\"532.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714\" id=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"1015.0\" y=\"250.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\" id=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"791.0\" y=\"396.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF\" id=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"791.0\" y=\"250.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235\" id=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"285.0\" y=\"250.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6\" id=\"sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6_gui\">\n            <omgdi:waypoint x=\"1065.0\" y=\"532.0\"/>\n            <omgdi:waypoint x=\"1065.0\" y=\"330.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4\" id=\"sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4_gui\">\n            <omgdi:waypoint x=\"240.0\" y=\"570.0\"/>\n            <omgdi:waypoint x=\"262.5\" y=\"570.0\"/>\n            <omgdi:waypoint x=\"262.5\" y=\"685.0\"/>\n            <omgdi:waypoint x=\"285.0\" y=\"685.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-FFDFD243-271D-458F-8DF4-854C3668FED8\" id=\"sid-FFDFD243-271D-458F-8DF4-854C3668FED8_gui\">\n            <omgdi:waypoint x=\"556.0\" y=\"808.0\"/>\n            <omgdi:waypoint x=\"605.0\" y=\"808.0\"/>\n            <omgdi:waypoint x=\"605.0\" y=\"572.0\"/>\n            <omgdi:waypoint x=\"634.0\" y=\"572.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-9218A35A-08ED-4C84-93AA-C568CADBD664\" id=\"sid-9218A35A-08ED-4C84-93AA-C568CADBD664_gui\">\n            <omgdi:waypoint x=\"861.0\" y=\"572.0\"/>\n            <omgdi:waypoint x=\"1015.0\" y=\"572.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39\" id=\"sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39_gui\">\n            <omgdi:waypoint x=\"335.0\" y=\"645.0\"/>\n            <omgdi:waypoint x=\"335.0\" y=\"330.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-1498D019-EFAC-4F53-9400-715E685CA2F4\" id=\"sid-1498D019-EFAC-4F53-9400-715E685CA2F4_gui\">\n            <omgdi:waypoint x=\"841.0\" y=\"552.0\"/>\n            <omgdi:waypoint x=\"841.0\" y=\"476.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E\" id=\"sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E_gui\">\n            <omgdi:waypoint x=\"841.0\" y=\"396.0\"/>\n            <omgdi:waypoint x=\"841.0\" y=\"330.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E\" id=\"sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E_gui\">\n            <omgdi:waypoint x=\"385.0\" y=\"685.0\"/>\n            <omgdi:waypoint x=\"420.5\" y=\"685.0\"/>\n            <omgdi:waypoint x=\"420.5\" y=\"808.0\"/>\n            <omgdi:waypoint x=\"456.0\" y=\"808.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174\" id=\"sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174_gui\">\n            <omgdi:waypoint x=\"734.0\" y=\"572.0\"/>\n            <omgdi:waypoint x=\"821.0\" y=\"572.0\"/>\n         </bpmndi:BPMNEdge>\n      </bpmndi:BPMNPlane>\n   </bpmndi:BPMNDiagram>\n</definitions>\n'),('107',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Approval Workflow - V2.startMlrApproval.png','101','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0Y\0\00\0\0\0t��\0\09qIDATx���t��/�D造�0T�ҢG��S�bZ��Җr�L����z\\ږC˱c���Z[��L�eT�u�j�FT��Ǻ�eUNA�V.�D��~d�!f������,vv���}��}�\'���.*\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0h\"��馫�=z�����Rz��#ݭ��ڶmSq�	���s}�p±i\0� sr\"\'n�����o[��=cҤ+����(-]+�^Vڎ%%3c��qѧϱ;z�<���&?�Q`2!\'rB+5}������-�L��O�>��S�9�t����ڟ���A� sr\"\'��Оx�\'��]��_����8��.��v�0G!`2���9��H���_E�����c�������A����	-_z�dz^o:�,D�&O��]�v�8\"s�2!\'rB��L�^8�y���v��Nn�pT� eBN�,]�3]�����vcƌ����رc�0alL�;rEn�NvT� s�99�Z���\n�ҟ�w�ĉ1w���v�~�b��E���O��G����dA.++�=�G��Ϲm��Q	���A� �DNh�қ�~o���[���&m���ٿ�=�X����Ѯ]�X�hQv�ҥK�7n�(�{����W��A� sr\"\'�luL�W������Kb�������??��n������e����;���[��o����aU�\0� s�99�Zkp�k��&:t�6����ƍ���Z���f��ر#�xٲeNA.�9����	�[[�S�ӧO�:��m���9�������cĈ��o�y|����Ĵi�b��ق(�\0� sr\"\'n-QV��͋��7d��+����_=�X�j��D�0���9ApkD[�n�\ni�xȐ!�s�Ϊ���Q�FUu���}Qp�A� �DN���_��:t�G�{�d�ۼ�����0a� \n.�9����	�[ۋ)/���x�����ف�^\\�w���W]���_ɂ�f�\Z/�\\\0s�99��f���[1��/�nޒE���fd�R�{����\\�]�Ӱ-;Eݭ[�x��gQp�A� �DN����/f҇���n�%+_��e���T�j^�jUTTD<���p��زe�s|�̙#��`2!\'r���<������Ц o�V�m-;f�E߾}��˾�K.q\nZp�A� �DN�ڂ�u���4tz���ݻǫ�ގ��wT������/�G}t���һ������Aȉ� �^L��2M��ȥ��~�q����S�N�O�;^}}U|�S}bÆ\r�;A\\\0s�99��`p|�����������+ؤJ�8^VV&��`2!\'r��\n�����9HN�� ��`R� 9Qr��*�0���9Ap�����Aȉ�Wp�A�$\'JN\\�\\���Aȉ� �Jp�A� �DN@p���Ar���\\\0s�2ɉ�W	.�9����	���dBN�� ��`R� 9Qr��*��A����	���dBN�Wp�A�$\'JN\\�0)s��(9��w����g?��ѡC�8����{nl�s����=z��m۶������x<��A� sr\"\'���sL��\0�ӟ�ǆ\r��+��F��G�Mt�ءί�޽k�s7mzd��L�0���9Ap��ܹS�W��^������-��|���������3v�M7]=zt�����������O�Ci�G�3�u=N\n܅�~��Q�������U��W�|��ٲ�;�������M?\'�����!?Sp�A� �DNܪ����U�8������ݷj��ٿ)�����+�p�ۃ��>7r䗳����xᅻ��\'5}<�����t_��4$T����e�g̸���-��yd��L�0���9Ap�jɒ�V��7�[�}5�*�>�/$���Ν��{p��8���\'U]?O�[��Ͳe��i����u����.�9����	4(���\"�H��.-]������܍\rn�y�M�B�y^�z�����Aȉ��	{��א���W_��H}��ݟ�g�>�8���3�f��x���O.����#�N?/Z���v�/}���;f�]��w̘�����տ&�ם�i�Ti���3r[p�A� �DN8H�;k�\r�%4�_6�:��\0�_И�[[�Tt}!I�M��./:o�/�8��g�����*��_}fڴ�z�7���KGT5����n��:�ˊ��3�dBN��U���dBN�� ��`R� 9Qr��*�0���9Ap�����Aȉ���\n.�9H���D�	�+�\0� eBN��U�`2!\'r�+�\0� e�%\'�����9HN�� �Jp�A� �DN\\%�\0� sr���\\\0s�2ɉ�Wp0)sr\"\'�\\\0s�99�\\�0)s��(9Ap���Ar���U�`2!\'r��*�0���%\'�����9HN�� �Jps�2!\'r��*�0���9�\\\0s�2ɉ�Wp�A�$\'JN\\%�\0� sr\"\'t:th_QZ�\\h�Am���r���Q	���99�Z��O>nmq�͂���~�`n��pT� eBN���3O�;i���j��3���ɎJ���Aȉ�Ђ��խ�^���˟�[o�i���.��A����	-\\ϞG��2e���\Z6��[r�b��0)sr\"\'�\'u�|�����Bt\0�����r�`M�Nu(� eBN��cP��]����6mڬ�m�aA�d2!\'rB+o���������~���z+O;�Z\0s�99�Z��ڶ���N�[w�e_��C�f��fh��VH��LW��|�dz^���\0� sr\"\'$��jR��UiѮw�V{W����{+�K�2\r�9����	4[�+��Φ\0�r��W�����9\00��\0{g^�_O�9\00��\0{�_�_�2�/W�r��E}�b����`9\Z�����k���9�h�EE�/�Yb�\0`9\Z����x�/\0� ������fr�^�9��a\0`9\0s�	 �\0`9�\0s�	 �\0��@N\0���	.\0��@N\0��r.\0� �@p�r�\0� �@p0��\0��9�\\\00��\0��9�\\\0�A \'���9�\0�A \'��`9\0s \' �\0`9\0s�	 �\0��@N\0�\0s�	.\0��@N\0��r.\0��@N@p�r.\0� �@p0r�\0� �@p0��\0��9�\\\00�����9�\\��m0��Co��+G��|����zh����^T۶m*N8����}��N8v�#\rk5@p�����췭w�1i�Q\\|s��.���j/+mǒ��1y�����={�|n����Z\r\\�Vj�����ѣ[L�2>�˟���:���K�N�;��9�>��\0�h�\r։\'~\";Ӣ���K���#������X���J���3X\Z��hu��~s��b�.@+�����\Z��A\rρ�ɓ�ml׮�#�H��\0�h��U�E.���W׮�����J��\0�h��e��Uk_��n̘1U�%;vl�qjL�;rEn�NvTb�.@��+]���&k�ĉ1w���v�~�b��E���O��G����dMWYY�&k��Go�sn�pTb�.@��h���`Elݺu�7�-..��}��⭷ގv��ŢE����.]���q�FM���Vn��;*�V���݅��W��\Z��^�ׯ�n����.w���<F��(����߉��ߒ�~�74Y{X�R��\0�h�MVr�5�D�bÆ]g�6n�Tuf��w���۱cG��e�<]P���\Z �\0���*=�o���UOܶm{̜y{v{���1bĈ���7�<>������bڴi1{�lM�&k5@p4Y�,���7o^�_�!��.��w�u�e������ƪU�}��I���\Z �\0��\Z�-[�W5T��!C��Ν;��K�G�\ZU�T�m��I���\Z �\0��g����W����&�.�>�y�{U_?a�M�&k5@p4Y�]����/�g�Y�mޜ5�By�_}�_~���	[�f�_h��V@�U��z�Uy����[�����fd��^�{������]冀-{:a�n���g��4i��V@�U�^|��l��a��E��bkY��e%UO\\�jUTTD<���p��زe���̙�i�da�.�&����k��\rՀ�+5]����{[K����Aѷo�8�㲯��K<]P���\Z �\0��ښ��[�fOL�ս{�xu�۱y����뽭��/�G}t���}�֯_���da�.�&�Ѕ/�WLg�.���㌳��ݞ:uj|�����ⓟ�6l��i�4YX����*�d=���Ug�JJJbs��S-]�4���4Y�,��\0�0vkn4YX���&K��j �\0h��&k5@p4YJ���\Z �\0h�4Y`���&K�e.�Z\r\\\0M��da�.\0�,M�j�����d��\Z.\0�,���Z\r\\\0M��da�.\0�,MX�����R�,��\0��d)M�j�����da�.\0�,MX����\Z��ʱ=��߲�o�骪�\Z�0h�4YX��H�r��\\�����R�Ks�f�����ʯ��QM�!���q㾕}|�C�E�u�;vh���da�Fވ\\-�l�n��7ruR�:U~>��;W�r5���su�M����&k�������d��?\r�H���C����c,_>��&k�����g:^x��X�nI���{�?�諭�f�qǵѳ�QѶm���Jߓ��}��u}]m�`m?��ڲ�;��ѹs�&o$ͥX�8�qZ���*�v�����Z���su�M����[�=4k<^{�޸첯l,R��?��.i`n�yB�8�AI�wޠ�s�Vݟ�ۿ�)���|��i�#\r:KV��P�ǅ~Vͺ��e_7c���da��J��՚�]O�k�����qV��dչ�>��ٿ�F\r�{��#�ʲe��i���=�����F�)��g���橨���~������L�ڵl���\Z�d�Y�\Z��;��da��\n��V�5V����V>ޥ6-���*��_��ٿ�M�lV�F�czd�^���\'��J�^z鈏4*��������Yg�CU�S�Y��whH�U�gj��&k��odeCԻ��w��:���h�j]���QR�d�֨����=�/UzZ`Q����#G~y�hL�xY�?ʔ�U�u����O��}�3N�~��_7hP����^c���w,��jV��-�u��0���i��VkR#���7`=~���?ɦFp4Y5��]5p޼_F��]��\\\rzFՅ#j~]�i����X�ni����O,�t��5��f�Pu��i�Ruቺ~��_��+���?~X���-k��j<]���Uۅ/�Y�|cW_c���Z�yJW�r���\Z��mj@��\\�k��.]�=]E����+�A@��4YX��r�,ְ���9���h��&k�V�WѮ�R�ۏ?3�aqo���d)M�j�Qz�Դ��3\'���!�\0h��&k�bN����fzj�|���d)M�j�Qz�މ��g���;6=���R�,��Z�-�괟f�y�6=���R�,��l����-�\\\04Y�,�V+ș,�}\0�&K��jM�k�4�\0�,MX�5!Wt��\0h�4Y`�ք�O���>\0@���k�&�+Wo��~���)��4�\0�,���Z��ZR��)|�à\\=��>\0�d)M�j�و\\�T��f��Յ\Z�\0M��da������I��=^����\\\04YJ�����;Wkr5`=~���?�A�}\0��R�,��#�v]��>h���^��>\0�d)M�j�1�\rQS���[�x?r�`\0h��&k��U:���h�k����S�5Xk*7\r��&Ki��V;��.�ui�t������t����]�8�A�}\0`���h��V㯾Q�l���M+�u��sթ��*�ԈM.��Fé�\ZSԲ�\"蠱\0�di��Zm�ꕫ��jNe�UZ�ӿ��j~Ѯ��v�`\0���da�\Z�\0�}�C�����58͠�o�O�]R��Z\r\r�@v��ǭ-.�Y����~�`n��pTb����\0�;����N�t�&����g�U��u�`����\0���ի[�^�����?��9��f�6m^)j��Ck54�\0JϞG��2e�F�\0ְag�R���`����\0�8�s�÷������*.���>X��S�X���>\0h=u���=���o�ڴi�*���9�V�A�}\0�\n�����\r7|o��h���`U>Ep�k54�\0��Im�~lq�N���첯�ᡇ~���h5��`�˴��V^�\"��S�V�A�}\0p�蛫I�z<W��c�ڻJo4��+]��U�V�A�}\0@�5�rjgS��\Z\Z�\0\0�N:#T^9\r�9�Z\r�}\0\0{g^�_�����\0k54�\0�~E=��/W�k54�\0��E��D:��Y`����>\0�F��,V�F�<`����>\0��YTT�R�%6X�ᠱ\0���:��Y`����>\0�F�_T���� X�ᠱ\0�r���\0\0s�	\Z�\0�A \'࠱\0�r���>\0\0s�	\Z�\0�A \'࠱\00r���>\0\0s�	\Z�\0�A \'8h�\00�����>\0\0s�	\Z�\0\0�A \'8h�\00�����>\0\0s�	\Z�\0\0�A \'8h�\00����>\0��	\Z�\0\0�A \'8h��\0\0s�	\Z�\0�A \'࠱\0�r���>\0\0s�	\Z�\0�A \'8h�\00r���>\0\0s�	\Z�\0�A \'8h�\00�����>\0\0s�	\Z�\0\0�A \'8h�\00����>\0��	\Z�\0\0�A \'8h�\00����>\0�r\Z�\0\0�A \'8h�\00����>\0�r\Z�\0\0s \'8h�\00����>\0�r�����g��Лn���ѣ�=߿�)���>*��j/�m�6\'��k��>�w�	\';Pl0�\0�q��`p����^>p`�m�{��I����⛣�tyD�T{Yi;��̌ɓ�E�>������s��T��`�:\0���iZ��:}������-�L��O�>��S�9�t����ڟ#B�X�q�8M+\\ShO<�Y�.T��^zivyd��s�`�a0`\0�i�4�dpM���_E�����c�������`�:\0���iZ���^8��כN;с�ɓ�ml׮�#��1��u\0��Ӵ��5]�&�p��z|u�����. N�X�q�8M\\ӥ?ӕij?�j7f̘�K]�;6\n���ر#W��dq�����&k5e�6�6Zzo�t����;q�Ę;wnv�_�~�p�x���裏��{��� ���	�֣�����6\\!N4�1ࣵp��������v�_��~�G�s�X6X���q\Z�k^z����u���ޤ���8�������z;ڵk�-��[�ti��ƍwߛ!���ŉ�3�^�����9]�֑]��\\M=���ِ�1���Λ�ɱ1�:\0�Ռ�\\�{�)b���Y~xI�_�>�}���Ǻ�����=��쾷�~\'�Kv��7��=,5�kؽ�\r�3s��/r��φ�:\"�vT�p`��_g����Z�8���!�M����СClذ�7n��kɻﾛݷcǎ��e˖9-���&�ggu����$V/�E<5���>*�����+>T|X����Z�8�����S�ӧO�:��m���9�������cĈ��o�y|����Ĵi�b��ق(���&�+N�ҵ��_���oɿ���5�\\�����1���F�NW�2X���q\Z�k��-����y�������酕y�]w]v�믿o�Z�����Һ��9ל���+֭�۟�;W��ۛ��;�f���Z�8���!�ݲu{UH��C���;wVݗn�\Z5�2����+��K�i���W�\'�*��/>X9=J_������3_��\'?����cc�u\0X��1�6��#��ٿC��Hp/����s�7�W��&LD���4Y;�VČ�F��K�巷��-f�xxV�����%z}�����Z�5Z�\0�\0�V3Ncp��Ŕ�_~y<�̊xo����J/�̻��뫮R��˯d�^�f�S\n.��ɪ�Xw..�c�=��z{ܹ:b��W<qɲ��FL|4b����د>�޷8�?\\!��\0�\0�V3N�ɪ��z�U���w7oɢx�m3���)�=���vz���iؖ���֭[<�쳂(���&k�ӏĠo�\"�<�a��cK*��⊸tqE���\"�5�\"�q�Θ�(�lͮ�/��1�-�/��\0�\0�V3N�ɪ�^|���@�0����ۢd���,b񲒪SͫV�����\'�x\".\\[��z��9sQpiM�sW��X7�G��cL���daķ���97�`�gE�;cg|mZE|�W�NY\Z�:���Ty6X���q\ZMV�S������0 m\n��me�����k~qP���7�;���.�����V�d��k�6=|m��lf\\�d[\\07bԬ����kzE����3#~t�1d���y�x�G�ȳ1�:\0�Ռ�h�\nw�֭�i���ݻw�WW����\n�{[?��_|%�>�����{/�w\\��u4Y�^��q�܄���̘���⊸�Z}��N�������x��O���n�gc��dp�~lS`�f�F�ՠ�V�2M��ȥ��~�q����S�N�O�;^}}U|�S}bÆ\r�;A\\Zz��jgyIl�V��>�wg�2lN|j�ŧύ�ϊӇ�..��ű��\'���ec�AdP�6�jJ��l�Ռ�h�\Z�|��\"%%%���\n6��;����	���\n�,e��+�X�3Y�5ZX���dY`	.Ȟ1��o�����i4YJp1y+c\0�����k�0N��d)��䭌4�����rF�q\ZM��.&oe�����}��`�6N��R���[�k�|���i�4�,%����1��o������h��d	�\\���j�w����8��qZ�%�Jp1(c@�o���~�ha�6N��R���[Z5/Z�\\]T�oc��>��8m�F����2�\rְj��m���F4Z��O��q�8�&K	.&oe8h�����h��7X�\Z]��^��q�8�&K	.&oeh�\n5>�5Lu5Z�}��Z��k�0N�i�M֒\\�!���A�`p�7��\Z܈Ʃ�F��\rV�F�S��i�4-���)�Wp9(�%c�1�q_��6:�5Z�m�����E�h9^���iZT���E�Z\"0�K랼��\0�}�\rVC�B�V�=l��F��j�6N�B���-9Cp��n�6�{�`��h��\r���>��qZ���[��%�n����߲�o�骪�\Zx\n��҂ǀ�a���C�Eǎb۶\'d�В���=sT[��7\rV�F�k���j�i\Z�f]�HiQ5nܷ��/�pH��G�M��k��kf�m�E+[\Zz|N�2>z�<*��i����L��i�h��I�){��6���z����1žQ�j�,Z��lE�_��5�t��������>��\Z��@�?�С}|�+�˗Ϩ5�+W�.�>����wǺuK��M�s��W�=5�;��um۶�z��=5��~���������U[m��X����ܹS�N���(-o�7�P�N;��X�����Ǆ��S��B�o��ԬM��i�N�=j��쾺�_�8Q(�u=�ɻ��}�]Mpo�d5�\r���r�Z�Y��[3�E\r\r���C�������.�z��5�1��]>��o��=N:����7(�ܪU�g���J�??���=�����ϪY_����f̸�`����u4Y{4�멧n�&�t�_�L���=wW��)t��7�qjVjz��͟?%�t;�W_�k~�P��z|�w����\r͖�}�=i��&��j�f�F\nnI�F�`\Z���Z����?r�/[vK��t����K:�?�ٿ�5����Q���/��_g֮}�`\0��\Z�B?��`�s�3�,ZB��Wc@�.�tD�y����Y7d����_gv\n�_(��}������c7��*������澚�MC��L{mWlL��ꂎWk5k5Z���zר����\"��BR��?��ǫW/�54�/#�ߴH�y�.��秿T�u�?|��I��\Z�B?�Pp���\ZLZ�o�1 U��E�Ϳ���R�e��������iH�����4Y�y|�w���jk��kp�{���aqM���O���Z�Z�ַ���@ڱ����J~�?��*�j.�q\n:�9�˻�(s����}�}��?�c���v�/�W�B�Cͯ4����o/�1c���;�Y5+�}����i�M�l��{\\���_P��	.���e�������ǩY�i.E�O�K�,�|\nM}ٯ9N�x]���7yP��Ij�\r7���?eq�]����\ZYp�ͼy���ݻf���CϨz1bͯ˟�>��ǺuK�S��OE:��&��1=E)��Y�Ōu�5��W���ѣ[Ԣ\Z��k�Y���2����_�5Y��&+=���f�v��ϫz�u���?�%���7�qj�kj\Z�צJ���ۯ+�5ǉB���5Y�B�3Z\rm�\Z�h��k5k5��*�5�h��KD���Oݻ����U[�5����\Z,���iWi��d)��A�hU�F���1o4\\��\\��q\Z�U�k0�d)��A��SS�4���e��}{��\\X�)�4��4Yh��ɻ�4Z{���\"�q�8��*M�,e��h5��\rk5e�Fp�&M�2y�X{�?O�8m�Fp��j�4Y�@�F��g�\\��q\Z�U����d)c\0�4L���׃q�8��*M�,e��h�/W[���0�i�4��W���R�\0�5���\Z\\���k�0N�\\%��,M�2�p��hy� �i�4��j�4Y�\0c\0M�hi�0N�ApW���2h�Fk��q\ZWp5Y�,c�1��1�r��L;�i�4��j�4Y�\0c\0M��M�q�8\r�+��,M�1�\0X�)�4�+��,M�1�\0X�)�4��4Yh���0N�\\%��,M�2\0�j�8��\n�&K�e0\0�j�8��*M�,e����iWi��d)�7`�f��\\M�&�`\0�Քq\Z�\\M�&�`\0�Քq���C����˅����O�)�K�5Y��mo0\0�j�8M;�����,8͠���v�\nM�s�����2N���<����&]!8͠�?��.���bn{c�1\0�VS�i�X�^����ս���i�9��f�6m^���,��7\0k5e�f���S���XÆ�yKnW,9X��mo0\0�j�8M�;�s�÷����P�ŷܗ�kru�&����\0�ZM��u���=����mӦͪ��f�����\0c\0`������o�_n��{=�w�?����ZM�����\0�ZM��Nj��c�;u:l�e�}�=��5ޛ���[!]�3]����y�-ᴳ&�����\0c\0`�f�6N����դ\\=����ŞڻJo^��[!]��%]�F�upn{c�1\0�V3Nh�l{\0\0�B�\0\0,���\0\0}�\0\0�з�\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0,���\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0,���\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}��&\0\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0,���\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�5�_����V�L�,\0\0�a�媼�&k�ͤ�\0\0\ZnQ\rVI���D�,\0\0�����d��y4Y\0\0@���ڬ�Z,g�4Y\0\0����Y^���\0\0�¼\"g�4Y\0\0@�P��\Zlsh�\0\0���m��X�,\0\0�	����d\0@�]1G�7�tՕ�G{��SJ=�}]o��\ZXm۶�8�^�?���N8�؁�,\0\08�v�O/8�߶޽{ƤIWDq��QZ�<\"V����KJf����O�cw��y��M~�&\0\0Z��ӯ��G�n1e��(/Zc��k���.]:m?���h�\0\0�6X\'����L�h��K/͎#���~Q�~/M\0\0T��\"��`i�\\�ձc��E-����,\0\0�ZG��5X�)�\Z�W�\'��خ]�G4Y\0\0�¥���\\x\rց��];�S��͕5Y\0\0�R�˴��jr|�;rEn�L�d\0@��+]����������#z���%~1�s\\��q�Mc5IMP�>z˟s�d�&\0\0Z��F�\r}��n�a����q�Ȯqs���{D�lH����q�M�4JM�>Z�]R��\0����M����cf���E�����TG��Î��\Z�묿�(5A�ІE�\0\0{�d��N�д���E����� ���G��A����q�珊˟�(i�\0\0@�����+N�ҵ��_���oɿ���5�\\�����1���F�NW(�d\0\0\rn\0�\\sNlz�X�rNl��\\��noz�����$M\0\0��&��|E�pү���⃕ӣ��yYm�?��+��~��(����&\0\04Y�-�w���.�^����oo��?�Z��������{K���#���(�k�4Y\0\0��*�诨Xw..�c�=��z{ܹ:b��W<qɲ��FL|4b����د>�޷8�?\\�a�d\0�&��Z��#1���9O}��B�ؒ����\".]\\�﯈oͪ�oܾ3&,�5[�+�#n�yK�k�4Y\0\0�ɪ����ĺ�?��̘cr��%#��@E�Ϲ��?+��;�k�*⋿��v��������U�j�4Y\0\0�ɪ�^�5Y��6F^63�[�-.�1jVE|�\\�5�\"��]Ĉ�?���2��ؼtR<�S4L�,\0\0�d�V�^��q��䒟�g�ǷW����ەu�W����s�~:�>w��I�\0\0��B���$�m+�	�~λ3N6\'>5��S���Ƀg������������j�4Y\0\0��R�,M\0\0h�4Y�,\0\0�d)M\0\0h��&K�\0\0�,M�&\0\04Y�,M\0\0h��&\0\0�di�4Y\0\0���di�\0\0@��4Y\0\0��R�,M\0\0h�4Y�,\0\0�d)M\0\0h��&K�\0\0�,M�&\0\04Y�,M\0\0h��&\0\04Y��6l�}rH��رCl����J�\0\0����)S�GϞGe�Ĵi������j�\0\0�&��N�%K~�-�O?��G��;�6?�c���3V��]�?w�=7F�^ݳ�6mz$.�pHv��C��1j������3��?��]���q�ǺuK�+_���{�?��b���d\0\0ͷ�z����?����>�[��o�/�M<�ح���~�����o�P�sRS��?JV�v�/}�뾓}���\\�}<a���_� �}�y��ϭZu�o���h�\0\0���d]z鈸��I��Y�n�\Z��~��ݚ���Fz�Vc?�*��J����LV�v�/}����e��d�׮}x���W�{4Y\0\0@�k��E.����j��\'�WZ�|�f)����l���\\]MV�9����\'g�M����Z��K��_\0\0\0ͺ�JO�7�[�ݗ��WT�T�|���s��r����\Z��>W�qG�Z�t�E�~��N��??y��t�3���?��+�?q�e�,\0\0�y7Y��M/�4{���O�0പf)=�0�QJ_�z���\Z��>W�q�E.RS��6U����E�?�i~�S���J_�^�U�i��,\0\0�Y6Y\r�tyc?�}�\0\0\0MV-�?����i�\0\0\0M��d\0�&Ki�\0\0\0M�&\0\0�di�\0\0\0M��d\0\0�,M\0\0���d\0\0�,��\0\0M��d\0\0�,M\0\0��R�,\0\0�d)M\0\0���d\0\0�,M\0\0��R�,\0\0�d5�*+{*^{��FW�>M\0\0�ɪQ�a�lB\ZU��4Y\0\0�&˙,\0\0@��4Y\0\0��R�,\0\0@���\0\04Y^���\0�֫C�����]]����O�)���;*\0�;������LV3����價]��Q	\0\0-ؙg�>wҤ+�����gޕ�%��\0\0Ђ��խ�^���˟���z�M�6��v�\0G%\0\0�p={�rʔ�\Z�XÆ�yKnW,q4\0@�pR�·o+)��5Y���o�/����T�\"\0\0���w��^sn�Z��S�զM�U��s�C\0\0Za�վ�!���ml���jeg�ެ|��\Z\r\0\0�n\'�m��ŝ:��˾����͚��>Z-�}��e��U+/r�^��)�\0\0p�蛫I�z<W�E{�4=��Jo4��+]��U\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0���?#�h/iZ�!\0\0\0\0IEND�B`�'),('108',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Submit for QC - V2.submitForMlr.png','101','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0�\0\0�\0\0\0I8�2\0\0<�IDATx�����e�(�i@.����!R��z�6�?��%�KR�z�������X���j�8F�V�W�D(����dL�76up�	r�8$������;k��a��f�������Z/k�\Z�z�����VT\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\04o!�t�:��[F��r�~���С]��E�G�6���:���}�����:}��\0\0h�z��7�wg��=�ĉ7��������B+E#D|oKKg�I�Ƈs�9}oϞ���|X\0�g�������5L�|s��\\��k��2��ᤓ:�:��vWX#\0��P�}��ӭf�����^�N9�w��e�5\0\0h��.�q���y5�;�+/�+,\0\0Мœ��c(�.�����&��ڶm�笥\0\0@���\ZO����]�t~+���[S�qN��4��/�Ess\'��5S�ߘ1cr�ʸq�Ba��#�q�F�H��I�T�c�S�;\r;�K~ќ�/cb���TN�0!̙3\'�ݷo߰`������C����eO<�D�dVTTh*0���?\'��\nk*�1ʩٝ��%�h�����_Ƅ�cǎ�b����?�.]\Z֮]ڶm.\\�.[�xq�s�֭����\'yO+��\0�h����NÎ��_�\0�|�!�_�>m�}vQؼysz�ꫯ�����a��/��֭{+������o���l��4�\0K���iؑ_���TFw�qGh߾}ز�j�֭��\\����鲽{����,Yb�WM%\0G63;5�Ӱ#��������N�>=���Ν��̙���F��޾��={����ϟ�M�f͚%_4�\0�fw\Zv����4�EՎ��;wnؼyKz;��\'��;�L��Y�&�QVv���M%\0���ٝ��%�hMM���rY�2$�۷/�,�5jT����=�R�h*8N��4��/�Ekj*��?�N:����k�I���|[���v�\\�Tp8���iؑ_���Tf�x���W�m��i���d�u�]�3�������ܰa���T�X��S����_򋖒Wk׮\r�2[!�.ߞfʃ�H�Ǔ�\\y���x\\e�;;��c�v�\Z^z�%y\"�\0h�o|���D���_���z��W����ɽ��;C��W��YR�˫����/��BX�`Aؾ���ٳg��@CO�N�n\"F~�/Zڗ5���lٿ����Mf�Ί�m���1?9(���\'�q��㮿�z_��5\0\Zcr.rjv1�K~��x�r�6~aӭ[���l](ߵ7�;�v�	/��*w\"��eM��\\S)�\0h��ש�M��/�EKj*��O�Ry×�\Z.��S��)S�����;�^S>��s-[\\�G���Eo�S����_�cQ�4dS��SO�򦴴4ɣ���������T�5\0\Z�_�f7#��G#_2�����WB�Ь�^�f7#��M�TfcaQ�-��g�@K)z���D���_4aSY}��%�J�@,z���D���_4����������ʕ?\r_��о}�p�yg�����ןZ����>в�x�����{�m��5����\Z��ש���/���\Z*Q�K�vZ����|�ɰe˯j�\Z����hя��<���������nݺ����;�5���>�i.���^�fW�\"��M�X�HbXm�ʫΝ;�[�^{m��7mZ.�����3�|_X�l���ԩ��=��O<>����ӡ_���qǵ\r�G�WS��^����������^{퐴��èQ��e�v�f���B\ru��۷/\r��}!������:�����yM�@��Nͮ�E~�/�bSY���3�~xB�����ѹ�W]5(s������,Vo���\r���ȑ�N�?��]�W~^�������3f�Mel\"��\'���F��j����U_v�u�IoϘq{޿-�;唓���r\r�*z���%�8�Mei#�4��ʫ��iv7��㿘.��u.ޯ�y�[��}�^����gl(�\rdvY���sŨ�o8�����W̒%�/<;��Z�g��\0E���%�WkRh��!�Uv�\\�q�6]�w/�w�m���쮨���Y����ؐMe�cLׯ_�*��^I|%��I�Ib{�w\'�$�&qK�q\0(z��/!��3����ԭ^=\'���9��ty<�1ޟ0al���K.��~ᅇ·�u}����=��<>{Lf�z�p���qY��N��߸���n*�[G�sWLvkkv������5�s�)s-n�^�i$L��I��D�̿ǟ�3�PL�<�$�k���Wы������G�N/��2^z���2{��xc��`�j���I�s�K�̝{�!5����ډzb_?F��=��i�̝`�p���(�pÈ\\�]���4��С��.IR�k6צ26��2��o�vu���6��:�����Wы��K^�c+�F&���jwֶG�<_�<���\0&g1�K�/y%��\\�[%�1�����d���`r6#����W�u���Lػ�W�ޙ����l\"F~	�%�D+͵���]U�7Ҋ�\'����)\0����%䗼�/��^oi�)c�k9`r6#����W�u�Z�lH<�k���B���n�\0&g1�K�/y%ZU�ŭ�Î�\n5���J\0����%䗼�&�zU�D��Q\\��5�ɀ\0L�Bы��_�+���s�v�W�IE��&��Y(z�_�y%�B��N��Gy����>)�\0L�&b䗐_�J��\\����}�W��I�%�\0L�&b䗐_�J��\\۞D���R���-�\0\Znr^����?ڷo�;���c�4�Ds�&�������{�m��7���ϧ�_�K~Q�$w��޽L��Lc׮߼�|L�G4p6Հ��pL����#��|�ɰe˯�M7}�Y�\'���бc�Zӭ[��u�y繣���^����w�KJ���5Ә7�ާ��iő|ƭqK��8��Ν;�[^{m��7mZ.�����3�|_X�l���ԩ��=��O<>����ӡ_���qǵ\r�G����yb�z��C�B3���Q��e�v�/��5�,����ۗ�q㾐����n|����������%�h\\4g�ě4p�4��HQ��T[k<���sE��O�p_�����\Z�.++{<������}�ݒ����A��9����G�+�����U��{�X���O>99�x;.+Tl֧ ����>�ޞ1���[��9唓����^������յ_�^�**+�k�_����xU�1�?�ϸ5����sEo�E�~��Mo��/��jnA���*0㖏x{߾�,k{��k��Q��N�[���?fɒ��n	*ʳ�Eы��_}={��r��5q�,�\r�@��,:�Ϸ5^����Eo����2[�<ABmf�X�Oax�Eo����,z��~�E/�K~�|�۹�	;KKgj�I��<0/�\\6$q��~����Sm��\nw����h��7n9��@�^=\'-��9��ty<�*ޟ0al��K.��~ᅇ·�u�a�����y�0���;ᄎ����=�w̘����������%1F|o�z���V��/�%�8\"��u�Mc�<\Z���Ⲣ��HĢ�|��V�$~�ؓ�|�����G�N/��\\z���7{2�x�U����*0���s�K\'̝{�a��c�_;F��=��i�̝\0�p��X��pÈ\\��]��xf��zC�^��dB]���E~�/�N?Ю�q���lu�e�C���uCC��#�x���l�����VS	аE�pqv���9�M��<ө��Ǝ���~�G\\ǲq�C/��9)OܨxAc|�%E�{�c����Qh^\r>��W(z�_��O3=��ƿ���\Z�EU��m�7�ڻ�jh�F\\a��{4&�	(z���%���odQ�I{z7B�\Z��ڣ��0�\0�^��E~�/����4�\r�ŲO���q4\'#���^���N�b�����#9�1C�!Ө���G(z���%���ջ��@񬰃�w�eCJ���=�	�v����^�����g�˸봢�ˏ��D�̿w�4���gZ�i&��K�h*E�P�\"����J�I��4��S�Ɵo%�dQ���d���Bы��_\0�J\0E���%�\04�\0�^E/�K�/@S	���^�%���P�\nE/�K~h*E�P�\"����@ѫ�E~	�h*��^䗐_��@�+��K�/@S	���^���T(z��/���P�*z�_B~�J\0E�P��/!�\0M%��W(z�_�@S	(z���%�\04�\0�^E/�K�/@S	p4�o�n������ v��͛�GRi��_B~�J�����XRr���ļy�>�|$+���K�/@S	�bpќ�oRt6�>|�#�G2�Z)���4�\0-F�^]���խ��r�³i�O��ū�����R~	�h*Z��=O]9y��\n�&�a�<�|����K�/@S	��۹�	;KKg*@� JJ��|����(���4�\0-ՠnݺlS�������,y��Y嗐_�������������Vǀ5�1^�]�6(x嗐_���59�M��<ө��Ǝ���~�G\\g�ᮓ/k�B�9iH<��.y�K~�/@S	�*�Ibb�Nbwf<G����:y��B)���4�\04Of���\n�_\0�J\0E�\"Q��{;@~h*8s�����\no�/\0M%\0�շ�o[Q��L� �\04�\0�˓E��#nMq��/\0M%\0�ʷ%#�= �\04�\0�faQ�K�z{@~h*(���(������VO�}!tg�����x\r�� \n��\Z��A\0�5�/\0�\0�k@~D0^��0�`��`�x\r�\0�(��\Z�_\0Q\0�5 �\0�\0�A~D0^��0�`���A�x\r�/\0�(\0�k�_\0Q\0�� �\0�\0�A~`0^�� \n`���A\0�5�/\0�(\0�k�_\0Q\0�� �\00���`�x\r�� \n��\Z��A\0�5�/\0�\0�k@~D�׀�0�`��`�x\r�� \n��\Z�\0Q\0�5 �\0Z� \ZBI��So�e��a/��w��څ��!� ڴ)��Y�6�c}�u����`��@�Dz��7�wg��=�ĉ7��������B+�F|KKg�I�Ƈs�9}oϞ�����X��x\r�/�V1�N�~��=��ɓo���5��S�|=�tR�]���\n�9��Т��P�}���-i\Z����6+�r�I�&�0�:���\"Ѹ�k�B��l�Ʋc�v�Ev��5 �\0Z� \ZO������j�.&M\Z��m۶�Y��x\r�/�5�Ƴ�Ɠ�8���K��o%I�<���b�xِx����N~cƌ�]&cܸq�0��ĸq#W$��$�<���b�x�xِ���	&�9s椷���,X~�|y�޽{��\'�H�̊�\nM�a���?���=\\a��5 �\0Z� ڡC�Z�C;r[%c����?�.]\Z֮]ڶm.\\�.[�xq�s�֭��ü�e��UZ��x\r�/��4���脰~���Q|��Ea��������:lJnWTV�ѣ��.[�������o���0ä	�k@~���2��;B���Ö-U[ �n}\'������N��ݻ7��d���j*�x\r�/\0M��tw��ӧ�vyݹsW�9�������È#�����gϞ�����ôi�¬Y�4��J0^��Xn*��K9w�ܰy��v<iO֝wޙ.[�fMx�����$j*�x\r�/�c��ܾcW�������ۗ[o�\Z5*�DV�[�I�T��\Z�_\0�xS��?�au�s�С5��\\sM�o���r�����4��J0^��Xn*�n�����+¶���\'�ɺ뮻rg|}��Uiӹa�\'��T��\Z�_\0�zS�v��0*�����i���C3���$=W^yez;WYuM˝��]�v\r/���&QS	�k@~�M嫯��69M�m,�JW�vT��̒��n�eeea��^xᅰ`���}{��g��$j*�x\r�/�c���ⱑ����iC���aێ��c~rP�ӧO8�3��]��v�T��\Z�_\0�ʸK�t�x��nݺ��e�B����Fsێ=��WW��ݻ���שܼy��RS	�k@~h*�gy�[*o��W�%�}*�=eʔ���z��k��>xNزeK8�&QS	�k@~h*3M�SO=��BYZZ\Z�3g���x��PQQ���T��\Z�_\0�J��� �\0��JM%���`�Tj*�5�/\04�BS	�k@~h*��P��0�j*5�����ATS��� �\0�T\nM%T�J:L�z�-�G{�_��ww��.w]\\q�ѦM������c�3���NhMC=`�Tj*��y�o�8p`ߝ�{�\'�JJ��w/3F4@����tf�4i|8�����y���[~����ATS���Va�������5L�|s��\\n\\h�2��ᤓ:�:��vWX��TD5��Jh�\r��g�?ݒf<8z��k��)���n����0�j*5��\"�]^�J\re�5�;�+/�+,�J\0�h5���DS���f?ȅ��ʸ˫q��bҤ�[۶m��5M%�A���Ls�_S����*��5���1�M]�t~��94�\0�`S���E��\\*�4�Ф�eC�Y^��E~cƌɍo�ƍ�W%ƍ�\"yO\'Y+�TD�5�շ\\^���TBs�C/RWS9a0gΜ�v߾}Â�o�/ݻwO�=��iUTTh*3���?\'��\nk%�J\0�hmMe�-���j*�������N�5���\r�BMS	��С]-סaǎ�_%%%�ϥK���kׅ�mۆ���/^��ܺu���0�c����J4�\0�omԨ��9a��/����vH�h*O>���c��\r��f�T��A+�\n�E�ׯOׁg�]6oޜ޾��æ�vEee=�K�u��\nﾻ=���oh*}����FjF�nM�Zdm�.檊���/]tNz���xP���߾}�p��\r˖���T�\\��p�e	����iӢ���w�<�}�ߩ?��wBϞ��6m�s��fS��o��q�\Z�|��/�o_\Zƍ�B�ܹS�7�&Mń�2��w�qG���Ö-U[ �n}\'�o��v�l�޽��%K����8�y\0�Fh*k6��(檊�ѣ�������0v��\n6R�Q��O9夃\Z���-}�ؐ��W]5(�����ӟ���������;�<W�����\ru�/�Z5��>�>nƌ�s�	\Z�����N�>=���Ν��̙���F��޾��={����ϟ�M�f͚�I4a\0����$F�i&��TN�����Q��c��sPc�d����Nw�-ʳU06a_����bQ�-��^�������ҍ�-���7ԧ�,�Z�\Z�}�^T�)&h䦲�81w�ܰy��v<iO֝wޙ.[�fMx���޻���\0jWh��!7���8��M�[+kk���#��~���\r]�J�pÈ�\Z��\'稊����K?�k�\n5�u�\r�i*�V��R1����4��w��5����!C¾}�r���Q�Fe�J��JM�q�\0\0MX�e���{KsMe��,./��+kZn_z����ȑ�>��?&����\'O�9�\\\'��1�<ގ˲[�\r57hP��	=�1c��7z������Xp���\Z[Ŝb�Ck*��?�N:����k�I���|[���v�&QS�y\0���T�uV׹s�\rݺuI��:��܉nj>.��W|<lڴ8ݝ��n��v���\'�y�ѻs˧M�f�D9��\r5�j�c���=ztM�Ȣ\Z���{�|\'�[]��l]���R1���CUn�����+¶��4O�{����__}U�tnذ��z4��\0hM�P�)&h�qh�ڵaTf+�����6���f���Iz�����v<��Ꚗ;��c�v�\Z^z�%�q�\0\0�J���cyz��WӼ�kroc��P�򕰣\"�g����(++�����/���۫���={�q�8�y\0\0M�b�ı=����lٿ����Mf�Ί�m���1?9(���\'�q��㮿�z��\Z�0\0��T̡�0�]Zw����kUv��-�.[�w��5��v�	/��*t��=}|�N��͛5��!�\0h*s(&�Cဳ��-�7|����>�ޞ2eJx�i���5e�<\'lٲ%ȸb�<\0��R1��ES�i*�z������P�9l�ŋ���\nM�q�\0\0�J��	�0�y\0@1\'s�	�C�!0\0(�bN1�q�8�\0s�9��!a�<\0�bN1���8$�C�\0P�	Ŝb�q�\0�9��SL`2�y\0@1\'s�	�C�8�y\0\0ŜbN1�qH�0\0��S̡�0	��\0sB1���8d�\0�bN(���C`\0@1��SL`�!�\0(�s�	�C�8�y\0\0ŜP�)&0��<\0���9��!��\0sB1���9�C��?<����S�ޚ[VϜ2��0\0��T�)&8����kƏ�bz��k�4�����:vl�`�3�y\0�Ej߾��ݻ�i�A����7����Z���T����a��s�����������dL�_�Ѱlٌ�M�ʕ?\r�]����+?�6-J��3ߗ����|\'��yjhӦ8�\\�w�ϝ]��o��q�\Z�|��/�o_\Zƍ�B�ܹS�7��\r�\0\0�����XRr���ļy�>�|$+���	M��M���C�F��e;�s��(�姜r�A\r���ߖ>Ol�����\Z��[Y����~��������w���V������z��q�u�I7c���Tb\0�i\rpќ�o��5�>|�#�G2�Z���T��L�����Q��c��sPc�d����Nw�-ʳU06a_����bQ�-��^�������ҍ�-���7ԧ�,�Z�\Z�}�^�Tb\0�i��յ_�^�**+�k�6�T\\\\�*�H�[+�ʃ��E�~����&ƭ�5��N��_�~Aކ.n��?o�a�A�Y]�\0ĭ��^��\\�W�9��o�OSY�\n5����<\0@�г�+\'O�Ycׄ1l؀��b��Q1�������[�k*�5fqyQ�]Yc��\\�j��\Zo���N�3a��Z_?����:ᄎ���v\\�ݢX�o���A����׭�ƌ~��X�jF��.�a�]rkkl5��\0hh�v�|���ҙ\Z�&����%���$.�**&4�������:w[�.�1�C�^�;�M��ew��⊏�M����V�\r����1�:�>zwn��i�̝(������V�z,�x��G��iYTc��|���D=q�k������Tb\0�h�C�4�G��,...K��aVAń�R���\0Z|cٮ�q���lu�e�C���u��R1��RS	�����6m��L�N�o\Z;�s�{��mpˆ�e�lH<�k�<�J��*&�Tj*�<\0�*�Ibb�NbwQ�c��aGeQ�u(�eC��U1��RS	�\0@1��RS	�\0@1��Rh*1\0\0�	4�BS�y\0\0@1���J0\0\0�	4��J0\0\0�	4��J0\0\0�	4�BS�y\0\0PL���J�\0\0�	4��J0\0\0�	4��J0\0\0�	4�BS�y\0\0PL���J�\0\0�	M��T�y\0\0PL���T�y\0\0PL���T�y\0\0PL���J�\0�bM��Tb\0\0PL���T�y\0\0PL���T�y\0\0PL���T�7�\0�b���}�v�w�^��k�k�o�L>�Jk%�\0@1A�q�ygl,)�_S�b޼{�J>��J�\0�b�c����L�x������I>�I�J�\0�b��W���z��VQY�\\c״����U�G��Z�y\0\0PLТ��y��ɓo��5a6���Xdm�<\0\0(&h���������35xM%%�K>�\rI\\`U�<\0\0(&h�u��e����7����e��?�*�y\0\0PL���v����we�c,���.�4��\0\0��ɹmڼ�N���4v��~���?��:�\rw�xِx���Iy�1�vy�<\0\0(&h��$11�_\'�;��#�ʢ��P�ˆ8�+�\0@1�ē������\0�bE��W�YO{;�<\0\0(&�P�-���+�`\0\0P_}����2Τ\n�\0@1��d��\'É[+[	�\0@1�ʷ�2#�=`\0\0P��E�/�Q���\0\0���Bj�J��J0\0\0�	�Փu4���\0@1�S�_\0�b�� �\0\0�XOA~\0(&���\0PL���\0��\0�)�/\0@1�S�_\0\0�	����\0XO�\0(&�z\n�\0PL���\0��\0�)�/\0\0��S@~\0�	����\0`=�\0(&�z\n�\0@1���_\0�b�) �\0\0�XOA~\0�	����\0`=�\0���z\n�/\0@1�S�_\0�b�� �\0\0�XOA~\0(&���\0PL`=�\0��\0�)�/\0@1�S�_\0�b�� �\0\0XO�\0(&�z\n�\0PL���\0��\0�)�/\0\0��S@~\0�	����\0`=�\0(&�z\n�\0PL���\0�b�) �\0\0�XOA~\0�	����\0`=�\0���z\n�/\0@1���_\0�b�� �\0\0�XOA~\0�	����\0PL`=�\0��\0�)�/\0@1�S�_\0�b�� �\0\0XO�\0(&���\0PL���\0��\0�)�/\0@1�S�_\0\0�	����\0XO�\04�\'3�Cm��ۄ��\0@>}������mB��\0����4��I�����\0PH�Z���� �\0\0����x,���(zA~\0�)߱���D��\0����J���_\0�a�_����@��\0�Pe����E/�/\0�C�W1��䗷\0\0�ˬJ:L�z�-�G{�_��ww�ЮХ;�!D�6���:���}�����:}�5M���\0\0�:=��컳w�a�ěBI��a��e!���#����3äI��9眾�g�S_N���u�J0�\0�U�>��y=zt\r�\'�*+�+\0\Z9�L�z8�N��?���>M%��\0�����~�\r�����k��\n��rһE�����y\0\0Z���S�fZ!�tEǎ�ʋ����y\0\0Z\\�J:�cg�N&���I��om۶�s�HM%��\0Тĳ�œ18v��K��o%Ik����\0h1������L�M�ƍ\\�|$����Jh>�@~cƌ�]&cܸq�0c�y\0�V/^,�.��ނ�7�����Г��w���ᱩ�\r�?�����d��RS	�g��	&�9s椷���,X~�|y�޽{��\'�H�̊�\nM�y\0�cI��u}�?���_?��Kxpd�pS�<9|wH�p��;��M� h��%I��RS	�gaǎ���1JJJҟK�.\rk׮m۶\r.L�-^�8��u�VM�y\0�c�ȭ�d7mh�03i&��4��\Z��pװS��v	����� h��th*�y�!�_�>��}vQؼysz�ꫯ�����a��/��֭{+������o���4\0��88�{i����o���~;�������p����Sg��>~j�k�o�	4����2��;B���Ö-U[ �n}\'������N��ݻ7��d����\0PL䏟�t~ؽ��/�����Wf�k����w��R�����}���@SIk���ӧO���s�0s�����Ç�#F����{aϞ=�����iӦ�Y�f��\0h*��w\\�y�i����\'�hz���~~y�pŀbM%�l(�v,�ܹs���[���=Yw�yg�l͚5፲�~Ǹn\0@S�����k�-�|\\߰g������i����_��7��v�yQA��@SI+k*��ؕk ��!C��}����ۣF��4�U��V\Z��\0h*��[�\"�xjq�5xqx}ݎ0�_>���i�ۿ����ק�����a��R1���V�Tf�X��:t�AM�5�\\��[y����o��6�y\0\0M�ʰ���gJ���^��~W���n~=����p���� �	χ�l��p�?�&<8�P��\n��JZ�<P��o/��\"l+/Oǩx➬��+w���__�6�6lp��\0\0���I\Z�?}��a�o�\Z���ƕ�7��7<�?�~|������n[�4����O��\'�]���@1���V0�]�6��l�|�|{�&>�Ќ�~<IϕW^�ގ�UV]�rg�{l׮]�K/�d\\7\0��\\~�\r���F�ь\'Ø�q�~A�������$\r��W��>3m�俅�ɋ�֧o�����bM%�`x��W�q�ɽ��;C��W��YR��͵��,���/�,X�o�:�r����u�\0\0�ʕᕤ�|��cg�;���	aԣ��~�4�����~\Z�!|�{�?�\'��q��@1���V1����lٿ����Mf�Ί�m���1?9(���\'�q��㮿�z���\0PLTŦ�f����9I�����cJ�?>�?\\W-�1g����}������H����	68���VВ��;v����kUv��-�.[�w��5��v�	/��*t��=}|�N��͛5��\0�}��a���p۴_��W�,�?lv���_����h�h�O×������ߤ�U(&8Ƞ$ʓ���9�[BKi*���5n����_\r�\\�����)S��O�V�)��9a˖-�@�u�\0\0�J����\Z�l#�Tn�X��ʧ�z*�����4�g�c��š��BSi\0@1!4bCY�r0��\0(&�b���:\Z��;��0\0���	�a���0\0���	�6����`0\0�bB(&ZyC9�0~o���0\0���	\r�&�}0�\0@1��PL�Pu���h?��\0��PL��\Zʆ�u�K��\0PL��1��Ivޓė2?E��s���\0PL��1�P��>��:�Ʋ��i,1�\0@1!�HC})���Ch,�7��]��c�y�<\0\0�	��he\n5z�\Z��\Z�|������<`\0\0ńPL43����\Z����(�k,��P�l,�\nˡ��Ĺ��\0\0h*4�z���\\����Է����<Ԇ���o.�ŒC_�����\0\0h*4|����E�o�<�]P5�Ň�Pf9ƒ#Y���>��\0\0h*4|�]}��%G�P��X��\Z�#�{��Zύ��\0h����Ӊ�����˖�H��0\'�������{�m���,pj��b�:���[.�n�G�e0_cy$\re���1�4�z^�q����ޟ:��z��-m6\0@#6�ӧ;��n���,�����o;�I�[�.����s�	��b��X����?f��ə��D�$�K�}�8>�NI�7���89��I��D�$z$�3�^I����I���YI| �U���U�wa��EI|$��I��4$\'�$$��$&�L���L��IMbxW$1\"��%��$�N�I|1�k��oEU�U��uIܐ�O�$qS���W���I|5����_I|-�[��f��ķ��=�	I|\'��Iܕ�w��^�O��$~���N�$�$15�iI�{d��I�L��M�I�G�$�h�%1\'�_$17�Ǔx2���X���I<[T�u�WI�$�|K�x!�eI�6��I�H�$~���I����I�g�3�͚$ʒ���x3�uI�O��&�!�������R�u{&���;�m?����k��,�ʓO~o�ر}�=NS	\0��T��[\ZN8�c:1�޽,]�}��p�ǧ�7mZ.����}�v��3ߗn��^P<��=�W�ny���N\\֯��l]����#9�`��|\'����Ε+��u\n����1n�B�Ν��x�D�X�D,��Vɶ���u�׺Զ��>ױ,$��2>Ͼ$��DE{�ؕ��$�\'�����4��ؘi>b�Vk3�IlR�H��d���ĬJ�Ls���%�2�>�&�4��d���$-I�L���g����-�4YO$1/�|�&,�7+Ӝ�&�gI�$Ӽ�̼Gf�����8�e���2���e���$ޓ�ݙ�16�w$�I|;�dޖ�7��z�	���?\'1>Ӥ�f��Iܘibc3{}��irc�{m�2Mpl��J��L����d֡��f��$>���b�}Y�f��،4��\'Ӥ�f��EU�=�]��??��2M~l��N��̗\0�ˀ�\'�̗�˂nI�����z�2�Ϭ�u�Kq���.:\'����<�Y�ޏ�e7k��������>^y���c��޳����漑�o��q��|��/\n��\04�uD��:>>�erʔ�������W]5(�����ӟ�)�>Y�ܚYۤ�}�x?N���u�}&�w�-��g��QX������(����s��3f�~�sz���U6�5�ɚ\r����9��p\ZK�Tr��y��`d����qi��i���?�2�����Tl��SN9�18��ybCv(cl�1��\'���\ru�/�Z5�мa\0@SYG�o����V���}0�%����~m�~]�|����Vq����4~�e˯��<��j+j������������xMe�*�㖶��|�\\}�X�lH���Jc���zw��[X;�<�={ĨQ��=Oj��K�<.���t�-ʳU06aտ�;�1�����ܳq��������?�^�P�[s�0\0��<�������	6�kl]�$�5�Ǹ⊏犘��/��x�8���q7�ښʚcvy�����5��Bm[&�j,�j��e]ױ,$�\Z�:���erL=��:ǥxB���[+k�٧��#��~���\r]�J���u��_2^z��R3߼Q��P����k��\0pMe�0�[�M�fny��\r&�m��2�i0����~�M���s£�ޝ�9��y����8f��t�?<`y�������QL�\num)���,:��2�����_��9.�=F�Me��-./��+k���kQ��_����c\'O�9�\\տ4�����BC��\r\Z�/��n�������V�(4o4�|`\0��7��Oؓ=.&��nܢX}w�#m*��xVظl�]������ML_;���.�\rt��1�?��ق���k*�y��Xַ�<��2�z\ZJ�d��>w��x��:��܉nj>.�m�eӦ��\Zc�.��1�:�������f�D9��\r5�j�c�I�z��6�E5v��Z�Nԓo��T��hfw��EA�4���5�-F�bYsW�/�u(�5��k��c(9f�ש\0��E�&;~c�=P��Z��	th,����z�X������;ԓ���<\0\0�	ńb����+llG�eBj�ޑ^��\0PL(&h���ʷk-��\0���	��QoP�<`\0\0ńb��HwY��+�a\0@1!\Z�����<��y\0\0ńPLp@�8����\0\0(&�bBc����$Fx�0�\0\0�	�������qp-��YC�y@�\0PL���bi�W��<\0�bB(&8��RC�y@�\0PL���XN�Pb�\0B1����|]6�0\0���	�I���<\0�bB(&\0�0\0�bB1`0\0�bB1`0\0�bB(&\0�y�<\0�bB(&\0�0\0���	�< �\0��PL\0��\0��PL\0��\0���	�< �\0��۷ۿ{�2y3�]�~�f�TZ+�y\0\0Z���;ccI��&�f����T򑬰V��\0\0�\\4g�ěL�� ��H�L�V��\0\0��zu�׫W�����&���?�J>���J�<`\0��g�SWN�|�	�	cذ$�\"k#`0\0@Ktn��\'�,-�ibo�()y`^�lH��\"`0\0@K5�[�.�G��(...K��aVA�<`\0�_P�kw�_��+[[����dvuڠ�\0���\0�ɹmڼ�N���4v��~���?���e\rw��x��xv����3vu��\0h��$11�_\'�;� �8���������0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0����Yp[�S�7C\0\0\0\0IEND�B`�'),('109',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/Vault-Test-Workflow.sid-3806d577-ddd6-479c-85f6-11805b79addd.png','101','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0\0\0�\0\0\0�Ɍ\n\0\0\Z�IDATx���tTս��c��!m\"��Ҵ�J���x����Kk�Dӂ�\"SᲤ%��j�Zkڦ)�,!`�AP\"�-u!\\���`$�{�\'�\'�L2��$��~������d�$�7{��0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��)�`��&M�}}ذ�u]�$�^LW�:%���~�+�l�߿�H�4\0@�Z���[G�z:#��̝;M��I]��CY�:VT,���2p`߳}��z�|ɳ��\0\0Q���u�{������중s͟�II�^ۭ[���}\0��	|��Lc�~UY�JRSS>5߂\\�B\0@D�a�Y ,t\\h��5��`x\0�t���Y�a\Z��\'_d�\0D$=\ZB\'82g��G��̷$��\0q��I=\Z��㫰p�n�-)b�\0D]gA��Q�X2S���G�0&E�I����-O-(��A����ͷd7{%\0 ��L��������O�������Ef�{���,3��,�̠��:\r�[�a�\0D����c�e�6C¼1Z������=俯�2�~�hX\0��\r��.��%�K�WK~.�sU/�9����]\"Ӿ�K>�J�O`\0\0�{`xb�`�;�|��B����˾U��C~#\'�&kn˒��iA`\0\0�}`X}��rrדrt�j�}}�Y+��\'_[!�ܛG�O`\0\0�{`8��-�����(*g��Hݛk����Wyf�P��]���3�h�	\0�x\rg�향�o���-����dկ����ʳJOo�G���E��ӥRKh 0\0\0�/0����˥o���õ�����7E����U��\"s^ٱ�V�^��<�n�x>�M�O`\0\0�S`ز�E5�aY��g��}\"��rky�L�\\/�������q���٥fpXU/W�?$k����\'0\0\0�)0������;�ѥ���7���z��5fX�k��]z^�[X/����}�[�Ħ{��;�h�	\0�x\n���p��d��er�i�i�Ȅ���\'̰PR/7�E$�ȝϜ�ѓ�K�������4��X��K\0\0������\Z(�\'ϓo��O6��-���]�}V���w���.�#{��\'0Ă����\0�\0��8祖ӧ+d�¿I�\r�ep�j�t��ri�\Z��R.�������R��+�mi�	1`�Y�f�?syI\0h�	�r]=\r�\r\04��-]\0��grZ���i\0�T�Y��Zm��j��uf2k�Y��ہ�@`@0a�� \n��|���\'��Y���4��}��̰����ە�5����@`�������i{���W�T���\ZJͪ��@b+?E�6k�Yx�	�B{Fٟ�\r�N�+AT����ͪ��9���t{;�6����?-��=���{�\01Ŭ,�R���bg��}V�艨i��W�(u\\$;D�boo\n�����wZ�����\"U��A���qDo������w�ݸg�x��v㹧�����a!T�	�4 �{�\r�~�7�-�~�^�!�m�6�U!�Y���Pew�(Cl�x�Y7�?[��~L�D�Ѷ�=��R�ܦW�M�\'B����g����n����a!���/�_�%�\r��Gh@,)��YH\r�vS�z\ZB������/�^#>�&h�	�����=\ZHhp��&��gNbA��t�BV���e4�I�[֎x�ⴗ������5�\Z��B��ۗ��=s\ZK�Eaz�y��e�աtBbb;�H��SF���	�8۟����V�\0_�!а�\Z�@����>�{��ԨGO�����y����$5+Fv�M�$��@`����-�vK���a���|�%���h��v���|V�kV�\Z��/fn;�@��c8U�\'Y�Pbi��6��BCBÂw���w�n���|Ƈ*���@{/߬]\"���N��	�ݟ��\ro�}>@G�p]����z���^=>�{;���xu1�c��ev��!;������}���;v,��kc��x~ÆG$#��t� �}������K�\"%%w�|C����~��\Z�	ޡ�9\r����Z���0?��c���E�\ZsL�\r�ncڴqM.�2%_-�t������ɓ/>*+WY�;wN��*�*�@������˃Q�{CEY�w�KC���F�o���;{�B.���t��Y��vX���l�/|��u���2��oY\r�~�מw���SIzz��F�W�6lp����?��j10�__l�4�������(,��$\'w���0D������0�fq\'z�G5���������X�P�3~l=�ӣ0�/�g?��:}�\r���|���\r���������>����ڐ��[n�NydV���5���#���]�Q���.���v�ݞ����8���Ж��D��QT�a�(���Fê]�ֳ��۷o��X�]6�:��o\\������;�3$�o8����C:�⋿h�n������|��{������jr_�\'0D����O����u�DkBGI �����(	�aNs��B\ZܓuN�{��i\\�ኖ\Z�����Ǻl�˻2�	�-���\\�}_�\'0D��ܖO�-����!C\r�a@��E�:���N�3\r�-����l�|�����s�L\rY`x���7^����=ǂ�<����?6����	��O��.�������� �D�J����������C����60��=����m	����z���N�ܞ����4�v�@C��x�D���.	��׷U�G/�v��m�QPo����.�N�d�&����{x�f���,�m��Q�m�������v�I��#�\ZU�à���\"0 �����n�[�(���q�$bI��A�o����#�ݻ��7ì*#�/�i�!��3�t�`IfC<�����!h�����v� �dؽr�=�Рa�=�Q��W�8d�~Bb��݉q�S�P�=4��炣!����x�4�ux\"˫g!�C�\n��=T=\rC����;\r5���9��4^�AO�D���_o7�k�m�x;�뜆`�<t�B��E)�!r;��0⩧�ګ��\0��nG�����a�L�>+��KM;����f\rc �m�C�O���v#~�,�|��ۆ�%����\r�ˬ:��g?yV�!�0��	���ۿ`��}r�~�F|\rA` 0���?7L�b���zZ\"���U�n3\Z������??��Lf�#����Ї��+8�����r�0�����\0��z\Z�\0@`��&0��p@X\0\0�͆�b�\0hN��~p�$\0h�\0\0\0��@`\0�\0\0��@`\0�\0���\0\0��@`\0\0E` 0\0\0��\"0��\0\0\0�Ν����v�XG@�־���x�+	\0q\r�w��|\rvԺu�}�|Kv�W\0 �qٚ�s��`G@��x�|K��+	\0q��{KOO;������:������d�W\0 \"���kOq�L\Z�����ͷ�����\0\0�,39����h�;����3߃*���	\0�F��������a!!!����\0 jBCR҅�<����i��{���@`\0�h�٩���w�vt���}ӦG�X�!t�,術z4�=�Q�,0A`\0��6Ĭ�fm7����IW�(�����Nr4�\0\0\0\0\0�\0\0��\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0\0�\0\0\0\0\0�\0\0\0\0\0�\0\0\0\0\0�\0\0\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0�8���\\��e\0 �\r5��B`��e\0\0�̈́�\n�y�\0\0��fC>/\0\0p��ˠs�]\0\0\0�|�e`�\0\0����\0\0�ٮ����\0\0�q�2л\0\0\0�r��\0\0\0�E��˂w̚4)��a��u��Ҫ�T\0թSB}���Ǯ�bȆ����dO\0D�%K�u�ȡ�32��ܹӤ�|�����=T���cE�2)*�!�=ۧO��͗<��\0UJJ�Y׻wO).�)�N\Z�0��������ݺ%]��\0���0`�W�O�4��W���$55�S��#\0\0�N�!�g���q��kפj��	\0@��	�:gA�!h�;���f�HLL|�=\0��h��Ȝ���=�?0\Z�\0 �術z4��F̷����C�?B@k��p�.m]�^	\0�8�΂:�R`�3g��Y��:=t�Pٸ�T^ݹS���/Y�=��sV�8w������K�?4\Z�\0�Ȣ�2�_gA�ԩSM*//�~n۶M�{�}ILL���R�-[�X?O�8A`h�:\rF÷o\0q�i�D>l��^(�cǎY�o��F9j�>��ȤI7[������5��w�y����2X�\Z\0��A�{�ҹsg9~����ĉ��=}��u�ٳg��[�neH��\0\0����C%%%���O�ʲe�[����$??�:��C��3g�X�7l� .�U�V\0\0�x�k��ڵk�ر��i�\0������8 �<��>\0\0 NCͩ��p��G�-�ϟo�LOO�0�\r��\0\0 �z������1c�|.0�t�M�u��7�~���\0\0 ���[o�]�v����V��� <�@�o���(�����H`\0\0�K`x��d��{�Qu�[��:�ǎk��y\rk6���,z��)���\Z��\0\0�����oX\r�g�#է�b�>9uNd�֊ơ��J}���/�,7n������W�&\0\0\0���Ep�Avv�4@T�>\'���n3�dȐ!ү_?�v�\'OfH��\0\0����KC밄�Ő��&��/յgC�ǧ���o�������I` 0\0\0�(0�����)?�M�_�=������_ː�ʿ]:P�?ηU\0\0����ƞ���\n����0�/�j�M�\0 NE`\0\0��@`\0\0��@`\0l�fM7k�Y̪���:���֬Y��\0��@`@��7��`��5��\0����ؗaVi+B���n�\0^J\0b�x��y5�;@L1+ˬT��)fe��Y��\'���\0bO��A���qDo����0��\0\r5��ӳ�n��۽	m����\n\r�4\0\Zj��� ��!t�Aj����2���D&/5@`��^e^=�!�n�WO�v^j��@��s�B��,��PC\0��\"0 �{�����e\0@CM`@���-�&����c�o�F=z�}�eo@`����M�48d���t��և���v=�,���@Y���R=�F� ��g|;�\0���6w���.�\n���t=��=\0�����{���`�Y�.�0Q�+���س�/��o��t�$�����z����\n��Z���0?��c���\0�	_�E�ڵsP�AO��K2f�p���E�C֐�k��m���z9~�o2mڸ��wKᇢZY�b\0Z��\'��O.���dd��>��u~�I�\rl}ڟ4i����\'�k����ر�:��G��;z�̺^��䒯X��y����_b�?y�E�8q�հ��&Lȱ.��jj�Ia�$9�{��F���)A*+W5^��y�Àv�a�m4|gD�Ѱ�\"=\0\"?0l���l���uZ������o���d߾>T\rzYjj��Ǻ�Q�����~j\0	�y�?��qxB�k@����[���2��[���/]zO�\r���i|��o����	��`����Z?��ym��o�u�b���X�mn{ޟ��|k�<��3T�lK��&����k�1/+�S��Č?&0��C�Ѱ�c���p����\ZZ�v7��u:4��0�Aw\Z�Ç7��v�z��[o=e����[�=��mН�\'pF�z����c�^m�s(-�c���W�IC��\ZY��A�E�uÇ�ο�����M\ZU���j��y\Z6���#/X��9z~Μ�m���=z�3�B�#�w��z�^WP���;6�ڣ��S_�����{?o!(�`A`@+��#���i�=Y��k�mMht7�˗ϵ��9�k�L:�I�z��ڵ�m�X����V��\r�N$��.�e��Cg���3pMrd�n���Ɇ\r�4^�����mi�i���)S������P�����\\y�Z�A/�~�t\0�K@���\\[������m�\0\"708��i�	��}��C,�P�J�ޅD^n��@�2�:�հ\Z4,��;t͇L^j��@���G�WOC[�\'��z�\0@` 0 �x�=�!{x��<�mh��K�������i��j�5\0����8�:�i�g��t�ig���\0����ؔa4�Ж�n0g\0�����0��V�U�h\0⌮\0y�Ѱ��!�_�6�O�\")��e��#\0�\0\0�\0\0�\0\0E`\0\0(\0��@\0\0��\0\0\0\r5�\0\0�\0\0�\0\0�\0\0E`\0\0(\0��@\0\0��\0\0��s����4�P����k�%�J\0@�4�ߑ��E4�P����y�-��^	\0�8#F\\�f��i4�Pyy#�4ߒ\"�J\0@�IO�9,==�ǳ�F�c�PBB�[�[��^	\0�H}���S\\<�F�+7w�b�(co\0D����NWT,���*/_��|���bW\0D�Qii=>&4�XHHH8h����\0��	\rII~����O0�!�s�a�*�\0 \Zev�t���ݻ�:��ߴ��*�i�:z�\raOp�9C\0\0���暵ݬ:�aB*��E�t�=t��!\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��!�]�]�\0\0\0\0IEND�B`�'),('11',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/MLR Review - V2.startMlrReview.png','1','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0�\0\0e\0\0\0��]�\0\0C�IDATx�����e�7�i@.J��X���X�-1*⒔i��H���N�ǲC�AQ��x��T�yT�/���#�yA��p����܆d``F�y��g�vf�s�������[�ٳ��޿�������\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�\\���n��o=zث���Yܮ]��V�V�Z����s�y��]ػ��u\Z\0�y�<Ɵ�c��h��{�u��իW�0y��!/�P\\�\"��Zb��1?v�2e|����=z��j�.?K�`���c�)�ř9��ݻw	S�N��+5�a�i�~:u���c�\\��\0��[�1���ע����>�$My�j͚9�:}�z��B\0���y�?e�5{����M�x�߾}��O�`���c�)�9�/4�ǂǧ�5a�Ք)㷵n��i	�u�:�񧌿f+��*��ر��_�;w|/��Е\0X����k��)��Y��?ٍ3&sJ�q�ƅ�i��Ըq#W���)�\0�u���3�����;�ɵ5��I�¼y������-/�\\�u�\\��c�%���D�d=���O݇�t%\0�y�<Ɵ�g�5K��]������*��i^^^���>�}w}hݺuX�xqrݲe˒�mۦ���{R�_���:o���3���檆#�\r6$���SKÖ-[�˗_~y؜�\\RZ\ZF��*�n�����L.����\Z� �|�\0�u���3�Z^�G��rKh۶mغu�_L�m۞�K��￟\\�w�����˗{�^�`�W�y�?�O�W��T�̙33O��ڵ;̞}_ry���aĈ��;��yسgOry�aƌaΜ9\ZY�`�W�y���3�4~�!����-[�&����n����u�օ�\n*}�F��\0X�u��3��?��yv��4y�xȐ!a߾}����Q�F�7�~�c�5���:���Ɵ�g�i����׿���;t��\Z��+�H>WX�#s��\'jd��u^Y�?������_||�uׅ�^Zv&L|1r���ߞ9��o�M�ƍ��X�`�W�y���3�4�G���Q�5y�pg����;+�8����K/M.������Ϯ��.]���_~Y#k|\0���:o�Ɵ��_���z�@|��hSᮐ���PT�����\n\nBYY�?�|X�hQعs�q�s����\Z\0뼲�Ɵ��?z�>�n�$MBᮒ���8���/\n}��\r��rJr�k���S�\Z\0뼲�Ɵ��+7~QQQ��}|���]��7և��{3�aGў���kC�nݒ����ٲe����\0X�u��3��?�����g��Y�����_����i��\'O��\\W>��>a�֭�2�����+��g�\Z?K�?����������]�Z�lY())��\Z\0뼲�c�\Z_i|\0���:��g���5>\0X������J�`���c�)�O�+��u^Y�1��?4���:����\Z_��u�:��?��4>\0�y�<Ɵ2�4���\0X�u���C�k|\0���:��g���5>\0X������J�`�W�y�O\Z_i|\0���:��g���5>\0X������\Z_�`���c�)�O�+��u^Y�1��?��4>\0�ye���3�h��_�����s?ڶm�8���#w6�{�\Z(���	�z��Z�f�o�ߥ!����:o���3�����\'��=y\0�����֭�����M��?��B��mk�M׮����}��G�gj|\0���y��g�G}�w��!�Ě5s*]�y���|>��˩�~\"�X1�R3O�~c�޽K��Ǐ�|����d����p�1����Ckl���Ol�+��4[���F\rN����_W��V���竻�Νφq㾕�߳}m�9����u��\Z\0�u�:o�Ɵ�O�}�M�<�?������]6(�������������dp�˃�O>7r䗓�|����kթ�}��������T������Ҕ����%�gͺ9���}N8�S����:o���Ɵ�w�7~��K�y\n��o\'�U��B���&�	����{������v�>�j�������,_~w8��Ӓ�e�����:o���c�������n�ts��s���.�Q��O�ݐ�_�q�6,��\0X���y�?��6~�KJ<V��7�%D�>\'\'��c��Ǔ&��s��~������~��k���Ǖǧ�/�mr9^?w�q퓏7mz�̘�����+�&�����\n��}S�Ϭ�e��u�:��g�-��~���T��\"^�/�\0H�\08�]�)�ښ,��9~�x\Z���yЍvl���c���3U͘��\rl��f����FO_�xf���=?s���~���:o���Ɵ��7��:����\Z_��u�:��?��4>\0�y�<Ɵ2�4���\0X�u���C�k|\0���:��g���5>\0X������J�`���c�)�O�+��u^Y�1��?4���:����\Z_��u�:��?��4>\0�ye�7������:����\Z_����:��?����:o���SƟ�W\Z\0뼲�c�\Z_i|\0���:��g���5>\0X������J�`���c�)�O�+��u^Y�1��?4���:����\Z_��u�:��?��4>\0�y�<Ɵ2�4���\0X�u���C�k|\0���:��g�q�mۦ��x��k�{�K=$��\0�u�O��g��)/�.��j��_>�zHV�J\0���y�?e�5K\\pμɓ��xM������C2EW`���c�)�Y�ٳK��=������|�[����M=$t%\0�y�<Ɵ2���=N\\=u��׈5l�w����\0�u�O���;�+?�&l��˻{A�1ؘ���\"\0�y�<Ɵ2�Z�A]�vޡ��|�������aZ\0�u�O-��۴9�w��m�?�ǂ�?]�Q�`���c�)㯥:�U��-�����c�~�/O>���޿���\'�\"9�Ū����XpO�`���c�)��뛪ɩz.Uũ\n�+��k|��x�dg��:o���3��?�������\0���+�������;\0�:p���|���*w\0X��F������.g�\0�<`�A�=�s�|�_W#\0�y���:��W�t�p�\0�u0����T��|w\0X��ꢦ��8F\0���u�xN�o��LW\0`��?8(�]\0\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0`��?��\0�u�?��\0�u�?��\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0`��?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0@�f�k7}��7�=�����,n׮M(_G�!T�V�e�{��r�y}��}�@��}6h|\0h�����췫W�a���C^�]��xEa�:Ċ�c~��0e��Ч��{{�8���]~���>4>\0�83g޼�{�.a��	��t�Px�kڴ�N�:�>��6��>�A�@�\n������6���՚5s�	\'t� �Ӆ�g���f/��./d�oߦ0���g���f�8��v�5��Xa��jʔ��Z�n����>4>\04[�l��>^s��չs��R�\0]�}6h|\0h��[�ĳ�f=ٍ3&���ƍ�\Z�S�ƍ\\��O��J�A�@���2�ImsҤIa޼y��~���E��W�ݺuK�{�ǒ�YRR\"`d=���O݇�t%�٠��Yj׮M\r�sBQQQ���Xyyyɿ�>�lx����u��a����u˖-K�ݶm��y���Ju%�٠��ٮ�Շ�6lؐ�Ƨ�Z\Z�lْ\\����������0z�U�u�׿>�`gr����0��W���\0-6`F��rKh۶mغu�3�۶m�<�����\'��ݻ7�x����0���\0f劇�Μ93sX�]�����%��F��\\��Ο�={�$�.\\f̘�̙#0\n��g��\0�R�Ij���a˖���x��n�-�nݺu�킂J_#0\n��g��\03vv�΄���!C¾}�2��ˣF�*���_�)0\n��g��\0�g0���7��z@���+�����~�ĉ���}6h|\00+����/��*�(,LP<�O���ߞ9s�o�M�ƍ��G��>4>\0�ջ�F�?;�~��$2�s���x��K/�4�_���=3w%��v��%�������}6h|\00��믿��Sm*��W��JBX�<?s(lAAA(+����-\n;w���ܹsF�l��\0 `~t�l|-e:L0 	�1p�*	;������зo�p�)�$���k\"+`b�\r\Z\0�����(9L6�f׮]Û�C�й�hOx����[�n����`nٲE�0���\0�\'�I�-6>�y�w�ο�K��iӦ�O��+��� |��}�֭[Ce���}6h|\00��\'�x\"��e~~~(,?�l�e˖���S��>4>\0�J��l��\0 `\n�`�\r\Z\0L%`b�\r\Z\0L%`�}6\Z\00L���\0��	�٠�@�T&�٠�@�T&�g��\0S��l��\0 `\n�`�\r\Z\0L%`b�\r\Z\0L%`�}6h|\00L���\0��i��}6h|\00���}6h|\00��	�٠������3<�诓��O�1s]Ò�(`b�\r\Z\0����cZ��㿝||�C�D�<��\n�۷m��	�`�\r\Z\0�@�0��p�9}���������m۶	_���Ê���ի�.����\n�7/Mn���S?����u����=N�Z�f�W����N_��w��v�Bp����v�|6���бc���+�g���0G�������s;�Ն�\Z��\'����v�]���Y����%�+(x4���3���tHܾ��:=;Z��P������u��_Kn7k�͞��l4>\0P��9s�Mɿ�F\r�<r�!m����g��J�������=�3Y�cN�g����/�\\����MOUk��0��YՅ�}�^0�>�\0�\'`.]�����Z��,fՐv�Iݓ�7lX�5��g/��^;‐V\\��Ɵ�M������bm�C]fu?����5�`���\0�0�����l!-^�S�p�X�P؜*����#G~��ɂ&M\Z[�ϟ:uB�{w\\����r�.�Lcu�C��\r\Z�?�x���a̘�~��~V�J��ſ���ݚB���}6h|\08jfmg��?���k���k,�=?s����Kj{�%_�7/K��x�lu����ē�<����g����Ivj���n��G��?~l�޽K(s�\"��ge;�O|66jk�&�٠�@�Tަ�A���)`�}6h|\00L���\0�0���\0�0�>4>\0�&�g��\0S	��g��\0S	�`���\0L�A���)`�}6h|\00���}6h|\00��	�٠�@�0�>4>\0�&�g��\0S	��g��\0S	�`�\r\Z\0L�A���)`jI�A����L�A����L���\0��	�٠�@�T&�٠�@�T&�g��\0S��l��\0��ڶmSV\\�B�k�{�K=$���l��\0�,�q�)�����@-X��\'R�*]�}6h|\0h�.���y�\'_/�5�\Z>��S�]�}6h|\0h�z��ҿgϮ%��+��ƭwrssצ���l��\0�l��q��S\'y�XÆ]pw�X����\0���;�+?�����w���c�1UgiE�A�@K0�k��;��#.sssR��0-�}6h|\0hQ!�M�c�q����5���5���n.���\0-��Z}lI��n;�y���m�>�\r�>��H��b�O�_s�X�A�@��7U�S�\\����QuhU���}.�[�8[,�٠��&���u����A�\0+>XZ��vw�}6h|\0�`�����U����\0�~9={�.gd�l��\0@�=�s��tⳘ^�	�٠��:���e�F�{�>4>\0PW�s�;�|w�g��\0ꢦg/��A�\0u�x-��e�>4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0�@��u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>���B^���o�a��a���fq�vmB����Z��-�ݻ���뻰w��4�<����`�Z�{�麁���իG�<����wW(.^BX������?;L�2>��s��=N|5u�����`�7烅hqfμyA��]�ԩBi�J��\\Ӧ�(t��a��Ƕ�D�Y���o�Т6\Z����䯭6G�֬�N8����`�.�΃9ߜ�ً�Hſb�h4ކ�}�6�9��΃9ߜ�YOT!�]|�M<D���x5e��m�[�~ZGZ���o��l�3Ɠ;x�M�W���K=$t�u���|��\0�R<-}<s�ž�kܸ��R�]i��ƙ�3fL�7ƍ�g7�c��z�=��i�� .�9!�vD���v\n��1�������l\Z��y��U��:�3�dҤIa޼y��~���E��W�ݺuK�{�ǒ�YRR\"`���\0|$��v]�����~~I�p����TM����!Ä/tLo��\0zHJu�u\Zg����(�le������g�}6����кu�x���e˖%�n۶M�4�c��h���b8ch�0;,�\n�?\Z��p���v����ah��vX����6lؐ�CO=�4lٲ%�|�嗇ͩ�%��a�諒�֯/|������o�-`���\0�?`�����?\r�\"�8���\\tb�0�ÿ~��p�N��h�`�a��f0�[n�%�m�6lݺ���m۶g��|�������ݛ|�|�r�Ț��\0\\����3C��?^����_����Gxg����w^�V(�笄6�yh�s~<�u�̙��bw��fϾ/�<|��0bĈ��w�<�ٳ\'��p��0cƌ0g�s�9@���[.	�_z0l^=7�~��T=�\\���C�Ϸ�Y�ٰ�C3��s*��r���a˖���x��n�-�nݺu�킂J_c7�c��s�,)]~8�W�q��3C������?��/��?�{^�a�ٰ�C3�;�vg�d�xȐ!a߾}����Q�F���ү�4����\0�)`�-Yf=�,��,���(������?�T����=��t�ß��B�͆u�m�L�_��f��СC�W\\qE�����O�8�n���P{�,+[X�N�|��\r��B��Fׯ��!\\�(�Iτ�����䯾�Y�$�~����f�:�n����/��*�(,L�xҟ��o�=s��7�X�Ѝ7:ɏ9@�沕O�A��E������0.�,\\�W�]RF?Z��pY��}���ũ�9�,\\<��0��66�yhfs���F�?;�~��$2�s���x��K/�4�_���=3w%��v��%�����ps>���+7��������z<�I��k��������\n��Y.��/|mFY��B�u결�ɛË7�e�`�a��f6������a�M��B���BQIK��g�-((ee!<���aѢEa������;w�9ܜ������Z*`n��0r��p��]�y!�z�,|��T��Y.�c#f���?�	CF?\n�M/��L�\r�<4�9?$��L��$�2��]%aGQqr��_���N9��v�\\s�Cd��Xx\0�07����>�\r�������,)WW���i_}4��wo\n���ٰ�{ll6Z����`w-)`%������ڵkx�`}(ܽ7:w�	���6t��-�}|�-[���|L���X�J�î]�a��\n.{ �9ln���?�O���p8g��U��$�����6\r6-ܠT�jj����%���Y�l���k���p�E_J.O�6-|�^��u�S���n�\Z*3���0�w��.ӡ2>��EȤ%�\'�x\"��e~~~(,?�l�e˖�������=S��T�MպT�,��ũz\'U�SuC��0L�\r\Z\'\\�v=��U���#R��<Tޓ�o���Tu(�|��W��5��vy���80l6l68��\"ӟ��L������\Z�jM��պ��ݪT=��ӌ�f�f��.k���Ĝ�\Z}����9�ym}��{��ǳ�&�͆�\rgPN��������o�o0�[�5�\Z���-�~�\Z7&�͆�\r.����21�#9�,��\Z�az�_�d\n�\06&G>\\6�׃9ߜ_�gp���o��?��0l6Lꭶ����|s~��bo8̍_����#`�l�T(l��[=��9_�9?�I<[l�#�<��8TV���0�oL�ˏ������9��|u����G�/Xq2�,��	`�!`R�p9�BH����!���21���{��?O�#�@�r\Z�DB��͆0[r���*,B=Bf�p���>�5���U����u�3�pM�9���D�l6���\\U���ŚBf����r{��Ĝ�\ZlΟ��o�����Ƒ�	���2η�0�z��rp=Bc��Y�pY5d:\\s�9����x��iG��z��=�M���9(�o:�l�z��B^m!�����ߒ�L����C�3U�p�şWl�	��Q��H���ݶ��GK�W\r�u\rwՅ�܃�i^��9ߜ�Ao\n�֐��	4��Fſn�o�!`��~0Ღ���!��C�}��o�oq�`Z\0�G@��lT�����l6�n��p�1�C��mî]���ͭ���l!�P�eՐ�5�4��������:�x��3��q�2�{\r��$`�h�pX���ԩB�\'&_3cƿ������)	�Mh��_M=�1\Z�����[�������������㿝||�C�D���<ݔ�sg���G@K�7��\\�*_���\\���OK��>�>��\'s���ߚ�V�r+m.��~���+_�|h۶M8��O�+f���Z۷?�l���\"~�Q�\'�e���?�����ڹ��0nܷBǎ*]_���f���L�^�������̺�Ofu<�I�����8;3���k�?�\\��ë�n��?��.�lx�������V��>�<��w��v�Bp]ז���)<	���0�n2r�0_|�d�/_|�����+VZ����ꮿ�A���&����A}��_���OM*^��նɨ�����Z��Y7����Mjc]]����Μ��\Z̃	�^�I�����G�������s;�Ն�\Z��\'��逹���&&�\'������U�|0�Cm�um�nMh\ns~�T�S�dw���r{	O&pT��T��a���\"x��#�LN.?��������R���޴���Tw}տ2Ǐ��T�1ٷ磻*~��̊ߧ��_�l��z�j���3�սI����\'d:�,�1j��fμ)��G��9r��#Z⡴�����=�3Y�Ϫ�~ժn����P��y0kKS|���#8����9�I���y��gq�0�	}�a�U6��u��+���^x�p�luק���Uq����K�L��`f}���٨����Bfm����粶�ɬN��I㌃Z��K!���Ŭ:G�tR���\re�K㳗��������l/u�6���w�K�����TO����&��<���J�I���\Z0�f!}tM�xuf#O���w\\�J\'�v}���&�=�$B��>U+��S~��ſ��?7hP������1c�W�d��/�����|�u���mpr��zk�U�3���˜����tGxhjs~��޽�5��.���᮱�stN�Cd��#�\\㼟�dr�����tu�C���4��um�nMh�y���������ȴ�z��s��\0����$`��ʬY3��u���Ϝ\"f��O���x�S���_��W�����]��/�1���Ɗ�ӯ�Y������w�l**��xF����\'t�y���mm�_�l��{&��Ჾ!3��K�l�������2t��9��=?s����Kj{�%_�7/�:�g;D6�|_u���w�z������-խ	M)`�J�Ɯ��Cs8�-���O&`�h����f�ɬz��U9���l!st���5�4逩��?2g�	z5p��*��W��4>�G����2+���bX�K�̩��	�:�s���0�P�d�-�~?�L��a�a�A�U=\\6���9�둪_w��	�|s~��g2�!�5���z���ˍ�Ux0�͆�\r2U��oA�T�u���K��_��$��&����#ݜƧ�0��)d��\n�|s~�|�<h��\\g��?��i��P���a4��)�ZW,����=��G���̣š��XL�d�����~�文����}/U���?����d��7l6l68�!���@:��2�O�#@�T6T��ۃ9ߜ���>Le�!d�_�v����ʜ/<��\0S�l�����8���o.0�9_xr�E�f����L��\"`*s���>�66r�.0�9_xr�\r�\r�\r9dN.0�9_xr�\r�\r�\r�����[� `*s���>�66�N�Le���G�y�f�f0�9�&�}�7l6l6\0���|� ����l6\0S��\'� `*�\r@�T�|��}�7l6l6\0���|� �`ްٰ�\00��m�p���\0Le���G���l6\0S��\'�`ްٰ�\00��m��G\0�1o�mۦ��x���	���/�-���\Z�9ߜo��h���g��)/�.�}�~�D�!Ye\0�|s�M�#�Y�\\pμɓ���7�\Z>��S��\00��m�p�r��ٳK��=�������7n�����6��0\0s�9�&��l�=N\\=u�~#ְaܝz(�\Z�9ߜo��h����;�+?���*/����`c��2\0s�9�&���A]�v�a�q�7\Z����������|� �G@K�7�is�?��{ۼ>�������6\Z�9ߜ���>Z�qz�V[ҡñ�ǎ��_�|�w�gZý�Y<-}<s`����o\"����O�#���}S59Uϥ���wS�V�\r��{����;s Д����6A�#��\0`�>0o\0\0���\0�\0�M��0o\0\0���\0�\0�M����\0`�>�\0\06A�#��\0`�>�\0\06A��\0�\0�M��0o\0\0���\0�\0�M����\0��>�\0\06A�#��\0`�>�\0\06A�#\0�\0�M��0o\0\0���\0�\0�M��0o\0\0���\0�\0\06A�#��\0`�>�\0\06A�#��\0`��0o\0\0���\0�\0�M��0o\0\0���\0�\0�M�#��\0`�>�\0\06A�#��\0`�>0o\0\04�&(��vӧ�x����^�����v�ڄ��AB�j�[ֻw�-��wa��\'�� `\0��Mн��t����v���#L�|}�˻+�!�V�X�~�ϟ�L��9yo�\'������� `\0��M�̙7/�޽K�:uB(-])�6�G�S���=��%ZL\0��	����>�<�&��Z�fN8�N��a�L\0�f�	����g.�����۷)�q�,�\0\0�yO�_s��\Z��L��u��Oku0\0��&(�-6���k.�:w��^�!��A�\0h����V$�l��COvcƌɼ�Ƹq�B����Ըq#W���)�L\0�f�	��sߊ���9iҤ0o޼�r�~�¢E�Ë+W�nݺ%�=��cI�,))0��y���U�L\0�f�	j׮M\r�sBQQQ���Xyyyɿ�>�lx����u��a����u˖-K�ݶm��y���J�;�\0\0�uTC�	aÆ\rIh|ꩥa˖-���/�<lN].)-\r�G_�\\�~�{�v&��~�m� ˆL\0�0�[n�%�m�6lݺ���m۶g��|�������ݛ|�|�r��\n�`n\00+W<�u�̙��bw��fϾ/�<|��0bĈ��w�<�ٳ\'��p��0cƌ0g��Q�\0@��v��?~زekr9��\'���nK�[�n]x������&�\0\0f&��,ڝ	���!C��}��e���G�\ZU(�K�S`0A�\00x�}3�w�С�+��\"�\\a���\'N�(0\n� `\0��O�s�uׅ�^Zv&(��\'���oϜ9��7�&t�ƍN�#`��	\0 `~T��nU�����;��xϽ����	~.����r|����ܕBۥK����/�&�\0\0��z��ד��a�M��B���BQIK��g�-((ee!<���aѢEa������;w��(`��	\0 `~t�l|-e:L0 	�1p�*	;������зo�p�)�$���k\"+`��	\0 `V�EEE�a��0�v�\Z�,X\nw�̈́�E{«��\rݺuKn�s˖-��	&\0��y�I~�g���`^����/�Rryڴi�\'�\no�+��t��u��P��(`��	\0 `f	�O<�D�����PX~V�X˖-%%%��	�V\0\0S	�`n\0�	0L@�\00��	&\0���L0�\0�	�&`�\06A��	�\0\0�0A�\00��	�V\0\0� S�L\0\0S�L\0\0S	� `\0�J�s+\0�MP���:_�0\0�P7A���\0S�L\0�C\r��Z���M�N�s+\0�MP�f�g4�0L�x\0l�5`V|Fs@m3}�G�u����7f��cXL0\0��&�Um��cZ��㿝||�C�D�<��\n�۷m��5��ٔ\nL\0\0�\r�R5,U�ӷ�-�pv8�>��_����c��mۄ�|��aŊYY���]����k�͛�&��_s꩟�|Mպ��[C�\'�V�r3�+~MՀ��w��v�Bp����v�|6���бc��Ml�k�	\0@րY5X��5`�=4	]o���0v�7�\rU14��O8������&&�\'����e�\rJ>WP�h�o��gf��鐸}��uzv��ߡ����YU�꫿��n֬�[�!�6��\0\0T\n���\Z�%X�9`ΜyS��Q��#��y@H[���p�٧%���dy�0�s��L���S��l?��?�|>>��i�S���~����~Vu�w߾�L0\0�\n�=cYt���k㳘UC�I\'uO>ްaQ�p����^{�BZq�\Z~|6���%�����u	������Qp��oL\0\0���-��ݛ�	��BZ�>��᮱⡰9U���G��r��M�4�Ɵ?u���:������x]����~���4�����Ø1�+���������ŋ[�a�5�\\�8\0�Q0k;;����]�vN^c9t�����T�]�P�K.�BؼyYr�m�Ce�;D6�&�|����\\?cƿgN�S��P�vk�>>��cC��]�@�S��l?+�I~⳱�P[[�0A�\00���>�6��\0\0���7�q\0\0S���=\0 `*L\0\0S	�6��\0\0���7�q\0\0S	���\0L%`��	\0��)`�|�\00L�o��\0\0�0A�\00��	&\0\0��i�\rz\0@�0m�A�\0�J�\0@�T��7&\0\0��i�\rz\0@�0L��\0\0�0A�\00;��\0\0S���=\0 `*�8\0���L0\00L�o��\0\0\r�m�6e��+��&P�w���CRj�\rz\0�Y:�S6���%�5�Z���O��U6ߠ�\0��.8g����xM������C2���8\0@�Գg��={v-)-])�5n�����6����=\0�l��q��S\'y�XÆ]pw�Xj�\rz\0��;�c��v����\Z����^�z6��,�o��\0\0-���];�2�|����-H���l�A�\0���٦�1����m��������b76�pi�	\0�!;�U��-�����c�~�/O>����\'����2�I<[l�	}�k.�j½`�	\0@�蛪ɩz.U��1uhU���}.�[�h=`�	\0\0�|�\0\0�o��\0\0`�\rz\0\0��F�\0\06ߠ�\0���8\0\0�|�\0\0l�A�\0��7�q\0\0��=\0\06ߠ�\0\0�o��\0\0`�\rz\0\0l�A�\0\06��q\0\0���8\0\0�|�\0\0�o��\0\0��7z\0\0��=\0\06ߠ�\0���8\0\0`�\rz\0\0l�A�\0��7�q\0\0��=\0\0�|�\0\0�o��\0\0`�\rz\0\0��F�\0\06ߠ�\0���8\0\0�|�\0\0l���\0\0��7�q\0\0��=\0\06ߠ�\0\0�o��\0\0`�\rz\0\0l�A�\0��7�q\0\0���8\0\0�|����vӧ�x����^�����v�ڄ��[B�j�[ֻw�-��wa��\'��\0\0@��|�{�M�\r�oW�^=���ׇ���Bq��ju������aʔ�O�����q⫩��,=\0\0����̙7/�޽K�:uB(-])�6�G�S���=��%z\0\0h1��.O;��3l�ߑ�5k�N��A�!��\0�f������g.�����۷)�i9��\n�\0\0p4n��	}�k.�a��^�Ք)㷵n��i=\0\04��w<[l<���\\6~u����C2@�\0\0�r�ߊ$�-V�k�\Z7n��C2E�\0\0�r���2�I]CТ��oG��wh����í_��>N@l�z晻��zHV�q\0\0�Yn�۵kS���|���_�9�3�s�+U�.=>�lH�0�����\r�>����T�\0\0�u�]�\04ch�0;,�\n�?\Z��p���v����ZH80\0@���~va��䌟�\r�^����.:1L���_�tj��\'�K_L\0\0������ś�\n�xaF�K^��ᝅ?��y9���Y�l�3�\n�&\0\0�u���\\���`ؼzn���C�z8����o. \n�&\0\0�����Uᇓ\Z�/�Y=3�1?�ݯ�g��\r��~��P��%!Q�0\0@������\n��Xz^�X_���k��O*^~�w��_~:��O��n!S�0\0@��Vee��K���Þ��awx`C����!\\�<���0�V��;����=���W	�&\0\0 `V�e+�����0���^a\\~Y�.�,\\��,�~�,|���������SAsNY�xĝa��ǅE\0\00+�+7��������z<�I��k��������\n��Y.��/|mFY��B�u결�ɛË7�%,\n�\0\0��Y�^K��O�\ZF��n[�+\\1/�Q��oݟ\n�3��ea��~��=a��B����),\n�\0\0��Y�6����>�k~>7&/|gIY��B}��N���߾{Sx��φM��#,\n�\0\0��y`�+��v凉3�+��p氹��C�>=x^8c����W�ǒ����\n��a\0\0�0L\0\0��0L=\0\0���L\0\0@�T��	\0\06���)`\0�ͷ�)`�q\0\0@�T&\0\0 `*S�\0\0�oS�0\0@�0L=\0\0�J�\0\0L%`\n�\0\0`�-`\n�z\0\0��4U����	\0\0fC��P4���	\0\0��t-�����`\'`\n�\0\0`�]��Y���LS�\0\0��C\r���P]��x�c�i��99,^�ۆOM9�	�&\0\04��w����ҥ�O.��qbK���-�\0\0�� ȮJհT�N߮���o�K��m�$o޼4|�+�O>>��O�+f%ןsN��v���`����L���=���ڷo���O�E�k��W��k_��\Z�W���~��C��]��?~lX�fN����п��ɳ��G�Sؾ��[�ߡW������y��гgW�`\0\0GE��\Z,s�\Z0�M�Qry��/\'_v٠�もG�cP���v�w����k��\'N�:��W?��=�����ە��y���������1���\Zo{�	�*�������\r�|.�_������S�\\��w��g�I.�{�g*}&:D\0\0h�3?U#��:�t�gcH����V�\\���u��\'�S\'%�7mz*k���{����&ץCh�k��\\��w]���p�٧%�FV�UCcŏ�ϒ�ggk;D6���?�pZ+`\0\0MQu�X�+`�CS�<�����O��R8����u�����fμ)\\|��\rj5}��!�]�����g���kYדN��fÆEu��0Z��`�d9\\8}���	\0\0Pǀ/�]�H�_r���3��+�$��4i�_7e���	�fͺ���Y��HjC�W�znr9�~2~�l�k����ᶱv�|�ƀy��}���?orHo]�o�?|G�Å�}]u!�.aT�\0\0Zt������`�}��aԨ��sM��o{<si�g\'+޶��_��nƌ�\\��.��:�/C׮��g>�=?�j���,�?�\'�\'�9�S���K�|�����_/\Z�-0\0\0�0U㼅��	\0\0�-�ⳖOB$`\0\0����R\0\0\0S	�\0\0���L\0\0\0S�\0\00��	\0\0�J�\0\00L\0\0\0S�\0\00��	\0\0�J�\0\00L\0\0\0S�\0\00��	\0\0�J�\0\00L\0\0\0��*)y1��֟�]��L\0\0\03S1,��zU�:\0\0@��&\0\0���L\0\0@�T&\0\0��)`\0\0�^�)`\0\0�S۶mʊ�W8�l�ݻ_�[��-Օ\0\0@�t��l�˻�3�M�,����d��\0\0��.8g����{�c���/x0��Lѕ\0\0@�Գg��={v-)-])�5n�����6��Е\0\0@�գǉ��N� �5b\rv�ݩ�b�n\0\0���;v<nW~�l��l��˻{A�1ؘ���\"\0\0��ڵ�2[�Ydc����-H��ô \0\0ТBf�6���;���)�&��=��N�a��K\0\0��:�U��-�����c�~�/O>�����}2���\\Ʒ\"�g�-?�O|ͥ�b\0��o�&��T�ġ��*���>��H�-\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�����Ny��\0\0\0\0IEND�B`�'),('110',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/PrepareForReview.sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa.png','101','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0�\0\0\0_\0\0\0�\r��\0\0�IDATx���\rp���m����@�t��H(��e�Q���FL$V��R�BSIQ\Z���JZQRM1\"Ey\Z1� WK!@����8�C���b5%\'���}6�99	���K���gr�������������0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�G��kѢ;*�L)|s���^��D\'S�U�)�!C�]|�ȍC�ˑ\0\0���O�s�ر��ɒ��2���߿CD�P�n���eRU5S�\r|4+k���&��\0\0q��v��A�Ζ��Y�\"�E�.�]��Mo��\'mG\0\0�y:��V�AͻjiY%��������\0\0 &�kX[	����{�}b�e\0\0��7��5��5L0�]UU��855u+G$\0\0��5�7�p�`�+33�}s��sT\0\0���1z�0a,�U^^�d�*�J\0\0�GP���j`y�v�<V�%���,(ȐyW���\\��^{���.i�\0\0����:��gf�2eII�,6k�5�d���uY�,_4�@��8��.	pT\0\0/u9��\\�!��������\'�/ ��f�//=�@�B�O+\0\0��08��t�Ts�|P�@v��&���\0�5�����I�e���Na\0\0$k|�l��?|G>�^#�jxD���W�o|H�]�ܚ\'�cܑL\0\0IW�?A�~A�Y-mo�0k�����+d��E�9� \0\0H�0�E�IfW>,+�/���Ԋ����jk~^�U\\$�����?�M�#\0�d�G�h��/o��-��?�+��JV�Ud����v�d�d�<��^���A\0\0�<a0�#�7�dp�����d�\"��)�%2�/\"7�\"r�k\";����+�˒��%�e��0\0\0�!nٵU�M\\ �w~)O�)o�;�L��)�r�ʠL|��Yo��UA�a��Җ:�a\0\0$Cl��9X7G_Z\'�f����ȍ\n�/֘A���\\���\\U�=,2�z�|�i��#�`G\0\0���a���I��e�#��5\"ׯʵϚA�6(��)^&2g��2~�r�dK��3�`G\0\0��,�7�\Z&S�Η�����A��Q7�5�\r�_�#��.�����\0\0  ֱ@�9�(wּ*���ˈ��r���r~�\Z^�RF=\'7ܻYZ?�n�K�#\0�$\n�a\0\0)� \0\0 R�A\0pG�Y3�Zmֻf��\'A�Y��zɬ\n{>\0�A� \0$I�*6��^�%fM4+׬t���3ǬB����|fMf�A� \0��V~�ҕ�I�ҀWoV��RO�f5��ͬ��s�0H���J\"(ϲU�Y������3�^��\0a�0�;�lu($��p8ͬ<�����5�{a�3څڂ�\Z�l����O����F�˛�q\0	�\0��B;�9C`���R����!�0*٪�n9./7�^.-�\0a�0�;)		q���V�ӡ�k		��f+\rlڥ���1�^~.�@$�4[9���z��.S����]Ʈe+]pE�7�{C\0 $;gh��Bt���b{��<�0���\0a�0 ����sy�yF��爳�&�B�6�8��A�0H�ܜ��UQ����K��:���ܑ��ҁ�s8N���8.!$��Z�UG�]���D�.c�3����:�\Z�7�&�\n�\'�sȳKO4��A� ��=W�p�W��Y����H2��\rY�����Q�a��$������J��>%nO�����q9f���|�	����%�A� ��9W�똞���u|�ig+�&��\r����x�t�q���i��������:M߱c����C��o�����dI�)r����{�8#U�;�[R[{�	��0 ���m�����>}��䏊ǍIQT��$�h��e��M�4}ڴbY��Έ��������\ZQ�liYe���3-Z)��י��A 	��l2گ����?��r�h��yfo�ݻ���;�i������c�?x�A.���VӖ9m1t��_|P���0��(��=�SKdC�r��O\Z�ꪭ�Ç���߅[G�;�˯����.�O� �(��[�e�k��<���@Í0�5s�u�wt�.\\x��r�Ϭד&��~��{��\Z�a,��0��2��.[�kH��7�t�<�h�I[�4�^}�Xk=B�nu��~�ҹ��n~� �(���A�&���h��\\n�ψ�޽+��\Z5j�����?�r��g�]G7����daP�����~��i�Jj��G�ڥ���.�:vL?vlw�φ��0 J�ꄻ��q���%��0輑D�!tv�v��.����A�	.��]}�]��QNCױcz�g��O�su3�H���ԍ�ݽ�,m�������\'����w�t����\r���v킈�`�u,--����?�iz��	�\0�$�@�x61�� �F���٭�-xή�Hàv��]��Uڅ{:a0�:�ߡwE��ԕ�	�\0�(��M��ϻ��uP�W\'s�\0�\r�^�;דּ��Wt\Z@7Q�j�:���$y��[ŭl�3��;��r�\0�\'�5��*:�5a@��9��>ýaft8��s��+�*ǬF��.Gj���\\�\r�{�AG���lu($�E\Z5:��[��Vz{�~���;r���=	�\0��V��\\m!<�.c���Eе��P�vps��p���9\0a�0�*\r	��Zo*����|�C�!��V�b�YS�!��Z�<`D�E�0H�3�V���9\rwuvN�V��.d�\nε?����x���aO�U���\'�͑��Y>FG��fp� @$�3[I�l5�^q���\'����,�t������Q�<ߵWT+w\r�A� \0�8[N1�<[��ٷ\Z�=�`�1���#k3g���E\0� a\0�V\0��A\0\0\0� a\0\0�0H\0\0 \0\0��A\0\0\0� a\0\0�0H\0\0 \0\0��A\0\0\0� a\0\0�0H\0\0 \0\0��A\0\0@$�\0\0a�\"\0�n�gϴ�߿� �ֶ��.	pT\0\0�~�>�b�X���x��%M�\0\0�3cƌZSYYF��**\Z�K�8*\0�g�����=��@`�,��?%%���.��\0\0���\Z���z�,�UX8�)sW4p4\0�X���8�Hc�2�Y��{j����ǡ\0\0be�����!zSRR�3�!� \0\0�y LK;����1�F�\ZA�k�\0A\0\0ē�=��9=������ƦM�`B����c��a�f�F��a\0\0�F�Ui�6��F��1��J��qu��\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0���\0�	�(�E/1\0\0\0\0IEND�B`�'),('111',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/MLR Review - V2.startMlrReview.png','101','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0�\0\0e\0\0\0��]�\0\0C�IDATx�����e�7�i@.J��X���X�-1*⒔i��H���N�ǲC�AQ��x��T�yT�/���#�yA��p����܆d``F�y��g�vf�s�������[�ٳ��޿�������\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�\\���n��o=zث���Yܮ]��V�V�Z����s�y��]ػ��u\Z\0�y�<Ɵ�c��h��{�u��իW�0y��!/�P\\�\"��Zb��1?v�2e|����=z��j�.?K�`���c�)�ř9��ݻw	S�N��+5�a�i�~:u���c�\\��\0��[�1���ע����>�$My�j͚9�:}�z��B\0���y�?e�5{����M�x�߾}��O�`���c�)�9�/4�ǂǧ�5a�Ք)㷵n��i	�u�:�񧌿f+��*��ر��_�;w|/��Е\0X����k��)��Y��?ٍ3&sJ�q�ƅ�i��Ըq#W���)�\0�u���3�����;�ɵ5��I�¼y������-/�\\�u�\\��c�%���D�d=���O݇�t%\0�y�<Ɵ�g�5K��]������*��i^^^���>�}w}hݺuX�xqrݲe˒�mۦ���{R�_���:o���3���檆#�\r6$���SKÖ-[�˗_~y؜�\\RZ\ZF��*�n�����L.����\Z� �|�\0�u���3�Z^�G��rKh۶mغu�_L�m۞�K��￟\\�w�����˗{�^�`�W�y�?�O�W��T�̙33O��ڵ;̞}_ry���aĈ��;��yسgOry�aƌaΜ9\ZY�`�W�y���3�4~�!����-[�&����n����u�օ�\n*}�F��\0X�u��3��?��yv��4y�xȐ!a߾}����Q�F�7�~�c�5���:���Ɵ�g�i����׿���;t��\Z��+�H>WX�#s��\'jd��u^Y�?������_||�uׅ�^Zv&L|1r���ߞ9��o�M�ƍ��X�`�W�y���3�4�G���Q�5y�pg����;+�8����K/M.������Ϯ��.]���_~Y#k|\0���:o�Ɵ��_���z�@|��hSᮐ���PT�����\n\nBYY�?�|X�hQعs�q�s����\Z\0뼲�Ɵ��?z�>�n�$MBᮒ���8���/\n}��\r��rJr�k���S�\Z\0뼲�Ɵ��+7~QQQ��}|���]��7և��{3�aGў���kC�nݒ����ٲe����\0X�u��3��?�����g��Y�����_����i��\'O��\\W>��>a�֭�2�����+��g�\Z?K�?����������]�Z�lY())��\Z\0뼲�c�\Z_i|\0���:��g���5>\0X������J�`���c�)�O�+��u^Y�1��?4���:����\Z_��u�:��?��4>\0�y�<Ɵ2�4���\0X�u���C�k|\0���:��g���5>\0X������J�`�W�y�O\Z_i|\0���:��g���5>\0X������\Z_�`���c�)�O�+��u^Y�1��?��4>\0�ye���3�h��_�����s?ڶm�8���#w6�{�\Z(���	�z��Z�f�o�ߥ!����:o���3�����\'��=y\0�����֭�����M��?��B��mk�M׮����}��G�gj|\0���y��g�G}�w��!�Ě5s*]�y���|>��˩�~\"�X1�R3O�~c�޽K��Ǐ�|����d����p�1����Ckl���Ol�+��4[���F\rN����_W��V���竻�Νφq㾕�߳}m�9����u��\Z\0�u�:o�Ɵ�O�}�M�<�?������]6(�������������dp�˃�O>7r䗓�|����kթ�}��������T������Ҕ����%�gͺ9���}N8�S����:o���Ɵ�w�7~��K�y\n��o\'�U��B���&�	����{������v�>�j�������,_~w8��Ӓ�e�����:o���c�������n�ts��s���.�Q��O�ݐ�_�q�6,��\0X���y�?��6~�KJ<V��7�%D�>\'\'��c��Ǔ&��s��~������~��k���Ǖǧ�/�mr9^?w�q퓏7mz�̘�����+�&�����\n��}S�Ϭ�e��u�:��g�-��~���T��\"^�/�\0H�\08�]�)�ښ,��9~�x\Z���yЍvl���c���3U͘��\rl��f����FO_�xf���=?s���~���:o���Ɵ��7��:����\Z_��u�:��?��4>\0�y�<Ɵ2�4���\0X�u���C�k|\0���:��g���5>\0X������J�`���c�)�O�+��u^Y�1��?4���:����\Z_��u�:��?��4>\0�ye�7������:����\Z_����:��?����:o���SƟ�W\Z\0뼲�c�\Z_i|\0���:��g���5>\0X������J�`���c�)�O�+��u^Y�1��?4���:����\Z_��u�:��?��4>\0�y�<Ɵ2�4���\0X�u���C�k|\0���:��g�q�mۦ��x��k�{�K=$��\0�u�O��g��)/�.��j��_>�zHV�J\0���y�?e�5K\\pμɓ��xM������C2EW`���c�)�Y�ٳK��=������|�[����M=$t%\0�y�<Ɵ2���=N\\=u��׈5l�w����\0�u�O���;�+?�&l��˻{A�1ؘ���\"\0�y�<Ɵ2�Z�A]�vޡ��|�������aZ\0�u�O-��۴9�w��m�?�ǂ�?]�Q�`���c�)㯥:�U��-�����c�~�/O>���޿���\'�\"9�Ū����XpO�`���c�)��뛪ɩz.Uũ\n�+��k|��x�dg��:o���3��?�������\0���+�������;\0�:p���|���*w\0X��F������.g�\0�<`�A�=�s�|�_W#\0�y���:��W�t�p�\0�u0����T��|w\0X��ꢦ��8F\0���u�xN�o��LW\0`��?8(�]\0\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0`��?��\0�u�?��\0�u�?��\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0`��?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0@�f�k7}��7�=�����,n׮M(_G�!T�V�e�{��r�y}��}�@��}6h|\0h�����췫W�a���C^�]��xEa�:Ċ�c~��0e��Ч��{{�8���]~���>4>\0�83g޼�{�.a��	��t�Px�kڴ�N�:�>��6��>�A�@�\n������6���՚5s�	\'t� �Ӆ�g���f/��./d�oߦ0���g���f�8��v�5��Xa��jʔ��Z�n����>4>\04[�l��>^s��չs��R�\0]�}6h|\0h��[�ĳ�f=ٍ3&���ƍ�\Z�S�ƍ\\��O��J�A�@���2�ImsҤIa޼y��~���E��W�ݺuK�{�ǒ�YRR\"`d=���O݇�t%�٠��Yj׮M\r�sBQQQ���Xyyyɿ�>�lx����u��a����u˖-K�ݶm��y���Ju%�٠��ٮ�Շ�6lؐ�Ƨ�Z\Z�lْ\\����������0z�U�u�׿>�`gr����0��W���\0-6`F��rKh۶mغu�3�۶m�<�����\'��ݻ7�x����0���\0f劇�Μ93sX�]�����%��F��\\��Ο�={�$�.\\f̘�̙#0\n��g��\0�R�Ij���a˖���x��n�-�nݺu�킂J_#0\n��g��\03vv�΄���!C¾}�2��ˣF�*���_�)0\n��g��\0�g0���7��z@���+�����~�ĉ���}6h|\00+����/��*�(,LP<�O���ߞ9s�o�M�ƍ��G��>4>\0�ջ�F�?;�~��$2�s���x��K/�4�_���=3w%��v��%�������}6h|\00��믿��Sm*��W��JBX�<?s(lAAA(+����-\n;w���ܹsF�l��\0 `~t�l|-e:L0 	�1p�*	;������зo�p�)�$���k\"+`b�\r\Z\0�����(9L6�f׮]Û�C�й�hOx����[�n����`nٲE�0���\0�\'�I�-6>�y�w�ο�K��iӦ�O��+��� |��}�֭[Ce���}6h|\00��\'�x\"��e~~~(,?�l�e˖���S��>4>\0�J��l��\0 `\n�`�\r\Z\0L%`b�\r\Z\0L%`�}6\Z\00L���\0��	�٠�@�T&�٠�@�T&�g��\0S��l��\0 `\n�`�\r\Z\0L%`b�\r\Z\0L%`�}6h|\00L���\0��i��}6h|\00���}6h|\00��	�٠������3<�诓��O�1s]Ò�(`b�\r\Z\0����cZ��㿝||�C�D�<��\n�۷m��	�`�\r\Z\0�@�0��p�9}���������m۶	_���Ê���ի�.����\n�7/Mn���S?����u����=N�Z�f�W����N_��w��v�Bp����v�|6���бc���+�g���0G�������s;�Ն�\Z��\'����v�]���Y����%�+(x4���3���tHܾ��:=;Z��P������u��_Kn7k�͞��l4>\0P��9s�Mɿ�F\r�<r�!m����g��J�������=�3Y�cN�g����/�\\����MOUk��0��YՅ�}�^0�>�\0�\'`.]�����Z��,fՐv�Iݓ�7lX�5��g/��^;‐V\\��Ɵ�M������bm�C]fu?����5�`���\0�0�����l!-^�S�p�X�P؜*����#G~��ɂ&M\Z[�ϟ:uB�{w\\����r�.�Lcu�C��\r\Z�?�x���a̘�~��~V�J��ſ���ݚB���}6h|\08jfmg��?���k���k,�=?s����Kj{�%_�7/K��x�lu����ē�<����g����Ivj���n��G��?~l�޽K(s�\"��ge;�O|66jk�&�٠�@�Tަ�A���)`�}6h|\00L���\0�0���\0�0�>4>\0�&�g��\0S	��g��\0S	�`���\0L�A���)`�}6h|\00���}6h|\00��	�٠�@�0�>4>\0�&�g��\0S	��g��\0S	�`�\r\Z\0L�A���)`jI�A����L�A����L���\0��	�٠�@�T&�٠�@�T&�g��\0S��l��\0��ڶmSV\\�B�k�{�K=$���l��\0�,�q�)�����@-X��\'R�*]�}6h|\0h�.���y�\'_/�5�\Z>��S�]�}6h|\0h�z��ҿgϮ%��+��ƭwrssצ���l��\0�l��q��S\'y�XÆ]pw�X����\0���;�+?�����w���c�1UgiE�A�@K0�k��;��#.sssR��0-�}6h|\0hQ!�M�c�q����5���5���n.���\0-��Z}lI��n;�y���m�>�\r�>��H��b�O�_s�X�A�@��7U�S�\\����QuhU���}.�[�8[,�٠��&���u����A�\0+>XZ��vw�}6h|\0�`�����U����\0�~9={�.gd�l��\0@�=�s��tⳘ^�	�٠��:���e�F�{�>4>\0PW�s�;�|w�g��\0ꢦg/��A�\0u�x-��e�>4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0�@��u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>���B^���o�a��a���fq�vmB����Z��-�ݻ���뻰w��4�<����`�Z�{�麁���իG�<����wW(.^BX������?;L�2>��s��=N|5u�����`�7烅hqfμyA��]�ԩBi�J��\\Ӧ�(t��a��Ƕ�D�Y���o�Т6\Z����䯭6G�֬�N8����`�.�΃9ߜ�ً�Hſb�h4ކ�}�6�9��΃9ߜ�YOT!�]|�M<D���x5e��m�[�~ZGZ���o��l�3Ɠ;x�M�W���K=$t�u���|��\0�R<-}<s�ž�kܸ��R�]i��ƙ�3fL�7ƍ�g7�c��z�=��i�� .�9!�vD���v\n��1�������l\Z��y��U��:�3�dҤIa޼y��~���E��W�ݺuK�{�ǒ�YRR\"`���\0|$��v]�����~~I�p����TM����!Ä/tLo��\0zHJu�u\Zg����(�le������g�}6����кu�x���e˖%�n۶M�4�c��h���b8ch�0;,�\n�?\Z��p���v����ah��vX����6lؐ�CO=�4lٲ%�|�嗇ͩ�%��a�諒�֯/|������o�-`���\0�?`�����?\r�\"�8���\\tb�0�ÿ~��p�N��h�`�a��f0�[n�%�m�6lݺ���m۶g��|�������ݛ|�|�r�Ț��\0\\����3C��?^����_����Gxg����w^�V(�笄6�yh�s~<�u�̙��bw��fϾ/�<|��0bĈ��w�<�ٳ\'��p��0cƌ0g�s�9@���[.	�_z0l^=7�~��T=�\\���C�Ϸ�Y�ٰ�C3��s*��r���a˖���x��n�-�nݺu�킂J_c7�c��s�,)]~8�W�q��3C������?��/��?�{^�a�ٰ�C3�;�vg�d�xȐ!a߾}����Q�F���ү�4����\0�)`�-Yf=�,��,���(������?�T����=��t�ß��B�͆u�m�L�_��f��СC�W\\qE�����O�8�n���P{�,+[X�N�|��\r��B��Fׯ��!\\�(�Iτ�����䯾�Y�$�~����f�:�n����/��*�(,L�xҟ��o�=s��7�X�Ѝ7:ɏ9@�沕O�A��E������0.�,\\�W�]RF?Z��pY��}���ũ�9�,\\<��0��66�yhfs���F�?;�~��$2�s���x��K/�4�_���=3w%��v��%�����ps>���+7��������z<�I��k��������\n��Y.��/|mFY��B�u결�ɛË7�e�`�a��f6������a�M��B���BQIK��g�-((ee!<���aѢEa������;w�9ܜ������Z*`n��0r��p��]�y!�z�,|��T��Y.�c#f���?�	CF?\n�M/��L�\r�<4�9?$��L��$�2��]%aGQqr��_���N9��v�\\s�Cd��Xx\0�07����>�\r�������,)WW���i_}4��wo\n���ٰ�{ll6Z����`w-)`%������ڵkx�`}(ܽ7:w�	���6t��-�}|�-[���|L���X�J�î]�a��\n.{ �9ln���?�O���p8g��U��$�����6\r6-ܠT�jj����%���Y�l���k���p�E_J.O�6-|�^��u�S���n�\Z*3���0�w��.ӡ2>��EȤ%�\'�x\"��e~~~(,?�l�e˖�������=S��T�MպT�,��ũz\'U�SuC��0L�\r\Z\'\\�v=��U���#R��<Tޓ�o���Tu(�|��W��5��vy���80l6l68��\"ӟ��L������\Z�jM��պ��ݪT=��ӌ�f�f��.k���Ĝ�\Z}����9�ym}��{��ǳ�&�͆�\rgPN��������o�o0�[�5�\Z���-�~�\Z7&�͆�\r.����21�#9�,��\Z�az�_�d\n�\06&G>\\6�׃9ߜ_�gp���o��?��0l6Lꭶ����|s~��bo8̍_����#`�l�T(l��[=��9_�9?�I<[l�#�<��8TV���0�oL�ˏ������9��|u����G�/Xq2�,��	`�!`R�p9�BH����!���21���{��?O�#�@�r\Z�DB��͆0[r���*,B=Bf�p���>�5���U����u�3�pM�9���D�l6���\\U���ŚBf����r{��Ĝ�\ZlΟ��o�����Ƒ�	���2η�0�z��rp=Bc��Y�pY5d:\\s�9����x��iG��z��=�M���9(�o:�l�z��B^m!�����ߒ�L����C�3U�p�şWl�	��Q��H���ݶ��GK�W\r�u\rwՅ�܃�i^��9ߜ�Ao\n�֐��	4��Fſn�o�!`��~0Ღ���!��C�}��o�oq�`Z\0�G@��lT�����l6�n��p�1�C��mî]���ͭ���l!�P�eՐ�5�4��������:�x��3��q�2�{\r��$`�h�pX���ԩB�\'&_3cƿ������)	�Mh��_M=�1\Z�����[�������������㿝||�C�D���<ݔ�sg���G@K�7��\\�*_���\\���OK��>�>��\'s���ߚ�V�r+m.��~���+_�|h۶M8��O�+f���Z۷?�l���\"~�Q�\'�e���?�����ڹ��0nܷBǎ*]_���f���L�^�������̺�Ofu<�I�����8;3���k�?�\\��ë�n��?��.�lx�������V��>�<��w��v�Bp]ז���)<	���0�n2r�0_|�d�/_|�����+VZ����ꮿ�A���&����A}��_���OM*^��նɨ�����Z��Y7����Mjc]]����Μ��\Z̃	�^�I�����G�������s;�Ն�\Z��\'��逹���&&�\'������U�|0�Cm�um�nMh\ns~�T�S�dw���r{	O&pT��T��a���\"x��#�LN.?��������R���޴���Tw}տ2Ǐ��T�1ٷ磻*~��̊ߧ��_�l��z�j���3�սI����\'d:�,�1j��fμ)��G��9r��#Z⡴�����=�3Y�Ϫ�~ժn����P��y0kKS|���#8����9�I���y��gq�0�	}�a�U6��u��+���^x�p�luק���Uq����K�L��`f}���٨����Bfm����粶�ɬN��I㌃Z��K!���Ŭ:G�tR���\re�K㳗��������l/u�6���w�K�����TO����&��<���J�I���\Z0�f!}tM�xuf#O���w\\�J\'�v}���&�=�$B��>U+��S~��ſ��?7hP������1c�W�d��/�����|�u���mpr��zk�U�3���˜����tGxhjs~��޽�5��.���᮱�stN�Cd��#�\\㼟�dr�����tu�C���4��um�nMh�y���������ȴ�z��s��\0����$`��ʬY3��u���Ϝ\"f��O���x�S���_��W�����]��/�1���Ɗ�ӯ�Y������w�l**��xF����\'t�y���mm�_�l��{&��Ჾ!3��K�l�������2t��9��=?s����Kj{�%_�7/�:�g;D6�|_u���w�z������-խ	M)`�J�Ɯ��Cs8�-���O&`�h����f�ɬz��U9���l!st���5�4逩��?2g�	z5p��*��W��4>�G����2+���bX�K�̩��	�:�s���0�P�d�-�~?�L��a�a�A�U=\\6���9�둪_w��	�|s~��g2�!�5���z���ˍ�Ux0�͆�\r2U��oA�T�u���K��_��$��&����#ݜƧ�0��)d��\n�|s~�|�<h��\\g��?��i��P���a4��)�ZW,����=��G���̣š��XL�d�����~�文����}/U���?����d��7l6l68�!���@:��2�O�#@�T6T��ۃ9ߜ���>Le�!d�_�v����ʜ/<��\0S�l�����8���o.0�9_xr�E�f����L��\"`*s���>�66r�.0�9_xr�\r�\r�\r9dN.0�9_xr�\r�\r�\r�����[� `*s���>�66�N�Le���G�y�f�f0�9�&�}�7l6l6\0���|� ����l6\0S��\'� `*�\r@�T�|��}�7l6l6\0���|� �`ްٰ�\00��m�p���\0Le���G���l6\0S��\'�`ްٰ�\00��m��G\0�1o�mۦ��x���	���/�-���\Z�9ߜo��h���g��)/�.�}�~�D�!Ye\0�|s�M�#�Y�\\pμɓ���7�\Z>��S��\00��m�p�r��ٳK��=�������7n�����6��0\0s�9�&��l�=N\\=u�~#ְaܝz(�\Z�9ߜo��h����;�+?���*/����`c��2\0s�9�&���A]�v�a�q�7\Z����������|� �G@K�7�is�?��{ۼ>�������6\Z�9ߜ���>Z�qz�V[ҡñ�ǎ��_�|�w�gZý�Y<-}<s`����o\"����O�#���}S59Uϥ���wS�V�\r��{����;s Д����6A�#��\0`�>0o\0\0���\0�\0�M��0o\0\0���\0�\0�M����\0`�>�\0\06A�#��\0`�>�\0\06A��\0�\0�M��0o\0\0���\0�\0�M����\0��>�\0\06A�#��\0`�>�\0\06A�#\0�\0�M��0o\0\0���\0�\0�M��0o\0\0���\0�\0\06A�#��\0`�>�\0\06A�#��\0`��0o\0\0���\0�\0�M��0o\0\0���\0�\0�M�#��\0`�>�\0\06A�#��\0`�>0o\0\04�&(��vӧ�x����^�����v�ڄ��AB�j�[ֻw�-��wa��\'�� `\0��Mн��t����v���#L�|}�˻+�!�V�X�~�ϟ�L��9yo�\'������� `\0��M�̙7/�޽K�:uB(-])�6�G�S���=��%ZL\0��	����>�<�&��Z�fN8�N��a�L\0�f�	����g.�����۷)�q�,�\0\0�yO�_s��\Z��L��u��Oku0\0��&(�-6���k.�:w��^�!��A�\0h����V$�l��COvcƌɼ�Ƹq�B����Ըq#W���)�L\0�f�	��sߊ���9iҤ0o޼�r�~�¢E�Ë+W�nݺ%�=��cI�,))0��y���U�L\0�f�	j׮M\r�sBQQQ���Xyyyɿ�>�lx����u��a����u˖-K�ݶm��y���J�;�\0\0�uTC�	aÆ\rIh|ꩥa˖-���/�<lN].)-\r�G_�\\�~�{�v&��~�m� ˆL\0�0�[n�%�m�6lݺ���m۶g��|�������ݛ|�|�r��\n�`n\00+W<�u�̙��bw��fϾ/�<|��0bĈ��w�<�ٳ\'��p��0cƌ0g��Q�\0@��v��?~زekr9��\'���nK�[�n]x������&�\0\0f&��,ڝ	���!C��}��e���G�\ZU(�K�S`0A�\00x�}3�w�С�+��\"�\\a���\'N�(0\n� `\0��O�s�uׅ�^Zv&(��\'���oϜ9��7�&t�ƍN�#`��	\0 `~T��nU�����;��xϽ����	~.����r|����ܕBۥK����/�&�\0\0��z��ד��a�M��B���BQIK��g�-((ee!<���aѢEa������;w��(`��	\0 `~t�l|-e:L0 	�1p�*	;������зo�p�)�$���k\"+`��	\0 `V�EEE�a��0�v�\Z�,X\nw�̈́�E{«��\rݺuKn�s˖-��	&\0��y�I~�g���`^����/�Rryڴi�\'�\no�+��t��u��P��(`��	\0 `f	�O<�D�����PX~V�X˖-%%%��	�V\0\0S	�`n\0�	0L@�\00��	&\0���L0�\0�	�&`�\06A��	�\0\0�0A�\00��	�V\0\0� S�L\0\0S�L\0\0S	� `\0�J�s+\0�MP���:_�0\0�P7A���\0S�L\0�C\r��Z���M�N�s+\0�MP�f�g4�0L�x\0l�5`V|Fs@m3}�G�u����7f��cXL0\0��&�Um��cZ��㿝||�C�D�<��\n�۷m��5��ٔ\nL\0\0�\r�R5,U�ӷ�-�pv8�>��_����c��mۄ�|��aŊYY���]����k�͛�&��_s꩟�|Mպ��[C�\'�V�r3�+~MՀ��w��v�Bp����v�|6���бc��Ml�k�	\0@րY5X��5`�=4	]o���0v�7�\rU14��O8������&&�\'����e�\rJ>WP�h�o��gf��鐸}��uzv��ߡ����YU�꫿��n֬�[�!�6��\0\0T\n���\Z�%X�9`ΜyS��Q��#��y@H[���p�٧%���dy�0�s��L���S��l?��?�|>>��i�S���~����~Vu�w߾�L0\0�\n�=cYt���k㳘UC�I\'uO>ްaQ�p����^{�BZq�\Z~|6���%�����u	������Qp��oL\0\0���-��ݛ�	��BZ�>��᮱⡰9U���G��r��M�4�Ɵ?u���:������x]����~���4�����Ø1�+���������ŋ[�a�5�\\�8\0�Q0k;;����]�vN^c9t�����T�]�P�K.�BؼyYr�m�Ce�;D6�&�|����\\?cƿgN�S��P�vk�>>��cC��]�@�S��l?+�I~⳱�P[[�0A�\00���>�6��\0\0���7�q\0\0S���=\0 `*L\0\0S	�6��\0\0���7�q\0\0S	���\0L%`��	\0��)`�|�\00L�o��\0\0�0A�\00��	&\0\0��i�\rz\0@�0m�A�\0�J�\0@�T��7&\0\0��i�\rz\0@�0L��\0\0�0A�\00;��\0\0S���=\0 `*�8\0���L0\00L�o��\0\0\r�m�6e��+��&P�w���CRj�\rz\0�Y:�S6���%�5�Z���O��U6ߠ�\0��.8g����xM������C2���8\0@�Գg��={v-)-])�5n�����6����=\0�l��q��S\'y�XÆ]pw�Xj�\rz\0��;�c��v����\Z����^�z6��,�o��\0\0-���];�2�|����-H���l�A�\0���٦�1����m��������b76�pi�	\0�!;�U��-�����c�~�/O>����\'����2�I<[l�	}�k.�j½`�	\0@�蛪ɩz.U��1uhU���}.�[�h=`�	\0\0�|�\0\0�o��\0\0`�\rz\0\0��F�\0\06ߠ�\0���8\0\0�|�\0\0l�A�\0��7�q\0\0��=\0\06ߠ�\0\0�o��\0\0`�\rz\0\0l�A�\0\06��q\0\0���8\0\0�|�\0\0�o��\0\0��7z\0\0��=\0\06ߠ�\0���8\0\0`�\rz\0\0l�A�\0��7�q\0\0��=\0\0�|�\0\0�o��\0\0`�\rz\0\0��F�\0\06ߠ�\0���8\0\0�|�\0\0l���\0\0��7�q\0\0��=\0\06ߠ�\0\0�o��\0\0`�\rz\0\0l�A�\0��7�q\0\0���8\0\0�|����vӧ�x����^�����v�ڄ��[B�j�[ֻw�-��wa��\'��\0\0@��|�{�M�\r�oW�^=���ׇ���Bq��ju������aʔ�O�����q⫩��,=\0\0����̙7/�޽K�:uB(-])�6�G�S���=��%z\0\0h1��.O;��3l�ߑ�5k�N��A�!��\0�f������g.�����۷)�i9��\n�\0\0p4n��	}�k.�a��^�Ք)㷵n��i=\0\04��w<[l<���\\6~u����C2@�\0\0�r�ߊ$�-V�k�\Z7n��C2E�\0\0�r���2�I]CТ��oG��wh����í_��>N@l�z晻��zHV�q\0\0�Yn�۵kS���|���_�9�3�s�+U�.=>�lH�0�����\r�>����T�\0\0�u�]�\04ch�0;,�\n�?\Z��p���v����ZH80\0@���~va��䌟�\r�^����.:1L���_�tj��\'�K_L\0\0������ś�\n�xaF�K^��ᝅ?��y9���Y�l�3�\n�&\0\0�u���\\���`ؼzn���C�z8����o. \n�&\0\0�����Uᇓ\Z�/�Y=3�1?�ݯ�g��\r��~��P��%!Q�0\0@������\n��Xz^�X_���k��O*^~�w��_~:��O��n!S�0\0@��Vee��K���Þ��awx`C����!\\�<���0�V��;����=���W	�&\0\0 `V�e+�����0���^a\\~Y�.�,\\��,�~�,|���������SAsNY�xĝa��ǅE\0\00+�+7��������z<�I��k��������\n��Y.��/|mFY��B�u결�ɛË7�%,\n�\0\0��Y�^K��O�\ZF��n[�+\\1/�Q��oݟ\n�3��ea��~��=a��B����),\n�\0\0��Y�6����>�k~>7&/|gIY��B}��N���߾{Sx��φM��#,\n�\0\0��y`�+��v凉3�+��p氹��C�>=x^8c����W�ǒ����\n��a\0\0�0L\0\0��0L=\0\0���L\0\0@�T��	\0\06���)`\0�ͷ�)`�q\0\0@�T&\0\0 `*S�\0\0�oS�0\0@�0L=\0\0�J�\0\0L%`\n�\0\0`�-`\n�z\0\0��4U����	\0\0fC��P4���	\0\0��t-�����`\'`\n�\0\0`�]��Y���LS�\0\0��C\r���P]��x�c�i��99,^�ۆOM9�	�&\0\04��w����ҥ�O.��qbK���-�\0\0�� ȮJհT�N߮���o�K��m�$o޼4|�+�O>>��O�+f%ןsN��v���`����L���=���ڷo���O�E�k��W��k_��\Z�W���~��C��]��?~lX�fN����п��ɳ��G�Sؾ��[�ߡW������y��гgW�`\0\0GE��\Z,s�\Z0�M�Qry��/\'_v٠�もG�cP���v�w����k��\'N�:��W?��=�����ە��y���������1���\Zo{�	�*�������\r�|.�_������S�\\��w��g�I.�{�g*}&:D\0\0h�3?U#��:�t�gcH����V�\\���u��\'�S\'%�7mz*k���{����&ץCh�k��\\��w]���p�٧%�FV�UCcŏ�ϒ�ggk;D6���?�pZ+`\0\0MQu�X�+`�CS�<�����O��R8����u�����fμ)\\|��\rj5}��!�]�����g���kYדN��fÆEu��0Z��`�d9\\8}���	\0\0Pǀ/�]�H�_r���3��+�$��4i�_7e���	�fͺ���Y��HjC�W�znr9�~2~�l�k����ᶱv�|�ƀy��}���?orHo]�o�?|G�Å�}]u!�.aT�\0\0Zt������`�}��aԨ��sM��o{<si�g\'+޶��_��nƌ�\\��.��:�/C׮��g>�=?�j���,�?�\'�\'�9�S���K�|�����_/\Z�-0\0\0�0U㼅��	\0\0�-�ⳖOB$`\0\0����R\0\0\0S	�\0\0���L\0\0\0S�\0\00��	\0\0�J�\0\00L\0\0\0S�\0\00��	\0\0�J�\0\00L\0\0\0S�\0\00��	\0\0�J�\0\00L\0\0\0��*)y1��֟�]��L\0\0\03S1,��zU�:\0\0@��&\0\0���L\0\0@�T&\0\0��)`\0\0�^�)`\0\0�S۶mʊ�W8�l�ݻ_�[��-Օ\0\0@�t��l�˻�3�M�,����d��\0\0��.8g����{�c���/x0��Lѕ\0\0@�Գg��={v-)-])�5n�����6��Е\0\0@�գǉ��N� �5b\rv�ݩ�b�n\0\0���;v<nW~�l��l��˻{A�1ؘ���\"\0\0��ڵ�2[�Ydc����-H��ô \0\0ТBf�6���;���)�&��=��N�a��K\0\0��:�U��-�����c�~�/O>�����}2���\\Ʒ\"�g�-?�O|ͥ�b\0��o�&��T�ġ��*���>��H�-\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�����Ny��\0\0\0\0IEND�B`�'),('118',1,'PrepareForReview.bpmn20.xml','117','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" \r\n			xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" \r\n			xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" \r\n			xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" \r\n			xmlns:signavio=\"http://www.signavio.com\" \r\n			xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \r\n			exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" \r\n			expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-8f89b30b-9210-479a-946b-4c3f75405b76\" \r\n			targetNamespace=\"http://www.signavio.com/bpmn20\" \r\n			typeLanguage=\"http://www.w3.org/2001/XMLSchema\" \r\n			xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\"\r\n			xmlns:activiti=\"http://activiti.org/bpmn\" >\r\n   <process name=\"PrepareForReview\" id=\"sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa\" isExecutable=\"false\">\r\n      <startEvent id=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240\" name=\"Start\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <outgoing>sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F</outgoing>\r\n      </startEvent>\r\n      <userTask activiti:candidateUsers=\"${groupService.getCandidateTaskUsers(execution,\'reviewer\')}\" completionQuantity=\"1\" id=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\" implementation=\"webService\" isForCompensation=\"false\" name=\"Verify Piece&#10;\" startQuantity=\"1\" >\r\n         <documentation>PM-SubmitMLR-QualityControl</documentation>	\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\r\n         </extensionElements>\r\n         <incoming>sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F</incoming>\r\n         <outgoing>sid-12195530-42AD-4E34-BDC9-740FBF187574</outgoing>\r\n      </userTask>\r\n      <endEvent id=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B\" name=\"End&#10;\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-12195530-42AD-4E34-BDC9-740FBF187574</incoming>\r\n      </endEvent>\r\n      <sequenceFlow id=\"sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F\" name=\"\" sourceRef=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240\" targetRef=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\"/>\r\n      <sequenceFlow id=\"sid-12195530-42AD-4E34-BDC9-740FBF187574\" name=\"\" sourceRef=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\" targetRef=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B\"/>\r\n   </process>\r\n   <bpmndi:BPMNDiagram id=\"sid-af2d216f-2a59-433a-bb05-69b2533a21cf\">\r\n      <bpmndi:BPMNPlane bpmnElement=\"sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa\" id=\"sid-d4f7ba96-f1cc-45d4-a894-c7ba60deb401\">\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240\" id=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240_gui\">\r\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"120.0\" y=\"85.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\" id=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464_gui\">\r\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"357.0\" y=\"60.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B\" id=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B_gui\">\r\n            <omgdc:Bounds height=\"28.0\" width=\"28.0\" x=\"720.0\" y=\"86.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F\" id=\"sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F_gui\">\r\n            <omgdi:waypoint x=\"150.0\" y=\"100.0\"/>\r\n            <omgdi:waypoint x=\"357.0\" y=\"100.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-12195530-42AD-4E34-BDC9-740FBF187574\" id=\"sid-12195530-42AD-4E34-BDC9-740FBF187574_gui\">\r\n            <omgdi:waypoint x=\"457.0\" y=\"100.0\"/>\r\n            <omgdi:waypoint x=\"720.0\" y=\"100.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n      </bpmndi:BPMNPlane>\r\n   </bpmndi:BPMNDiagram>\r\n</definitions>\r\n'),('119',1,'MLR Review - V2.bpmn20.xml','117','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns:activiti=\"http://activiti.org/bpmn\" xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:signavio=\"http://www.signavio.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-6c9c2dee-be44-4ebb-8302-e85cf48d4d44\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\">\n   <process name=\"MLR Review\" id=\"startMlrReview\" isExecutable=\"false\">\n   \n     <extensionElements>\n        <activiti:executionListener delegateExpression=\"${workflowListener}\" event=\"end\" />\n     </extensionElements>\n     \n      <startEvent id=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C\" name=\"Start review workflow \">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <outgoing>sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4</outgoing>\n      </startEvent>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'inMlrReview\')}\" completionQuantity=\"1\" id=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change (READY FOR MLR to IN REVIEW)\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4</incoming>\n         <outgoing>sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E</outgoing>\n         <outgoing>sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39</outgoing>\n      </serviceTask>\n      <userTask activiti:assignee=\"${assignee}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\" implementation=\"webService\" isForCompensation=\"false\" name=\"Review and provide annotations\" startQuantity=\"1\">\n         <documentation>PM-MLRReview-ReviewAnnotate</documentation>	\n         <extensionElements>\n			<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E</incoming>\n         <outgoing>sid-FFDFD243-271D-458F-8DF4-854C3668FED8</outgoing>\n         <multiInstanceLoopCharacteristics activiti:collection=\"${groupService.getCandidateTaskUsers(execution,\'reviewer\')}\"  activiti:elementVariable=\"assignee\" behavior=\"All\" id=\"sid-83c385a5-2e28-45aa-a49d-8105f3aedc2d\" isSequential=\"false\"/>\n      </userTask>\n      <userTask activiti:assignee=\"${groupService.getTaskAssignee(execution,\'coordinator\')}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\" implementation=\"webService\" isForCompensation=\"false\" name=\"Assess outcome\" startQuantity=\"1\">\n      	 <documentation>PM-MLRReview-CompleteReview</documentation>\n         <extensionElements>\n           	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-FFDFD243-271D-458F-8DF4-854C3668FED8</incoming>\n         <outgoing>sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174</outgoing>\n      </userTask>\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" name=\"Are changes required?\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <incoming>sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174</incoming>\n         <outgoing>sid-9218A35A-08ED-4C84-93AA-C568CADBD664</outgoing>\n         <outgoing>sid-1498D019-EFAC-4F53-9400-715E685CA2F4</outgoing>\n      </exclusiveGateway>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'readyForMlr\')}\" completionQuantity=\"1\" id=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN REVIEW to READY FOR MLR\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-9218A35A-08ED-4C84-93AA-C568CADBD664</incoming>\n         <outgoing>sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'mlrReviewedApproved\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Review completed, changes not required\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'draft\')}\" completionQuantity=\"1\" id=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN REVIEW to DRAFT\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-1498D019-EFAC-4F53-9400-715E685CA2F4</incoming>\n         <outgoing>sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'mlrReviewedWithChanges\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Review completed, changes required\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'inMlrReview\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Piece is in review\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39</incoming>\n      </serviceTask>\n      <sequenceFlow id=\"sid-FFDFD243-271D-458F-8DF4-854C3668FED8\" name=\"\" sourceRef=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\" targetRef=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\"/>\n      <sequenceFlow id=\"sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174\" name=\"\" sourceRef=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\" targetRef=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\"/>\n      <sequenceFlow id=\"sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4\" name=\"\" sourceRef=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C\" targetRef=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\"/>\n      <sequenceFlow id=\"sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E\" name=\"\" sourceRef=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" targetRef=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\"/>\n      <sequenceFlow id=\"sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39\" name=\"\" sourceRef=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" targetRef=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235\"/>\n      <sequenceFlow id=\"sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E\" name=\"\" sourceRef=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\" targetRef=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF\"/>\n      <sequenceFlow id=\"sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6\" name=\"\" sourceRef=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\" targetRef=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714\"/>\n      <sequenceFlow id=\"sid-9218A35A-08ED-4C84-93AA-C568CADBD664\" name=\"no\" sourceRef=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" targetRef=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\">\n         <conditionExpression id=\"sid-ffc0c9dc-bfc1-4cb5-b41f-6cd53e3707cb\" xsi:type=\"tFormalExpression\">${documentService.all(execution, \'finalOutcome\', \'Changes not required\')}</conditionExpression>\n      </sequenceFlow>\n      <sequenceFlow id=\"sid-1498D019-EFAC-4F53-9400-715E685CA2F4\" name=\"yes\" sourceRef=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" targetRef=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\">\n         <conditionExpression id=\"sid-933c53b2-38a4-407f-863e-999540f85fd0\" xsi:type=\"tFormalExpression\">${documentService.any(execution, \'finalOutcome\', \'Changes required\')}</conditionExpression>\n      </sequenceFlow>\n   </process>\n   <bpmndi:BPMNDiagram id=\"sid-8458580e-bbc3-4cbb-b62e-ab1badc59ee5\">\n      <bpmndi:BPMNPlane bpmnElement=\"startMlrReview\" id=\"sid-e350d9f4-f8f1-48a5-b091-f75355ef3655\">\n         <bpmndi:BPMNShape bpmnElement=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C\" id=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C_gui\">\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"210.0\" y=\"555.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" id=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"285.0\" y=\"645.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\" id=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"456.0\" y=\"768.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\" id=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"634.0\" y=\"532.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" id=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8_gui\" isMarkerVisible=\"true\">\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"821.0\" y=\"552.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\" id=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"1015.0\" y=\"532.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714\" id=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"1015.0\" y=\"250.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\" id=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"791.0\" y=\"396.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF\" id=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"791.0\" y=\"250.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235\" id=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"285.0\" y=\"250.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6\" id=\"sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6_gui\">\n            <omgdi:waypoint x=\"1065.0\" y=\"532.0\"/>\n            <omgdi:waypoint x=\"1065.0\" y=\"330.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4\" id=\"sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4_gui\">\n            <omgdi:waypoint x=\"240.0\" y=\"570.0\"/>\n            <omgdi:waypoint x=\"262.5\" y=\"570.0\"/>\n            <omgdi:waypoint x=\"262.5\" y=\"685.0\"/>\n            <omgdi:waypoint x=\"285.0\" y=\"685.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-FFDFD243-271D-458F-8DF4-854C3668FED8\" id=\"sid-FFDFD243-271D-458F-8DF4-854C3668FED8_gui\">\n            <omgdi:waypoint x=\"556.0\" y=\"808.0\"/>\n            <omgdi:waypoint x=\"605.0\" y=\"808.0\"/>\n            <omgdi:waypoint x=\"605.0\" y=\"572.0\"/>\n            <omgdi:waypoint x=\"634.0\" y=\"572.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-9218A35A-08ED-4C84-93AA-C568CADBD664\" id=\"sid-9218A35A-08ED-4C84-93AA-C568CADBD664_gui\">\n            <omgdi:waypoint x=\"861.0\" y=\"572.0\"/>\n            <omgdi:waypoint x=\"1015.0\" y=\"572.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39\" id=\"sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39_gui\">\n            <omgdi:waypoint x=\"335.0\" y=\"645.0\"/>\n            <omgdi:waypoint x=\"335.0\" y=\"330.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-1498D019-EFAC-4F53-9400-715E685CA2F4\" id=\"sid-1498D019-EFAC-4F53-9400-715E685CA2F4_gui\">\n            <omgdi:waypoint x=\"841.0\" y=\"552.0\"/>\n            <omgdi:waypoint x=\"841.0\" y=\"476.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E\" id=\"sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E_gui\">\n            <omgdi:waypoint x=\"841.0\" y=\"396.0\"/>\n            <omgdi:waypoint x=\"841.0\" y=\"330.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E\" id=\"sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E_gui\">\n            <omgdi:waypoint x=\"385.0\" y=\"685.0\"/>\n            <omgdi:waypoint x=\"420.5\" y=\"685.0\"/>\n            <omgdi:waypoint x=\"420.5\" y=\"808.0\"/>\n            <omgdi:waypoint x=\"456.0\" y=\"808.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174\" id=\"sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174_gui\">\n            <omgdi:waypoint x=\"734.0\" y=\"572.0\"/>\n            <omgdi:waypoint x=\"821.0\" y=\"572.0\"/>\n         </bpmndi:BPMNEdge>\n      </bpmndi:BPMNPlane>\n   </bpmndi:BPMNDiagram>\n</definitions>\n'),('120',1,'Approval Workflow - V2.bpmn20.xml','117','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns:activiti=\"http://activiti.org/bpmn\" xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:signavio=\"http://www.signavio.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-39062ac0-8ac8-43bf-b32f-639d483f2094\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\">\n   <process name=\"MLR Approval\" id=\"startMlrApproval\" isExecutable=\"false\">\n   \n      <extensionElements>\n        <activiti:executionListener delegateExpression=\"${workflowListener}\" event=\"end\" />\n      </extensionElements>\n     \n      <startEvent id=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B\" name=\"Start approval workflow\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <outgoing>sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB</outgoing>\n      </startEvent>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'inMlrApproval\')}\" completionQuantity=\"1\" id=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: READY FOR MLR to IN APPROVAL\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB</incoming>\n         <outgoing>sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD</outgoing>\n         <outgoing>sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172</outgoing>\n      </serviceTask>\n      <userTask activiti:assignee=\"${assignee}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\" implementation=\"webService\" isForCompensation=\"false\" name=\"Approve\" startQuantity=\"1\">\n         <documentation>PM-MLRApproval-Approve</documentation>	\n         <extensionElements>\n         	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD</incoming>\n         <outgoing>sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE</outgoing>\n         <multiInstanceLoopCharacteristics activiti:collection=\"${groupService.getCandidateTaskUsers(execution,\'approver\')}\" activiti:elementVariable=\"assignee\" behavior=\"All\" id=\"sid-d5e84c0f-762a-4cf7-92de-bde3c31f3a5a\" isSequential=\"false\"/>\n      </userTask>\n      <userTask activiti:assignee=\"${groupService.getTaskAssignee(execution,\'coordinator\')}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\" implementation=\"webService\" isForCompensation=\"false\" name=\"Assess outcome\" startQuantity=\"1\">\n      	<documentation>PM-MLRApproval-CompleteApproval</documentation>\n         <extensionElements>\n           	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE</incoming>\n         <outgoing>sid-4D72DB74-7882-494E-A373-460453FDCED0</outgoing>\n      </userTask>\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" name=\"Any changes required?\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <incoming>sid-4D72DB74-7882-494E-A373-460453FDCED0</incoming>\n         <outgoing>sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA</outgoing>\n         <outgoing>sid-79C7E93B-C8EB-488F-8BBD-F323077A925E</outgoing>\n      </exclusiveGateway>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'draft\')}\" completionQuantity=\"1\" id=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN APPROVAL to DRAFT\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA</incoming>\n         <outgoing>sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'approvedWithChanges\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Approval workflow completed, changes needed\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'approvedForProduction\')}\" completionQuantity=\"1\" id=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN APPROVAL to APPROVED\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-79C7E93B-C8EB-488F-8BBD-F323077A925E</incoming>\n         <outgoing>sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'approved\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Approval workflow completed, no changes needed\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'approvalWorkflowStarted\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Approval workflow started\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172</incoming>\n      </serviceTask>\n      <sequenceFlow id=\"sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB\" name=\"\" sourceRef=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B\" targetRef=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\"/>\n      <sequenceFlow id=\"sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD\" name=\"\" sourceRef=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" targetRef=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\"/>\n      <sequenceFlow id=\"sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE\" name=\"\" sourceRef=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\" targetRef=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\"/>\n      <sequenceFlow id=\"sid-4D72DB74-7882-494E-A373-460453FDCED0\" name=\"\" sourceRef=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\" targetRef=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\"/>\n      <sequenceFlow id=\"sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172\" name=\"\" sourceRef=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" targetRef=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805\"/>\n      <sequenceFlow id=\"sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3\" name=\"\" sourceRef=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\" targetRef=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C\"/>\n      <sequenceFlow id=\"sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE\" name=\"\" sourceRef=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\" targetRef=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436\"/>\n      <sequenceFlow id=\"sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA\" name=\"yes\" sourceRef=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" targetRef=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\">\n         <conditionExpression id=\"sid-82a14cc3-9c6f-441d-9b20-f9b90e0f42c8\" xsi:type=\"tFormalExpression\">${documentService.any(execution, \'finalOutcome\', \'Not approved for production\')}</conditionExpression>\n      </sequenceFlow>\n      <sequenceFlow id=\"sid-79C7E93B-C8EB-488F-8BBD-F323077A925E\" name=\"no\" sourceRef=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" targetRef=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\">\n         <conditionExpression id=\"sid-d2ecff9b-876c-4dd1-95af-49a17646e205\" xsi:type=\"tFormalExpression\">${documentService.all(execution, \'finalOutcome\', \'Approved for production\')}</conditionExpression>\n      </sequenceFlow>\n   </process>\n   <bpmndi:BPMNDiagram id=\"sid-39fbb042-c68b-432b-8516-9f9dd5a5c37b\">\n      <bpmndi:BPMNPlane bpmnElement=\"startMlrApproval\" id=\"sid-b872979f-4d08-4cd9-b6b5-fc9ec1e165c2\">\n         <bpmndi:BPMNShape bpmnElement=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B\" id=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B_gui\">\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"180.0\" y=\"360.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" id=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"303.0\" y=\"450.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\" id=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"390.0\" y=\"660.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\" id=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"560.0\" y=\"450.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" id=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2_gui\" isMarkerVisible=\"true\">\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"750.0\" y=\"470.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\" id=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"720.0\" y=\"320.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C\" id=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"720.0\" y=\"195.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\" id=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"922.5\" y=\"450.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436\" id=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"922.5\" y=\"195.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805\" id=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"303.0\" y=\"195.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-79C7E93B-C8EB-488F-8BBD-F323077A925E\" id=\"sid-79C7E93B-C8EB-488F-8BBD-F323077A925E_gui\">\n            <omgdi:waypoint x=\"790.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"922.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172\" id=\"sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172_gui\">\n            <omgdi:waypoint x=\"353.0\" y=\"450.0\"/>\n            <omgdi:waypoint x=\"353.0\" y=\"275.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-4D72DB74-7882-494E-A373-460453FDCED0\" id=\"sid-4D72DB74-7882-494E-A373-460453FDCED0_gui\">\n            <omgdi:waypoint x=\"660.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"750.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE\" id=\"sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE_gui\">\n            <omgdi:waypoint x=\"972.0\" y=\"450.0\"/>\n            <omgdi:waypoint x=\"972.0\" y=\"275.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA\" id=\"sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA_gui\">\n            <omgdi:waypoint x=\"770.0\" y=\"470.0\"/>\n            <omgdi:waypoint x=\"770.0\" y=\"400.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB\" id=\"sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB_gui\">\n            <omgdi:waypoint x=\"210.0\" y=\"375.0\"/>\n            <omgdi:waypoint x=\"256.5\" y=\"375.0\"/>\n            <omgdi:waypoint x=\"256.5\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"303.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE\" id=\"sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE_gui\">\n            <omgdi:waypoint x=\"490.0\" y=\"700.0\"/>\n            <omgdi:waypoint x=\"535.0\" y=\"700.0\"/>\n            <omgdi:waypoint x=\"535.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"560.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3\" id=\"sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3_gui\">\n            <omgdi:waypoint x=\"770.0\" y=\"320.0\"/>\n            <omgdi:waypoint x=\"770.0\" y=\"275.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD\" id=\"sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD_gui\">\n            <omgdi:waypoint x=\"403.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"440.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"440.0\" y=\"660.0\"/>\n         </bpmndi:BPMNEdge>\n      </bpmndi:BPMNPlane>\n   </bpmndi:BPMNDiagram>\n</definitions>\n'),('121',1,'Submit for QC - V2.bpmn20.xml','117','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns:activiti=\"http://activiti.org/bpmn\"  xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:signavio=\"http://www.signavio.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-b2bce920-71f1-4aae-8167-84d32c7fd8c4\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\">\n   <process name=\"Submit for MLR\" id=\"submitForMlr\" isExecutable=\"false\">\n   \n     <extensionElements>\n        <activiti:executionListener delegateExpression=\"${workflowListener}\" event=\"end\" />\n     </extensionElements>\n   \n      <startEvent id=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04\" name=\"Submit piece for QC\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <outgoing>sid-D8055B00-59CB-4604-A217-C0C44E4F218D</outgoing>\n      </startEvent>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'pendingQc\')}\" completionQuantity=\"1\" id=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: DRAFT to PENDING QC\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-D8055B00-59CB-4604-A217-C0C44E4F218D</incoming>\n         <outgoing>sid-B60C9535-C150-4B52-B391-2A45A1DF2A06</outgoing>\n      </serviceTask>\n      <userTask activiti:assignee=\"${groupService.getTaskAssignee(execution,\'coordinator\')}\" activiti:dueDate=\"${documentService.getDate(execution,\'requestedDate\')}\" completionQuantity=\"1\" id=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\" implementation=\"webService\" isForCompensation=\"false\" name=\"Verify piece meets QC criteria\" startQuantity=\"1\">\n         <documentation>PM-SubmitMLR-QualityControl</documentation>	\n         <extensionElements>\n           	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-B60C9535-C150-4B52-B391-2A45A1DF2A06</incoming>\n         <outgoing>sid-F226E847-2506-4D6D-A759-D787243EAD7C</outgoing>\n      </userTask>\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" name=\"Does it meet criteria?\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <incoming>sid-F226E847-2506-4D6D-A759-D787243EAD7C</incoming>\n         <outgoing>sid-FC7B38D5-7383-417D-8157-DAFADD605E4B</outgoing>\n         <outgoing>sid-85967F7A-4BB4-4860-9C86-671BC8892D6D</outgoing>\n      </exclusiveGateway>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'readyForMlr\')}\" completionQuantity=\"1\" id=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: PENDING QC to READY FOR MLR \" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-FC7B38D5-7383-417D-8157-DAFADD605E4B</incoming>\n         <outgoing>sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'readyForMLR\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Piece is ready for MLR.\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'draft\')}\" completionQuantity=\"1\" id=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: PENDING QC to DRAFT\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-85967F7A-4BB4-4860-9C86-671BC8892D6D</incoming>\n         <outgoing>sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'notReadyForMLR\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Piece is not ready for MLR.\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415</incoming>\n      </serviceTask>\n      \n      \n    <serviceTask id=\"servicetask6\" name=\"Send Notification: Coordinator reminder\" activiti:expression=\"#{documentService.sendNotification(execution,\'readyForMLRReminder\',\'coordinator\')}\">\n    </serviceTask>\n    <sequenceFlow id=\"flow8\" name=\"\" sourceRef=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\" targetRef=\"servicetask6\"></sequenceFlow>      \n      \n      \n      <sequenceFlow id=\"sid-F226E847-2506-4D6D-A759-D787243EAD7C\" name=\"\" sourceRef=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\" targetRef=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\"/>\n      <sequenceFlow id=\"sid-D8055B00-59CB-4604-A217-C0C44E4F218D\" name=\"\" sourceRef=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04\" targetRef=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\"/>\n      <sequenceFlow id=\"sid-B60C9535-C150-4B52-B391-2A45A1DF2A06\" name=\"\" sourceRef=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\" targetRef=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\"/>\n      <sequenceFlow id=\"sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415\" name=\"\" sourceRef=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\" targetRef=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45\"/>\n      <sequenceFlow id=\"sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF\" name=\"\" sourceRef=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\" targetRef=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\"/>\n      <sequenceFlow id=\"sid-FC7B38D5-7383-417D-8157-DAFADD605E4B\" name=\"Yes\" sourceRef=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" targetRef=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\">\n         <conditionExpression id=\"sid-87f8fded-4359-4ffb-8cd7-c8dd1fc4d4eb\" xsi:type=\"tFormalExpression\">${documentService.all(execution, \'outcome\', \'Ready for MLR\')}</conditionExpression>\n      </sequenceFlow>\n      <sequenceFlow id=\"sid-85967F7A-4BB4-4860-9C86-671BC8892D6D\" name=\"No\" sourceRef=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" targetRef=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\">\n         <conditionExpression id=\"sid-925e18ad-e84a-44de-951b-b9e34abd3c48\" xsi:type=\"tFormalExpression\">${documentService.any(execution, \'outcome\', \'Not ready for MLR\')}</conditionExpression>\n      </sequenceFlow>\n   </process>\n   <bpmndi:BPMNDiagram id=\"sid-6543adbe-9e4b-4ca5-8393-86755e55b4d2\">\n      <bpmndi:BPMNPlane bpmnElement=\"submitForMlr\" id=\"sid-5c9125a0-8c36-41bc-9956-eb6dc41661ec\">\n         <bpmndi:BPMNShape bpmnElement=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04\" id=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04_gui\">\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"103.0\" y=\"129.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\" id=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"180.0\" y=\"225.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\" id=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"300.0\" y=\"414.5\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" id=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7_gui\" isMarkerVisible=\"true\">\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"500.0\" y=\"434.5\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\" id=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"686.0\" y=\"414.5\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\" id=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"686.0\" y=\"104.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\" id=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"470.0\" y=\"255.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45\" id=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"470.0\" y=\"104.0\"/>\n         </bpmndi:BPMNShape>\n         \n      <bpmndi:BPMNShape bpmnElement=\"servicetask6\" id=\"BPMNShape_servicetask6\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"900\" y=\"97\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"flow8\" id=\"BPMNEdge_flow8\">\n        <omgdi:waypoint x=\"825\" y=\"124\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"900\" y=\"124\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>      \n      \n         <bpmndi:BPMNEdge bpmnElement=\"sid-FC7B38D5-7383-417D-8157-DAFADD605E4B\" id=\"sid-FC7B38D5-7383-417D-8157-DAFADD605E4B_gui\">\n            <omgdi:waypoint x=\"540.0\" y=\"455.0\"/>\n            <omgdi:waypoint x=\"686.0\" y=\"454.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-F226E847-2506-4D6D-A759-D787243EAD7C\" id=\"sid-F226E847-2506-4D6D-A759-D787243EAD7C_gui\">\n            <omgdi:waypoint x=\"400.0\" y=\"454.0\"/>\n            <omgdi:waypoint x=\"500.0\" y=\"454.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-D8055B00-59CB-4604-A217-C0C44E4F218D\" id=\"sid-D8055B00-59CB-4604-A217-C0C44E4F218D_gui\">\n            <omgdi:waypoint x=\"133.0\" y=\"144.0\"/>\n            <omgdi:waypoint x=\"156.5\" y=\"144.0\"/>\n            <omgdi:waypoint x=\"156.5\" y=\"265.0\"/>\n            <omgdi:waypoint x=\"180.0\" y=\"265.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-B60C9535-C150-4B52-B391-2A45A1DF2A06\" id=\"sid-B60C9535-C150-4B52-B391-2A45A1DF2A06_gui\">\n            <omgdi:waypoint x=\"280.0\" y=\"265.0\"/>\n            <omgdi:waypoint x=\"350.0\" y=\"265.0\"/>\n            <omgdi:waypoint x=\"350.0\" y=\"414.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-85967F7A-4BB4-4860-9C86-671BC8892D6D\" id=\"sid-85967F7A-4BB4-4860-9C86-671BC8892D6D_gui\">\n            <omgdi:waypoint x=\"520.0\" y=\"434.0\"/>\n            <omgdi:waypoint x=\"520.0\" y=\"335.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF\" id=\"sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF_gui\">\n            <omgdi:waypoint x=\"736.0\" y=\"414.0\"/>\n            <omgdi:waypoint x=\"736.0\" y=\"184.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415\" id=\"sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415_gui\">\n            <omgdi:waypoint x=\"520.0\" y=\"255.0\"/>\n            <omgdi:waypoint x=\"520.0\" y=\"184.0\"/>\n         </bpmndi:BPMNEdge>\n      </bpmndi:BPMNPlane>\n   </bpmndi:BPMNDiagram>\n</definitions>\n'),('122',1,'Vault-Test-Workflow.bpmn20.xml','117','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" \r\n             xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" \r\n             xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" \r\n             xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" \r\n             xmlns:signavio=\"http://www.signavio.com\" \r\n             xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-7bef7376-9427-41db-8394-3de98e53008e\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\" \r\n             xmlns:activiti=\"http://activiti.org/bpmn\">\r\n   <process name=\"TestWorkflow\" id=\"sid-3806d577-ddd6-479c-85f6-11805b79addd\" isExecutable=\"false\">\r\n      <startEvent id=\"sid-64325C52-DE2E-4295-9079-C8C25306742D\" name=\"Start\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <outgoing>sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A</outgoing>\r\n      </startEvent>\r\n      <userTask activiti:candidateUsers=\"${groupService.getCandidateTaskUsers(execution,\'reviewer\')}\" completionQuantity=\"1\" id=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\" implementation=\"webService\" isForCompensation=\"false\" name=\"Verify Piece\" startQuantity=\"1\">\r\n         <documentation>PM-SubmitMLR-QualityControl</documentation>	\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\r\n         </extensionElements>\r\n         <incoming>sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A</incoming>\r\n         <outgoing>sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56</outgoing>\r\n      </userTask>\r\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" name=\"\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56</incoming>\r\n         <outgoing>sid-60F7520F-7792-475F-9E43-B59408B7D40E</outgoing>\r\n         <outgoing>sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F</outgoing>\r\n      </exclusiveGateway>\r\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'inMlrReview\')}\" completionQuantity=\"1\" id=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\" implementation=\"webService\" isForCompensation=\"false\" name=\"Update Doc Status (ReadyForReview)\" startQuantity=\"1\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\r\n         </extensionElements>\r\n         <incoming>sid-60F7520F-7792-475F-9E43-B59408B7D40E</incoming>\r\n         <outgoing>sid-E5617A55-5B8C-4599-AA65-BB847858AED7</outgoing>\r\n      </serviceTask>\r\n      <endEvent id=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB\" name=\"End (good)&#10;\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-E5617A55-5B8C-4599-AA65-BB847858AED7</incoming>\r\n      </endEvent>\r\n      <endEvent id=\"sid-5E2E046B-9380-4584-A037-D196E92AB302\" name=\"End (bad)\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F</incoming>\r\n      </endEvent>\r\n      <sequenceFlow id=\"sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A\" name=\"\" sourceRef=\"sid-64325C52-DE2E-4295-9079-C8C25306742D\" targetRef=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\"/>\r\n      <sequenceFlow id=\"sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56\" name=\"\" sourceRef=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\" targetRef=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\"/>\r\n      <sequenceFlow id=\"sid-E5617A55-5B8C-4599-AA65-BB847858AED7\" name=\"\" sourceRef=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\" targetRef=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB\"/>\r\n      <sequenceFlow id=\"sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F\" name=\"Failed\" sourceRef=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" targetRef=\"sid-5E2E046B-9380-4584-A037-D196E92AB302\">\r\n         <conditionExpression id=\"sid-a1188d86-b470-4645-b51a-33c5d64d5727\" xsi:type=\"tFormalExpression\">${pieceVerified == false}</conditionExpression>\r\n      </sequenceFlow>\r\n      <sequenceFlow id=\"sid-60F7520F-7792-475F-9E43-B59408B7D40E\" isImmediate=\"false\" name=\"Passed\" sourceRef=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" targetRef=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\">\r\n         <conditionExpression id=\"sid-61fb3b25-e618-4f43-b0b8-dec5d94df430\" xsi:type=\"tFormalExpression\">${pieceVerified == true}</conditionExpression>\r\n      </sequenceFlow>\r\n   </process>\r\n   <bpmndi:BPMNDiagram id=\"sid-c14dc061-b8a6-41f5-841c-45a0544d01eb\">\r\n      <bpmndi:BPMNPlane bpmnElement=\"sid-3806d577-ddd6-479c-85f6-11805b79addd\" id=\"sid-5d36db64-e793-4274-a948-b95a943640c3\">\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-64325C52-DE2E-4295-9079-C8C25306742D\" id=\"sid-64325C52-DE2E-4295-9079-C8C25306742D_gui\">\r\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"30.0\" y=\"103.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\" id=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191_gui\">\r\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"120.0\" y=\"78.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" id=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D_gui\" isMarkerVisible=\"true\">\r\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"300.0\" y=\"98.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\" id=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E_gui\">\r\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"270.0\" y=\"222.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB\" id=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB_gui\">\r\n            <omgdc:Bounds height=\"28.0\" width=\"28.0\" x=\"511.0\" y=\"248.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-5E2E046B-9380-4584-A037-D196E92AB302\" id=\"sid-5E2E046B-9380-4584-A037-D196E92AB302_gui\">\r\n            <omgdc:Bounds height=\"28.0\" width=\"28.0\" x=\"510.0\" y=\"104.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-60F7520F-7792-475F-9E43-B59408B7D40E\" id=\"sid-60F7520F-7792-475F-9E43-B59408B7D40E_gui\">\r\n            <omgdi:waypoint x=\"320.0\" y=\"138.0\"/>\r\n            <omgdi:waypoint x=\"320.0\" y=\"222.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F\" id=\"sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F_gui\">\r\n            <omgdi:waypoint x=\"340.0\" y=\"118.0\"/>\r\n            <omgdi:waypoint x=\"510.0\" y=\"118.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56\" id=\"sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56_gui\">\r\n            <omgdi:waypoint x=\"220.0\" y=\"118.0\"/>\r\n            <omgdi:waypoint x=\"300.0\" y=\"118.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-E5617A55-5B8C-4599-AA65-BB847858AED7\" id=\"sid-E5617A55-5B8C-4599-AA65-BB847858AED7_gui\">\r\n            <omgdi:waypoint x=\"370.0\" y=\"262.0\"/>\r\n            <omgdi:waypoint x=\"511.0\" y=\"262.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A\" id=\"sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A_gui\">\r\n            <omgdi:waypoint x=\"60.0\" y=\"118.0\"/>\r\n            <omgdi:waypoint x=\"120.0\" y=\"118.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n      </bpmndi:BPMNPlane>\r\n   </bpmndi:BPMNDiagram>\r\n</definitions>\r\n'),('123',1,'PrepareForReview.sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa.png','117','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0�\0\0\0_\0\0\0�\r��\0\0�IDATx���\rp���m����@�t��H(��e�Q���FL$V��R�BSIQ\Z���JZQRM1\"Ey\Z1� WK!@����8�C���b5%\'���}6�99	���K���gr�������������0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�G��kѢ;*�L)|s���^��D\'S�U�)�!C�]|�ȍC�ˑ\0\0���O�s�ر��ɒ��2���߿CD�P�n���eRU5S�\r|4+k���&��\0\0q��v��A�Ζ��Y�\"�E�.�]��Mo��\'mG\0\0�y:��V�AͻjiY%��������\0\0 &�kX[	����{�}b�e\0\0��7��5��5L0�]UU��855u+G$\0\0��5�7�p�`�+33�}s��sT\0\0���1z�0a,�U^^�d�*�J\0\0�GP���j`y�v�<V�%���,(ȐyW���\\��^{���.i�\0\0����:��gf�2eII�,6k�5�d���uY�,_4�@��8��.	pT\0\0/u9��\\�!��������\'�/ ��f�//=�@�B�O+\0\0��08��t�Ts�|P�@v��&���\0�5�����I�e���Na\0\0$k|�l��?|G>�^#�jxD���W�o|H�]�ܚ\'�cܑL\0\0IW�?A�~A�Y-mo�0k�����+d��E�9� \0\0H�0�E�IfW>,+�/���Ԋ����jk~^�U\\$�����?�M�#\0�d�G�h��/o��-��?�+��JV�Ud����v�d�d�<��^���A\0\0�<a0�#�7�dp�����d�\"��)�%2�/\"7�\"r�k\";����+�˒��%�e��0\0\0�!nٵU�M\\ �w~)O�)o�;�L��)�r�ʠL|��Yo��UA�a��Җ:�a\0\0$Cl��9X7G_Z\'�f����ȍ\n�/֘A���\\���\\U�=,2�z�|�i��#�`G\0\0���a���I��e�#��5\"ׯʵϚA�6(��)^&2g��2~�r�dK��3�`G\0\0��,�7�\Z&S�Η�����A��Q7�5�\r�_�#��.�����\0\0  ֱ@�9�(wּ*���ˈ��r���r~�\Z^�RF=\'7ܻYZ?�n�K�#\0�$\n�a\0\0)� \0\0 R�A\0pG�Y3�Zmֻf��\'A�Y��zɬ\n{>\0�A� \0$I�*6��^�%fM4+׬t���3ǬB����|fMf�A� \0��V~�ҕ�I�ҀWoV��RO�f5��ͬ��s�0H���J\"(ϲU�Y������3�^��\0a�0�;�lu($��p8ͬ<�����5�{a�3څڂ�\Z�l����O����F�˛�q\0	�\0��B;�9C`���R����!�0*٪�n9./7�^.-�\0a�0�;)		q���V�ӡ�k		��f+\rlڥ���1�^~.�@$�4[9���z��.S����]Ʈe+]pE�7�{C\0 $;gh��Bt���b{��<�0���\0a�0 ����sy�yF��爳�&�B�6�8��A�0H�ܜ��UQ����K��:���ܑ��ҁ�s8N���8.!$��Z�UG�]���D�.c�3����:�\Z�7�&�\n�\'�sȳKO4��A� ��=W�p�W��Y����H2��\rY�����Q�a��$������J��>%nO�����q9f���|�	����%�A� ��9W�똞���u|�ig+�&��\r����x�t�q���i��������:M߱c����C��o�����dI�)r����{�8#U�;�[R[{�	��0 ���m�����>}��䏊ǍIQT��$�h��e��M�4}ڴbY��Έ��������\ZQ�liYe���3-Z)��י��A 	��l2گ����?��r�h��yfo�ݻ���;�i������c�?x�A.���VӖ9m1t��_|P���0��(��=�SKdC�r��O\Z�ꪭ�Ç���߅[G�;�˯����.�O� �(��[�e�k��<���@Í0�5s�u�wt�.\\x��r�Ϭד&��~��{��\Z�a,��0��2��.[�kH��7�t�<�h�I[�4�^}�Xk=B�nu��~�ҹ��n~� �(���A�&���h��\\n�ψ�޽+��\Z5j�����?�r��g�]G7����daP�����~��i�Jj��G�ڥ���.�:vL?vlw�φ��0 J�ꄻ��q���%��0輑D�!tv�v��.����A�	.��]}�]��QNCױcz�g��O�su3�H���ԍ�ݽ�,m�������\'����w�t����\r���v킈�`�u,--����?�iz��	�\0�$�@�x61�� �F���٭�-xή�Hàv��]��Uڅ{:a0�:�ߡwE��ԕ�	�\0�(��M��ϻ��uP�W\'s�\0�\r�^�;דּ��Wt\Z@7Q�j�:���$y��[ŭl�3��;��r�\0�\'�5��*:�5a@��9��>ýaft8��s��+�*ǬF��.Gj���\\�\r�{�AG���lu($�E\Z5:��[��Vz{�~���;r���=	�\0��V��\\m!<�.c���Eе��P�vps��p���9\0a�0�*\r	��Zo*����|�C�!��V�b�YS�!��Z�<`D�E�0H�3�V���9\rwuvN�V��.d�\nε?����x���aO�U���\'�͑��Y>FG��fp� @$�3[I�l5�^q���\'����,�t������Q�<ߵWT+w\r�A� \0�8[N1�<[��ٷ\Z�=�`�1���#k3g���E\0� a\0�V\0��A\0\0\0� a\0\0�0H\0\0 \0\0��A\0\0\0� a\0\0�0H\0\0 \0\0��A\0\0\0� a\0\0�0H\0\0 \0\0��A\0\0@$�\0\0a�\"\0�n�gϴ�߿� �ֶ��.	pT\0\0�~�>�b�X���x��%M�\0\0�3cƌZSYYF��**\Z�K�8*\0�g�����=��@`�,��?%%���.��\0\0���\Z���z�,�UX8�)sW4p4\0�X���8�Hc�2�Y��{j����ǡ\0\0be�����!zSRR�3�!� \0\0�y LK;����1�F�\ZA�k�\0A\0\0ē�=��9=������ƦM�`B����c��a�f�F��a\0\0�F�Ui�6��F��1��J��qu��\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0���\0�	�(�E/1\0\0\0\0IEND�B`�'),('124',1,'MLR Review - V2.startMlrReview.png','117','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0�\0\0e\0\0\0��]�\0\0C�IDATx�����e�7�i@.J��X���X�-1*⒔i��H���N�ǲC�AQ��x��T�yT�/���#�yA��p����܆d``F�y��g�vf�s�������[�ٳ��޿�������\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�\\���n��o=zث���Yܮ]��V�V�Z����s�y��]ػ��u\Z\0�y�<Ɵ�c��h��{�u��իW�0y��!/�P\\�\"��Zb��1?v�2e|����=z��j�.?K�`���c�)�ř9��ݻw	S�N��+5�a�i�~:u���c�\\��\0��[�1���ע����>�$My�j͚9�:}�z��B\0���y�?e�5{����M�x�߾}��O�`���c�)�9�/4�ǂǧ�5a�Ք)㷵n��i	�u�:�񧌿f+��*��ر��_�;w|/��Е\0X����k��)��Y��?ٍ3&sJ�q�ƅ�i��Ըq#W���)�\0�u���3�����;�ɵ5��I�¼y������-/�\\�u�\\��c�%���D�d=���O݇�t%\0�y�<Ɵ�g�5K��]������*��i^^^���>�}w}hݺuX�xqrݲe˒�mۦ���{R�_���:o���3���檆#�\r6$���SKÖ-[�˗_~y؜�\\RZ\ZF��*�n�����L.����\Z� �|�\0�u���3�Z^�G��rKh۶mغu�_L�m۞�K��￟\\�w�����˗{�^�`�W�y�?�O�W��T�̙33O��ڵ;̞}_ry���aĈ��;��yسgOry�aƌaΜ9\ZY�`�W�y���3�4~�!����-[�&����n����u�օ�\n*}�F��\0X�u��3��?��yv��4y�xȐ!a߾}����Q�F�7�~�c�5���:���Ɵ�g�i����׿���;t��\Z��+�H>WX�#s��\'jd��u^Y�?������_||�uׅ�^Zv&L|1r���ߞ9��o�M�ƍ��X�`�W�y���3�4�G���Q�5y�pg����;+�8����K/M.������Ϯ��.]���_~Y#k|\0���:o�Ɵ��_���z�@|��hSᮐ���PT�����\n\nBYY�?�|X�hQعs�q�s����\Z\0뼲�Ɵ��?z�>�n�$MBᮒ���8���/\n}��\r��rJr�k���S�\Z\0뼲�Ɵ��+7~QQQ��}|���]��7և��{3�aGў���kC�nݒ����ٲe����\0X�u��3��?�����g��Y�����_����i��\'O��\\W>��>a�֭�2�����+��g�\Z?K�?����������]�Z�lY())��\Z\0뼲�c�\Z_i|\0���:��g���5>\0X������J�`���c�)�O�+��u^Y�1��?4���:����\Z_��u�:��?��4>\0�y�<Ɵ2�4���\0X�u���C�k|\0���:��g���5>\0X������J�`�W�y�O\Z_i|\0���:��g���5>\0X������\Z_�`���c�)�O�+��u^Y�1��?��4>\0�ye���3�h��_�����s?ڶm�8���#w6�{�\Z(���	�z��Z�f�o�ߥ!����:o���3�����\'��=y\0�����֭�����M��?��B��mk�M׮����}��G�gj|\0���y��g�G}�w��!�Ě5s*]�y���|>��˩�~\"�X1�R3O�~c�޽K��Ǐ�|����d����p�1����Ckl���Ol�+��4[���F\rN����_W��V���竻�Νφq㾕�߳}m�9����u��\Z\0�u�:o�Ɵ�O�}�M�<�?������]6(�������������dp�˃�O>7r䗓�|����kթ�}��������T������Ҕ����%�gͺ9���}N8�S����:o���Ɵ�w�7~��K�y\n��o\'�U��B���&�	����{������v�>�j�������,_~w8��Ӓ�e�����:o���c�������n�ts��s���.�Q��O�ݐ�_�q�6,��\0X���y�?��6~�KJ<V��7�%D�>\'\'��c��Ǔ&��s��~������~��k���Ǖǧ�/�mr9^?w�q퓏7mz�̘�����+�&�����\n��}S�Ϭ�e��u�:��g�-��~���T��\"^�/�\0H�\08�]�)�ښ,��9~�x\Z���yЍvl���c���3U͘��\rl��f����FO_�xf���=?s���~���:o���Ɵ��7��:����\Z_��u�:��?��4>\0�y�<Ɵ2�4���\0X�u���C�k|\0���:��g���5>\0X������J�`���c�)�O�+��u^Y�1��?4���:����\Z_��u�:��?��4>\0�ye�7������:����\Z_����:��?����:o���SƟ�W\Z\0뼲�c�\Z_i|\0���:��g���5>\0X������J�`���c�)�O�+��u^Y�1��?4���:����\Z_��u�:��?��4>\0�y�<Ɵ2�4���\0X�u���C�k|\0���:��g�q�mۦ��x��k�{�K=$��\0�u�O��g��)/�.��j��_>�zHV�J\0���y�?e�5K\\pμɓ��xM������C2EW`���c�)�Y�ٳK��=������|�[����M=$t%\0�y�<Ɵ2���=N\\=u��׈5l�w����\0�u�O���;�+?�&l��˻{A�1ؘ���\"\0�y�<Ɵ2�Z�A]�vޡ��|�������aZ\0�u�O-��۴9�w��m�?�ǂ�?]�Q�`���c�)㯥:�U��-�����c�~�/O>���޿���\'�\"9�Ū����XpO�`���c�)��뛪ɩz.Uũ\n�+��k|��x�dg��:o���3��?�������\0���+�������;\0�:p���|���*w\0X��F������.g�\0�<`�A�=�s�|�_W#\0�y���:��W�t�p�\0�u0����T��|w\0X��ꢦ��8F\0���u�xN�o��LW\0`��?8(�]\0\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0`��?��\0�u�?��\0�u�?��\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0`��?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0@�f�k7}��7�=�����,n׮M(_G�!T�V�e�{��r�y}��}�@��}6h|\0h�����췫W�a���C^�]��xEa�:Ċ�c~��0e��Ч��{{�8���]~���>4>\0�83g޼�{�.a��	��t�Px�kڴ�N�:�>��6��>�A�@�\n������6���՚5s�	\'t� �Ӆ�g���f/��./d�oߦ0���g���f�8��v�5��Xa��jʔ��Z�n����>4>\04[�l��>^s��չs��R�\0]�}6h|\0h��[�ĳ�f=ٍ3&���ƍ�\Z�S�ƍ\\��O��J�A�@���2�ImsҤIa޼y��~���E��W�ݺuK�{�ǒ�YRR\"`d=���O݇�t%�٠��Yj׮M\r�sBQQQ���Xyyyɿ�>�lx����u��a����u˖-K�ݶm��y���Ju%�٠��ٮ�Շ�6lؐ�Ƨ�Z\Z�lْ\\����������0z�U�u�׿>�`gr����0��W���\0-6`F��rKh۶mغu�3�۶m�<�����\'��ݻ7�x����0���\0f劇�Μ93sX�]�����%��F��\\��Ο�={�$�.\\f̘�̙#0\n��g��\0�R�Ij���a˖���x��n�-�nݺu�킂J_#0\n��g��\03vv�΄���!C¾}�2��ˣF�*���_�)0\n��g��\0�g0���7��z@���+�����~�ĉ���}6h|\00+����/��*�(,LP<�O���ߞ9s�o�M�ƍ��G��>4>\0�ջ�F�?;�~��$2�s���x��K/�4�_���=3w%��v��%�������}6h|\00��믿��Sm*��W��JBX�<?s(lAAA(+����-\n;w���ܹsF�l��\0 `~t�l|-e:L0 	�1p�*	;������зo�p�)�$���k\"+`b�\r\Z\0�����(9L6�f׮]Û�C�й�hOx����[�n����`nٲE�0���\0�\'�I�-6>�y�w�ο�K��iӦ�O��+��� |��}�֭[Ce���}6h|\00��\'�x\"��e~~~(,?�l�e˖���S��>4>\0�J��l��\0 `\n�`�\r\Z\0L%`b�\r\Z\0L%`�}6\Z\00L���\0��	�٠�@�T&�٠�@�T&�g��\0S��l��\0 `\n�`�\r\Z\0L%`b�\r\Z\0L%`�}6h|\00L���\0��i��}6h|\00���}6h|\00��	�٠������3<�诓��O�1s]Ò�(`b�\r\Z\0����cZ��㿝||�C�D�<��\n�۷m��	�`�\r\Z\0�@�0��p�9}���������m۶	_���Ê���ի�.����\n�7/Mn���S?����u����=N�Z�f�W����N_��w��v�Bp����v�|6���бc���+�g���0G�������s;�Ն�\Z��\'����v�]���Y����%�+(x4���3���tHܾ��:=;Z��P������u��_Kn7k�͞��l4>\0P��9s�Mɿ�F\r�<r�!m����g��J�������=�3Y�cN�g����/�\\����MOUk��0��YՅ�}�^0�>�\0�\'`.]�����Z��,fՐv�Iݓ�7lX�5��g/��^;‐V\\��Ɵ�M������bm�C]fu?����5�`���\0�0�����l!-^�S�p�X�P؜*����#G~��ɂ&M\Z[�ϟ:uB�{w\\����r�.�Lcu�C��\r\Z�?�x���a̘�~��~V�J��ſ���ݚB���}6h|\08jfmg��?���k���k,�=?s����Kj{�%_�7/K��x�lu����ē�<����g����Ivj���n��G��?~l�޽K(s�\"��ge;�O|66jk�&�٠�@�Tަ�A���)`�}6h|\00L���\0�0���\0�0�>4>\0�&�g��\0S	��g��\0S	�`���\0L�A���)`�}6h|\00���}6h|\00��	�٠�@�0�>4>\0�&�g��\0S	��g��\0S	�`�\r\Z\0L�A���)`jI�A����L�A����L���\0��	�٠�@�T&�٠�@�T&�g��\0S��l��\0��ڶmSV\\�B�k�{�K=$���l��\0�,�q�)�����@-X��\'R�*]�}6h|\0h�.���y�\'_/�5�\Z>��S�]�}6h|\0h�z��ҿgϮ%��+��ƭwrssצ���l��\0�l��q��S\'y�XÆ]pw�X����\0���;�+?�����w���c�1UgiE�A�@K0�k��;��#.sssR��0-�}6h|\0hQ!�M�c�q����5���5���n.���\0-��Z}lI��n;�y���m�>�\r�>��H��b�O�_s�X�A�@��7U�S�\\����QuhU���}.�[�8[,�٠��&���u����A�\0+>XZ��vw�}6h|\0�`�����U����\0�~9={�.gd�l��\0@�=�s��tⳘ^�	�٠��:���e�F�{�>4>\0PW�s�;�|w�g��\0ꢦg/��A�\0u�x-��e�>4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0���\0�u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>\0`��4>\0`��?��\0�u�?��\0�u�?��\0�u0�@��u0�@�\0�y0�@�\0�y0�@�\0�y���\0�y���\0X����\0X����\0X��4>\0X��4>\0`��4>���B^���o�a��a���fq�vmB����Z��-�ݻ���뻰w��4�<����`�Z�{�麁���իG�<����wW(.^BX������?;L�2>��s��=N|5u�����`�7烅hqfμyA��]�ԩBi�J��\\Ӧ�(t��a��Ƕ�D�Y���o�Т6\Z����䯭6G�֬�N8����`�.�΃9ߜ�ً�Hſb�h4ކ�}�6�9��΃9ߜ�YOT!�]|�M<D���x5e��m�[�~ZGZ���o��l�3Ɠ;x�M�W���K=$t�u���|��\0�R<-}<s�ž�kܸ��R�]i��ƙ�3fL�7ƍ�g7�c��z�=��i�� .�9!�vD���v\n��1�������l\Z��y��U��:�3�dҤIa޼y��~���E��W�ݺuK�{�ǒ�YRR\"`���\0|$��v]�����~~I�p����TM����!Ä/tLo��\0zHJu�u\Zg����(�le������g�}6����кu�x���e˖%�n۶M�4�c��h���b8ch�0;,�\n�?\Z��p���v����ah��vX����6lؐ�CO=�4lٲ%�|�嗇ͩ�%��a�諒�֯/|������o�-`���\0�?`�����?\r�\"�8���\\tb�0�ÿ~��p�N��h�`�a��f0�[n�%�m�6lݺ���m۶g��|�������ݛ|�|�r�Ț��\0\\����3C��?^����_����Gxg����w^�V(�笄6�yh�s~<�u�̙��bw��fϾ/�<|��0bĈ��w�<�ٳ\'��p��0cƌ0g�s�9@���[.	�_z0l^=7�~��T=�\\���C�Ϸ�Y�ٰ�C3��s*��r���a˖���x��n�-�nݺu�킂J_c7�c��s�,)]~8�W�q��3C������?��/��?�{^�a�ٰ�C3�;�vg�d�xȐ!a߾}����Q�F���ү�4����\0�)`�-Yf=�,��,���(������?�T����=��t�ß��B�͆u�m�L�_��f��СC�W\\qE�����O�8�n���P{�,+[X�N�|��\r��B��Fׯ��!\\�(�Iτ�����䯾�Y�$�~����f�:�n����/��*�(,L�xҟ��o�=s��7�X�Ѝ7:ɏ9@�沕O�A��E������0.�,\\�W�]RF?Z��pY��}���ũ�9�,\\<��0��66�yhfs���F�?;�~��$2�s���x��K/�4�_���=3w%��v��%�����ps>���+7��������z<�I��k��������\n��Y.��/|mFY��B�u결�ɛË7�e�`�a��f6������a�M��B���BQIK��g�-((ee!<���aѢEa������;w�9ܜ������Z*`n��0r��p��]�y!�z�,|��T��Y.�c#f���?�	CF?\n�M/��L�\r�<4�9?$��L��$�2��]%aGQqr��_���N9��v�\\s�Cd��Xx\0�07����>�\r�������,)WW���i_}4��wo\n���ٰ�{ll6Z����`w-)`%������ڵkx�`}(ܽ7:w�	���6t��-�}|�-[���|L���X�J�î]�a��\n.{ �9ln���?�O���p8g��U��$�����6\r6-ܠT�jj����%���Y�l���k���p�E_J.O�6-|�^��u�S���n�\Z*3���0�w��.ӡ2>��EȤ%�\'�x\"��e~~~(,?�l�e˖�������=S��T�MպT�,��ũz\'U�SuC��0L�\r\Z\'\\�v=��U���#R��<Tޓ�o���Tu(�|��W��5��vy���80l6l68��\"ӟ��L������\Z�jM��պ��ݪT=��ӌ�f�f��.k���Ĝ�\Z}����9�ym}��{��ǳ�&�͆�\rgPN��������o�o0�[�5�\Z���-�~�\Z7&�͆�\r.����21�#9�,��\Z�az�_�d\n�\06&G>\\6�׃9ߜ_�gp���o��?��0l6Lꭶ����|s~��bo8̍_����#`�l�T(l��[=��9_�9?�I<[l�#�<��8TV���0�oL�ˏ������9��|u����G�/Xq2�,��	`�!`R�p9�BH����!���21���{��?O�#�@�r\Z�DB��͆0[r���*,B=Bf�p���>�5���U����u�3�pM�9���D�l6���\\U���ŚBf����r{��Ĝ�\ZlΟ��o�����Ƒ�	���2η�0�z��rp=Bc��Y�pY5d:\\s�9����x��iG��z��=�M���9(�o:�l�z��B^m!�����ߒ�L����C�3U�p�şWl�	��Q��H���ݶ��GK�W\r�u\rwՅ�܃�i^��9ߜ�Ao\n�֐��	4��Fſn�o�!`��~0Ღ���!��C�}��o�oq�`Z\0�G@��lT�����l6�n��p�1�C��mî]���ͭ���l!�P�eՐ�5�4��������:�x��3��q�2�{\r��$`�h�pX���ԩB�\'&_3cƿ������)	�Mh��_M=�1\Z�����[�������������㿝||�C�D���<ݔ�sg���G@K�7��\\�*_���\\���OK��>�>��\'s���ߚ�V�r+m.��~���+_�|h۶M8��O�+f���Z۷?�l���\"~�Q�\'�e���?�����ڹ��0nܷBǎ*]_���f���L�^�������̺�Ofu<�I�����8;3���k�?�\\��ë�n��?��.�lx�������V��>�<��w��v�Bp]ז���)<	���0�n2r�0_|�d�/_|�����+VZ����ꮿ�A���&����A}��_���OM*^��նɨ�����Z��Y7����Mjc]]����Μ��\Z̃	�^�I�����G�������s;�Ն�\Z��\'��逹���&&�\'������U�|0�Cm�um�nMh\ns~�T�S�dw���r{	O&pT��T��a���\"x��#�LN.?��������R���޴���Tw}տ2Ǐ��T�1ٷ磻*~��̊ߧ��_�l��z�j���3�սI����\'d:�,�1j��fμ)��G��9r��#Z⡴�����=�3Y�Ϫ�~ժn����P��y0kKS|���#8����9�I���y��gq�0�	}�a�U6��u��+���^x�p�luק���Uq����K�L��`f}���٨����Bfm����粶�ɬN��I㌃Z��K!���Ŭ:G�tR���\re�K㳗��������l/u�6���w�K�����TO����&��<���J�I���\Z0�f!}tM�xuf#O���w\\�J\'�v}���&�=�$B��>U+��S~��ſ��?7hP������1c�W�d��/�����|�u���mpr��zk�U�3���˜����tGxhjs~��޽�5��.���᮱�stN�Cd��#�\\㼟�dr�����tu�C���4��um�nMh�y���������ȴ�z��s��\0����$`��ʬY3��u���Ϝ\"f��O���x�S���_��W�����]��/�1���Ɗ�ӯ�Y������w�l**��xF����\'t�y���mm�_�l��{&��Ჾ!3��K�l�������2t��9��=?s����Kj{�%_�7/�:�g;D6�|_u���w�z������-խ	M)`�J�Ɯ��Cs8�-���O&`�h����f�ɬz��U9���l!st���5�4逩��?2g�	z5p��*��W��4>�G����2+���bX�K�̩��	�:�s���0�P�d�-�~?�L��a�a�A�U=\\6���9�둪_w��	�|s~��g2�!�5���z���ˍ�Ux0�͆�\r2U��oA�T�u���K��_��$��&����#ݜƧ�0��)d��\n�|s~�|�<h��\\g��?��i��P���a4��)�ZW,����=��G���̣š��XL�d�����~�文����}/U���?����d��7l6l68�!���@:��2�O�#@�T6T��ۃ9ߜ���>Le�!d�_�v����ʜ/<��\0S�l�����8���o.0�9_xr�E�f����L��\"`*s���>�66r�.0�9_xr�\r�\r�\r9dN.0�9_xr�\r�\r�\r�����[� `*s���>�66�N�Le���G�y�f�f0�9�&�}�7l6l6\0���|� ����l6\0S��\'� `*�\r@�T�|��}�7l6l6\0���|� �`ްٰ�\00��m�p���\0Le���G���l6\0S��\'�`ްٰ�\00��m��G\0�1o�mۦ��x���	���/�-���\Z�9ߜo��h���g��)/�.�}�~�D�!Ye\0�|s�M�#�Y�\\pμɓ���7�\Z>��S��\00��m�p�r��ٳK��=�������7n�����6��0\0s�9�&��l�=N\\=u�~#ְaܝz(�\Z�9ߜo��h����;�+?���*/����`c��2\0s�9�&���A]�v�a�q�7\Z����������|� �G@K�7�is�?��{ۼ>�������6\Z�9ߜ���>Z�qz�V[ҡñ�ǎ��_�|�w�gZý�Y<-}<s`����o\"����O�#���}S59Uϥ���wS�V�\r��{����;s Д����6A�#��\0`�>0o\0\0���\0�\0�M��0o\0\0���\0�\0�M����\0`�>�\0\06A�#��\0`�>�\0\06A��\0�\0�M��0o\0\0���\0�\0�M����\0��>�\0\06A�#��\0`�>�\0\06A�#\0�\0�M��0o\0\0���\0�\0�M��0o\0\0���\0�\0\06A�#��\0`�>�\0\06A�#��\0`��0o\0\0���\0�\0�M��0o\0\0���\0�\0�M�#��\0`�>�\0\06A�#��\0`�>0o\0\04�&(��vӧ�x����^�����v�ڄ��AB�j�[ֻw�-��wa��\'�� `\0��Mн��t����v���#L�|}�˻+�!�V�X�~�ϟ�L��9yo�\'������� `\0��M�̙7/�޽K�:uB(-])�6�G�S���=��%ZL\0��	����>�<�&��Z�fN8�N��a�L\0�f�	����g.�����۷)�q�,�\0\0�yO�_s��\Z��L��u��Oku0\0��&(�-6���k.�:w��^�!��A�\0h����V$�l��COvcƌɼ�Ƹq�B����Ըq#W���)�L\0�f�	��sߊ���9iҤ0o޼�r�~�¢E�Ë+W�nݺ%�=��cI�,))0��y���U�L\0�f�	j׮M\r�sBQQQ���Xyyyɿ�>�lx����u��a����u˖-K�ݶm��y���J�;�\0\0�uTC�	aÆ\rIh|ꩥa˖-���/�<lN].)-\r�G_�\\�~�{�v&��~�m� ˆL\0�0�[n�%�m�6lݺ���m۶g��|�������ݛ|�|�r��\n�`n\00+W<�u�̙��bw��fϾ/�<|��0bĈ��w�<�ٳ\'��p��0cƌ0g��Q�\0@��v��?~زekr9��\'���nK�[�n]x������&�\0\0f&��,ڝ	���!C��}��e���G�\ZU(�K�S`0A�\00x�}3�w�С�+��\"�\\a���\'N�(0\n� `\0��O�s�uׅ�^Zv&(��\'���oϜ9��7�&t�ƍN�#`��	\0 `~T��nU�����;��xϽ����	~.����r|����ܕBۥK����/�&�\0\0��z��ד��a�M��B���BQIK��g�-((ee!<���aѢEa������;w��(`��	\0 `~t�l|-e:L0 	�1p�*	;������зo�p�)�$���k\"+`��	\0 `V�EEE�a��0�v�\Z�,X\nw�̈́�E{«��\rݺuKn�s˖-��	&\0��y�I~�g���`^����/�Rryڴi�\'�\no�+��t��u��P��(`��	\0 `f	�O<�D�����PX~V�X˖-%%%��	�V\0\0S	�`n\0�	0L@�\00��	&\0���L0�\0�	�&`�\06A��	�\0\0�0A�\00��	�V\0\0� S�L\0\0S�L\0\0S	� `\0�J�s+\0�MP���:_�0\0�P7A���\0S�L\0�C\r��Z���M�N�s+\0�MP�f�g4�0L�x\0l�5`V|Fs@m3}�G�u����7f��cXL0\0��&�Um��cZ��㿝||�C�D�<��\n�۷m��5��ٔ\nL\0\0�\r�R5,U�ӷ�-�pv8�>��_����c��mۄ�|��aŊYY���]����k�͛�&��_s꩟�|Mպ��[C�\'�V�r3�+~MՀ��w��v�Bp����v�|6���бc��Ml�k�	\0@րY5X��5`�=4	]o���0v�7�\rU14��O8������&&�\'����e�\rJ>WP�h�o��gf��鐸}��uzv��ߡ����YU�꫿��n֬�[�!�6��\0\0T\n���\Z�%X�9`ΜyS��Q��#��y@H[���p�٧%���dy�0�s��L���S��l?��?�|>>��i�S���~����~Vu�w߾�L0\0�\n�=cYt���k㳘UC�I\'uO>ްaQ�p����^{�BZq�\Z~|6���%�����u	������Qp��oL\0\0���-��ݛ�	��BZ�>��᮱⡰9U���G��r��M�4�Ɵ?u���:������x]����~���4�����Ø1�+���������ŋ[�a�5�\\�8\0�Q0k;;����]�vN^c9t�����T�]�P�K.�BؼyYr�m�Ce�;D6�&�|����\\?cƿgN�S��P�vk�>>��cC��]�@�S��l?+�I~⳱�P[[�0A�\00���>�6��\0\0���7�q\0\0S���=\0 `*L\0\0S	�6��\0\0���7�q\0\0S	���\0L%`��	\0��)`�|�\00L�o��\0\0�0A�\00��	&\0\0��i�\rz\0@�0m�A�\0�J�\0@�T��7&\0\0��i�\rz\0@�0L��\0\0�0A�\00;��\0\0S���=\0 `*�8\0���L0\00L�o��\0\0\r�m�6e��+��&P�w���CRj�\rz\0�Y:�S6���%�5�Z���O��U6ߠ�\0��.8g����xM������C2���8\0@�Գg��={v-)-])�5n�����6����=\0�l��q��S\'y�XÆ]pw�Xj�\rz\0��;�c��v����\Z����^�z6��,�o��\0\0-���];�2�|����-H���l�A�\0���٦�1����m��������b76�pi�	\0�!;�U��-�����c�~�/O>����\'����2�I<[l�	}�k.�j½`�	\0@�蛪ɩz.U��1uhU���}.�[�h=`�	\0\0�|�\0\0�o��\0\0`�\rz\0\0��F�\0\06ߠ�\0���8\0\0�|�\0\0l�A�\0��7�q\0\0��=\0\06ߠ�\0\0�o��\0\0`�\rz\0\0l�A�\0\06��q\0\0���8\0\0�|�\0\0�o��\0\0��7z\0\0��=\0\06ߠ�\0���8\0\0`�\rz\0\0l�A�\0��7�q\0\0��=\0\0�|�\0\0�o��\0\0`�\rz\0\0��F�\0\06ߠ�\0���8\0\0�|�\0\0l���\0\0��7�q\0\0��=\0\06ߠ�\0\0�o��\0\0`�\rz\0\0l�A�\0��7�q\0\0���8\0\0�|����vӧ�x����^�����v�ڄ��[B�j�[ֻw�-��wa��\'��\0\0@��|�{�M�\r�oW�^=���ׇ���Bq��ju������aʔ�O�����q⫩��,=\0\0����̙7/�޽K�:uB(-])�6�G�S���=��%z\0\0h1��.O;��3l�ߑ�5k�N��A�!��\0�f������g.�����۷)�i9��\n�\0\0p4n��	}�k.�a��^�Ք)㷵n��i=\0\04��w<[l<���\\6~u����C2@�\0\0�r�ߊ$�-V�k�\Z7n��C2E�\0\0�r���2�I]CТ��oG��wh����í_��>N@l�z晻��zHV�q\0\0�Yn�۵kS���|���_�9�3�s�+U�.=>�lH�0�����\r�>����T�\0\0�u�]�\04ch�0;,�\n�?\Z��p���v����ZH80\0@���~va��䌟�\r�^����.:1L���_�tj��\'�K_L\0\0������ś�\n�xaF�K^��ᝅ?��y9���Y�l�3�\n�&\0\0�u���\\���`ؼzn���C�z8����o. \n�&\0\0�����Uᇓ\Z�/�Y=3�1?�ݯ�g��\r��~��P��%!Q�0\0@������\n��Xz^�X_���k��O*^~�w��_~:��O��n!S�0\0@��Vee��K���Þ��awx`C����!\\�<���0�V��;����=���W	�&\0\0 `V�e+�����0���^a\\~Y�.�,\\��,�~�,|���������SAsNY�xĝa��ǅE\0\00+�+7��������z<�I��k��������\n��Y.��/|mFY��B�u결�ɛË7�%,\n�\0\0��Y�^K��O�\ZF��n[�+\\1/�Q��oݟ\n�3��ea��~��=a��B����),\n�\0\0��Y�6����>�k~>7&/|gIY��B}��N���߾{Sx��φM��#,\n�\0\0��y`�+��v凉3�+��p氹��C�>=x^8c����W�ǒ����\n��a\0\0�0L\0\0��0L=\0\0���L\0\0@�T��	\0\06���)`\0�ͷ�)`�q\0\0@�T&\0\0 `*S�\0\0�oS�0\0@�0L=\0\0�J�\0\0L%`\n�\0\0`�-`\n�z\0\0��4U����	\0\0fC��P4���	\0\0��t-�����`\'`\n�\0\0`�]��Y���LS�\0\0��C\r���P]��x�c�i��99,^�ۆOM9�	�&\0\04��w����ҥ�O.��qbK���-�\0\0�� ȮJհT�N߮���o�K��m�$o޼4|�+�O>>��O�+f%ןsN��v���`����L���=���ڷo���O�E�k��W��k_��\Z�W���~��C��]��?~lX�fN����п��ɳ��G�Sؾ��[�ߡW������y��гgW�`\0\0GE��\Z,s�\Z0�M�Qry��/\'_v٠�もG�cP���v�w����k��\'N�:��W?��=�����ە��y���������1���\Zo{�	�*�������\r�|.�_������S�\\��w��g�I.�{�g*}&:D\0\0h�3?U#��:�t�gcH����V�\\���u��\'�S\'%�7mz*k���{����&ץCh�k��\\��w]���p�٧%�FV�UCcŏ�ϒ�ggk;D6���?�pZ+`\0\0MQu�X�+`�CS�<�����O��R8����u�����fμ)\\|��\rj5}��!�]�����g���kYדN��fÆEu��0Z��`�d9\\8}���	\0\0Pǀ/�]�H�_r���3��+�$��4i�_7e���	�fͺ���Y��HjC�W�znr9�~2~�l�k����ᶱv�|�ƀy��}���?orHo]�o�?|G�Å�}]u!�.aT�\0\0Zt������`�}��aԨ��sM��o{<si�g\'+޶��_��nƌ�\\��.��:�/C׮��g>�=?�j���,�?�\'�\'�9�S���K�|�����_/\Z�-0\0\0�0U㼅��	\0\0�-�ⳖOB$`\0\0����R\0\0\0S	�\0\0���L\0\0\0S�\0\00��	\0\0�J�\0\00L\0\0\0S�\0\00��	\0\0�J�\0\00L\0\0\0S�\0\00��	\0\0�J�\0\00L\0\0\0��*)y1��֟�]��L\0\0\03S1,��zU�:\0\0@��&\0\0���L\0\0@�T&\0\0��)`\0\0�^�)`\0\0�S۶mʊ�W8�l�ݻ_�[��-Օ\0\0@�t��l�˻�3�M�,����d��\0\0��.8g����{�c���/x0��Lѕ\0\0@�Գg��={v-)-])�5n�����6��Е\0\0@�գǉ��N� �5b\rv�ݩ�b�n\0\0���;v<nW~�l��l��˻{A�1ؘ���\"\0\0��ڵ�2[�Ydc����-H��ô \0\0ТBf�6���;���)�&��=��N�a��K\0\0��:�U��-�����c�~�/O>�����}2���\\Ʒ\"�g�-?�O|ͥ�b\0��o�&��T�ġ��*���>��H�-\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�����Ny��\0\0\0\0IEND�B`�'),('125',1,'Approval Workflow - V2.startMlrApproval.png','117','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0Y\0\00\0\0\0t��\0\09qIDATx���t��/�D造�0T�ҢG��S�bZ��Җr�L����z\\ږC˱c���Z[��L�eT�u�j�FT��Ǻ�eUNA�V.�D��~d�!f������,vv���}��}�\'���.*\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0h\"��馫�=z�����Rz��#ݭ��ڶmSq�	���s}�p±i\0� sr\"\'n�����o[��=cҤ+����(-]+�^Vڎ%%3c��qѧϱ;z�<���&?�Q`2!\'rB+5}������-�L��O�>��S�9�t����ڟ���A� sr\"\'��Оx�\'��]��_����8��.��v�0G!`2���9��H���_E�����c�������A����	-_z�dz^o:�,D�&O��]�v�8\"s�2!\'rB��L�^8�y���v��Nn�pT� eBN�,]�3]�����vcƌ����رc�0alL�;rEn�NvT� s�99�Z���\n�ҟ�w�ĉ1w���v�~�b��E���O��G����dA.++�=�G��Ϲm��Q	���A� �DNh�қ�~o���[���&m���ٿ�=�X����Ѯ]�X�hQv�ҥK�7n�(�{����W��A� sr\"\'�luL�W������Kb�������??��n������e����;���[��o����aU�\0� s�99�Zkp�k��&:t�6����ƍ���Z���f��ر#�xٲeNA.�9����	�[[�S�ӧO�:��m���9�������cĈ��o�y|����Ĵi�b��ق(�\0� sr\"\'n-QV��͋��7d��+����_=�X�j��D�0���9ApkD[�n�\ni�xȐ!�s�Ϊ���Q�FUu���}Qp�A� �DN���_��:t�G�{�d�ۼ�����0a� \n.�9����	�[ۋ)/���x�����ف�^\\�w���W]���_ɂ�f�\Z/�\\\0s�99��f���[1��/�nޒE���fd�R�{����\\�]�Ӱ-;Eݭ[�x��gQp�A� �DN����/f҇���n�%+_��e���T�j^�jUTTD<���p��زe�s|�̙#��`2!\'r���<������Ц o�V�m-;f�E߾}��˾�K.q\nZp�A� �DN�ڂ�u���4tz���ݻǫ�ގ��wT������/�G}t���һ������Aȉ� �^L��2M��ȥ��~�q����S�N�O�;^}}U|�S}bÆ\r�;A\\\0s�99��`p|�����������+ؤJ�8^VV&��`2!\'r��\n�����9HN�� ��`R� 9Qr��*�0���9Ap�����Aȉ�Wp�A�$\'JN\\�\\���Aȉ� �Jp�A� �DN@p���Ar���\\\0s�2ɉ�W	.�9����	���dBN�� ��`R� 9Qr��*��A����	���dBN�Wp�A�$\'JN\\�0)s��(9��w����g?��ѡC�8����{nl�s����=z��m۶������x<��A� sr\"\'���sL��\0�ӟ�ǆ\r��+��F��G�Mt�ءί�޽k�s7mzd��L�0���9Ap��ܹS�W��^������-��|���������3v�M7]=zt�����������O�Ci�G�3�u=N\n܅�~��Q�������U��W�|��ٲ�;�������M?\'�����!?Sp�A� �DNܪ����U�8������ݷj��ٿ)�����+�p�ۃ��>7r䗳����xᅻ��\'5}<�����t_��4$T����e�g̸���-��yd��L�0���9Ap�jɒ�V��7�[�}5�*�>�/$���Ν��{p��8���\'U]?O�[��Ͳe��i����u����.�9����	4(���\"�H��.-]������܍\rn�y�M�B�y^�z�����Aȉ��	{��א���W_��H}��ݟ�g�>�8���3�f��x���O.����#�N?/Z���v�/}���;f�]��w̘�����տ&�ם�i�Ti���3r[p�A� �DN8H�;k�\r�%4�_6�:��\0�_И�[[�Tt}!I�M��./:o�/�8��g�����*��_}fڴ�z�7���KGT5����n��:�ˊ��3�dBN��U���dBN�� ��`R� 9Qr��*�0���9Ap�����Aȉ���\n.�9H���D�	�+�\0� eBN��U�`2!\'r�+�\0� e�%\'�����9HN�� �Jp�A� �DN\\%�\0� sr���\\\0s�2ɉ�Wp0)sr\"\'�\\\0s�99�\\�0)s��(9Ap���Ar���U�`2!\'r��*�0���%\'�����9HN�� �Jps�2!\'r��*�0���9�\\\0s�2ɉ�Wp�A�$\'JN\\%�\0� sr\"\'t:th_QZ�\\h�Am���r���Q	���99�Z��O>nmq�͂���~�`n��pT� eBN���3O�;i���j��3���ɎJ���Aȉ�Ђ��խ�^���˟�[o�i���.��A����	-\\ϞG��2e���\Z6��[r�b��0)sr\"\'�\'u�|�����Bt\0�����r�`M�Nu(� eBN��cP��]����6mڬ�m�aA�d2!\'rB+o���������~���z+O;�Z\0s�99�Z��ڶ���N�[w�e_��C�f��fh��VH��LW��|�dz^���\0� sr\"\'$��jR��UiѮw�V{W����{+�K�2\r�9����	4[�+��Φ\0�r��W�����9\00��\0{g^�_O�9\00��\0{�_�_�2�/W�r��E}�b����`9\Z�����k���9�h�EE�/�Yb�\0`9\Z����x�/\0� ������fr�^�9��a\0`9\0s�	 �\0`9�\0s�	 �\0��@N\0���	.\0��@N\0��r.\0� �@p�r�\0� �@p0��\0��9�\\\00��\0��9�\\\0�A \'���9�\0�A \'��`9\0s \' �\0`9\0s�	 �\0��@N\0�\0s�	.\0��@N\0��r.\0��@N@p�r.\0� �@p0r�\0� �@p0��\0��9�\\\00�����9�\\��m0��Co��+G��|����zh����^T۶m*N8����}��N8v�#\rk5@p�����췭w�1i�Q\\|s��.���j/+mǒ��1y�����={�|n����Z\r\\�Vj�����ѣ[L�2>�˟���:���K�N�;��9�>��\0�h�\r։\'~\";Ӣ���K���#������X���J���3X\Z��hu��~s��b�.@+�����\Z��A\rρ�ɓ�ml׮�#�H��\0�h��U�E.���W׮�����J��\0�h��e��Uk_��n̘1U�%;vl�qjL�;rEn�NvTb�.@��+]���&k�ĉ1w���v�~�b��E���O��G����dMWYY�&k��Go�sn�pTb�.@��h���`Elݺu�7�-..��}��⭷ގv��ŢE����.]���q�FM���Vn��;*�V���݅��W��\Z��^�ׯ�n����.w���<F��(����߉��ߒ�~�74Y{X�R��\0�h�MVr�5�D�bÆ]g�6n�Tuf��w���۱cG��e�<]P���\Z �\0���*=�o���UOܶm{̜y{v{���1bĈ���7�<>������bڴi1{�lM�&k5@p4Y�,���7o^�_�!��.��w�u�e������ƪU�}��I���\Z �\0��\Z�-[�W5T��!C��Ν;��K�G�\ZU�T�m��I���\Z �\0��g����W����&�.�>�y�{U_?a�M�&k5@p4Y�]����/�g�Y�mޜ5�By�_}�_~���	[�f�_h��V@�U��z�Uy����[�����fd��^�{������]冀-{:a�n���g��4i��V@�U�^|��l��a��E��bkY��e%UO\\�jUTTD<���p��زe���̙�i�da�.�&����k��\rՀ�+5]����{[K����Aѷo�8�㲯��K<]P���\Z �\0��ښ��[�fOL�ս{�xu�۱y����뽭��/�G}t���}�֯_���da�.�&�Ѕ/�WLg�.���㌳��ݞ:uj|�����ⓟ�6l��i�4YX����*�d=���Ug�JJJbs��S-]�4���4Y�,��\0�0vkn4YX���&K��j �\0h��&k5@p4YJ���\Z �\0h�4Y`���&K�e.�Z\r\\\0M��da�.\0�,M�j�����d��\Z.\0�,���Z\r\\\0M��da�.\0�,MX�����R�,��\0��d)M�j�����da�.\0�,MX����\Z��ʱ=��߲�o�骪�\Z�0h�4YX��H�r��\\�����R�Ks�f�����ʯ��QM�!���q㾕}|�C�E�u�;vh���da�Fވ\\-�l�n��7ruR�:U~>��;W�r5���su�M����&k�������d��?\r�H���C����c,_>��&k�����g:^x��X�nI���{�?�諭�f�qǵѳ�QѶm���Jߓ��}��u}]m�`m?��ڲ�;��ѹs�&o$ͥX�8�qZ���*�v�����Z���su�M����[�=4k<^{�޸첯l,R��?��.i`n�yB�8�AI�wޠ�s�Vݟ�ۿ�)���|��i�#\r:KV��P�ǅ~Vͺ��e_7c���da��J��՚�]O�k�����qV��dչ�>��ٿ�F\r�{��#�ʲe��i���=�����F�)��g���橨���~������L�ڵl���\Z�d�Y�\Z��;��da��\n��V�5V����V>ޥ6-���*��_��ٿ�M�lV�F�czd�^���\'��J�^z鈏4*��������Yg�CU�S�Y��whH�U�gj��&k��odeCԻ��w��:���h�j]���QR�d�֨����=�/UzZ`Q����#G~y�hL�xY�?ʔ�U�u����O��}�3N�~��_7hP����^c���w,��jV��-�u��0���i��VkR#���7`=~���?ɦFp4Y5��]5p޼_F��]��\\\rzFՅ#j~]�i����X�ni����O,�t��5��f�Pu��i�Ruቺ~��_��+���?~X���-k��j<]���Uۅ/�Y�|cW_c���Z�yJW�r���\Z��mj@��\\�k��.]�=]E����+�A@��4YX��r�,ְ���9���h��&k�V�WѮ�R�ۏ?3�aqo���d)M�j�Qz�Դ��3\'���!�\0h��&k�bN����fzj�|���d)M�j�Qz�މ��g���;6=���R�,��Z�-�괟f�y�6=���R�,��l����-�\\\04Y�,�V+ș,�}\0�&K��jM�k�4�\0�,MX�5!Wt��\0h�4Y`�ք�O���>\0@���k�&�+Wo��~���)��4�\0�,���Z��ZR��)|�à\\=��>\0�d)M�j�و\\�T��f��Յ\Z�\0M��da������I��=^����\\\04YJ�����;Wkr5`=~���?�A�}\0��R�,��#�v]��>h���^��>\0�d)M�j�1�\rQS���[�x?r�`\0h��&k��U:���h�k����S�5Xk*7\r��&Ki��V;��.�ui�t������t����]�8�A�}\0`���h��V㯾Q�l���M+�u��sթ��*�ԈM.��Fé�\ZSԲ�\"蠱\0�di��Zm�ꕫ��jNe�UZ�ӿ��j~Ѯ��v�`\0���da�\Z�\0�}�C�����58͠�o�O�]R��Z\r\r�@v��ǭ-.�Y����~�`n��pTb����\0�;����N�t�&����g�U��u�`����\0���ի[�^�����?��9��f�6m^)j��Ck54�\0JϞG��2e�F�\0ְag�R���`����\0�8�s�÷������*.���>X��S�X���>\0h=u���=���o�ڴi�*���9�V�A�}\0�\n�����\r7|o��h���`U>Ep�k54�\0��Im�~lq�N���첯�ᡇ~���h5��`�˴��V^�\"��S�V�A�}\0p�蛫I�z<W��c�ڻJo4��+]��U�V�A�}\0@�5�rjgS��\Z\Z�\0\0�N:#T^9\r�9�Z\r�}\0\0{g^�_�����\0k54�\0�~E=��/W�k54�\0��E��D:��Y`����>\0�F��,V�F�<`����>\0��YTT�R�%6X�ᠱ\0���:��Y`����>\0�F�_T���� X�ᠱ\0�r���\0\0s�	\Z�\0�A \'࠱\0�r���>\0\0s�	\Z�\0�A \'࠱\00r���>\0\0s�	\Z�\0�A \'8h�\00�����>\0\0s�	\Z�\0\0�A \'8h�\00�����>\0\0s�	\Z�\0\0�A \'8h�\00����>\0��	\Z�\0\0�A \'8h��\0\0s�	\Z�\0�A \'࠱\0�r���>\0\0s�	\Z�\0�A \'8h�\00r���>\0\0s�	\Z�\0�A \'8h�\00�����>\0\0s�	\Z�\0\0�A \'8h�\00����>\0��	\Z�\0\0�A \'8h�\00����>\0�r\Z�\0\0�A \'8h�\00����>\0�r\Z�\0\0s \'8h�\00����>\0�r�����g��Лn���ѣ�=߿�)���>*��j/�m�6\'��k��>�w�	\';Pl0�\0�q��`p����^>p`�m�{��I����⛣�tyD�T{Yi;��̌ɓ�E�>������s��T��`�:\0���iZ��:}������-�L��O�>��S�9�t����ڟ#B�X�q�8M+\\ShO<�Y�.T��^zivyd��s�`�a0`\0�i�4�dpM���_E�����c�������`�:\0���iZ���^8��כN;с�ɓ�ml׮�#��1��u\0��Ӵ��5]�&�p��z|u�����. N�X�q�8M\\ӥ?ӕij?�j7f̘�K]�;6\n���ر#W��dq�����&k5e�6�6Zzo�t����;q�Ę;wnv�_�~�p�x���裏��{��� ���	�֣�����6\\!N4�1ࣵp��������v�_��~�G�s�X6X���q\Z�k^z����u���ޤ���8�������z;ڵk�-��[�ti��ƍwߛ!���ŉ�3�^�����9]�֑]��\\M=���ِ�1���Λ�ɱ1�:\0�Ռ�\\�{�)b���Y~xI�_�>�}���Ǻ�����=��쾷�~\'�Kv��7��=,5�kؽ�\r�3s��/r��φ�:\"�vT�p`��_g����Z�8���!�M����СClذ�7n��kɻﾛݷcǎ��e˖9-���&�ggu����$V/�E<5���>*�����+>T|X����Z�8�����S�ӧO�:��m���9�������cĈ��o�y|����Ĵi�b��ق(���&�+N�ҵ��_���oɿ���5�\\�����1���F�NW�2X���q\Z�k��-����y�������酕y�]w]v�믿o�Z�����Һ��9ל���+֭�۟�;W��ۛ��;�f���Z�8���!�ݲu{UH��C���;wVݗn�\Z5�2����+��K�i���W�\'�*��/>X9=J_������3_��\'?����cc�u\0X��1�6��#��ٿC��Hp/����s�7�W��&LD���4Y;�VČ�F��K�巷��-f�xxV�����%z}�����Z�5Z�\0�\0�V3Ncp��Ŕ�_~y<�̊xo����J/�̻��뫮R��˯d�^�f�S\n.��ɪ�Xw..�c�=��z{ܹ:b��W<qɲ��FL|4b����د>�޷8�?\\!��\0�\0�V3N�ɪ��z�U���w7oɢx�m3���)�=���vz���iؖ���֭[<�쳂(���&k�ӏĠo�\"�<�a��cK*��⊸tqE���\"�5�\"�q�Θ�(�lͮ�/��1�-�/��\0�\0�V3N�ɪ�^|���@�0����ۢd���,b񲒪SͫV�����\'�x\".\\[��z��9sQpiM�sW��X7�G��cL���daķ���97�`�gE�;cg|mZE|�W�NY\Z�:���Ty6X���q\ZMV�S������0 m\n��me�����k~qP���7�;���.�����V�d��k�6=|m��lf\\�d[\\07bԬ����kzE����3#~t�1d���y�x�G�ȳ1�:\0�Ռ�h�\nw�֭�i���ݻw�WW����\n�{[?��_|%�>�����{/�w\\��u4Y�^��q�܄���̘���⊸�Z}��N�������x��O���n�gc��dp�~lS`�f�F�ՠ�V�2M��ȥ��~�q����S�N�O�;^}}U|�S}bÆ\r�;A\\Zz��jgyIl�V��>�wg�2lN|j�ŧύ�ϊӇ�..��ű��\'���ec�AdP�6�jJ��l�Ռ�h�\Z�|��\"%%%���\n6��;����	���\n�,e��+�X�3Y�5ZX���dY`	.Ȟ1��o�����i4YJp1y+c\0�����k�0N��d)��䭌4�����rF�q\ZM��.&oe�����}��`�6N��R���[�k�|���i�4�,%����1��o������h��d	�\\���j�w����8��qZ�%�Jp1(c@�o���~�ha�6N��R���[Z5/Z�\\]T�oc��>��8m�F����2�\rְj��m���F4Z��O��q�8�&K	.&oe8h�����h��7X�\Z]��^��q�8�&K	.&oeh�\n5>�5Lu5Z�}��Z��k�0N�i�M֒\\�!���A�`p�7��\Z܈Ʃ�F��\rV�F�S��i�4-���)�Wp9(�%c�1�q_��6:�5Z�m�����E�h9^���iZT���E�Z\"0�K랼��\0�}�\rVC�B�V�=l��F��j�6N�B���-9Cp��n�6�{�`��h��\r���>��qZ���[��%�n����߲�o�骪�\Zx\n��҂ǀ�a���C�Eǎb۶\'d�В���=sT[��7\rV�F�k���j�i\Z�f]�HiQ5nܷ��/�pH��G�M��k��kf�m�E+[\Zz|N�2>z�<*��i����L��i�h��I�){��6���z����1žQ�j�,Z��lE�_��5�t��������>��\Z��@�?�С}|�+�˗Ϩ5�+W�.�>����wǺuK��M�s��W�=5�;��um۶�z��=5��~���������U[m��X����ܹS�N���(-o�7�P�N;��X�����Ǆ��S��B�o��ԬM��i�N�=j��쾺�_�8Q(�u=�ɻ��}�]Mpo�d5�\r���r�Z�Y��[3�E\r\r���C�������.�z��5�1��]>��o��=N:����7(�ܪU�g���J�??���=�����ϪY_����f̸�`����u4Y{4�멧n�&�t�_�L���=wW��)t��7�qjVjz��͟?%�t;�W_�k~�P��z|�w����\r͖�}�=i��&��j�f�F\nnI�F�`\Z���Z����?r�/[vK��t����K:�?�ٿ�5����Q���/��_g֮}�`\0��\Z�B?��`�s�3�,ZB��Wc@�.�tD�y����Y7d����_gv\n�_(��}������c7��*������澚�MC��L{mWlL��ꂎWk5k5Z���zר����\"��BR��?��ǫW/�54�/#�ߴH�y�.��秿T�u�?|��I��\Z�B?�Pp���\ZLZ�o�1 U��E�Ϳ���R�e��������iH�����4Y�y|�w���jk��kp�{���aqM���O���Z�Z�ַ���@ڱ����J~�?��*�j.�q\n:�9�˻�(s����}�}��?�c���v�/�W�B�Cͯ4����o/�1c���;�Y5+�}����i�M�l��{\\���_P��	.���e�������ǩY�i.E�O�K�,�|\nM}ٯ9N�x]���7yP��Ij�\r7���?eq�]����\ZYp�ͼy���ݻf���CϨz1bͯ˟�>��ǺuK�S��OE:��&��1=E)��Y�Ōu�5��W���ѣ[Ԣ\Z��k�Y���2����_�5Y��&+=���f�v��ϫz�u���?�%���7�qj�kj\Z�צJ���ۯ+�5ǉB���5Y�B�3Z\rm�\Z�h��k5k5��*�5�h��KD���Oݻ����U[�5����\Z,���iWi��d)��A�hU�F���1o4\\��\\��q\Z�U�k0�d)��A��SS�4���e��}{��\\X�)�4��4Yh��ɻ�4Z{���\"�q�8��*M�,e��h5��\rk5e�Fp�&M�2y�X{�?O�8m�Fp��j�4Y�@�F��g�\\��q\Z�U����d)c\0�4L���׃q�8��*M�,e��h�/W[���0�i�4��W���R�\0�5���\Z\\���k�0N�\\%��,M�2�p��hy� �i�4��j�4Y�\0c\0M�hi�0N�ApW���2h�Fk��q\ZWp5Y�,c�1��1�r��L;�i�4��j�4Y�\0c\0M��M�q�8\r�+��,M�1�\0X�)�4�+��,M�1�\0X�)�4��4Yh���0N�\\%��,M�2\0�j�8��\n�&K�e0\0�j�8��*M�,e����iWi��d)�7`�f��\\M�&�`\0�Քq\Z�\\M�&�`\0�Քq���C����˅����O�)�K�5Y��mo0\0�j�8M;�����,8͠���v�\nM�s�����2N���<����&]!8͠�?��.���bn{c�1\0�VS�i�X�^����ս���i�9��f�6m^���,��7\0k5e�f���S���XÆ�yKnW,9X��mo0\0�j�8M�;�s�÷����P�ŷܗ�kru�&����\0�ZM��u���=����mӦͪ��f�����\0c\0`������o�_n��{=�w�?����ZM�����\0�ZM��Nj��c�;u:l�e�}�=��5ޛ���[!]�3]����y�-ᴳ&�����\0c\0`�f�6N����դ\\=����ŞڻJo^��[!]��%]�F�upn{c�1\0�V3Nh�l{\0\0�B�\0\0,���\0\0}�\0\0�з�\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0,���\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0,���\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}��&\0\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0,���\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�5�_����V�L�,\0\0�a�媼�&k�ͤ�\0\0\ZnQ\rVI���D�,\0\0�����d��y4Y\0\0@���ڬ�Z,g�4Y\0\0����Y^���\0\0�¼\"g�4Y\0\0@�P��\Zlsh�\0\0���m��X�,\0\0�	����d\0@�]1G�7�tՕ�G{��SJ=�}]o��\ZXm۶�8�^�?���N8�؁�,\0\08�v�O/8�߶޽{ƤIWDq��QZ�<\"V����KJf����O�cw��y��M~�&\0\0Z��ӯ��G�n1e��(/Zc��k���.]:m?���h�\0\0�6X\'����L�h��K/͎#���~Q�~/M\0\0T��\"��`i�\\�ձc��E-����,\0\0�ZG��5X�)�\Z�W�\'��خ]�G4Y\0\0�¥���\\x\rց��];�S��͕5Y\0\0�R�˴��jr|�;rEn�L�d\0@��+]����������#z���%~1�s\\��q�Mc5IMP�>z˟s�d�&\0\0Z��F�\r}��n�a����q�Ȯqs���{D�lH����q�M�4JM�>Z�]R��\0����M����cf���E�����TG��Î��\Z�묿�(5A�ІE�\0\0{�d��N�д���E����� ���G��A����q�珊˟�(i�\0\0@�����+N�ҵ��_���oɿ���5�\\�����1���F�NW(�d\0\0\rn\0�\\sNlz�X�rNl��\\��noz�����$M\0\0��&��|E�pү���⃕ӣ��yYm�?��+��~��(����&\0\04Y�-�w���.�^����oo��?�Z��������{K���#���(�k�4Y\0\0��*�诨Xw..�c�=��z{ܹ:b��W<qɲ��FL|4b����د>�޷8�?\\�a�d\0�&��Z��#1���9O}��B�ؒ����\".]\\�﯈oͪ�oܾ3&,�5[�+�#n�yK�k�4Y\0\0�ɪ����ĺ�?��̘cr��%#��@E�Ϲ��?+��;�k�*⋿��v��������U�j�4Y\0\0�ɪ�^�5Y��6F^63�[�-.�1jVE|�\\�5�\"��]Ĉ�?���2��ؼtR<�S4L�,\0\0�d�V�^��q��䒟�g�ǷW����ەu�W����s�~:�>w��I�\0\0��B���$�m+�	�~λ3N6\'>5��S���Ƀg������������j�4Y\0\0��R�,M\0\0h�4Y�,\0\0�d)M\0\0h��&K�\0\0�,M�&\0\04Y�,M\0\0h��&\0\0�di�4Y\0\0���di�\0\0@��4Y\0\0��R�,M\0\0h�4Y�,\0\0�d)M\0\0h��&K�\0\0�,M�&\0\04Y�,M\0\0h��&\0\04Y��6l�}rH��رCl����J�\0\0����)S�GϞGe�Ĵi������j�\0\0�&��N�%K~�-�O?��G��;�6?�c���3V��]�?w�=7F�^ݳ�6mz$.�pHv��C��1j������3��?��]���q�ǺuK�+_���{�?��b���d\0\0ͷ�z����?����>�[��o�/�M<�ح���~�����o�P�sRS��?JV�v�/}�뾓}���\\�}<a���_� �}�y��ϭZu�o���h�\0\0���d]z鈸��I��Y�n�\Z��~��ݚ���Fz�Vc?�*��J����LV�v�/}����e��d�׮}x���W�{4Y\0\0@�k��E.����j��\'�WZ�|�f)����l���\\]MV�9����\'g�M����Z��K��_\0\0\0ͺ�JO�7�[�ݗ��WT�T�|���s��r����\Z��>W�qG�Z�t�E�~��N��??y��t�3���?��+�?q�e�,\0\0�y7Y��M/�4{���O�0പf)=�0�QJ_�z���\Z��>W�q�E.RS��6U����E�?�i~�S���J_�^�U�i��,\0\0�Y6Y\r�tyc?�}�\0\0\0MV-�?����i�\0\0\0M��d\0�&Ki�\0\0\0M�&\0\0�di�\0\0\0M��d\0\0�,M\0\0���d\0\0�,��\0\0M��d\0\0�,M\0\0��R�,\0\0�d)M\0\0���d\0\0�,M\0\0��R�,\0\0�d5�*+{*^{��FW�>M\0\0�ɪQ�a�lB\ZU��4Y\0\0�&˙,\0\0@��4Y\0\0��R�,\0\0@���\0\04Y^���\0�֫C�����]]����O�)���;*\0�;������LV3����價]��Q	\0\0-ؙg�>wҤ+�����gޕ�%��\0\0Ђ��խ�^���˟���z�M�6��v�\0G%\0\0�p={�rʔ�\Z�XÆ�yKnW,q4\0@�pR�·o+)��5Y���o�/����T�\"\0\0���w��^sn�Z��S�զM�U��s�C\0\0Za�վ�!���ml���jeg�ެ|��\Z\r\0\0�n\'�m��ŝ:��˾����͚��>Z-�}��e��U+/r�^��)�\0\0p�蛫I�z<W�E{�4=��Jo4��+]��U\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0���?#�h/iZ�!\0\0\0\0IEND�B`�'),('126',1,'Submit for QC - V2.submitForMlr.png','117','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0�\0\0�\0\0\0I8�2\0\0<�IDATx�����e�(�i@.����!R��z�6�?��%�KR�z�������X���j�8F�V�W�D(����dL�76up�	r�8$������;k��a��f�������Z/k�\Z�z�����VT\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\04o!�t�:��[F��r�~���С]��E�G�6���:���}�����:}��\0\0h�z��7�wg��=�ĉ7��������B+E#D|oKKg�I�Ƈs�9}oϞ���|X\0�g�������5L�|s��\\��k��2��ᤓ:�:��vWX#\0��P�}��ӭf�����^�N9�w��e�5\0\0h��.�q���y5�;�+/�+,\0\0Мœ��c(�.�����&��ڶm�笥\0\0@���\ZO����]�t~+���[S�qN��4��/�Ess\'��5S�ߘ1cr�ʸq�Ba��#�q�F�H��I�T�c�S�;\r;�K~ќ�/cb���TN�0!̙3\'�ݷo߰`������C����eO<�D�dVTTh*0���?\'��\nk*�1ʩٝ��%�h�����_Ƅ�cǎ�b����?�.]\Z֮]ڶm.\\�.[�xq�s�֭����\'yO+��\0�h����NÎ��_�\0�|�!�_�>m�}vQؼysz�ꫯ�����a��/��֭{+������o���l��4�\0K���iؑ_���TFw�qGh߾}ز�j�֭��\\����鲽{����,Yb�WM%\0G63;5�Ӱ#��������N�>=���Ν��̙���F��޾��={����ϟ�M�f͚%_4�\0�fw\Zv����4�EՎ��;wnؼyKz;��\'��;�L��Y�&�QVv���M%\0���ٝ��%�hMM���rY�2$�۷/�,�5jT����=�R�h*8N��4��/�Ekj*��?�N:����k�I���|[���v�\\�Tp8���iؑ_���Tf�x���W�m��i���d�u�]�3�������ܰa���T�X��S����_򋖒Wk׮\r�2[!�.ߞfʃ�H�Ǔ�\\y���x\\e�;;��c�v�\Z^z�%y\"�\0h�o|���D���_���z��W����ɽ��;C��W��YR�˫����/��BX�`Aؾ���ٳg��@CO�N�n\"F~�/Zڗ5���lٿ����Mf�Ί�m���1?9(���\'�q��㮿�z_��5\0\Zcr.rjv1�K~��x�r�6~aӭ[���l](ߵ7�;�v�	/��*w\"��eM��\\S)�\0h��ש�M��/�EKj*��O�Ry×�\Z.��S��)S�����;�^S>��s-[\\�G���Eo�S����_�cQ�4dS��SO�򦴴4ɣ���������T�5\0\Z�_�f7#��G#_2�����WB�Ь�^�f7#��M�TfcaQ�-��g�@K)z���D���_4aSY}��%�J�@,z���D���_4����������ʕ?\r_��о}�p�yg�����ןZ����>в�x�����{�m��5����\Z��ש���/���\Z*Q�K�vZ����|�ɰe˯j�\Z����hя��<���������nݺ����;�5���>�i.���^�fW�\"��M�X�HbXm�ʫΝ;�[�^{m��7mZ.�����3�|_X�l���ԩ��=��O<>����ӡ_���qǵ\r�G�WS��^����������^{퐴��èQ��e�v�f���B\ru��۷/\r��}!������:�����yM�@��Nͮ�E~�/�bSY���3�~xB�����ѹ�W]5(s������,Vo���\r���ȑ�N�?��]�W~^�������3f�Mel\"��\'���F��j����U_v�u�IoϘq{޿-�;唓���r\r�*z���%�8�Mei#�4��ʫ��iv7��㿘.��u.ޯ�y�[��}�^����gl(�\rdvY���sŨ�o8�����W̒%�/<;��Z�g��\0E���%�WkRh��!�Uv�\\�q�6]�w/�w�m���쮨���Y����ؐMe�cLׯ_�*��^I|%��I�Ib{�w\'�$�&qK�q\0(z��/!��3����ԭ^=\'���9��ty<�1ޟ0al���K.��~ᅇ·�u}����=��<>{Lf�z�p���qY��N��߸���n*�[G�sWLvkkv������5�s�)s-n�^�i$L��I��D�̿ǟ�3�PL�<�$�k���Wы������G�N/��2^z���2{��xc��`�j���I�s�K�̝{�!5����ډzb_?F��=��i�̝`�p���(�pÈ\\�]���4��С��.IR�k6צ26��2��o�vu���6��:�����Wы��K^�c+�F&���jwֶG�<_�<���\0&g1�K�/y%��\\�[%�1�����d���`r6#����W�u���Lػ�W�ޙ����l\"F~	�%�D+͵���]U�7Ҋ�\'����)\0����%䗼�/��^oi�)c�k9`r6#����W�u�Z�lH<�k���B���n�\0&g1�K�/y%ZU�ŭ�Î�\n5���J\0����%䗼�&�zU�D��Q\\��5�ɀ\0L�Bы��_�+���s�v�W�IE��&��Y(z�_�y%�B��N��Gy����>)�\0L�&b䗐_�J��\\����}�W��I�%�\0L�&b䗐_�J��\\۞D���R���-�\0\Znr^����?ڷo�;���c�4�Ds�&�������{�m��7���ϧ�_�K~Q�$w��޽L��Lc׮߼�|L�G4p6Հ��pL����#��|�ɰe˯�M7}�Y�\'���бc�Zӭ[��u�y繣���^����w�KJ���5Ә7�ާ��iő|ƭqK��8��Ν;�[^{m��7mZ.�����3�|_X�l���ԩ��=��O<>����ӡ_���qǵ\r�G����yb�z��C�B3���Q��e�v�/��5�,����ۗ�q㾐����n|����������%�h\\4g�ě4p�4��HQ��T[k<���sE��O�p_�����\Z�.++{<������}�ݒ����A��9����G�+�����U��{�X���O>99�x;.+Tl֧ ����>�ޞ1���[��9唓����^������յ_�^�**+�k�_����xU�1�?�ϸ5����sEo�E�~��Mo��/��jnA���*0㖏x{߾�,k{��k��Q��N�[���?fɒ��n	*ʳ�Eы��_}={��r��5q�,�\r�@��,:�Ϸ5^����Eo����2[�<ABmf�X�Oax�Eo����,z��~�E/�K~�|�۹�	;KKgj�I��<0/�\\6$q��~����Sm��\nw����h��7n9��@�^=\'-��9��ty<�*ޟ0al��K.��~ᅇ·�u�a�����y�0���;ᄎ����=�w̘����������%1F|o�z���V��/�%�8\"��u�Mc�<\Z���Ⲣ��HĢ�|��V�$~�ؓ�|�����G�N/��\\z���7{2�x�U����*0���s�K\'̝{�a��c�_;F��=��i�̝\0�p��X��pÈ\\��]��xf��zC�^��dB]���E~�/�N?Ю�q���lu�e�C���uCC��#�x���l�����VS	аE�pqv���9�M��<ө��Ǝ���~�G\\ǲq�C/��9)OܨxAc|�%E�{�c����Qh^\r>��W(z�_��O3=��ƿ���\Z�EU��m�7�ڻ�jh�F\\a��{4&�	(z���%���odQ�I{z7B�\Z��ڣ��0�\0�^��E~�/����4�\r�ŲO���q4\'#���^���N�b�����#9�1C�!Ө���G(z���%���ջ��@񬰃�w�eCJ���=�	�v����^�����g�˸봢�ˏ��D�̿w�4���gZ�i&��K�h*E�P�\"����J�I��4��S�Ɵo%�dQ���d���Bы��_\0�J\0E���%�\04�\0�^E/�K�/@S	���^�%���P�\nE/�K~h*E�P�\"����@ѫ�E~	�h*��^䗐_��@�+��K�/@S	���^���T(z��/���P�*z�_B~�J\0E�P��/!�\0M%��W(z�_�@S	(z���%�\04�\0�^E/�K�/@S	p4�o�n������ v��͛�GRi��_B~�J�����XRr���ļy�>�|$+���K�/@S	�bpќ�oRt6�>|�#�G2�Z)���4�\0-F�^]���խ��r�³i�O��ū�����R~	�h*Z��=O]9y��\n�&�a�<�|����K�/@S	��۹�	;KKg*@� JJ��|����(���4�\0-ՠnݺlS�������,y��Y嗐_�������������Vǀ5�1^�]�6(x嗐_���59�M��<ө��Ǝ���~�G\\g�ᮓ/k�B�9iH<��.y�K~�/@S	�*�Ibb�Nbwf<G����:y��B)���4�\04Of���\n�_\0�J\0E�\"Q��{;@~h*8s�����\no�/\0M%\0�շ�o[Q��L� �\04�\0�˓E��#nMq��/\0M%\0�ʷ%#�= �\04�\0�faQ�K�z{@~h*(���(������VO�}!tg�����x\r�� \n��\Z��A\0�5�/\0�\0�k@~D0^��0�`��`�x\r�\0�(��\Z�_\0Q\0�5 �\0�\0�A~D0^��0�`���A�x\r�/\0�(\0�k�_\0Q\0�� �\0�\0�A~`0^�� \n`���A\0�5�/\0�(\0�k�_\0Q\0�� �\00���`�x\r�� \n��\Z��A\0�5�/\0�\0�k@~D�׀�0�`��`�x\r�� \n��\Z�\0Q\0�5 �\0Z� \ZBI��So�e��a/��w��څ��!� ڴ)��Y�6�c}�u����`��@�Dz��7�wg��=�ĉ7��������B+�F|KKg�I�Ƈs�9}oϞ�����X��x\r�/�V1�N�~��=��ɓo���5��S�|=�tR�]���\n�9��Т��P�}���-i\Z����6+�r�I�&�0�:���\"Ѹ�k�B��l�Ʋc�v�Ev��5 �\0Z� \ZO������j�.&M\Z��m۶�Y��x\r�/�5�Ƴ�Ɠ�8���K��o%I�<���b�xِx����N~cƌ�]&cܸq�0��ĸq#W$��$�<���b�x�xِ���	&�9s椷���,X~�|y�޽{��\'�H�̊�\nM�a���?���=\\a��5 �\0Z� ڡC�Z�C;r[%c����?�.]\Z֮]ڶm.\\�.[�xq�s�֭��ü�e��UZ��x\r�/��4���脰~���Q|��Ea��������:lJnWTV�ѣ��.[�������o���0ä	�k@~���2��;B���Ö-U[ �n}\'������N��ݻ7��d���j*�x\r�/\0M��tw��ӧ�vyݹsW�9�������È#�����gϞ�����ôi�¬Y�4��J0^��Xn*��K9w�ܰy��v<iO֝wޙ.[�fMx�����$j*�x\r�/�c��ܾcW�������ۗ[o�\Z5*�DV�[�I�T��\Z�_\0�xS��?�au�s�С5��\\sM�o���r�����4��J0^��Xn*�n�����+¶���\'�ɺ뮻rg|}��Uiӹa�\'��T��\Z�_\0�zS�v��0*�����i���C3���$=W^yez;WYuM˝��]�v\r/���&QS	�k@~�M嫯��69M�m,�JW�vT��̒��n�eeea��^xᅰ`���}{��g��$j*�x\r�/�c���ⱑ����iC���aێ��c~rP�ӧO8�3��]��v�T��\Z�_\0�ʸK�t�x��nݺ��e�B����Fsێ=��WW��ݻ���שܼy��RS	�k@~h*�gy�[*o��W�%�}*�=eʔ���z��k��>xNزeK8�&QS	�k@~h*3M�SO=��BYZZ\Z�3g���x��PQQ���T��\Z�_\0�J��� �\0��JM%���`�Tj*�5�/\04�BS	�k@~h*��P��0�j*5�����ATS��� �\0�T\nM%T�J:L�z�-�G{�_��ww��.w]\\q�ѦM������c�3���NhMC=`�Tj*��y�o�8p`ߝ�{�\'�JJ��w/3F4@����tf�4i|8�����y���[~����ATS���Va�������5L�|s��\\n\\h�2��ᤓ:�:��vWX��TD5��Jh�\r��g�?ݒf<8z��k��)���n����0�j*5��\"�]^�J\re�5�;�+/�+,�J\0�h5���DS���f?ȅ��ʸ˫q��bҤ�[۶m��5M%�A���Ls�_S����*��5���1�M]�t~��94�\0�`S���E��\\*�4�Ф�eC�Y^��E~cƌɍo�ƍ�W%ƍ�\"yO\'Y+�TD�5�շ\\^���TBs�C/RWS9a0gΜ�v߾}Â�o�/ݻwO�=��iUTTh*3���?\'��\nk%�J\0�hmMe�-���j*�������N�5���\r�BMS	��С]-סaǎ�_%%%�ϥK���kׅ�mۆ���/^��ܺu���0�c����J4�\0�omԨ��9a��/����vH�h*O>���c��\r��f�T��A+�\n�E�ׯOׁg�]6oޜ޾��æ�vEee=�K�u��\nﾻ=���oh*}����FjF�nM�Zdm�.檊���/]tNz���xP���߾}�p��\r˖���T�\\��p�e	����iӢ���w�<�}�ߩ?��wBϞ��6m�s��fS��o��q�\Z�|��/�o_\Zƍ�B�ܹS�7�&Mń�2��w�qG���Ö-U[ �n}\'�o��v�l�޽��%K����8�y\0�Fh*k6��(檊�ѣ�������0v��\n6R�Q��O9夃\Z���-}�ؐ��W]5(�����ӟ���������;�<W�����\ru�/�Z5��>�>nƌ�s�	\Z�����N�>=���Ν��̙���F��޾��={����ϟ�M�f͚�I4a\0����$F�i&��TN�����Q��c��sPc�d����Nw�-ʳU06a_����bQ�-��^�������ҍ�-���7ԧ�,�Z�\Z�}�^T�)&h䦲�81w�ܰy��v<iO֝wޙ.[�fMx���޻���\0jWh��!7���8��M�[+kk���#��~���\r]�J�pÈ�\Z��\'稊����K?�k�\n5�u�\r�i*�V��R1����4��w��5����!C¾}�r���Q�Fe�J��JM�q�\0\0MX�e���{KsMe��,./��+kZn_z����ȑ�>��?&����\'O�9�\\\'��1�<ގ˲[�\r57hP��	=�1c��7z������Xp���\Z[Ŝb�Ck*��?�N:����k�I���|[���v�&QS�y\0���T�uV׹s�\rݺuI��:��܉nj>.��W|<lڴ8ݝ��n��v���\'�y�ѻs˧M�f�D9��\r5�j�c���=ztM�Ȣ\Z���{�|\'�[]��l]���R1���CUn�����+¶��4O�{����__}U�tnذ��z4��\0hM�P�)&h�qh�ڵaTf+�����6���f���Iz�����v<��Ꚗ;��c�v�\Z^z�%�q�\0\0�J���cyz��WӼ�kroc��P�򕰣\"�g����(++�����/���۫���={�q�8�y\0\0M�b�ı=����lٿ����Mf�Ί�m���1?9(���\'�q��㮿�z��\Z�0\0��T̡�0�]Zw����kUv��-�.[�w��5��v�	/��*t��=}|�N��͛5��!�\0h*s(&�Cဳ��-�7|����>�ޞ2eJx�i���5e�<\'lٲ%ȸb�<\0��R1��ES�i*�z������P�9l�ŋ���\nM�q�\0\0�J��	�0�y\0@1\'s�	�C�!0\0(�bN1�q�8�\0s�9��!a�<\0�bN1���8$�C�\0P�	Ŝb�q�\0�9��SL`2�y\0@1\'s�	�C�8�y\0\0ŜbN1�qH�0\0��S̡�0	��\0sB1���8d�\0�bN(���C`\0@1��SL`�!�\0(�s�	�C�8�y\0\0ŜP�)&0��<\0���9��!��\0sB1���9�C��?<����S�ޚ[VϜ2��0\0��T�)&8����kƏ�bz��k�4�����:vl�`�3�y\0�Ej߾��ݻ�i�A����7����Z���T����a��s�����������dL�_�Ѱlٌ�M�ʕ?\r�]����+?�6-J��3ߗ����|\'��yjhӦ8�\\�w�ϝ]��o��q�\Z�|��/�o_\Zƍ�B�ܹS�7��\r�\0\0�����XRr���ļy�>�|$+���	M��M���C�F��e;�s��(�姜r�A\r���ߖ>Ol�����\Z��[Y����~��������w���V������z��q�u�I7c���Tb\0�i\rpќ�o��5�>|�#�G2�Z���T��L�����Q��c��sPc�d����Nw�-ʳU06a_����bQ�-��^�������ҍ�-���7ԧ�,�Z�\Z�}�^�Tb\0�i��յ_�^�**+�k�6�T\\\\�*�H�[+�ʃ��E�~����&ƭ�5��N��_�~Aކ.n��?o�a�A�Y]�\0ĭ��^��\\�W�9��o�OSY�\n5����<\0@�г�+\'O�Ycׄ1l؀��b��Q1�������[�k*�5fqyQ�]Yc��\\�j��\Zo���N�3a��Z_?����:ᄎ���v\\�ݢX�o���A����׭�ƌ~��X�jF��.�a�]rkkl5��\0hh�v�|���ҙ\Z�&����%���$.�**&4�������:w[�.�1�C�^�;�M��ew��⊏�M����V�\r����1�:�>zwn��i�̝(������V�z,�x��G��iYTc��|���D=q�k������Tb\0�h�C�4�G��,...K��aVAń�R���\0Z|cٮ�q���lu�e�C���u��R1��RS	�����6m��L�N�o\Z;�s�{��mpˆ�e�lH<�k�<�J��*&�Tj*�<\0�*�Ibb�NbwQ�c��aGeQ�u(�eC��U1��RS	�\0@1��RS	�\0@1��Rh*1\0\0�	4�BS�y\0\0@1���J0\0\0�	4��J0\0\0�	4��J0\0\0�	4�BS�y\0\0PL���J�\0\0�	4��J0\0\0�	4��J0\0\0�	4�BS�y\0\0PL���J�\0\0�	M��T�y\0\0PL���T�y\0\0PL���T�y\0\0PL���J�\0�bM��Tb\0\0PL���T�y\0\0PL���T�y\0\0PL���T�7�\0�b���}�v�w�^��k�k�o�L>�Jk%�\0@1A�q�ygl,)�_S�b޼{�J>��J�\0�b�c����L�x������I>�I�J�\0�b��W���z��VQY�\\c״����U�G��Z�y\0\0PLТ��y��ɓo��5a6���Xdm�<\0\0(&h���������35xM%%�K>�\rI\\`U�<\0\0(&h�u��e����7����e��?�*�y\0\0PL���v����we�c,���.�4��\0\0��ɹmڼ�N���4v��~���?��:�\rw�xِx���Iy�1�vy�<\0\0(&h��$11�_\'�;��#�ʢ��P�ˆ8�+�\0@1�ē������\0�bE��W�YO{;�<\0\0(&�P�-���+�`\0\0P_}����2Τ\n�\0@1��d��\'É[+[	�\0@1�ʷ�2#�=`\0\0P��E�/�Q���\0\0���Bj�J��J0\0\0�	�Փu4���\0@1�S�_\0�b�� �\0\0�XOA~\0(&���\0PL���\0��\0�)�/\0@1�S�_\0\0�	����\0XO�\0(&�z\n�\0PL���\0��\0�)�/\0\0��S@~\0�	����\0`=�\0(&�z\n�\0@1���_\0�b�) �\0\0�XOA~\0�	����\0`=�\0���z\n�/\0@1�S�_\0�b�� �\0\0�XOA~\0(&���\0PL`=�\0��\0�)�/\0@1�S�_\0�b�� �\0\0XO�\0(&�z\n�\0PL���\0��\0�)�/\0\0��S@~\0�	����\0`=�\0(&�z\n�\0PL���\0�b�) �\0\0�XOA~\0�	����\0`=�\0���z\n�/\0@1���_\0�b�� �\0\0�XOA~\0�	����\0PL`=�\0��\0�)�/\0@1�S�_\0�b�� �\0\0XO�\0(&���\0PL���\0��\0�)�/\0@1�S�_\0\0�	����\0XO�\04�\'3�Cm��ۄ��\0@>}������mB��\0����4��I�����\0PH�Z���� �\0\0����x,���(zA~\0�)߱���D��\0����J���_\0�a�_����@��\0�Pe����E/�/\0�C�W1��䗷\0\0�ˬJ:L�z�-�G{�_��ww�ЮХ;�!D�6���:���}�����:}�5M���\0\0�:=��컳w�a�ěBI��a��e!���#����3äI��9眾�g�S_N���u�J0�\0�U�>��y=zt\r�\'�*+�+\0\Z9�L�z8�N��?���>M%��\0�����~�\r�����k��\n��rһE�����y\0\0Z���S�fZ!�tEǎ�ʋ����y\0\0Z\\�J:�cg�N&���I��om۶�s�HM%��\0Тĳ�œ18v��K��o%Ik����\0h1������L�M�ƍ\\�|$����Jh>�@~cƌ�]&cܸq�0c�y\0�V/^,�.��ނ�7�����Г��w���ᱩ�\r�?�����d��RS	�g��	&�9s椷���,X~�|y�޽{��\'�H�̊�\nM�y\0�cI��u}�?���_?��Kxpd�pS�<9|wH�p��;��M� h��%I��RS	�gaǎ���1JJJҟK�.\rk׮m۶\r.L�-^�8��u�VM�y\0�c�ȭ�d7mh�03i&��4��\Z��pװS��v	����� h��th*�y�!�_�>��}vQؼysz�ꫯ�����a��/��֭{+������o���4\0��88�{i����o���~;�������p����Sg��>~j�k�o�	4����2��;B���Ö-U[ �n}\'������N��ݻ7��d����\0PL䏟�t~ؽ��/�����Wf�k����w��R�����}���@SIk���ӧO���s�0s�����Ç�#F����{aϞ=�����iӦ�Y�f��\0h*��w\\�y�i����\'�hz���~~y�pŀbM%�l(�v,�ܹs���[���=Yw�yg�l͚5፲�~Ǹn\0@S�����k�-�|\\߰g������i����_��7��v�yQA��@SI+k*��ؕk ��!C��}����ۣF��4�U��V\Z��\0h*��[�\"�xjq�5xqx}ݎ0�_>���i�ۿ����ק�����a��R1���V�Tf�X��:t�AM�5�\\��[y����o��6�y\0\0M�ʰ���gJ���^��~W���n~=����p���� �	χ�l��p�?�&<8�P��\n��JZ�<P��o/��\"l+/Oǩx➬��+w���__�6�6lp��\0\0���I\Z�?}��a�o�\Z���ƕ�7��7<�?�~|������n[�4����O��\'�]���@1���V0�]�6��l�|�|{�&>�Ќ�~<IϕW^�ގ�UV]�rg�{l׮]�K/�d\\7\0��\\~�\r���F�ь\'Ø�q�~A�������$\r��W��>3m�俅�ɋ�֧o�����bM%�`x��W�q�ɽ��;C��W��YR��͵��,���/�,X�o�:�r����u�\0\0�ʕᕤ�|��cg�;���	aԣ��~�4�����~\Z�!|�{�?�\'��q��@1���V1����lٿ����Mf�Ί�m���1?9(���\'�q��㮿�z���\0PLTŦ�f����9I�����cJ�?>�?\\W-�1g����}������H����	68���VВ��;v����kUv��-�.[�w��5��v�	/��*t��=}|�N��͛5��\0�}��a���p۴_��W�,�?lv���_����h�h�O×������ߤ�U(&8Ƞ$ʓ���9�[BKi*���5n����_\r�\\�����)S��O�V�)��9a˖-�@�u�\0\0�J����\Z�l#�Tn�X��ʧ�z*�����4�g�c��š��BSi\0@1!4bCY�r0��\0(&�b���:\Z��;��0\0���	�a���0\0���	�6����`0\0�bB(&ZyC9�0~o���0\0���	\r�&�}0�\0@1��PL�Pu���h?��\0��PL��\Zʆ�u�K��\0PL��1��Ivޓė2?E��s���\0PL��1�P��>��:�Ʋ��i,1�\0@1!�HC})���Ch,�7��]��c�y�<\0\0�	��he\n5z�\Z��\Z�|������<`\0\0ńPL43����\Z����(�k,��P�l,�\nˡ��Ĺ��\0\0h*4�z���\\����Է����<Ԇ���o.�ŒC_�����\0\0h*4|����E�o�<�]P5�Ň�Pf9ƒ#Y���>��\0\0h*4|�]}��%G�P��X��\Z�#�{��Zύ��\0h����Ӊ�����˖�H��0\'�������{�m���,pj��b�:���[.�n�G�e0_cy$\re���1�4�z^�q����ޟ:��z��-m6\0@#6�ӧ;��n���,�����o;�I�[�.����s�	��b��X����?f��ə��D�$�K�}�8>�NI�7���89��I��D�$z$�3�^I����I���YI| �U���U�wa��EI|$��I��4$\'�$$��$&�L���L��IMbxW$1\"��%��$�N�I|1�k��oEU�U��uIܐ�O�$qS���W���I|5����_I|-�[��f��ķ��=�	I|\'��Iܕ�w��^�O��$~���N�$�$15�iI�{d��I�L��M�I�G�$�h�%1\'�_$17�Ǔx2���X���I<[T�u�WI�$�|K�x!�eI�6��I�H�$~���I����I�g�3�͚$ʒ���x3�uI�O��&�!�������R�u{&���;�m?����k��,�ʓO~o�ر}�=NS	\0��T��[\ZN8�c:1�޽,]�}��p�ǧ�7mZ.����}�v��3ߗn��^P<��=�W�ny���N\\֯��l]����#9�`��|\'����Ε+��u\n����1n�B�Ν��x�D�X�D,��Vɶ���u�׺Զ��>ױ,$��2>Ͼ$��DE{�ؕ��$�\'�����4��ؘi>b�Vk3�IlR�H��d���ĬJ�Ls���%�2�>�&�4��d���$-I�L���g����-�4YO$1/�|�&,�7+Ӝ�&�gI�$Ӽ�̼Gf�����8�e���2���e���$ޓ�ݙ�16�w$�I|;�dޖ�7��z�	���?\'1>Ӥ�f��Iܘibc3{}��irc�{m�2Mpl��J��L����d֡��f��$>���b�}Y�f��،4��\'Ӥ�f��EU�=�]��??��2M~l��N��̗\0�ˀ�\'�̗�˂nI�����z�2�Ϭ�u�Kq���.:\'����<�Y�ޏ�e7k��������>^y���c��޳����漑�o��q��|��/\n��\04�uD��:>>�erʔ�������W]5(�����ӟ�)�>Y�ܚYۤ�}�x?N���u�}&�w�-��g��QX������(����s��3f�~�sz���U6�5�ɚ\r����9��p\ZK�Tr��y��`d����qi��i���?�2�����Tl��SN9�18��ybCv(cl�1��\'���\ru�/�Z5�мa\0@SYG�o����V���}0�%����~m�~]�|����Vq����4~�e˯��<��j+j������������xMe�*�㖶��|�\\}�X�lH���Jc���zw��[X;�<�={ĨQ��=Oj��K�<.���t�-ʳU06aտ�;�1�����ܳq��������?�^�P�[s�0\0��<�������	6�kl]�$�5�Ǹ⊏犘��/��x�8���q7�ښʚcvy�����5��Bm[&�j,�j��e]ױ,$�\Z�:���erL=��:ǥxB���[+k�٧��#��~���\r]�J���u��_2^z��R3߼Q��P����k��\0pMe�0�[�M�fny��\r&�m��2�i0����~�M���s£�ޝ�9��y����8f��t�?<`y�������QL�\num)���,:��2�����_��9.�=F�Me��-./��+k���kQ��_����c\'O�9�\\տ4�����BC��\r\Z�/��n�������V�(4o4�|`\0��7��Oؓ=.&��nܢX}w�#m*��xVظl�]������ML_;���.�\rt��1�?��ق���k*�y��Xַ�<��2�z\ZJ�d��>w��x��:��܉nj>.�m�eӦ��\Zc�.��1�:�������f�D9��\r5�j�c�I�z��6�E5v��Z�Nԓo��T��hfw��EA�4���5�-F�bYsW�/�u(�5��k��c(9f�ש\0��E�&;~c�=P��Z��	th,����z�X������;ԓ���<\0\0�	ńb����+llG�eBj�ޑ^��\0PL(&h���ʷk-��\0���	��QoP�<`\0\0ńb��HwY��+�a\0@1!\Z�����<��y\0\0ńPLp@�8����\0\0(&�bBc����$Fx�0�\0\0�	�������qp-��YC�y@�\0PL���bi�W��<\0�bB(&8��RC�y@�\0PL���XN�Pb�\0B1����|]6�0\0���	�I���<\0�bB(&\0�0\0�bB1`0\0�bB1`0\0�bB(&\0�y�<\0�bB(&\0�0\0���	�< �\0��PL\0��\0��PL\0��\0���	�< �\0��۷ۿ{�2y3�]�~�f�TZ+�y\0\0Z���;ccI��&�f����T򑬰V��\0\0�\\4g�ěL�� ��H�L�V��\0\0��zu�׫W�����&���?�J>���J�<`\0��g�SWN�|�	�	cذ$�\"k#`0\0@Ktn��\'�,-�ibo�()y`^�lH��\"`0\0@K5�[�.�G��(...K��aVA�<`\0�_P�kw�_��+[[����dvuڠ�\0���\0�ɹmڼ�N���4v��~���?���e\rw��x��xv����3vu��\0h��$11�_\'�;� �8���������0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0����Yp[�S�7C\0\0\0\0IEND�B`�'),('127',1,'Vault-Test-Workflow.sid-3806d577-ddd6-479c-85f6-11805b79addd.png','117','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0\0\0�\0\0\0�Ɍ\n\0\0\Z�IDATx���tTս��c��!m\"��Ҵ�J���x����Kk�Dӂ�\"SᲤ%��j�Zkڦ)�,!`�AP\"�-u!\\���`$�{�\'�\'�L2��$��~������d�$�7{��0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��)�`��&M�}}ذ�u]�$�^LW�:%���~�+�l�߿�H�4\0@�Z���[G�z:#��̝;M��I]��CY�:VT,���2p`߳}��z�|ɳ��\0\0Q���u�{������중s͟�II�^ۭ[���}\0��	|��Lc�~UY�JRSS>5߂\\�B\0@D�a�Y ,t\\h��5��`x\0�t���Y�a\Z��\'_d�\0D$=\ZB\'82g��G��̷$��\0q��I=\Z��㫰p�n�-)b�\0D]gA��Q�X2S���G�0&E�I����-O-(��A����ͷd7{%\0 ��L��������O�������Ef�{���,3��,�̠��:\r�[�a�\0D����c�e�6C¼1Z������=俯�2�~�hX\0��\r��.��%�K�WK~.�sU/�9����]\"Ӿ�K>�J�O`\0\0�{`xb�`�;�|��B����˾U��C~#\'�&kn˒��iA`\0\0�}`X}��rrדrt�j�}}�Y+��\'_[!�ܛG�O`\0\0�{`8��-�����(*g��Hݛk����Wyf�P��]���3�h�	\0�x\rg�향�o���-����dկ����ʳJOo�G���E��ӥRKh 0\0\0�/0����˥o���õ�����7E����U��\"s^ٱ�V�^��<�n�x>�M�O`\0\0�S`ز�E5�aY��g��}\"��rky�L�\\/�������q���٥fpXU/W�?$k����\'0\0\0�)0������;�ѥ���7���z��5fX�k��]z^�[X/����}�[�Ħ{��;�h�	\0�x\n���p��d��er�i�i�Ȅ���\'̰PR/7�E$�ȝϜ�ѓ�K�������4��X��K\0\0������\Z(�\'ϓo��O6��-���]�}V���w���.�#{��\'0Ă����\0�\0��8祖ӧ+d�¿I�\r�ep�j�t��ri�\Z��R.�������R��+�mi�	1`�Y�f�?syI\0h�	�r]=\r�\r\04��-]\0��grZ���i\0�T�Y��Zm��j��uf2k�Y��ہ�@`@0a�� \n��|���\'��Y���4��}��̰����ە�5����@`�������i{���W�T���\ZJͪ��@b+?E�6k�Yx�	�B{Fٟ�\r�N�+AT����ͪ��9���t{;�6����?-��=���{�\01Ŭ,�R���bg��}V�艨i��W�(u\\$;D�boo\n�����wZ�����\"U��A���qDo������w�ݸg�x��v㹧�����a!T�	�4 �{�\r�~�7�-�~�^�!�m�6�U!�Y���Pew�(Cl�x�Y7�?[��~L�D�Ѷ�=��R�ܦW�M�\'B����g����n����a!���/�_�%�\r��Gh@,)��YH\r�vS�z\ZB������/�^#>�&h�	�����=\ZHhp��&��gNbA��t�BV���e4�I�[֎x�ⴗ������5�\Z��B��ۗ��=s\ZK�Eaz�y��e�աtBbb;�H��SF���	�8۟����V�\0_�!а�\Z�@����>�{��ԨGO�����y����$5+Fv�M�$��@`����-�vK���a���|�%���h��v���|V�kV�\Z��/fn;�@��c8U�\'Y�Pbi��6��BCBÂw���w�n���|Ƈ*���@{/߬]\"���N��	�ݟ��\ro�}>@G�p]����z���^=>�{;���xu1�c��ev��!;������}���;v,��kc��x~ÆG$#��t� �}������K�\"%%w�|C����~��\Z�	ޡ�9\r����Z���0?��c���E�\ZsL�\r�ncڴqM.�2%_-�t������ɓ/>*+WY�;wN��*�*�@������˃Q�{CEY�w�KC���F�o���;{�B.���t��Y��vX���l�/|��u���2��oY\r�~�מw���SIzz��F�W�6lp����?��j10�__l�4�������(,��$\'w���0D������0�fq\'z�G5���������X�P�3~l=�ӣ0�/�g?��:}�\r���|���\r���������>����ڐ��[n�NydV���5���#���]�Q���.���v�ݞ����8���Ж��D��QT�a�(���Fê]�ֳ��۷o��X�]6�:��o\\������;�3$�o8����C:�⋿h�n������|��{������jr_�\'0D����O����u�DkBGI �����(	�aNs��B\ZܓuN�{��i\\�ኖ\Z�����Ǻl�˻2�	�-���\\�}_�\'0D��ܖO�-����!C\r�a@��E�:���N�3\r�-����l�|�����s�L\rY`x���7^����=ǂ�<����?6����	��O��.�������� �D�J����������C����60��=����m	����z���N�ܞ����4�v�@C��x�D���.	��׷U�G/�v��m�QPo����.�N�d�&����{x�f���,�m��Q�m�������v�I��#�\ZU�à���\"0 �����n�[�(���q�$bI��A�o����#�ݻ��7ì*#�/�i�!��3�t�`IfC<�����!h�����v� �dؽr�=�Рa�=�Q��W�8d�~Bb��݉q�S�P�=4��炣!����x�4�ux\"˫g!�C�\n��=T=\rC����;\r5���9��4^�AO�D���_o7�k�m�x;�뜆`�<t�B��E)�!r;��0⩧�ګ��\0��nG�����a�L�>+��KM;����f\rc �m�C�O���v#~�,�|��ۆ�%����\r�ˬ:��g?yV�!�0��	���ۿ`��}r�~�F|\rA` 0���?7L�b���zZ\"���U�n3\Z������??��Lf�#����Ї��+8�����r�0�����\0��z\Z�\0@`��&0��p@X\0\0�͆�b�\0hN��~p�$\0h�\0\0\0��@`\0�\0\0��@`\0�\0���\0\0��@`\0\0E` 0\0\0��\"0��\0\0\0�Ν����v�XG@�־���x�+	\0q\r�w��|\rvԺu�}�|Kv�W\0 �qٚ�s��`G@��x�|K��+	\0q��{KOO;������:������d�W\0 \"���kOq�L\Z�����ͷ�����\0\0�,39����h�;����3߃*���	\0�F��������a!!!����\0 jBCR҅�<����i��{���@`\0�h�٩���w�vt���}ӦG�X�!t�,術z4�=�Q�,0A`\0��6Ĭ�fm7����IW�(�����Nr4�\0\0\0\0\0�\0\0��\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0\0�\0\0\0\0\0�\0\0\0\0\0�\0\0\0\0\0�\0\0\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0�8���\\��e\0 �\r5��B`��e\0\0�̈́�\n�y�\0\0��fC>/\0\0p��ˠs�]\0\0\0�|�e`�\0\0����\0\0�ٮ����\0\0�q�2л\0\0\0�r��\0\0\0�E��˂w̚4)��a��u��Ҫ�T\0թSB}���Ǯ�bȆ����dO\0D�%K�u�ȡ�32��ܹӤ�|�����=T���cE�2)*�!�=ۧO��͗<��\0UJJ�Y׻wO).�)�N\Z�0��������ݺ%]��\0���0`�W�O�4��W���$55�S��#\0\0�N�!�g���q��kפj��	\0@��	�:gA�!h�;���f�HLL|�=\0��h��Ȝ���=�?0\Z�\0 �術z4��F̷����C�?B@k��p�.m]�^	\0�8�΂:�R`�3g��Y��:=t�Pٸ�T^ݹS���/Y�=��sV�8w������K�?4\Z�\0�Ȣ�2�_gA�ԩSM*//�~n۶M�{�}ILL���R�-[�X?O�8A`h�:\rF÷o\0q�i�D>l��^(�cǎY�o��F9j�>��ȤI7[������5��w�y����2X�\Z\0��A�{�ҹsg9~����ĉ��=}��u�ٳg��[�neH��\0\0����C%%%���O�ʲe�[����$??�:��C��3g�X�7l� .�U�V\0\0�x�k��ڵk�ر��i�\0������8 �<��>\0\0 NCͩ��p��G�-�ϟo�LOO�0�\r��\0\0 �z������1c�|.0�t�M�u��7�~���\0\0 ���[o�]�v����V��� <�@�o���(�����H`\0\0�K`x��d��{�Qu�[��:�ǎk��y\rk6���,z��)���\Z��\0\0�����oX\r�g�#է�b�>9uNd�֊ơ��J}���/�,7n������W�&\0\0\0���Ep�Avv�4@T�>\'���n3�dȐ!ү_?�v�\'OfH��\0\0����KC밄�Ő��&��/յgC�ǧ���o�������I` 0\0\0�(0�����)?�M�_�=������_ː�ʿ]:P�?ηU\0\0����ƞ���\n����0�/�j�M�\0 NE`\0\0��@`\0\0��@`\0l�fM7k�Y̪���:���֬Y��\0��@`@��7��`��5��\0����ؗaVi+B���n�\0^J\0b�x��y5�;@L1+ˬT��)fe��Y��\'���\0bO��A���qDo����0��\0\r5��ӳ�n��۽	m����\n\r�4\0\Zj��� ��!t�Aj����2���D&/5@`��^e^=�!�n�WO�v^j��@��s�B��,��PC\0��\"0 �{�����e\0@CM`@���-�&����c�o�F=z�}�eo@`����M�48d���t��և���v=�,���@Y���R=�F� ��g|;�\0���6w���.�\n���t=��=\0�����{���`�Y�.�0Q�+���س�/��o��t�$�����z����\n��Z���0?��c���\0�	_�E�ڵsP�AO��K2f�p���E�C֐�k��m���z9~�o2mڸ��wKᇢZY�b\0Z��\'��O.���dd��>��u~�I�\rl}ڟ4i����\'�k����ر�:��G��;z�̺^��䒯X��y����_b�?y�E�8q�հ��&Lȱ.��jj�Ia�$9�{��F���)A*+W5^��y�Àv�a�m4|gD�Ѱ�\"=\0\"?0l���l���uZ������o���d߾>T\rzYjj��Ǻ�Q�����~j\0	�y�?��qxB�k@����[���2��[���/]zO�\r���i|��o����	��`����Z?��ym��o�u�b���X�mn{ޟ��|k�<��3T�lK��&����k�1/+�S��Č?&0��C�Ѱ�c���p����\ZZ�v7��u:4��0�Aw\Z�Ç7��v�z��[o=e����[�=��mН�\'pF�z����c�^m�s(-�c���W�IC��\ZY��A�E�uÇ�ο�����M\ZU���j��y\Z6���#/X��9z~Μ�m���=z�3�B�#�w��z�^WP���;6�ڣ��S_�����{?o!(�`A`@+��#���i�=Y��k�mMht7�˗ϵ��9�k�L:�I�z��ڵ�m�X����V��\r�N$��.�e��Cg���3pMrd�n���Ɇ\r�4^�����mi�i���)S������P�����\\y�Z�A/�~�t\0�K@���\\[������m�\0\"708��i�	��}��C,�P�J�ޅD^n��@�2�:�հ\Z4,��;t͇L^j��@���G�WOC[�\'��z�\0@` 0 �x�=�!{x��<�mh��K�������i��j�5\0����8�:�i�g��t�ig���\0����ؔa4�Ж�n0g\0�����0��V�U�h\0⌮\0y�Ѱ��!�_�6�O�\")��e��#\0�\0\0�\0\0�\0\0E`\0\0(\0��@\0\0��\0\0\0\r5�\0\0�\0\0�\0\0�\0\0E`\0\0(\0��@\0\0��\0\0��s����4�P����k�%�J\0@�4�ߑ��E4�P����y�-��^	\0�8#F\\�f��i4�Pyy#�4ߒ\"�J\0@�IO�9,==�ǳ�F�c�PBB�[�[��^	\0�H}���S\\<�F�+7w�b�(co\0D����NWT,���*/_��|���bW\0D�Qii=>&4�XHHH8h����\0��	\rII~����O0�!�s�a�*�\0 \Zev�t���ݻ�:��ߴ��*�i�:z�\raOp�9C\0\0���暵ݬ:�aB*��E�t�=t��!\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��!�]�]�\0\0\0\0IEND�B`�'),('2',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Approval Workflow - V2.bpmn20.xml','1','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns:activiti=\"http://activiti.org/bpmn\" xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:signavio=\"http://www.signavio.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-39062ac0-8ac8-43bf-b32f-639d483f2094\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\">\n   <process name=\"MLR Approval\" id=\"startMlrApproval\" isExecutable=\"false\">\n   \n      <extensionElements>\n        <activiti:executionListener delegateExpression=\"${workflowListener}\" event=\"end\" />\n      </extensionElements>\n     \n      <startEvent id=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B\" name=\"Start approval workflow\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <outgoing>sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB</outgoing>\n      </startEvent>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'inMlrApproval\')}\" completionQuantity=\"1\" id=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: READY FOR MLR to IN APPROVAL\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB</incoming>\n         <outgoing>sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD</outgoing>\n         <outgoing>sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172</outgoing>\n      </serviceTask>\n      <userTask activiti:assignee=\"${assignee}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\" implementation=\"webService\" isForCompensation=\"false\" name=\"Approve\" startQuantity=\"1\">\n         <documentation>PM-MLRApproval-Approve</documentation>	\n         <extensionElements>\n         	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD</incoming>\n         <outgoing>sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE</outgoing>\n         <multiInstanceLoopCharacteristics activiti:collection=\"${groupService.getCandidateTaskUsers(execution,\'approver\')}\" activiti:elementVariable=\"assignee\" behavior=\"All\" id=\"sid-d5e84c0f-762a-4cf7-92de-bde3c31f3a5a\" isSequential=\"false\"/>\n      </userTask>\n      <userTask activiti:assignee=\"${groupService.getTaskAssignee(execution,\'coordinator\')}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\" implementation=\"webService\" isForCompensation=\"false\" name=\"Assess outcome\" startQuantity=\"1\">\n      	<documentation>PM-MLRApproval-CompleteApproval</documentation>\n         <extensionElements>\n           	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE</incoming>\n         <outgoing>sid-4D72DB74-7882-494E-A373-460453FDCED0</outgoing>\n      </userTask>\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" name=\"Any changes required?\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <incoming>sid-4D72DB74-7882-494E-A373-460453FDCED0</incoming>\n         <outgoing>sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA</outgoing>\n         <outgoing>sid-79C7E93B-C8EB-488F-8BBD-F323077A925E</outgoing>\n      </exclusiveGateway>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'draft\')}\" completionQuantity=\"1\" id=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN APPROVAL to DRAFT\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA</incoming>\n         <outgoing>sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'approvedWithChanges\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Approval workflow completed, changes needed\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'approvedForProduction\')}\" completionQuantity=\"1\" id=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN APPROVAL to APPROVED\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-79C7E93B-C8EB-488F-8BBD-F323077A925E</incoming>\n         <outgoing>sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'approved\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Approval workflow completed, no changes needed\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'approvalWorkflowStarted\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Approval workflow started\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172</incoming>\n      </serviceTask>\n      <sequenceFlow id=\"sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB\" name=\"\" sourceRef=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B\" targetRef=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\"/>\n      <sequenceFlow id=\"sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD\" name=\"\" sourceRef=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" targetRef=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\"/>\n      <sequenceFlow id=\"sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE\" name=\"\" sourceRef=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\" targetRef=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\"/>\n      <sequenceFlow id=\"sid-4D72DB74-7882-494E-A373-460453FDCED0\" name=\"\" sourceRef=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\" targetRef=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\"/>\n      <sequenceFlow id=\"sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172\" name=\"\" sourceRef=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" targetRef=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805\"/>\n      <sequenceFlow id=\"sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3\" name=\"\" sourceRef=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\" targetRef=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C\"/>\n      <sequenceFlow id=\"sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE\" name=\"\" sourceRef=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\" targetRef=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436\"/>\n      <sequenceFlow id=\"sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA\" name=\"yes\" sourceRef=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" targetRef=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\">\n         <conditionExpression id=\"sid-82a14cc3-9c6f-441d-9b20-f9b90e0f42c8\" xsi:type=\"tFormalExpression\">${documentService.any(execution, \'finalOutcome\', \'Not approved for production\')}</conditionExpression>\n      </sequenceFlow>\n      <sequenceFlow id=\"sid-79C7E93B-C8EB-488F-8BBD-F323077A925E\" name=\"no\" sourceRef=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" targetRef=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\">\n         <conditionExpression id=\"sid-d2ecff9b-876c-4dd1-95af-49a17646e205\" xsi:type=\"tFormalExpression\">${documentService.all(execution, \'finalOutcome\', \'Approved for production\')}</conditionExpression>\n      </sequenceFlow>\n   </process>\n   <bpmndi:BPMNDiagram id=\"sid-39fbb042-c68b-432b-8516-9f9dd5a5c37b\">\n      <bpmndi:BPMNPlane bpmnElement=\"startMlrApproval\" id=\"sid-b872979f-4d08-4cd9-b6b5-fc9ec1e165c2\">\n         <bpmndi:BPMNShape bpmnElement=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B\" id=\"sid-3BD160BA-961A-40C6-B584-CEE835AFCE3B_gui\">\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"180.0\" y=\"360.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868\" id=\"sid-DC7632DB-D5AD-4B64-B7C8-3AD3D4535868_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"303.0\" y=\"450.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7\" id=\"sid-024352EA-E8C7-4773-8C01-6AD3893112E7_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"390.0\" y=\"660.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3\" id=\"sid-0C8688B9-AF68-4BC8-ACA5-D2D308755FB3_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"560.0\" y=\"450.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2\" id=\"sid-FA63E8D5-9335-48B7-B1E0-B06DE97E50A2_gui\" isMarkerVisible=\"true\">\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"750.0\" y=\"470.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A\" id=\"sid-1762485E-FCBF-4109-ABF5-5E0B80B8C75A_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"720.0\" y=\"320.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C\" id=\"sid-65A93663-82E7-4F9F-8A91-661AB122C40C_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"720.0\" y=\"195.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60\" id=\"sid-843910BE-1B73-48F6-B23C-B83E07542E60_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"922.5\" y=\"450.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436\" id=\"sid-4EA00FF7-F377-4EE5-88AB-B163B70F0436_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"922.5\" y=\"195.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805\" id=\"sid-C01F738E-D51F-4C5C-AFDD-1F7D5680F805_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"303.0\" y=\"195.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-79C7E93B-C8EB-488F-8BBD-F323077A925E\" id=\"sid-79C7E93B-C8EB-488F-8BBD-F323077A925E_gui\">\n            <omgdi:waypoint x=\"790.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"922.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172\" id=\"sid-9B3597B8-6E0F-4DBC-A42F-7B50253CF172_gui\">\n            <omgdi:waypoint x=\"353.0\" y=\"450.0\"/>\n            <omgdi:waypoint x=\"353.0\" y=\"275.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-4D72DB74-7882-494E-A373-460453FDCED0\" id=\"sid-4D72DB74-7882-494E-A373-460453FDCED0_gui\">\n            <omgdi:waypoint x=\"660.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"750.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE\" id=\"sid-37D5F58B-53EC-45DD-BFF4-DFCB05702DFE_gui\">\n            <omgdi:waypoint x=\"972.0\" y=\"450.0\"/>\n            <omgdi:waypoint x=\"972.0\" y=\"275.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA\" id=\"sid-B003A5E1-2802-4E34-B46A-4DB8ED4A9CDA_gui\">\n            <omgdi:waypoint x=\"770.0\" y=\"470.0\"/>\n            <omgdi:waypoint x=\"770.0\" y=\"400.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB\" id=\"sid-19D65EAB-C948-46AF-9E7F-85E4645A38DB_gui\">\n            <omgdi:waypoint x=\"210.0\" y=\"375.0\"/>\n            <omgdi:waypoint x=\"256.5\" y=\"375.0\"/>\n            <omgdi:waypoint x=\"256.5\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"303.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE\" id=\"sid-57CB34C4-157E-4F82-8BA4-F4D11D2C9BDE_gui\">\n            <omgdi:waypoint x=\"490.0\" y=\"700.0\"/>\n            <omgdi:waypoint x=\"535.0\" y=\"700.0\"/>\n            <omgdi:waypoint x=\"535.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"560.0\" y=\"490.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3\" id=\"sid-CF5BD2B4-B5F0-4ECA-8A89-1E8F164DF5F3_gui\">\n            <omgdi:waypoint x=\"770.0\" y=\"320.0\"/>\n            <omgdi:waypoint x=\"770.0\" y=\"275.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD\" id=\"sid-F031E8B5-B029-43A9-ACA1-E67996EB96FD_gui\">\n            <omgdi:waypoint x=\"403.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"440.0\" y=\"490.0\"/>\n            <omgdi:waypoint x=\"440.0\" y=\"660.0\"/>\n         </bpmndi:BPMNEdge>\n      </bpmndi:BPMNPlane>\n   </bpmndi:BPMNDiagram>\n</definitions>\n'),('202',1,'Proofing-Compliance.bpmn20.xml','201','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:activiti=\"http://activiti.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" expressionLanguage=\"http://www.w3.org/1999/XPath\" targetNamespace=\"http://www.activiti.org/test\">\n  <process id=\"sendForProofingCompliance\" name=\"sendForProofingCompliance\">\n    <documentation>Place documentation for the \'Proofing-Compliance\' process here.</documentation>\n\n     <extensionElements>\n        <activiti:executionListener delegateExpression=\"${workflowListener}\" event=\"end\" />\n     </extensionElements>\n\n    <startEvent id=\"startProofingCompliance\" name=\"Start Proofing and Compliance\"></startEvent>\n\n    <serviceTask id=\"csAFPtoProofReview\" name=\"Change State: In Proof Review\" activiti:expression=\"#{documentService.setStatus(execution,\'inProofReview\')}\" ></serviceTask>\n\n    <exclusiveGateway id=\"subRequired\" name=\"Submission Required?\"></exclusiveGateway>\n\n    <serviceTask id=\"csProofReviewtoAFD\" name=\"CS: Approved for Distribution\" activiti:expression=\"#{documentService.setStatus(execution,\'approvedForDistribution\')}\"></serviceTask>\n\n    <sequenceFlow id=\"flow24\" name=\"to CS: In Proof Review\" sourceRef=\"startProofingCompliance\" targetRef=\"csAFPtoProofReview\"></sequenceFlow>\n\n    <serviceTask id=\"ownerNotificationStateChange\" name=\"Owner Notification: Document is in Proof Review\" activiti:expression=\"#{documentService.sendNotification(execution,\'PM-DC-inProofReview\',\'originator\')}\"></serviceTask>\n\n    <sequenceFlow id=\"flow25\" name=\"to Send Notification to Owner: Document is in Proof Review\" sourceRef=\"csAFPtoProofReview\" targetRef=\"ownerNotificationStateChange\"></sequenceFlow>\n\n    <sequenceFlow id=\"flow26\" name=\"to Check Property: Is Submission Required?\" sourceRef=\"csAFPtoProofReview\" targetRef=\"subRequired\"></sequenceFlow>\n\n    <sequenceFlow id=\"flow28\" name=\"Submission NOT Required\" sourceRef=\"subRequired\" targetRef=\"taskPrepareProofA\">\n         <conditionExpression id=\"condition1\" xsi:type=\"tFormalExpression\">${documentService.propertyHasValue(execution, \'submissionRequired\', \'false\')}</conditionExpression>\n    </sequenceFlow>\n\n    <sequenceFlow id=\"flow29\" name=\"to CS: In Proof Review to AFD\" sourceRef=\"taskPrepareProofA\" targetRef=\"csProofReviewtoAFD\"></sequenceFlow>\n\n    <sequenceFlow id=\"flow30\" name=\"Submission Required\" sourceRef=\"subRequired\" targetRef=\"taskPrepareProofB\">\n         <conditionExpression id=\"condition2\" xsi:type=\"tFormalExpression\">${documentService.propertyHasValue(execution, \'submissionRequired\', \'true\')}</conditionExpression>\n    </sequenceFlow>\n\n    <userTask id=\"taskPrepareProofA\" name=\"Prepare Production Proof\" activiti:assignee=\"${groupService.getTaskAssignee(execution,\'coordinator\')}\" >\n         <documentation>PM-DC-Proofing</documentation>	\n         <extensionElements>\n           <activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n         </extensionElements>\n      </userTask>\n\n    <userTask id=\"taskPrepareProofB\" name=\"Prepare Production Proof\" activiti:assignee=\"${groupService.getTaskAssignee(execution,\'coordinator\')}\" >\n         <documentation>PM-DC-Proofing</documentation>	\n         <extensionElements>\n           <activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n         </extensionElements>\n    </userTask>\n\n    <userTask id=\"taskProcessSubmission\" name=\"Process Compliance Submission\" activiti:assignee=\"${groupService.getTaskAssignee(execution,\'submissionCoordinator\')}\" >\n         <documentation>PM-DC-Compliance</documentation>	\n         <extensionElements>\n           <activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n         </extensionElements>\n    </userTask>\n\n    <endEvent id=\"endevent1\" name=\"End\"></endEvent>\n\n    <serviceTask id=\"csProofReviewtoPendingCompliance\" name=\"CS: Pending Compliance Submission\" activiti:expression=\"#{documentService.setStatus(execution,\'pendingComplianceSubmission\')}\" ></serviceTask>\n\n    <sequenceFlow id=\"flow32\" name=\"to CS: Pending Compliance Submission\" sourceRef=\"taskPrepareProofB\" targetRef=\"csProofReviewtoPendingCompliance\"></sequenceFlow>\n    <sequenceFlow id=\"flow33\" name=\"to Process Compliance Submission\" sourceRef=\"csProofReviewtoPendingCompliance\" targetRef=\"taskProcessSubmission\"></sequenceFlow>\n    <serviceTask id=\"cPendingCompliancetoAFD\" name=\"CS: Approved for Distribution\" activiti:expression=\"#{documentService.setStatus(execution,\'approvedForDistribution\')}\"></serviceTask>\n    <sequenceFlow id=\"flow34\" name=\"to CS: Approved for Distribution\" sourceRef=\"taskProcessSubmission\" targetRef=\"cPendingCompliancetoAFD\"></sequenceFlow>\n\n    <serviceTask id=\"coordNotifSubComplete\" name=\"Coord Notification: Submission Complete\" activiti:expression=\"#{documentService.sendNotification(execution,\'PM-DC-SubComplete\',\'coordinator\')}\" >\n      <extensionElements></extensionElements>\n    </serviceTask>\n\n    <sequenceFlow id=\"flow35\" name=\"to Send Notification: Submission Complete\" sourceRef=\"cPendingCompliancetoAFD\" targetRef=\"coordNotifSubComplete\"></sequenceFlow>\n\n    <serviceTask id=\"ownerNotificationStateChange2\" name=\"Owner Notification: Document is AFD\" activiti:expression=\"#{documentService.sendNotification(execution,\'PM-DC-AFD\',\'owner\')}\" >\n      <extensionElements></extensionElements>\n    </serviceTask>\n\n    <sequenceFlow id=\"flow36\" name=\"to Owner Notification: Document is AFD\" sourceRef=\"cPendingCompliancetoAFD\" targetRef=\"ownerNotificationStateChange2\"></sequenceFlow>\n\n    <serviceTask id=\"ownerNotificationStateChange3\" name=\"Owner Notification: Document is AFD\" activiti:expression=\"#{documentService.sendNotification(execution,\'PM-DC-AFD\',\'owner\')}\">\n      <extensionElements></extensionElements>\n    </serviceTask>\n\n    <sequenceFlow id=\"flow37\" name=\"to Owner Notification: Document is AFD\" sourceRef=\"csProofReviewtoAFD\" targetRef=\"ownerNotificationStateChange3\"></sequenceFlow>\n    <sequenceFlow id=\"flow38\" name=\"to End\" sourceRef=\"coordNotifSubComplete\" targetRef=\"endevent1\"></sequenceFlow>\n    <sequenceFlow id=\"flow39\" name=\"to End\" sourceRef=\"ownerNotificationStateChange2\" targetRef=\"endevent1\"></sequenceFlow>\n    <sequenceFlow id=\"flow40\" name=\"to End\" sourceRef=\"ownerNotificationStateChange3\" targetRef=\"endevent1\"></sequenceFlow>\n  </process>\n  <bpmndi:BPMNDiagram id=\"BPMNDiagram_Proofing-Compliance\">\n    <bpmndi:BPMNPlane bpmnElement=\"sendForProofingCompliance\" id=\"BPMNPlane_Proofing-Compliance\">\n      <bpmndi:BPMNShape bpmnElement=\"startProofingCompliance\" id=\"BPMNShape_startProofingCompliance\">\n        <omgdc:Bounds height=\"35\" width=\"35\" x=\"40\" y=\"191\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"taskPrepareProofA\" id=\"BPMNShape_taskPrepareProofA\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"700\" y=\"241\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"csAFPtoProofReview\" id=\"BPMNShape_csAFPtoProofReview\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"297\" y=\"180\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"subRequired\" id=\"BPMNShape_subRequired\">\n        <omgdc:Bounds height=\"40\" width=\"40\" x=\"530\" y=\"357\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"csProofReviewtoAFD\" id=\"BPMNShape_csProofReviewtoAFD\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"1000\" y=\"241\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"ownerNotificationStateChange\" id=\"BPMNShape_ownerNotificationStateChange\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"297\" y=\"0\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"taskPrepareProofB\" id=\"BPMNShape_taskPrepareProofB\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"700\" y=\"404\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"endevent1\" id=\"BPMNShape_endevent1\">\n        <omgdc:Bounds height=\"35\" width=\"35\" x=\"1657\" y=\"259\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"csProofReviewtoPendingCompliance\" id=\"BPMNShape_csProofReviewtoPendingCompliance\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"1000\" y=\"404\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"taskProcessSubmission\" id=\"BPMNShape_taskProcessSubmission\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"1000\" y=\"570\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"cPendingCompliancetoAFD\" id=\"BPMNShape_cPendingCompliancetoAFD\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"1313\" y=\"570\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"coordNotifSubComplete\" id=\"BPMNShape_coordNotifSubComplete\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"1313\" y=\"404\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"ownerNotificationStateChange2\" id=\"BPMNShape_ownerNotificationStateChange2\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"1490\" y=\"404\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNShape bpmnElement=\"ownerNotificationStateChange3\" id=\"BPMNShape_ownerNotificationStateChange3\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"1000\" y=\"10\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"flow24\" id=\"BPMNEdge_flow24\">\n        <omgdi:waypoint x=\"75\" y=\"208\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"297\" y=\"207\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow25\" id=\"BPMNEdge_flow25\">\n        <omgdi:waypoint x=\"402\" y=\"207\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"297\" y=\"27\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow26\" id=\"BPMNEdge_flow26\">\n        <omgdi:waypoint x=\"402\" y=\"207\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"297\" y=\"377\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow28\" id=\"BPMNEdge_flow28\">\n        <omgdi:waypoint x=\"550\" y=\"357\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"550\" y=\"268\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"700\" y=\"268\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow29\" id=\"BPMNEdge_flow29\">\n        <omgdi:waypoint x=\"805\" y=\"268\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1000\" y=\"268\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow30\" id=\"BPMNEdge_flow30\">\n        <omgdi:waypoint x=\"550\" y=\"397\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"550\" y=\"431\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"700\" y=\"431\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow32\" id=\"BPMNEdge_flow32\">\n        <omgdi:waypoint x=\"805\" y=\"431\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1000\" y=\"431\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow33\" id=\"BPMNEdge_flow33\">\n        <omgdi:waypoint x=\"1105\" y=\"431\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1000\" y=\"597\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow34\" id=\"BPMNEdge_flow34\">\n        <omgdi:waypoint x=\"1105\" y=\"597\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1313\" y=\"597\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow35\" id=\"BPMNEdge_flow35\">\n        <omgdi:waypoint x=\"1418\" y=\"597\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1313\" y=\"431\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow36\" id=\"BPMNEdge_flow36\">\n        <omgdi:waypoint x=\"1418\" y=\"597\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1542\" y=\"597\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1542\" y=\"459\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow37\" id=\"BPMNEdge_flow37\">\n        <omgdi:waypoint x=\"1105\" y=\"268\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1000\" y=\"37\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow38\" id=\"BPMNEdge_flow38\">\n        <omgdi:waypoint x=\"1418\" y=\"431\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1365\" y=\"276\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1657\" y=\"276\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow39\" id=\"BPMNEdge_flow39\">\n        <omgdi:waypoint x=\"1595\" y=\"431\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1674\" y=\"431\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1674\" y=\"294\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n      <bpmndi:BPMNEdge bpmnElement=\"flow40\" id=\"BPMNEdge_flow40\">\n        <omgdi:waypoint x=\"1105\" y=\"37\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1674\" y=\"37\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"1674\" y=\"259\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>\n    </bpmndi:BPMNPlane>\n  </bpmndi:BPMNDiagram>\n</definitions>'),('203',1,'Proofing-Compliance.sendForProofingCompliance.png','201','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0�\0\0{\0\0\0(v�x\0\0�\0IDATx����Uu�/��@<��K��c��8\\\n=j����<�]��T\\;�:��l-�N܎��q;e���C���|_+䚏�^&/����g�� �\0f�k�|����3{����g�\\k������\rB��u�%gmʔ��\Zuئ^�z�t���u��v�A�W�s���zױ^}\0\0\0\0\0@����o�q�#7:(̘15��\\6m�\r!< �4�˖]f�<3rȻ^4h������D\0\0\0\0\0��͙s�M���/̚uVز�^eMr�E���g�{���^�\0\0\0\0\0@�IE���3����)o{ln���W�e�W&\0\0\0\0\0����piF�\"�X�P��=�f��\0\0\0\0\0vG5��5���pJ�be��3Ww��}�W)\0\0\0\0\0��.���\r:�5�\n��}�<�L��R\0\0\0\0�]2e���g̘�L�c��zj�ߚgڴi�yʜ�ʹi\'��y�W*\0\0\0\0\0�KF�:lSM�孖A�����o�o�92,X�0�s�a������[�rh���ʠv�o~sş���\n\0\0\0\0\0�^�z�M�j�-�֯_�0(���&�x�w�g�y6t��=,\\�0�o�����իW+��)�o�t�W*\0\0\0\0\0��Z(#Bx�����;��+W�O:��r��y˖0e�g���}�������O=��2����8\0\0:dg��W��fZ>9͚Oo��\Z�J�/ݺUm;��+ǌ9|�A��X�>\0\0�=y��B�L�>=���3�ZU��z�����^z)����_�?_�t�e�A\0\0��+�uƱǎ�0t蠐����On~ּ�Ǭ�eˮ3g�9�]�\Z4���gx�W\"\0\0@�k�`--�6gΜ���6l��������	�ĉ��^����k���ϟfϞ�Ν�W\0PXs�w�����f��l��~gr�E���g�{���^�\0\0\0��\"ϼy��ʕ������\rs�?�����˗��V�h�=p�A\0\0E����~g>K��fy��csC����\Z�,�2\0\0:N�%ĺ��������n��p_�=y�����An�A\0\0E���K3�A�*�z��6�d\0\0@�i�̠������q��T�r�)��֮}����{�[e\0@�v|kz�k����g+3g���{��K�J\0\0:꘸ك��3�8#�w��ᕵk�bb���\r_������z����O�e�/�P�U�� \0��䒳�6t� �*h����\\�3��J\0\0h��=��3arݬ��֮�띟\\yU��ĉÉ\'���N�\rJ֯ߐ/#ׯ_����:�U\0ʔ)��1cj�o��ީ���0�}ڴi�y�[w7Ӧ�|�^�\0\0\0��ك�G}4?�}#~���\ra�����C�}鲆�+V�m�B�뮻ºu���p�\rh�A\0\0�2j�a�jj.o����7�xc~{�ȑq?wa���{�~���w�-���h�7oV�c~�+���~�T\0\0����2q��?Y]�3z��J���\r��+�7�9��c��8���q��v�be\0@����#l�T������\\7���&�x�w�g�y6t��=,\\�0�o�����իW��m���M�-^�\0\0\0���2(���ҁ���Ó+�\rk7��p������Ï>��.���燕+W: V\0Tܾ���?��{�qǢ|�6�>餓�����-[)���{���«��k�v�}_��\0\0\0}@\\o�֭�̠/|���c>�߾袋�;�\rO._�\r?$�Z�ʺ��\0*z�w���gϞq߶4�g��5\ro�z饗��^��樥K�����\0\0�9��ˠ�n���@xٲea�ڵ\r��e2�����1\0@������9s�4,\r�a��p����\'L�&N��߾����^{-��f�Ϟ=;̝;�~�}_\0\0��< �\0\0]e�7kt��y�慕+W巫����t�����-_�<<�bE���j�\0\0�\"��1\0@W��M_[�~cC�>?�����g�O�<���)��v��V��\0\0\0y@,�\0�ʾo�h����d�qܸqo*�N9��kk׾���s�=�~�}_\0\0��< N���0lؐ�ַv�?^u�y�:P�:uқ�ۍ�Ά����q:tP�֭����LK�8 \0���%g�qF����+u�ǜ>}z��.�������z*<��yY��/�^�}_\0\0�B۫��_������?��e˽�3�9>����/,ҁbX��ҝ*�����B��=[|���}�f��\0\0�G��\\�<�L�\\7�祵��z�\'W^�>q��p�\'��u����7������/<������\0\0\0�y@���7?{�������_埏=��1�TI��G)�6Ն���w���K����/���{��������~��g����jk�j�=�t\Z<x`����I���_~ӤJ%N*���K?�������l�m�y�7��u��Ӧ�C��g�~o�=鿭���1\0@��}}��|���ًk7�e<�o����\Z��V�X�mᮻ�\n,������p�\r�e\0\0@e���\Z}�����Ϫ9�C�����?�p�|�����ox�r��?�Z^���cǎʿ��O��;��9�8j�aM�����m�@���<��	MʠT��۷�:+O���kn�����gG���R�vs������ￏb\0�\n��Mҵ���F��A�Z�asxe���1�~xl8�����?��N�L�2\0\0�r��gѴT}�����;)_������J��g��w��<��IEPVW�l���s��-��l\'ˠ�?��c�.�\"�qpC����5�\0t��oi���Rq555a�����φ�_oد{e�k��G����_����燕+W*��A\0\0\0�{@|䑇�p���|\\�c�L��w�1�o8HN3�̙��AiY�����ʠ�dې!�7\\秹2����ڱ��9������ \0�\n��ml�֭�̠/|���c>�߾袋�;�\rO._�\r?$�Z�*4��Q\0\0T�q�l��I�J��t}��ǜ�����5�>����u�f�<�Ւ%]�\'ݮ�>}�ʠ�%K.k�}�%U�-�p�7�w����>��;v�����?��\ZJ�Kߥ��*��u�����\0���}�/�n����}�e˖��k�6|�x��y�fe�2\0\0�3?~���|G~P6lؐ7]#\']���x��o4ܾ��Z��Y�$��O���v�J9��O7�٩�I?;%�N�����٧��-�?Y+eP*x�rx��O�����0���~߸qG�nݪ�,��\0((�\0\0\0�b\0\0��b�\0\0��8 \0��+�}\0\0�b\0�B��^sMu�4�[��}�K$�{�p��I-.�����\r�ϟ��0t蠆%�wf�f��\0\0\0���X�A\0\0�D��}����]����Ö-���|����믿�H��aѢKw��w߿ʯs��c�5/�Ϫ��f�;EDDD:a\0�J�\0�\nz������7��z�������_埏=��1�TI��G)�6Ն���w�\"�K����/���{��������~��g����jk�j�=�t\Z<x`�����I���_~ӤJ%N*���K?��������\' v43h��;z̺uw�i��!����7����V��d�\0�M��\0��)\\�A\0\0]����}��p���u�}��g�q�!�}��ϟi(B������\roR����_�˟t{��Q��>�����+Vܜ5�&�s��綺Ox��?�?�z�&eP*��[o��\'�N�5��[�B���l�t��e���[���Ǿ/\0����ʿ!R�(�\0\0���-���Ϣi���\'���/vR��������,���I��̵y���������g��\\��[���N�A�~��,]zE1���\"�=�-��\0��}��\"��2\0��Ҿ�G��e���q���2e\\~�1Ǽ���H3�̙��AiY�����ʠ�dې!�7\\秹2����ڱ��9������ \0����ʿ!R�(�\0\0���-���/��\n�T����I�����)�/]#�c�`~;]h��3[-Y�5}�����w�JY��&�W_R��B�{�G�Zz���_�c�ˠ��H?��cꯡT��]J*��\\W�����}�N�>P�\r��E\0��ھ�O��p�������\r�k���훅ٳ��p����jɲf͒�:>�����2(��3?��g��\'��t;ݗ���g�>{�X�d��A��I��՗?��ϛ��|�R�}���u�j��2\0��}��\"��2\0�8ƾ�}_\0�2���C��1\0@p�}_��\0\0eb(��={l��\"�R�l�x���i�W*\0�n��)��A\0\0�a(�C=�Ś��x47������~�T\0��vj�R\0��}���:�g̘�����0�k�i�W*\0�n�)��A\0\0�a(�����\Z<x��-[�u�Y�<]UU�D�3��J\0h��p��� \0�r�\Z��\r\Z���Yg9�,XƏ?���Y�\n\0�~��3�A\0\0��5@��O��mX��j�IM�7ſ�1���\0h��p��� \0�r�\Z�(����P1������o2��\0�}��o*�\0\0ʱj�\"ۣ�[����~e�k��\ZAuKåA� \0�8�ϩ\0(�~�!\0���ݺ���������O?�w����/l�T�����q�����6a�Q�VUU=���di8\0�:��\0(�~�!\0���1���T����ϖ��cfƌ��\0�؃p��2\0����\0�t#b^��k(\0\0(��={l3�س�қ�\0\0:e�)\\\\\0\0(�C=�Ś��/MZ>9+͚\0�l�A@��f��A#\0\0Eu�QG�8c�T�KA������O\0�l�A@�15f�a\0\0���7j�����l�W�R�<]UU�D�:�\0@�:�n1��L2\0\0ՠA�5�,�K�2~�QW�?�\"�P\0��R���1O��6\0\0Ի��yۆeˮV�$55W��./ļ��\0褔A@�37�<�\0\0@��8��+\n�bAUUU+��d��%\0Љ)��NgH̪��\0\0PTc{�x�_��ݯ�v\r��\\#�ni�4#H\0tv� �SJ3��\Z\0\0\n��ݺ���������O?�w����/l�T���l�x��n�釷M�pԵUUUOd�kY\Z\0�\n�A@�����t��\0\0�3#�1��֥��%����1���\0�.DtZ�b��f(\0\0�PU1�c�\n\0\0v�2�Ԗ�L5\0\0T��?;�0\0\0��A@�6\"楘��\0�\nջn�v��\0\0`)��N��\0\0@��s�a\0\0`)��N/�\nJ�a(\0\0�Pb��}\0���������\0\0@K3��\r\0\0�@t	�b��d(\0\0�PóҌ�ކ\0������c�v�\0@���f�\0��P]�ܘ�\0\0jL��*C\0�NP]ʐ�Uu\0��Ɯ`\0\0�	� ��I3��\Z\0\0*T*�j\r\0\0;At9�A��AG\n\0\0*PZ\".-7�P\0\0�F� �K��pL7C\0@�\Z3�0\0\0�F� ��ZRw\r\0\0�&�v)f��\0\0�\r�A@�5��\0���\0\0�M���0\0\0�� �K��.\0\0PiĬ��\0\0-Q]Z��f�0\0\0T�43��0\0\0�\ne���-1\0\0T��Y��M�\r\0\0-P]^��Gb&\n\0\0*м��\'\0\0h�2 ::���;*\0�<cb��T\n\0\0���37�<�\0\0@��9�0\0\0�e@�!1��>\0@%IEP�a\0\0�� �F�̠��\0�\n���KKō1\0\0�2��t͠t���\r\0\0fj�<�\0\0�(�\0�3)��n�\0�\n����R�0C\0�v�A\0;�$+��\0\0*����\0\0�Q������ʾ�\0�\n2 fM�G\0\0��h��u\0�J�fU\0\0\ZQ4#�\nJ��F\n\0\0*����ކ\0�:� ���-1\0\0T�y�k`\0�_�A\0-��H�$C\0@�<��P\0\0�)�\0Zut�әe6\0\0�,�1\'\0\02e@�̍9�0\0\0PARTk\0\0ȔA\0m2$fU�G\0\0�i���T�C\0��)�\0�(��k\0\0� Sc�\0�.O�F�A��AG\n\0\0*h���a�\0�KS�I1�t3\0\0T��1�\0�.M���d��6\0\0��YS�\0��I��Fd��6�\Z\n\0\0*D�Tm\0\0�,e�.��.\0\0P	�g�74�6\0\0]�2`��;�a(\0\0��2�\0tU� �]���\0\0*Ę��1U�\0��Q�n1��L2\0\0T�ژ\0@���\rG�<�Y{\0�ʐ��Z�\0\0��(�\0v�ܘ�\0\0 -���c(\0\0�e�n\Z���#\0\0]���<�\0\0Х(�\0�A�4�0\0\0P��/�3\0\0]�2���ӵ��6\0\0T��1�\0�.C�N&�<��P\0\0Ppb��}\0��S��%Yi\rv\0\0(�43��0\0\0t	� �v4\"+����P\0\0Pp���]{\n\0�NO��.�\0\0ݼ��v\0��@��Ҭ����\0���<��P\0\0tj� ���]��0\0\0PjcN0\0\0��2�t�y$f��\0\0��RTk\0\0:5e@9:���y\0(��D\\Z*n��\0\0贔A\0hn�y�\0��K��3\0\0��2�\r�YU�\0\0�*�f)f��\0\0蔔A\0,��k\0\0(��1�\0�NI��һ,ӵ��6\0\0؀�5u\0�\\�A\0{����c�\n\0\0\n,��6\0\0��2`Y��.�\0\0E5<+];���\0\0�T�A\0{Ȉ�뾆\0���y\0@g�؃.�\0\0՘��1U�\0��P�AiVP�4�P\0\0P`�1\'\0�NC���%7�\0\0\n,A��\0��P�a�b��d(\0\0(��D\\Z*n��\0\0��A\0ept��1�\r\0\0�f��3\0\0��2�L�Ɯg\0\0(��ƥt��a�\0��)�\0�dH̪��\0\0PD�c.3\0\0OPFif�\\�\0\0@A\r�YS�\0�ʥ(���F�v�ц\0��J3��\r\0@ES�٤��c�\n\0\0\nhxV�vPoC\0P��A\0�$f�a\0\0����_\0�h� ����m��P\0\0P@cb��T\n\0���(���\0\0ETs�a\0\0�H� ��H�����\0�JEP�a\0\0�H� �I�/1\0\0PZ\".-7�P\0\0Te@�t�y$f��\0\0��қ��\0���(��c���m(\0\0(�����6f(\0\0*�2���Ɯg\0\0(��1�\0���(�!1��>\0@��YS�\0�ʠ(�43h�a\0\0���̠j�\0\0P1�A\0��cO�:�P\0\0P0óҵ�\\�\0�2(�\0\nlR��1�\0\03/f�a\0\0�� ��[� \0�\Z�<��P\0\0�2��Fd�%8�\Z\n\0\0\n�6��\0\0Px� �\npq]\0\0�HRTk\0\0\nOPҬ�4;h��\0\0�@�qi��1�\0�ДA\0\"]7h�a\0\0�����\0@�)�\0*D��Gb&\n\0\0\n�wV��>�P\0\0�2���t�7\0\0����\0@a)�\0*�ܘ�\0\02 fM�G\0\0�GPa�Ĭ��\0\0E�fU\0�BRT�43h�a\0\0�@�g�kY�\0�x�A\0(`�km(\0\0(�y1S\r\0@�(�\0*Ԥ��c�\n\0\0\nbL��*C\0P(� �\n�$��K\0\0��6��\0\0P(� �\n6\"+����P\0\0P��5\0\0���p�\0\0� -���c(\0\0\nCP�Ҭ�4;h��\0\0� �R��\0@a(�\0:����\0\0@A��JoX\Zf(\0\0\nA�	t�y$f��\0\0� ��\\f\0\0\nA�I�tVz&\0\0�ۀ�5u\0(/e@\'27�<�\0\0@A��AՆ\0��A\0�Ȑ�Uu\0�܆g�k��\0P^� �N&��k\0\0(�y1S\r\0@Y)�\0:����t���\r\0\00&fyL��\0\0(e@\'4)��n�\0���9�0\0\0��2��Z�Y�\0�bHEP�a\0\0(e@\'5\"+]����\0\0���qi��1�\0�,�A\0���u\0�rK���\0��PtbiVP�4�P\0\0Pf���M�\n\0�=N�ɥw`.1\0\0����\0��\n�e\n5�.���M�2��Q��ԫW�P���vN�nU�:h��1c��A�:֫\0�4 fM�G\0\0�eP,W^��3�=v䆡C�3�����æM�!�d�t�!��ƍw��kil�-�:̜yf8�w�>hЀ���=^�\0\0t�43��0\0\0�Q� �8��9����f�:+l�r�\"�s�I	ӧ���]tѿ�}��{�^{���W$\0\0`xV�vPoC\0��(��bHE���3����i�<��m�_�>�����csC����\Z�,�2\0�\0�ҵ-\0�3�A@����Ҍ EP�&�J3����T���cmf�8\0\0�ߘ��1U�\0`�Pe�\n��^�\ZAii8�M�&]3(];(]C�-��9���ݻw_�U\n\0@��9�0\0\0�� ��.��쯥��5��Ln����}��x�6=�o�>��?�h�T\0\0�Y*�j\r\0���kʔ�Ϙ1��2b�N=��P�ӦM�S��(�;2\\z�9mz�i\'��y�W*\0\0�,-���c(\0\0:�2(�Q��TSsy�ePuuu�����#G�,��{o�o����n�喼ڼy�2��<��ua���a��%�>�7����q\\��J\0�L��g\0\0:�2(�^�z�M�j�-�֯_�0(���&�x�w�g�y6t��=,\\�0�o�����իW+�Zɗ�|R����6qL�x�\0�zǼ3�P\0\0t(eP�\rQ�eD�?�|^��qǢ�r����I\'�^��7o��L�l~߳�>^}u]~����R��4+(�J��Z{��Y\0\0Ё��\\f\0\0:Tŝ��;fR�옅1Og�w�ן�|!�1?�95f��1C�R�L�>=���3�ZU��z����B/��R~�믿��t�R�ĵ1�A��A� \0\0�h@̚��\0\0t��9�72��M1�bΉ34�{�ǥ��Ș�d�u����L��q@�6D͕iٷ9s�4,\r�a��p����\'L�&N��߾����^{-�=��0{��0w�\\�O+y���{�;,�p���A\0\0�S�Tm\0\0:L���r�1+�vwv�O����R��P�q��P�\rQ+%D�y�慕+W巫��������}˗/O�X��{>�g��+�С��ƍw+�\0\0(��Y��A�\r\0@�(����\\*�f���?/�J�ȥF{��Cq6D-���ol(~���|غuk�}���ɓ�ʟ��k){ږ�N�H�>��� \0\0�)��1�0\0\0t���fV���v����R�<f(Ɔ�-3�~��\'��ƍ{St�)��_[����ǟ{�6�gn����?*�\0\0(�1u��U�\0����^}4����Ҭ������拊�3�8#�w��ᕵk�bb���\r_������z����O�e�/�P�UEO[�f�B� \0\0ʨ6��\0\0��\nw~/-\r��EP����.3�����g�<&���yi�����ɕW�O�81�x���tݠd��\r�2r���>���g\'������!�\0�LRTk\0\0�]���\r�J�u�ۃ�3�BJ�w�Z��m��+(}�Ѽ�x#~���\ra�����C�}鲆��V�X�mᮻ�\n,�֕�t�\r7(yv27�����\ro�q�2\0�rHKĥ���\n\0�vU��{7��*��]��(߆��e�ҵ�6Va���y�ʡ�6�W�o�s�ǆ�?<p���N;�4���b�=vd���s�A\0\0��Ԭ��M\0\0�Oa���J���)��~w�����\0���T�_�>_*���&80<��ٰv��\r�+�_?�D�o����ϟ??�\\�R��y����8�\r�W/Q\0P�c^�f(\0\0�Ma��]���l+��1��z��l�Z*��mݺ5��/}5y�G��]tQx琡���+°ᇄU�V���;��/��<� \0\0�dz�e�\0������1���5����4;ȵ���,�9�-���A��v[Ì�e˖��k�6|�x��y�feP;$�\nJ���,!e\0\0e0 fM�G\0\0v_!��M�J��)��fm;q\r�}��J��-=NS�����)�\0\0(�43��0\0\0��B��KK�}�\0�cFf�8h�\rL�,�v\\�*_\n�7޸/�����\r7|_\0@9�J��m(\0\0v[!���c�<�s��v���(�/�|)h�.�\":H\0@�̋�j\0\0v[!��=3�\0�#=��&�]70-%�i��VK��ګ¸qG�޽{��={�~�����T�<����/=�C= \\��ey�56\'��e\0\0�2&fyL��\0\0�-�8��%�{��-u;���W���1������b��w�_���\'fߘ�1���E.����_��yG�;c�ļ++P�sp̰�4���w�\Z�71��ޘ��sD���1�b�{V:������4�⨘��9:昘cc>��fB}$��c>\Zs\\��1�b��L��x�\'b&ƜsbV��ӧbN����O�L�9%�31�#fJ�gc>����b��1�ǜ�������/�|%fZ����j�?Ŝ��Yi	��ǜsN�7b΍�טo�|+��Yi-��1��JK���ݬ��߅1ߏ�A�̘��(fV����q̿�\\sI̥Yi�´F��1W��$�ʘ�b���&���4�g1?��6�1��̍�>�Ƙ_��*+������cn��-f~̂�T��:���4sfq��,�J3֖��&+]�ꮘ�cj����/���b��]�C1�<\Z�X��1O����\'c��Ǻ���bV��߬T�>�l�s1�����?Ǽ���jx9fe̪��Y�⮯��%�՘uY�eP�\n�4;�[��0lؐ��C���=��������eА!����O�5�Z��a��I�����Wy������q{����+�V?��\0�j�G\0�u��x�s��5捬TT��Z̦��1b�g�����w:�6+�O\'����t�<�0O\'��l�t=�LO\'��	�t��OY�{:��N§���|:A���t�>��O\'�Ӊ�tB���tr?��O\'��I�T\0�\"�ެT\n,��YN�A*��J%�ҬT*�r!\r�p�#+���HeD*&RAqkV*+Ri�ʋTb�B#7d��#���HH*BR!�Y�I%�5Y�8Iʜ�T��R%�+�hI�K*^.�J%L*cR)�\n�T��[V*mRy�J�T�b\'<d��\'�>����J��yY�J%Q*�Rq�\n�T$�KV*�R��J�T8���̬TB�2*�R��JEU*����ʫTb�2+[��:5+�]��J�W*�R!�����RI�ʲT��Xw��ɬT��r-�l�lK�[*�>��ʸTʥr.u���PV*��Je^*�R����f��/��L�`*ߟ�J�T��0���HL��aY�\\L%c*S�\n�TD��J�TN�J�e*.g�3����Lg*:S��?+�Meh*FSA���T�6W�<P7=��ݵTF���������\'��\\rv��~��o�+<���&_K3t>��䳋Z���_���C�[��=L�2.��m�\n��_^�����<�\r?o���w^^��̔�=�v�׎�ߎ����Z){�v*�Ҍ��� \0\0�焺c\\\0\0v��AE�D��I����{o���/H�n������?�Z^���cǎj�����ߧ��9���_{��G~ѤP�ԧ��W��9�8j�a;|��\\S��}_���V˙�_�כ���(�n����T\0PNi����1�\0`��fP#i�kA�n`R�l�4+��ҷ�B\"���Z)�v�yZ^nĈ��?�nFΎ�g��iG_�\Z��i�9/Zti�rqg����޶>�����Y������/���s�A\0\0�[Z�a�a\0\0�e�8���+;�\0�#-��[�	h7��zӆ�-��-Yr�N�A�eLZ�-kcT_̴Tm�Tۦ2�~�O}y���m������^e�W�:9|��o��\0\0�\\��{�R��\0�.)���t����y�������=�!j��H3h�cRyR��T���\"k���ݻg���R֭����9������|���_��g��oWW����M3���y����rȻ���?��w����\\s�7ۉe��r�q~�������V��Oe\0\0E1=�2�\0\0�K\nq~��4;�܊2C	�܆��Y*�J��I�d�u�\n������/l�ؙ7�a���aʔqaܸ#���\Z-����I�	\Z6lH^<]u�y����I_[�fI�<��&˶��^w�w�С��YFiVS*����g#��w��o��5�{�*��y�D���\0\0�4 fM�G\0\0vN!����Y�O��к���k���hW�2�Ȥ�A鹥B��ϯ�r�焣�:\"l�v�2\0��I3��\r\0�N+���[cN-��OK����ʳ!*R�f�Y8iFЇ>����g�w�\"(-M7`���\'���,#\0\0(��Y��A�\r\0�N)����b�Z��|GH3�V��z��l���̛\"\'-G��o|��%�\0\0���L5\0\0;�P����L~o�t���oC��)-�4p���q��� \0\0�lL��*C\0�f�:�wL�1���ߙv\"��Z��m��1��k�ՆaÆ����ïg� \0\0��6��\0\0�f�;��f��\"�3�������u\0��)dʛo��p�j��2\0�bIEP�a\0\0h��K%P�v���=C�Ҍ��z\r@�7D\n����U�������A\0\0T���c�1�\0�M\ny~�VZ.�����4~����!Rʔ/��0k�Y->&S\0P<Sc�\0�6)����1Og�_�����3#\n�!Rʔ\'?����#	o�q�2\0�J�;楘a�\0�U�>��f�w�<���Y�d���A��)f�|֮�	���ý���V�)�\0\0(��1�\0�VU����d�e�R�s�N~�И�4hv����P�\r�rf�g��Iy���L\0@1\r�YS�\0��U���T�T�<�,ff�Ęwo�����̟1�b�e�i��5wC��ٳ��k A�+�,U\0P�.�;_\0\0@�*������Y�B[��CR����蘘^��Pl={�ضiS��f%]��Ç�k���M�߸��?�mg\0���g�k�6\0\0��fo��=�kj.W����Gg��cG���7������~�T\0\0\n,]ox�a\0\0h�2(���:��3�*j�@�{nA�����?����3a�Q�f�ٖ\0\0PTcb��T\n\0�R�5xp�Q�ܼe˽\n��	\'|(TW��3��tUU�YiyN\0\0(�ژ\0�)���4h��f������v��\nÇ	�����3���ϳ�+\0�\n���Z�\0\0�C� ��ݧ��6,[v���a�]a��Aa��K��=55W��./ļ��\0�\n���KKō1\0\0o�\nc���}_Q��9��0y�q;UUUU����^�\0\0T��1�\0��(��Bۣ�[����~e�k�O��0`�����h�5�ꖆK3�A\0\0T��1/�3\0\0M(���yw�no�}��z���O�ݯ}��6�*vv!۶��:�0{�7�}�ƍw�馛~xۄ	G][UU�DV�F���\0\0�T�c.3\0\0M(���:<fF�oc6�m����%����1���\0\0�pb��}\0�D�I��y1+�j\0\0Е��AՆ\0��2���*�G�\0�.hxV�vPoC\0�StBG�<���\0\0���3�0\0\0�A\0��[c��9�P\0\0Ѕ��YSe(\0\0�A\0��91�\0\0�j3o�\0H�A\0��И�u��^n��u�%gmʔ��\Zuئ^�z���_i�t�V�����3�����c������\n\0�x���\0��H3��6@Ww��:��cGn:tP�1cj���<l�TBx@: il�-�:̜yf8�w�>hЀ���=^�`{U�l�z_8��w�����\n\0��A\0�ĉ1�g�ktYs�w�����f��l����2䢋�%����ګ�\'�\"���ܹ��s�ĉ��^\0]�2�x{̳1G\Z\n�+K\'V�;�ӻ��\0-o{ln���W�e�W&�^�37�����_�^\0]�2��s�a\0����Rz����*�z��6��^�9����_<��\n\0�ʔA\0c�\n���ц�^�i�%\'=���3�\\ݽ{�%^�`{Uμ������\0�ꮨ!\0�\\U1��|�P\0]�%����t�u��(f����\\�3��J۫r&����O��\0�*e@�\Zsg�[ЕM�2��3�:�Y�L�v����4�+Z�^�ة��\Z��㿧i�y�7-��U~��t\r!�+\0�RT��b^��Ctu�F�����6�\\0�����5n�������|l�p�%Ӝ,����7W�9����J�ֶW����:�x���#G��{�7��~�}��rK^m޼Y��8�p���^\0�ݭYݛ�Z�}�\n���\Z�,�իGش��M\'}��å��~rr�py�E\'��w|�p��}��.9���H���?��Thm{��������������3φ�ݻ����-^�8��z�jePs�]W��~gغ�>�+\0�3Y�O�R4�0�ؘ���P\0��|\"p��>�����\'|o\\ʾ���׏��������JfIh��*��>��r���ʕ+��\'�tRx9�޼eK�2��}�>�\\x��u����zJ����~���^\0��¬�\"hYLwCPl=c�s��\0h�擀����ïg3<���9���1�Yc�:|�#��Gol���Re�i{U2}��гgϰjUi����k\Z�_z����_=�|�ҥ���ɤ\"(B�W\0@\'72k��hx\0��:�F�\0�D�O�t�aaӋ�{v��~�������5O?n��{¶��:Y���l�Ҳos��iX\ZnÆ��ꫯ�oO�0!L�81�}�����Z~{���a���a�ܹ�5mLZ\".-�����\0:�];(]+Ȭ ���2����6��a�\'��\r/?pC���/b��o�y��W�\'8Q���m�\Z�ϛ7/�\\�*�]]]�0�������[�|yxjŊ&�c[��\\z�9a��cl�\0��nG�r� �\n�8櫆�M�t�o����g�[�Ŵ���M��˳񡟇_}md��o~?lz�>\'J�AP��U�ں��������n��p_�=y�����A�5m�ƍw����?��W�W\0@g7/3+��L��]L7C�&���{}�������-�?�>��׏���uB�t���g]��K�e�\\6*��AP��U������?�7nܛʠSN9%��ڵ�4<��sϵ��ɜw�?�/~��+\0���h?�8�Pl���9f��\0ءO�m��@���5�]��\n�����gχp��!L�7�Ӗ���!T�&��\'7�w}���n[޸��Re���U�g������ڵ����ӧ7|�.��{ꩧ��?��E/��B�Wmkv&/��(��_�m�\0�N���Afܥu`�Z<���%a���y#\\�HӖmg�l_�}[�r���붅I�l\r�.�z���y�ou�T{t{��3τ�u�~^Z�.�w~r�U��\'N\'�xb~;]7(Y�~C��\\�~�>h[�I3�����m�\0�Τ��13cjb�˚^3腘e1?��B�PCP�=����1\0�j�d�C��Mx��s��W�\ZN]�iB��-����q[��Ϸ���\Z>>{[���Y���_��9�=N�*�`�n�}�������n�x$����K�5��X�\"l��]w�,X֭+]/�n��م���ʯ��!d{\0T��Y��@����Okْ�ʡS33�\0�&]衘O\n��x����&���;��ӯ�/�N�1���m���m��9�§�#��W�pί^�O�YX�xF��Ü(U��^���?��G�΋�T�ݰ9��~S��c?<6~����w�i�Y&n72q�1��Kϱ�\0*�Ș���+��˓Yi�\0{�?�,2\0�j���]~�����N�^��Sk��n�>�(�����9�����Ë��IRe���UZ�m}�T\\MMM8p`xrųa����_Y�Zx��\'�~��?~���a�ʕʠ��]w]>��a���l�\0�J�4�g��>c�c��98f����J���f��v4�(�?���i��2f��\0hU�\'��nY6lXΝ��a��~C~�/���n�w]8b���~����ջ��:A��=���/[�n�g}�K_\rG���E]�9dhxr��0l�!aժU�)ۚ]�>����_���\n\0��ܩɚ8��u������\'朘�����Y�L���2�ۆ�����#��8\'3�A�	�WMˠ�n���`|ٲea�ڵ\r�/^�8l޼Y�NIEP*�l�\0�\n����fM��t͟�w���}5k:�(L�9@�󇘞����w@�I��-=��Le�^ɮ&-���KK��^\0�����T䴗w�<���e��v���W��16\0;\\�xG�w2S�W�[���s�ĉ��^\0E����9;j���u���g�����F�� ����19���+٭l�xw8���\0PT#��K�Uw���U�/[��^����\0�>�y9f?C��$�S)-��R��o�<�aΜoUJq��ϻ�g*��x۫���^ƍ;2���3���#|���\r7��������N�z����/l�U��3ߖ���a�]�\\(7������u�ڭ��y����\0PTeM��}��T��l�{g�3\0쾷��s����\"��N>����v:�ZI\'=��yg�/�D���K�^��Æ\r	=tm~�O\\N>��Ð!�����?ݚ�;S�Nj�Y��\n�\rȿ��ot�2�~FϚ5Kv���������.\0P c��K�����~�P�����{b���w۲��������wB�z���[g��;���O�<x`�	��~�y�f�w�7�?��w����;�yz�~㯥�I߻��������N\'>?��fL�|\\���?���aԨ��S��k�s��N�6�|ݺ;ôi���ٻ�\n$����jy{��w�����Ο��k��c*�RA��%��E�.�w����{[ھ����n���L��/��ϩ�m[�~�V6�^\0�Э��wf����+���4�żs���p���}��tR��q�� L\'(?��cÊ77�w���6<�S�\Z��W��t��ϸ������vZ�iG�$���&�Ӗ����;�pM��Ҩ�8J�������k�� <��/�\\}���o_u�y���=��JI�H��֭���k�\\S����)m.���p�1��o�Cۤ�ii�֖��u[t��_�?��7O�??��χ��jq������g>�^\0��j�T����4��ad��ܵ1���\0욫b~d�\"�`ag_/��!O̚_y�N������Y��>͕&���K:�wܧϳ���~~{=�t�8���NER���r��2���N�B�l�R�%���K�{ꗋ;��O�����01��g3���]���{���Zݾ���Z�-_>/�<-�>O_|���-m��A\0@\'�(��Vsr��:�岼���O��y6foCA�;��wֿ�u�^�ʦM��>��gZ2)}^R��������wD�N�6>������Pu��>���\r��-YrY�״i�u�6l���Tɶ�q��E-m�����l�R���C�����V>C��mcK�/e\0��Α��Rht3����q�e|�3\Z=�Y�t\0;�1OĜ`(�b;:��-Uҵ.�}�է��O>ycû���E�߹�x	�t��������믥��eZ��������#�<<�����̗gj��cǎ�o?���p��|�������Y+嗓��s۫�ن��h��vN*��ҍi&Pھ�mQz�!����i��g�%ڲ�%�Zھ��km��̜yf~ߠA�,A�ܶ���W[���W\0@�#i�4�g��B5��~d�뤬iy�N8\'�6�@�сv]�n��.t��o�xɢƏM�,����Y����k���0�7?�9nܑ��ݳf.b���o��~��)�v�E��E��E�=���6��\'��>���{����f�:����	紴T�N�B�n��J�T֦�i��f�\\��y!��=�3iQ*iZ����W�=6��}�K��=���[[���mQʟ�tk�Rs�g5�mli��\0:�9����q�5���2>��4z/����И�Yy.���сv]��f��PW����۫�ܾu�m���HOC\0�s$-e�v���2��{.\0�Q�t�a����@���:�`���:N�BѷW��}���>�+�NϬ���\"\"{:E8�c	`\'��xV�ft��`���x])\\�\\�+���ټ�)���s$;��1�c�gfT���<����oP�h�ו�N����^������s$͕@�\\3�B͊��0��wt��_WN`:�\n�Wb{���ż��P\0x�$eY�Ĭi	T��������I���\":���bL?CA�сv]9���*�^�������a\0:Ȏfm��J��2>��F��\":��U���9CA��r��U���+�|2f�a\0��3��0��<k�<&��\04oj̝1o1tqN��!�+\'0�\\�+������e�?��,TIQ󬽘JTXjVȡ(��<嫖i���x*-+3SQ��41<!\"$J*(����y\"�bqR��}3����.��s�~?�߇��#�>s�������W@;HL;��R\0�w���>�Ҙ�:�w�����;\0T���8\'f�\0,��ve��*�W�_�亘�*Ё�_7���c���1�\0M�!�|e�,,��ve��*�W�_�dp�_��@�O�J�z%���eV�\")����s\04n��Ŭ��e��vٮ,`Z\\�J�+����ffũ�\0:B�����9��?������S\04�k���R\n�c��vٮ,`Z\\�J�+���9I�T��A�:�x��	\r~�N�\0�3V`%Xhs]�vY�x��1�4�=�b���+�J�*U:��#�\0t�;3g��Y�ց��Ow��4n똹1�*��0�6���[�>e�2�4��v�]��4Ö\n��~T�5c^��R)��If�zc~@3)f�v�9i4�����\0M��Y�RJ`e�A��=��e�Q�X�,��Ǎ��t�-�+�\n�p���P��?f^�AM��O[^�,�g�EЄ����q�c��Y��!`e�A��޽���ݻǒ�K��Y~y�S�NO�X@�ү�+����2\0e`X�����*�o����.�/���A@3ҹ:_���(� �E�^ϼ䒓,f�Y����̊S\0��~T�5bfgV,�t�t-���r�o����Yq\Z��\r�W�� ����4�0���]�nZ8m���e�)S��-�]^�\\\0��~T��ǜ�@��WƼ�Yy���B�1����|p]��s�;�UD#cf5�i(t������tS\nh�a�i�=6|�ky,�v�����7b��J����e\0�Hz�N\Z�4|�Pk2.��1\0�J�J��\n2���ӥ�Z�9����s͍ܩ��3�-��~�_�(=���\0���1�b�eZ6\0J�J��뫄\0�91��P� Ja�5�\\c�z�;稣���{���k�?l᳝�h�C/�v��w\r�Ǎ�����C�&�J��Յ1�+P�ҩ����.ʬ��P��Ƚ���4������xi�>7fk��fQJ�M�Qɬx�S�vI:����F��W�P=�ye\0*D��>P�\Z�\0��֌HB��\0\0�J�Ϙݕ�\0�:@�\'��P����	�P� \0\0�ҝs�2\0en���\n�ǵZa�̊G~A)�h�A\0\0@��!��5�(cwfV=mnzuPg�h�bnPh� \0\0�\Z�-��\0���^��0�(ގ1sbz*��a\0\0P\r~s�2\0ejB��AP�4�(Nz�Ԙ��Z�0\0\0�[ż�I)�2S�UA����L���b\0\0T�t퍽�(3�]+��kP@���c�*��a\0\0P-N��R�\n`=������2��\0\0��m37��R\0e�z@��r�zJv>\0\0\0��b�U��Y�(�Z1O�|])��\0\0@�q1�*P��\0鴘���|\0\0\0ԳI��1]�(c�c\0��\'���}��|\0\0\04p_�W�(c�c\0��^t�2��\0\0�F�o��(c�c\0��_�S���|\0\0\04�Q̼�u�(S�c\0\n�p��1��|\0\0\0pO��\0�)�1\0\\s�2��\0\0�f\Zs�2\0e�z@v�y=��R��\0\0�ft�y\'��,#\0��z@#:�<s�R��\0\0�\"�s�2\0e�z@#���\Z��R��\0\0�\"\r��K�2d=���1sbvP\n��\0\0��eV�*n�\0ʌ��n�9_��\0\0@+�1�e\0ʌ��z���W�:Jv>\0\0\0Za��I�\0��1\09]c�/f�R��\0\0�VZ;f^L�\0ʈ����1c��|\0\0\0���c�U��X�������R��\0\0��4$�e\0ʈ��hr�	�\0v>\0\0\0�@���2�t\n��1@��ט5��|\0\0\0���3�x\n��1@M[?�՘~Jv>\0\0\0�Зb�+P&��\05�7�\0v>\0\0\0�R:�1}�(�c������tS\n��\0\0�.�9U�2`=�I��9�bR\n��\0\0�N>�Yq�b��f=�I\'�LR��\0\0Ў:eV��d�\0:�������R��\0\0�v���3��`�c��sk�Y�\0v>\0\0\0J����\0t0�1@M�Ϙ�Jv>\0\0\0J�Ř���@�c���n�1_P\n��\0\0PB?�9W�d=��ܠ`�\0\0��>�2\0�zPv���S)��\0\0@���]�� �c���F�Ԙ���|\0\0\0t�Q1)�A��\0U���1���|\0\0\0t�t֒�2+��\nPj�c���=����Jv>\0\0\0:�S1�Q�X���51?W��\0\0PΈ��2\0�zP���y9f=�\0;\0\0\0e`�W3Ne����*��t�ו�|\0\0\0���b�T�Ĭ�\0U鴘���|\0\0\0����V��T�>1ss�v>\0\0\0���1sb:+PB�c���^t�2��\0\0�25-���\0�������Tf�5�\0;\0\0\0����1�\0����j|8��Jv>\0\0\0�X���b�(P\"�c��qI���\0v>\0\0\0*��1_Q�D��\0Ua���c�+��\0\0\0�\0G�ܠ@�X�*^��GbS\n��\0\0P!6����R\0%`=�x��L�YC)��\0\0@��\re\0J�zP�z�̉�A)��\0\0@�9<f�2\0%`=�h�ܺ�+��\0\0\0�@�Ǽ��R\0��zP����Wƹu��\0\0@�3�`e\0ڙ��\"u����AJv>\0\0\0*X\Zݡ@;�T����;\0\0\0��Ù��[_)�vd=�8[�̍�T)��\0\0@HOx=L�vd=�8�cNP�Hs;�\0\0���1�hG�A@E�ט5�*�>1�b.��;DI\0\0\0���\'m�@;1*F:w�1��*N~�\0�W��\0\0��s�2\0��0����,\rA��\0\0P��s�2\0��0����wL7����^Th����k\0\0��K�[1U\n�e/]hV�AJ��A�W\0\0�0&�xe\0ځaP�N���PQ������\0\0\0���1+�����;��\"�[+T�־�\'}����\0\0P�:�̉�\\)�6f���Ncn�9Ky�b��)ߜ2\0\0�u��9E�6f��1{6�mp�?c�*T��5�����\0\0�D{�<�@3:\\�\\3Z\Zsi��1�Ƽ�偊���)޼B\0\0�U�b^�8e>ж���[���o��Ai�\"�7�A�\Z1����\r����\0\0���9C�6dt�����A�L��<P�\Z��\'\rr��݇��?j���\0\0�Z���\'�hC�A@�;4��0(eq̨���	�Nc�r�v��p1����|F���k\0\0�&\'���R\0m�0�p�e��L��N���45�il�Sh ���n��]C\0\0�5Ŝ�@1:�%�Ƈ@i��{1k+��� ��S�;*v��q\0\0@-I�X�?e\0ڈa��~�Yutg�&Je�؁Ls��������\0\0j�s1�T�\rnR���\'b(	��������P�L�Ay�!\0\0Ԋsc~�@0:�S1�b~�Y9�,���=�\r���i� hu\0\0�J�s̋�\0�� ���5f�2@�Z�W�46Z�AP�k\0\0�����\0�&� �C��RL����~ng���>��p\r_��������Zߟ\n\0\0�Rg��B��dt�u�\0�Z��A�}N�W]��� \0\0�B���wf��L�6� \0�Yi�2?�>�j�@�5�\0\0�Z2+���\0�� \0�(-}%Nc��t��N��ޒ���^�\0\0�Ҝ\Z�e\0V�a\0P�b_��� h�/f �[�܌A\0\0P[�ļ��R\0�d\0�Hs��inT��\n\r���O\0\0\0Ԡ�1_T���\0�k�B��\n}~c���3\0\0jՉ��%��0\0Z%�B��)۾�)~���@hD���\ZA\0\0@��4�͘.J��a\0�j�A�����;��2�|�>\r�?\0\0@-���D9�u�\0����qi�3\"S� (���54\0\0Ժcc�W��\0����5�Z��S�\0\0Ժ�c���h!� \0�M��@��K\0\0\0�dR�~�\0��a\0�fV��nN\r\0\0P�1T���\0�6����=�W\0\04mØwb�S\n��\0�6��S�95\0\0@��9H�0\0�E���b��S2\0\0��|+�ve\0Z�0\0h7�o�3M��-�>7c\0\0�ɬ8U\\7�\0�d\0���^!��p\0\0\0����C�(�a\0��\Z~�\0\0\0V�1�(�a\0P��%� \0\0�յnfũ�+P� \0�d�v>*\0\0�j�C�Q�\0�0\0��\0\0P���ܧ@��\0\0v>\0\0\0*Pט�c6Q\n��c\0\0;\0\0\0�w1�)��1\0��\0\0�\n�o�C�\04�z\0`�\0\0�B�37�cJ`=\0��\0\0P���9Y���\0\0v>��\r-LY���N=yĈ!�������r۟�q�\\����=����;~��6�����~�q��\Zkd�g�8��\0��5��|�^{���O�^aԨc)W�ŋ!̔vH���ic�E��v��z�����g�ɖ��~z�W������g��a�؟�q�a��X�\0�|P�F�>�M6�.�䤰t�#�: �^������-Zw�._�E�~�_��Ǖ&�������l���\0�����>��e�M��c��7��6Z����2Ė	��~z�����a�?��ن=Nc=\0��A�J�pH��s�V^n��e^Ʃ@�ү@��v���3B��=���~�g�8��\0��U�Q�)k�sy�S88X*�\\t��ou���>[)�W��q��9餃Ùg~G��\r{��z\0`��s�e���.��\\��\r7��J�3����~�_�\'�G�.l���z�m��4�c\0\0;T�#�<>j�1��4�w���g�Ȗ\n����z�!���Oǅ��7��q23l��a֬�8۰�i��\0\0v>�.����xʔ+�>��{�I�W�z�_^?�t`�pξ��?^v��v�\\�j�3Ͱ�Bs��#G�cǎ;��n�����?�H�ٳg��;�#��d��L��q�R~����i���ن=Nc=\0��AuY{�.a�⇋:����S�o��a���\r�1��A�`P�p�绅�/;�AV;$�m�i�-��W!,X�����)S�L��;u����K/�Ν;�	&do�<yr�߷�z�\"�~z�����o[l�K��\r{��z\0`��ۮ�=��|p�0��\r�OuN� �7d�p�^��|���)��PL�\na�����˽�N\ns��;}��9��%K��#�����_	�������=��E&�\n�8Y)�m�\'L�~�g�8��\0���rжr.��z����\'�4L}b�{nN���_�2�����K�;�r��Ȕ�}�١k׮��7W<����ޮ{�o�������˾���;��~z�����:*�x�p=�6�q\Z�1\0��j�m�\\w̎a����y����I?O�|Vxa�O��/<ƞ�SX��YڠC�U:���ѣ�N9�p�0f̵ٷ�\Z�\r�}���}�����Ǐ�_~y����\Z�\n�8��3ό\r�n�#,_>C��\r{��z\0`��?hk�[��jx��Ü���E��!���o?��𧳇:�r�֯2��A0nܸ0w�ٷ�E���=���m�>�lx���W�\Z�F�=N���g�0u�Uz�m��4�c\0\0;T�A[�,Y:#�2�g���ޝ9:,~j\\6�f��t�n��3~����,m�a�L�,�[PJ�\Z4(,[��������s�J+�I���W��I���wñ���ن=Nc=\0��A����ޒᚻ&��\'��^^n��W�M?\Z�Mz�/�z���[\'�EBڠ�����?���;x��U�>������{���O?�t�F�=NV����z��0����z�m��4�c\0\0;T�A[���3����͇<�>{Q�~v\'=�1��p��!|��F>���,\n���P�궉a��3l9h����>����3�;��e�;�B�y�w^���{.<����E��^{ͅ��+�����aҤ��q�a��X�\0�|P�m3��G��|�����>�qӖ���,GN\\Fܾ<t����k���\'�0���a�a�q��t��\rJگ^z�0<�l�7���.]u�5���E���o����zɂ����޽{x���\Z�\n�8Y%?�ى��#��q�a��X�\0�|P�m)���!̹���k��NሻC8���[c���nX��fY�����~�9�Lo�sf�~�N��AI�������}�������i3���0��iu��y������!<�������篸�-�ܢ��W���*y啻C���%��8۰�i�\0��A���<q���{�	5&�;ia8xl�oZ�y�����a�߇0lL����0h��a��Q���vt��\rJܯB�������g�ҢӼ�K�;g?g�/������b���q�N?�_�\'Mf���a��_�q�a�Ө\0`�j>h��<9&��G�����N	�M\\���r��������0�]�볮r��\rJ�ȴ`���)h�L�z���y��0o�{uO�,x7<���CϞ=��?~��0w�\\�L��q�d.���[�\Z��ن=N�v\0�����mE�-�.�N��ϡ��ׇ���tk�f�ذ����.C�}��0��e?ׁ��6�E��e˖e�q|�\'�{~1����^\Z>�Y��̳χ���6���aez�~z����_�7l��G����q�a���c\0\0�|P�&�X��6q��үV^d�뮻�i<mڴ0o޼��\'O��,Yb�I��������3{�{�ӟ.��l����\0\0���l�����ڿum�\r�+ѯ�T�P)�\\y��7��%=�6�q��*\0���v{��	�Ɵy���A�W�_A�B}�<��ݺ���ن=N�_\0��A�n/����9h�J�+(�^���o���3z�m���U\0\0;���R(����:h���k��9l��a��W�N������\rʯ_�<��5a��a�u���]���|����f��}��t���o���;��4�Oڳ�4U�\nʳ��}��ޔ>v�1�X嶶�Y���2���+��f���;r_,�w�;[�[�m8��kG����,������3����g۰�\0�z�DZ��b�|����i�R�Am�{g:~$\"M�����KZX�5���mO?��p��_\n�m�I��^|���i~\Z.��������K{������m]�l�b붦�u�0(eҤߴh�|�\r>�j��=6�~��߾�M�/������<��?w���|t�?����ÿ�.}$rȠ���>I�T�ە�=;���\0\0@y�5�1Cb::h��\0p睗d�NϪ�ߞ�z�����9�����Biz�|z�j��q�u�}h��3�ҳ��,}M�چ_S��_��;�����l�Xw�����~�v�>sqĈ�+}�L�g�ϟ?5w�7�W�3������>����O�z�*K�Ŵ��:�Y�l�K��w�]vj�d����^��g�\'\rV��3{��a�]���>���?�K����|�K��+X�^ؖ��Bkl���^�X�L����Ӡ���yS?;��id��Xl��Vc��M�L�d�ۆSҫ��ǟyfl����O����߹�s���n��R/~8�x��cds�����JޞW\0\0\0��e\n/�<h������k_�+<���u�]q��u�����do�<-$��\'�:����N��N:pK�o���+}M1�u�t �nK����t[�Xz�mz���O<񇢇A�َ���<텃6����*��l٣���&�}O9eD��A\r{Is�<�����O�f�IS�F�}���ۻ��iI]\n������G��}�ѯ`5{a[ޗ}�����zUc}2��.�{�CWZ</�g����-��jj��>Y�l�)i�X��i{�?��o�e�m��}���������?��M����=>���+}{�\r\0\0���2-fXf�EՂmM47v{�g��O��؁P:̟�d�?^w`�)p�즾[���`4@����g&�^2E�\n-�8h���W�>���.��%���?h�N�Ԓ~��P?Y���ifq�P]\n���&�~/=C{ƌ��+X�^ؖ��Bk�4׫\Z��ߴp��-�7֯��ٙ.�7��Ւ�K�d�ۆ���\n\r��/�m��NʑG��^�cds��հ=ۆ\0���zf}Qm�VU�i�����S�4���tڣL�à�߿=�A�҆�^̴p�օ^�t�*\n����m�����j�xd��w\r�/�OZ�_��>-�K���Ҥ��N�_��k��r��5�?Z�c2�Nq�����Ei��δr���Ԓ�K�d�ۆS���i��5 z�=�ܵ�o�^��^9S�>xs��հ=ۆ\0�6=h˴`���\0�ȑG5���鍊�Ͼ���q�٧�O��Z��W���p�g�M�������>xu8�#V��tP��~���u��4x6a�~M���.N8h���*=s6},-��ϧ�N��Qz%P�-����v��[|͠�����/�OZ2�/���Ny����P]\n�����?�������X�����B��b��\r��UM-���AU��/����;{��{[�x���VS��\rfk�����m�)�S��� \rT��t}���{�uO�w��d�N�����N�}|����q\0\0Цm�U�T�y�럞���kI���sw�ŋ��q�.�>�.H\r< {��L�m�����~~��)�����g��}�ӧW����w���O?���i:-�%����`��,�o��9h��YdJ�iP��˩����ًE�At�/�g��g�矑��^�\\�k�������5<��z��p��@��W�4U�B���~��}�~�����~��\nݗ�����\\�*�x������\n���/�a��m��_1��mM�o5���gf�J�\r�\\w�9a�-7��1=~4�FN�>P�o��f���cys�I���հ=;�\0\0\0�堭\rNC�n߿V�\rڿ_UZ�3�S=Ҁ��~��O?<p��+���a�\r\0\0�|Ж��/m`��m��}��jz���{�=]eG�O>ys�x�\r�k�Mԯ@��O�m�6\0\08hm�_USҵ���9\\y��+���a�\r\0\0��A�WՖ_��a�=v	˗�Я@��O�m�6\0\08hm�_USfϾ;{z�g��_�g�۰m\0\0p�&�@����g9�(�\n�8�d؆m�\0\0��6q��U�eܸ��v��	�7M�=�>�a�0\0\0�M��~UM���{�S�^�_�g�۰m\0\0p�&�@���|����|�k��q�ɰ\rۆ\0\0m�\r��j˴icB�^�y��W���\'�6l\0\0���6Я�)K�L;��U����+����q�a�0\0\0�M��~Um9�c����+��D��\rۆ\0�Jֵk��?��L�h�C/�?�R[*�W�γώm�~x饻�+����q�a�0\0\0Pɶ�~�קL��R���.�+��f�RA�*u��{�p�%\'�W�ǉg�\r\0\0�n�=v;j�1��4C��qc�3]dK���3欰�n;���T�=N�8۰m\0\0�t�{w�׻w�%K�>� ���B�N������TЯJ�9s&��=7�fݨ_�\'z�m�6\0\0T�^�6�ْSIi2d�W�?�$[(�W����N9e�~z��q�a�0\0\0Pe����C�M�`�L2eʕ�ſ�k1;�<A�*U&N�u�b�^a���+��D��\rۆ\0�*�O�����<�:u��|���Y�~U��Ж[n\Z��R�\n�8��löa\0\0��ܺtY�?������c��;��k�@�*u~���S��W�ǉg�\r\0\0�o�5�\\c�z�;稣���{���k�?쀪��h�C/�v��w\r�Ǎ����sy;��W%ͬY7��=7s�Lү@��O��نm�\0\0@\r�3*�/1�c��K��̈�(����+�\n���8۰؆\0\0\0hK��ܧP5��ݯ�ֻ3�b��Y)\0\0\0\0�t��̍�Z)�jl��_L)�Uҫjҫl�0h�r\0\0\0\0P��3��Pu~��-7.����f(\0\0\0\0�l��\'c�R\n�:�~�T�~o���\n�g��\0\0\0\0P�֏�w̧��֧s��nJE�_+�~ҫ�\\;\0\0\0��sE�e�\0U�71�U(Jc�\n�g��\0\0\0\0PIļ��������}�R@�&d\Z�LS\0\0\0\0*E����|M)�f|=w��ФB�\nr� \0\0\0\0*�Y1�j�m1g*4��k5v� \0\0\0\0(k��̉�T)�����R@т\0\0\0\0PIֈy �J5�{�>��R@Q�\0\0\0\0�(GfV\\���R@�J���~\04�0\0\0\0���3捘��j�ι~�S)�Y�A\0\0\0\0T��b�W ��\\_\0\n3\0\0\0�\"�y6fm�\0r����AJ\0\0\0P�>�b��J4��\\��R@��\0\0\0\0({?��V�&\\��@��\0\0\0\0(k�Ƽ�])�&t���]�\Ze\0\0\0@�Z3�1�(ЌCr�bM��U\0\0\0P�N��W�\"�~�}e�U\0\0\0P���̍�R)�\"m��}�Vb\0\0\0@Y��e\0Z��\\�\0>`\0\0\0@��xLg�\0Z�s��P�0\0\0\0���a̫1�+�J�r}dC��,� \0\0\0\0��51�P`5�2�je�,� \0\0\0\0�ƞ1/Ŭ��jZ/�O�T\n0\0\0\0�<t��g�P�\0���\\_��8� \0\0\0\0�¨�[�hc����2� \0\0\0\0:܎1sb>�@�h����0� \0\0\0\0:�\Z1���R\0��s}f\r��F\0\0\0С���K�\"-�~Ry0�o�\0\0\0�azeV��i{�\0���~�K)�A�A\0\0\0\0t�[c�V�DR��� � \0\0\0\0:�Wc��U)���;_Q\nj�a\0\0\0\0%�ᘗc>�@�}>�>��� \0\0\0\0J�Ҙ��� �c~��� \0\0\0\0J�S1��l�@� ׇ>��� \0\0\0\0J�s��b��@;0�\\_�jg\0\0\0@��0f�2\0e��Ӕ�\Z`\0\0\0@Il37fs�\0��湾��RP��\0\0\0\0(��1\')PfR_��T9� \0\0\0\0�݈��1k*PfR_z,ק�Z\0\0\0Ю6�y=f�\0��\'s}j#��J\0\0\0Ю���X�2wq�_A52\0\0\0��|1�_1�*P�>�B�>JA2\0\0\0�]��\\�@�\0*ľ����RPe�\0\0\0\0h�ܨ@�I}�e��\0\0\0��>�FL�\0*L�\\���RPE�\0\0\0\0hS�b�9\\)�\nuD��uR\n��a\0\0\0\0m���LQ���s�qJA�0\0\0\0��|,fn�6JT�mr���RP�\0\0\0\0h3wĜ�@��q���@0\0\0\0�M|#橘���]b���O)�p�A\0\0\0\0��n1�c>�@��l��uS\n*�a\0\0\0\0���\\\0�8(?�A\0\0\0\0�Ϛ�]�Տ�U\n*�a\0\0\0\0��z\Z@��/��(�0\0\0\0�V�q���\0Ԉ�s}*�a\0\0\0\0��m�ܘ�JԈ޹���RPa�\0\0\0\0h�5b�9N)�\Zs\\��A%1\0\0\0�Ŏ���I)�\Z���#1�+�0\0\0\0���F�\'��Q����JA�0\0\0\0�E�s�2\05���\na\0\0\0@���y.f�\0j�:�~8P)�\0�A\0\0\0\0�C1/��\0Y����JA�3\0\0\0�(?���2\0�亘���2g\0\0\0@�>�z�FJ���r��JA3\0\0\0��5c���R\04jD��\\��rd\0\0\0@A\'�LR��&��%�#� \0\0\0\0��y�ܘ�����r�rs��\0\0\0Ф�cNS�����Pn�\0\0\0\0hԁ1O�tV\n��t�����2c\0\0\0�*6�y5��R\0�ȧr�s���\0\0\0���1�R�VI��*e��\0\0\0���Ǽ�a�\0h����畂2a\0\0\0@��1����R\0�����iW��\0\0\0P眘��\0�&n�9[(�A\0\0\0\0d�3\'�J�&z���JA3\0\0\0 �F̃1G+@�:&�_�P\n:�a\0\0\0\0�!�C��\0mm�\\�_��\0\0\0ԸtZ8�1h?;f����e\0\0\0P�n�9G\0�ը\\���`\0\0\0PÆ��3��R\0����~;T)�\0�A\0\0\0\05j���b�T\n���3�w�S\nJ�0\0\0\0�F�2�je\0(��s�J�0\0\0\0���y5f�\0(�\rs�ww����\0\0\0\0jL��cR\n�qP�wV\nJ�0\0\0\0�Ɯ\Z3A\0:T��?PJ�0\0\0\0��l37��R\0t�>�~��RP�A\0\0\0\05�ޘ�+@Y89ח��\0\0\0Ԉo�<��R\0��ԏ�\Zs�R���\0\0\0\0j@���bvU\n���k�?wW\nڑa\0\0\0@\r�]�ϔ�,�<�Ze�\0\0\0T�/ļ�!�\0(K�?������\0\0\0\0���1��R\n��6(ׯ�V\nځa\0\0\0@;?�&e\0�7��6�5� \0\0\0�*�s�1=��\"���흕�6f\0\0\0P�:�L�9R)\0*J���r}ڊa\0\0\0@�^�1k(@EY#׿���!� \0\0\0�*�;fn̶JP�����M��6b\0\0\0Pen�9S\0*ڙ�~m�0\0\0\0��|=��1]���u���)m�0\0\0\0�J|$敘JP���G���d\0\0\0P%~�[e\0�*W�\\��&� \0\0\0�*��1ݔ�����O+��0\0\0\0�­�T�7��*���k)�d\0\0\0P�Έ�C\0�ڝ1?RZ�0\0\0\0��m37�cJP�>���[+�`\0\0\0P����?e\0�	��ܧ��a\0\0\0@�:,�јNJP:���aJA\0\0\0����}�e��<bĐ����q��kwI7�Ys�N˷ڪ���~������|/[�qz�~T\0\0\0�v��?>z��v[اO�0j�1aʔ+����f6�����`D���I��6mL���ö�n�^�^?�;��<z�N���q@���\0\0\0\0:���g޶�&��%���.}��E�{�4l��aѢ�,��Q.��a���[��]�j����q�|R�O���@��H�A\0\0\0\0%-�~���>�����[l�+�{�eD�8O>ys�h����,Cl��1=N��ĉ��>��=�\"\0\0\0t�tڤ�l��,��|��!��ڎ�u��2/�tJ�!=NZ��x��8�`\0\0\0Pj�B����I�.����7��=7s�N�ڎ�����ܹ�}�R(m���gΜI�ǅY�n��h�n�\0\0\0\0%v�e���.�^��3��Ѱ�ۇk�i��p�n��?S[*���I�3f�Ya��v�>N�q`\0\0\0Pj#Fy|Ԩc�^����O\n_�B?�%�q�8#��.��B{���z�i�:��;.4M�j����=��UXz\\M2\0\0\0(�~�v\\<e�E-ڽ���#�~�����?�-���&Ꮧg����W�\Z�L3l��=�#G�cǎ;��n�����?�H�ٳg��;�#;Z�d�aP�<�츰�F뇗^�K��)�A\0\0\0\0����]����h��\'�_�a�pՁ�+b.�o�p��n��w�_v���vH���?�R[*�G�a��u�J�2eJ�ߩS���^z9t��9L�0!{��ɓ�����[�Ar�yǆ���q4�0\0\0\0��`w�M�M?�f}��ᧃ���l��q8e�\r�w>�Q��플�3h���ٳ���{��Ν�}��s��K�.\r#F|;{��/�����ٷ�{�9àY�dz�i����7_����}R	\0\0\0\0J�م�y�^�6��]\'�s�a�����O�oύ�I�|4|��[�c>�qx�t��APA=n���>;t��5���W�����u�z�7�����{����~��+\"Ӧ��>n��=���I%\0\0\0\0(�f�󝯅�}�p�1;�ů�3���ÿ\'�<<q�Y��?	o��X{�Na��G,�\ZA���tڷѣGם\Zn��Ea̘k�o:46,����$���ٷǏ.���p��7�O���= ������>�\0\0\0\0���Ž��>�3���S�-g5���a��[¢��sS����C���C-~\ZAE��L�k�7.̝�f��#Gֽ���s�����φ�~��џ\n�?�y ���#L�z��J�I%\0\0\0\0(�&��}���v}¸q�%Kg�SF�,���»3G��O��fѬN�-�x�O��w�\0j�����/XT7�I�\Z4(,[��������sß��ҟ�Oz�H�#�7M���>�\0\0\0\0��.m��ng�uT����\n�-���kr�=prx����}%����٤�����������uBXd de��ye�?��L�����2:�����7�?�����\"�G��G�>�\0\0\0\0����&�����s��-a�7�gO�O�6�`���E���!��T�<������C�@?�(l��C��&���ϰ\0je�㚾�p��G�G�ޙ7/{;���>v�y�eo{���SO=����k���O�d�컳�\'�<3V�#��\r\0\0\0�1��~�!��+���/��.���0����>��i�e���O�pܴ���)�Ñ���/ݴ<|��e��	!��yy�{؅a��;-�\ZA�����[/��R�{����g�;W]}M��aÆ����/�v�nP�`���i�w�{�1��I�\'{�K��E�s�T\0\0\0��[�TI���p��§?���ݬ3ws�<-���;áB8���cy���������Y�r������p�%��[�������O� (���}�o�?h݇\0\0\n�IDAT[���~|��yô�O�KB�x���������^>���0����r�-�S�l٣�Ǖ+�8]���\0\0\0���ҿ����gǅ\'��!�}�9���Ƅs\'-�\ra�M��7�[��<�����	�?���>̛<*<zڎ?\r��lz\\��ĥk�|���gAi84o��΂��������}��-��\"�yGq��ĵ\"O>ys�tq��6Q���\0\0\0@������]�\'|#<x���#.�:%6qy8�^�������ìsv\r�Ϻ�§a�M�+4Z�`A�TqS�L	=z��<�r��轺>�΂w��{:���3���Ǐs��5jeN?��p�_���w\0\0\0\0Pb�Pz���|YX�pZ8��?���_vrK�fЭa��c��o\n��}��Y���>�-�f��0ʪ�\Z�-[�,�ʠ#�=!��ٷ/�������g�}>l�Ͷ��7�+ӟZ�ŋ[o�Y��_�q���\0\0\0@�5:��y�p�CüyS,`\ZA9�3��W�0讻��ӦM��o^���\'OK�,1j��w�o��4̟?U����\0\0\0\0Jl�AА!{�^�â�aTB�JC���>O)��\'�p�p=�v�\0\0\0\0�X��{�na̘��ҥ�X�4���]�L�4�J!}���[��M6�f̸N����.\0\0\0\0%:uZ#���3���]M������2��׏\n}�n����}\0\0\0�{�K�!D6k��9l��a��W�B��_�Y�k�ڵK��g>n����A;��f�^)Կ��`S}b���g�N�m��������o�9�3��\\���JB7��������\\��j��/}��W��]\0\0\0\0Jhݘ����b��Oޜ};-�V�B���_�]��z��¬Y7fo{��?��R��D��iM��l�M�_��wf�;��ﳗ\\rR��k���_~��rԣǆُ���}��s��m�&\0\0\0@��x睗d�Nϐ�ߞ�)߻w���ӂ�!��{v����	�ϟ\Z�;[��V��s�L\n_�򧳟�^u����n��s��\r�q����s��d�s�N�j���]�_ze@�����q�a��_g��������M�ׇ>�N�Y���^��]MdF̐�����B}\"���k� ��Cޝw�x�4�7���e�mW���zF1+�禟�>??�J�\'�{4�w_}��Яߎ�W4�1���3S�M�\0\0\0��Q�+R҂�׾�Wx����n����>7-D����(?8J���~�W��_s͙+}����\'{{�{���t{Z��4��no,��]���U>V�w���~�˓��ߛ};-_v٩u�o���+}n\Z����v:��a��we\n���q��ĵ׎��ާ�2������׆=��5���{��P�P�(�c���s�=6�~\Zd��O?��𳟝X��WC��o����h���V�M�\0\0\0��h�b_c��_TM��;�\\S�����~~5=�=\r`����%=�=��\"o�ߵ������},�\\� (M��eZ̰̪C�f{\\�>��^ݓ?]���T����#����}�����~�w�{@�=�%k��=������w���o�g6�w���~-2�A\0\0\0\0PU�m���������r\Z����b�������~[�0(��fZ9��tz\'� (��^	Tt�+�\'�I�U�y�����d\Z��2��B=�؏��Rv�u��m�G�8;Ho���c����a\0\0\0\0T�6�1�NW4a¯�o����=th��t{��I׻H��y�*_O��4�4u{S�O���՟�&���wgOYW�wʹb��3c�^�N����(S��EJ�t=�P�H�J�&ݿ��l�����g�i�\Z��(��-�;�[��Q�Ǌ��)]t|��t�����l���7{��^�=�\\��� =\0\0\0\0�[��ҫv�Bdz�yJz;m����N�T����I׸�ڢ����mi3-��?M\\c�7�����>��_�����)�.�^�wʹb�N	��O��FZH6���q��D�1}��ʾb&��(\ri��Ϧ��7�t[��m���\\�g�b{nʋ/�Yw����j��Μ����w�����]l\0\0\0\0U�MN�S�ɴ�i�,�B���B=�T���{)\0\0\0\0ЎvZ��3�-����=�T��0\0\0\0\0j����R��D�\0\0\0�*f1�B)�q��\0\0\0@�i��8��\0\0\0\0��Y��P\nz��q\0\0\0\0P�,FZ(=N�8\0\0\0\0�b#-��\'z\0\0\0\0T1��JA�=\0\0\0\0���H��ǉ\0\0\0\0U�b��R��D�\0\0\0�*f1�B)�q��\0\0\0@��ڵ��ŋ� Y�Y���i�-�8=\0\0\0\0h������)S��(Y��������T���8\0\0\0\0�U��c���FcQ�L3t�7�?�E�T���8\0\0\0\0�Uz��ޯw�K�.}��d��N�:=�L�m����q��o�U\Z��8\0�.�` ��\'X0W���d���ӊ�|��İ ����m \Z�\r=�=���~�������\0\0\0�_k6����ɒ%M;�x�\n:N�\0\0\0\0�j�뻟�٥eI2���x�y��x���q\0\0\0\0�&t��w��r,I�$y�7I�%�8\0\0\0\0lR�Z�����������&-���$��\0\0\0\0�B�*���Zm��?z�L.�y~o�YP���,;�z���!����M��\0\0\0\0��FN#��<�-��+�9����8\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0���\0LBƟ,���\0\0\0\0IEND�B`�'),('3',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Submit for QC - V2.bpmn20.xml','1','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns:activiti=\"http://activiti.org/bpmn\"  xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:signavio=\"http://www.signavio.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-b2bce920-71f1-4aae-8167-84d32c7fd8c4\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\">\n   <process name=\"Submit for MLR\" id=\"submitForMlr\" isExecutable=\"false\">\n   \n     <extensionElements>\n        <activiti:executionListener delegateExpression=\"${workflowListener}\" event=\"end\" />\n     </extensionElements>\n   \n      <startEvent id=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04\" name=\"Submit piece for QC\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <outgoing>sid-D8055B00-59CB-4604-A217-C0C44E4F218D</outgoing>\n      </startEvent>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'pendingQc\')}\" completionQuantity=\"1\" id=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: DRAFT to PENDING QC\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-D8055B00-59CB-4604-A217-C0C44E4F218D</incoming>\n         <outgoing>sid-B60C9535-C150-4B52-B391-2A45A1DF2A06</outgoing>\n      </serviceTask>\n      <userTask activiti:assignee=\"${groupService.getTaskAssignee(execution,\'coordinator\')}\" activiti:dueDate=\"${documentService.getDate(execution,\'requestedDate\')}\" completionQuantity=\"1\" id=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\" implementation=\"webService\" isForCompensation=\"false\" name=\"Verify piece meets QC criteria\" startQuantity=\"1\">\n         <documentation>PM-SubmitMLR-QualityControl</documentation>	\n         <extensionElements>\n           	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-B60C9535-C150-4B52-B391-2A45A1DF2A06</incoming>\n         <outgoing>sid-F226E847-2506-4D6D-A759-D787243EAD7C</outgoing>\n      </userTask>\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" name=\"Does it meet criteria?\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <incoming>sid-F226E847-2506-4D6D-A759-D787243EAD7C</incoming>\n         <outgoing>sid-FC7B38D5-7383-417D-8157-DAFADD605E4B</outgoing>\n         <outgoing>sid-85967F7A-4BB4-4860-9C86-671BC8892D6D</outgoing>\n      </exclusiveGateway>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'readyForMlr\')}\" completionQuantity=\"1\" id=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: PENDING QC to READY FOR MLR \" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-FC7B38D5-7383-417D-8157-DAFADD605E4B</incoming>\n         <outgoing>sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'readyForMLR\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Piece is ready for MLR.\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'draft\')}\" completionQuantity=\"1\" id=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: PENDING QC to DRAFT\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-85967F7A-4BB4-4860-9C86-671BC8892D6D</incoming>\n         <outgoing>sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'notReadyForMLR\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Piece is not ready for MLR.\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415</incoming>\n      </serviceTask>\n      \n      \n    <serviceTask id=\"servicetask6\" name=\"Send Notification: Coordinator reminder\" activiti:expression=\"#{documentService.sendNotification(execution,\'readyForMLRReminder\',\'coordinator\')}\">\n    </serviceTask>\n    <sequenceFlow id=\"flow8\" name=\"\" sourceRef=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\" targetRef=\"servicetask6\"></sequenceFlow>      \n      \n      \n      <sequenceFlow id=\"sid-F226E847-2506-4D6D-A759-D787243EAD7C\" name=\"\" sourceRef=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\" targetRef=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\"/>\n      <sequenceFlow id=\"sid-D8055B00-59CB-4604-A217-C0C44E4F218D\" name=\"\" sourceRef=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04\" targetRef=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\"/>\n      <sequenceFlow id=\"sid-B60C9535-C150-4B52-B391-2A45A1DF2A06\" name=\"\" sourceRef=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\" targetRef=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\"/>\n      <sequenceFlow id=\"sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415\" name=\"\" sourceRef=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\" targetRef=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45\"/>\n      <sequenceFlow id=\"sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF\" name=\"\" sourceRef=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\" targetRef=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\"/>\n      <sequenceFlow id=\"sid-FC7B38D5-7383-417D-8157-DAFADD605E4B\" name=\"Yes\" sourceRef=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" targetRef=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\">\n         <conditionExpression id=\"sid-87f8fded-4359-4ffb-8cd7-c8dd1fc4d4eb\" xsi:type=\"tFormalExpression\">${documentService.all(execution, \'outcome\', \'Ready for MLR\')}</conditionExpression>\n      </sequenceFlow>\n      <sequenceFlow id=\"sid-85967F7A-4BB4-4860-9C86-671BC8892D6D\" name=\"No\" sourceRef=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" targetRef=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\">\n         <conditionExpression id=\"sid-925e18ad-e84a-44de-951b-b9e34abd3c48\" xsi:type=\"tFormalExpression\">${documentService.any(execution, \'outcome\', \'Not ready for MLR\')}</conditionExpression>\n      </sequenceFlow>\n   </process>\n   <bpmndi:BPMNDiagram id=\"sid-6543adbe-9e4b-4ca5-8393-86755e55b4d2\">\n      <bpmndi:BPMNPlane bpmnElement=\"submitForMlr\" id=\"sid-5c9125a0-8c36-41bc-9956-eb6dc41661ec\">\n         <bpmndi:BPMNShape bpmnElement=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04\" id=\"sid-47D2C0A2-7AAC-4996-BC8D-D82BB6E94F04_gui\">\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"103.0\" y=\"129.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523\" id=\"sid-799C7B98-2F3F-49B9-8068-E1100F4E6523_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"180.0\" y=\"225.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5\" id=\"sid-30C80E2F-EBA5-43E5-86ED-164CF88DC1E5_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"300.0\" y=\"414.5\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7\" id=\"sid-80F9F2DD-7FE9-4BA4-9861-7F39EDA4CBD7_gui\" isMarkerVisible=\"true\">\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"500.0\" y=\"434.5\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED\" id=\"sid-3C87AE11-1293-4379-8E0A-AB4D4C5257ED_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"686.0\" y=\"414.5\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A\" id=\"sid-F1F90E0E-9461-4A79-8220-31C5D0188E9A_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"686.0\" y=\"104.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0\" id=\"sid-E6256413-0311-4EEE-9AD8-75FF558913E0_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"470.0\" y=\"255.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45\" id=\"sid-8AA73E87-0350-48B0-8293-A77D67544E45_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"470.0\" y=\"104.0\"/>\n         </bpmndi:BPMNShape>\n         \n      <bpmndi:BPMNShape bpmnElement=\"servicetask6\" id=\"BPMNShape_servicetask6\">\n        <omgdc:Bounds height=\"55\" width=\"105\" x=\"900\" y=\"97\"></omgdc:Bounds>\n      </bpmndi:BPMNShape>\n      <bpmndi:BPMNEdge bpmnElement=\"flow8\" id=\"BPMNEdge_flow8\">\n        <omgdi:waypoint x=\"825\" y=\"124\"></omgdi:waypoint>\n        <omgdi:waypoint x=\"900\" y=\"124\"></omgdi:waypoint>\n      </bpmndi:BPMNEdge>      \n      \n         <bpmndi:BPMNEdge bpmnElement=\"sid-FC7B38D5-7383-417D-8157-DAFADD605E4B\" id=\"sid-FC7B38D5-7383-417D-8157-DAFADD605E4B_gui\">\n            <omgdi:waypoint x=\"540.0\" y=\"455.0\"/>\n            <omgdi:waypoint x=\"686.0\" y=\"454.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-F226E847-2506-4D6D-A759-D787243EAD7C\" id=\"sid-F226E847-2506-4D6D-A759-D787243EAD7C_gui\">\n            <omgdi:waypoint x=\"400.0\" y=\"454.0\"/>\n            <omgdi:waypoint x=\"500.0\" y=\"454.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-D8055B00-59CB-4604-A217-C0C44E4F218D\" id=\"sid-D8055B00-59CB-4604-A217-C0C44E4F218D_gui\">\n            <omgdi:waypoint x=\"133.0\" y=\"144.0\"/>\n            <omgdi:waypoint x=\"156.5\" y=\"144.0\"/>\n            <omgdi:waypoint x=\"156.5\" y=\"265.0\"/>\n            <omgdi:waypoint x=\"180.0\" y=\"265.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-B60C9535-C150-4B52-B391-2A45A1DF2A06\" id=\"sid-B60C9535-C150-4B52-B391-2A45A1DF2A06_gui\">\n            <omgdi:waypoint x=\"280.0\" y=\"265.0\"/>\n            <omgdi:waypoint x=\"350.0\" y=\"265.0\"/>\n            <omgdi:waypoint x=\"350.0\" y=\"414.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-85967F7A-4BB4-4860-9C86-671BC8892D6D\" id=\"sid-85967F7A-4BB4-4860-9C86-671BC8892D6D_gui\">\n            <omgdi:waypoint x=\"520.0\" y=\"434.0\"/>\n            <omgdi:waypoint x=\"520.0\" y=\"335.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF\" id=\"sid-B84CC795-0DC8-4724-BFCF-FFEAFB217CEF_gui\">\n            <omgdi:waypoint x=\"736.0\" y=\"414.0\"/>\n            <omgdi:waypoint x=\"736.0\" y=\"184.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415\" id=\"sid-2E7A2C54-2255-47F9-AF96-D4134BCC4415_gui\">\n            <omgdi:waypoint x=\"520.0\" y=\"255.0\"/>\n            <omgdi:waypoint x=\"520.0\" y=\"184.0\"/>\n         </bpmndi:BPMNEdge>\n      </bpmndi:BPMNPlane>\n   </bpmndi:BPMNDiagram>\n</definitions>\n'),('4',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/Vault-Test-Workflow.bpmn20.xml','1','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" \r\n             xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" \r\n             xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" \r\n             xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" \r\n             xmlns:signavio=\"http://www.signavio.com\" \r\n             xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-7bef7376-9427-41db-8394-3de98e53008e\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\" \r\n             xmlns:activiti=\"http://activiti.org/bpmn\">\r\n   <process name=\"TestWorkflow\" id=\"sid-3806d577-ddd6-479c-85f6-11805b79addd\" isExecutable=\"false\">\r\n      <startEvent id=\"sid-64325C52-DE2E-4295-9079-C8C25306742D\" name=\"Start\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <outgoing>sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A</outgoing>\r\n      </startEvent>\r\n      <userTask activiti:candidateUsers=\"${groupService.getCandidateTaskUsers(execution,\'reviewer\')}\" completionQuantity=\"1\" id=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\" implementation=\"webService\" isForCompensation=\"false\" name=\"Verify Piece\" startQuantity=\"1\">\r\n         <documentation>PM-SubmitMLR-QualityControl</documentation>	\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\r\n         </extensionElements>\r\n         <incoming>sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A</incoming>\r\n         <outgoing>sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56</outgoing>\r\n      </userTask>\r\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" name=\"\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56</incoming>\r\n         <outgoing>sid-60F7520F-7792-475F-9E43-B59408B7D40E</outgoing>\r\n         <outgoing>sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F</outgoing>\r\n      </exclusiveGateway>\r\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'inMlrReview\')}\" completionQuantity=\"1\" id=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\" implementation=\"webService\" isForCompensation=\"false\" name=\"Update Doc Status (ReadyForReview)\" startQuantity=\"1\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\r\n         </extensionElements>\r\n         <incoming>sid-60F7520F-7792-475F-9E43-B59408B7D40E</incoming>\r\n         <outgoing>sid-E5617A55-5B8C-4599-AA65-BB847858AED7</outgoing>\r\n      </serviceTask>\r\n      <endEvent id=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB\" name=\"End (good)&#10;\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-E5617A55-5B8C-4599-AA65-BB847858AED7</incoming>\r\n      </endEvent>\r\n      <endEvent id=\"sid-5E2E046B-9380-4584-A037-D196E92AB302\" name=\"End (bad)\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F</incoming>\r\n      </endEvent>\r\n      <sequenceFlow id=\"sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A\" name=\"\" sourceRef=\"sid-64325C52-DE2E-4295-9079-C8C25306742D\" targetRef=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\"/>\r\n      <sequenceFlow id=\"sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56\" name=\"\" sourceRef=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\" targetRef=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\"/>\r\n      <sequenceFlow id=\"sid-E5617A55-5B8C-4599-AA65-BB847858AED7\" name=\"\" sourceRef=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\" targetRef=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB\"/>\r\n      <sequenceFlow id=\"sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F\" name=\"Failed\" sourceRef=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" targetRef=\"sid-5E2E046B-9380-4584-A037-D196E92AB302\">\r\n         <conditionExpression id=\"sid-a1188d86-b470-4645-b51a-33c5d64d5727\" xsi:type=\"tFormalExpression\">${pieceVerified == false}</conditionExpression>\r\n      </sequenceFlow>\r\n      <sequenceFlow id=\"sid-60F7520F-7792-475F-9E43-B59408B7D40E\" isImmediate=\"false\" name=\"Passed\" sourceRef=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" targetRef=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\">\r\n         <conditionExpression id=\"sid-61fb3b25-e618-4f43-b0b8-dec5d94df430\" xsi:type=\"tFormalExpression\">${pieceVerified == true}</conditionExpression>\r\n      </sequenceFlow>\r\n   </process>\r\n   <bpmndi:BPMNDiagram id=\"sid-c14dc061-b8a6-41f5-841c-45a0544d01eb\">\r\n      <bpmndi:BPMNPlane bpmnElement=\"sid-3806d577-ddd6-479c-85f6-11805b79addd\" id=\"sid-5d36db64-e793-4274-a948-b95a943640c3\">\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-64325C52-DE2E-4295-9079-C8C25306742D\" id=\"sid-64325C52-DE2E-4295-9079-C8C25306742D_gui\">\r\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"30.0\" y=\"103.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191\" id=\"sid-DFAA3C65-6C3F-4F1D-BAEA-4EFF0F413191_gui\">\r\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"120.0\" y=\"78.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D\" id=\"sid-95CCC0A4-F424-4D79-ABB8-EDF04D317F9D_gui\" isMarkerVisible=\"true\">\r\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"300.0\" y=\"98.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E\" id=\"sid-942BC060-8CC9-4C39-8146-6F85F5D56C6E_gui\">\r\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"270.0\" y=\"222.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB\" id=\"sid-DE5FECED-FE03-4A47-B0B0-3DC0DABC17DB_gui\">\r\n            <omgdc:Bounds height=\"28.0\" width=\"28.0\" x=\"511.0\" y=\"248.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-5E2E046B-9380-4584-A037-D196E92AB302\" id=\"sid-5E2E046B-9380-4584-A037-D196E92AB302_gui\">\r\n            <omgdc:Bounds height=\"28.0\" width=\"28.0\" x=\"510.0\" y=\"104.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-60F7520F-7792-475F-9E43-B59408B7D40E\" id=\"sid-60F7520F-7792-475F-9E43-B59408B7D40E_gui\">\r\n            <omgdi:waypoint x=\"320.0\" y=\"138.0\"/>\r\n            <omgdi:waypoint x=\"320.0\" y=\"222.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F\" id=\"sid-466CBA39-AE1E-4ADC-9D20-D4758F7D641F_gui\">\r\n            <omgdi:waypoint x=\"340.0\" y=\"118.0\"/>\r\n            <omgdi:waypoint x=\"510.0\" y=\"118.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56\" id=\"sid-8CE7C4DD-9076-4BE5-ABE2-74B26ACCAA56_gui\">\r\n            <omgdi:waypoint x=\"220.0\" y=\"118.0\"/>\r\n            <omgdi:waypoint x=\"300.0\" y=\"118.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-E5617A55-5B8C-4599-AA65-BB847858AED7\" id=\"sid-E5617A55-5B8C-4599-AA65-BB847858AED7_gui\">\r\n            <omgdi:waypoint x=\"370.0\" y=\"262.0\"/>\r\n            <omgdi:waypoint x=\"511.0\" y=\"262.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A\" id=\"sid-D3B7C27E-D587-483A-BD2E-B9858B631C2A_gui\">\r\n            <omgdi:waypoint x=\"60.0\" y=\"118.0\"/>\r\n            <omgdi:waypoint x=\"120.0\" y=\"118.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n      </bpmndi:BPMNPlane>\r\n   </bpmndi:BPMNDiagram>\r\n</definitions>\r\n'),('5',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/PrepareForReview.bpmn20.xml','1','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" \r\n			xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" \r\n			xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" \r\n			xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" \r\n			xmlns:signavio=\"http://www.signavio.com\" \r\n			xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \r\n			exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" \r\n			expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-8f89b30b-9210-479a-946b-4c3f75405b76\" \r\n			targetNamespace=\"http://www.signavio.com/bpmn20\" \r\n			typeLanguage=\"http://www.w3.org/2001/XMLSchema\" \r\n			xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\"\r\n			xmlns:activiti=\"http://activiti.org/bpmn\" >\r\n   <process name=\"PrepareForReview\" id=\"sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa\" isExecutable=\"false\">\r\n      <startEvent id=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240\" name=\"Start\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <outgoing>sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F</outgoing>\r\n      </startEvent>\r\n      <userTask activiti:candidateUsers=\"${groupService.getCandidateTaskUsers(execution,\'reviewer\')}\" completionQuantity=\"1\" id=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\" implementation=\"webService\" isForCompensation=\"false\" name=\"Verify Piece&#10;\" startQuantity=\"1\" >\r\n         <documentation>PM-SubmitMLR-QualityControl</documentation>	\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\r\n         </extensionElements>\r\n         <incoming>sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F</incoming>\r\n         <outgoing>sid-12195530-42AD-4E34-BDC9-740FBF187574</outgoing>\r\n      </userTask>\r\n      <endEvent id=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B\" name=\"End&#10;\">\r\n         <extensionElements>\r\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\r\n         </extensionElements>\r\n         <incoming>sid-12195530-42AD-4E34-BDC9-740FBF187574</incoming>\r\n      </endEvent>\r\n      <sequenceFlow id=\"sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F\" name=\"\" sourceRef=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240\" targetRef=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\"/>\r\n      <sequenceFlow id=\"sid-12195530-42AD-4E34-BDC9-740FBF187574\" name=\"\" sourceRef=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\" targetRef=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B\"/>\r\n   </process>\r\n   <bpmndi:BPMNDiagram id=\"sid-af2d216f-2a59-433a-bb05-69b2533a21cf\">\r\n      <bpmndi:BPMNPlane bpmnElement=\"sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa\" id=\"sid-d4f7ba96-f1cc-45d4-a894-c7ba60deb401\">\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240\" id=\"sid-50B806FF-17F7-4767-8D80-7B173C2B9240_gui\">\r\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"120.0\" y=\"85.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464\" id=\"sid-BD6104DA-2302-4F5E-AE91-2A036310E464_gui\">\r\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"357.0\" y=\"60.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNShape bpmnElement=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B\" id=\"sid-C3F30F49-EF07-4908-A6D7-8CA58EC1385B_gui\">\r\n            <omgdc:Bounds height=\"28.0\" width=\"28.0\" x=\"720.0\" y=\"86.0\"/>\r\n         </bpmndi:BPMNShape>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F\" id=\"sid-1ECCDDC1-C090-4196-900B-43F9E0EEE06F_gui\">\r\n            <omgdi:waypoint x=\"150.0\" y=\"100.0\"/>\r\n            <omgdi:waypoint x=\"357.0\" y=\"100.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n         <bpmndi:BPMNEdge bpmnElement=\"sid-12195530-42AD-4E34-BDC9-740FBF187574\" id=\"sid-12195530-42AD-4E34-BDC9-740FBF187574_gui\">\r\n            <omgdi:waypoint x=\"457.0\" y=\"100.0\"/>\r\n            <omgdi:waypoint x=\"720.0\" y=\"100.0\"/>\r\n         </bpmndi:BPMNEdge>\r\n      </bpmndi:BPMNPlane>\r\n   </bpmndi:BPMNDiagram>\r\n</definitions>\r\n'),('6',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/MLR Review - V2.bpmn20.xml','1','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<definitions xmlns:activiti=\"http://activiti.org/bpmn\" xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:signavio=\"http://www.signavio.com\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" exporter=\"Signavio Process Editor, http://www.signavio.com\" exporterVersion=\"\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"sid-6c9c2dee-be44-4ebb-8302-e85cf48d4d44\" targetNamespace=\"http://www.signavio.com/bpmn20\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\" xsi:schemaLocation=\"http://www.omg.org/spec/BPMN/20100524/MODEL http://www.omg.org/spec/BPMN/2.0/20100501/BPMN20.xsd\">\n   <process name=\"MLR Review\" id=\"startMlrReview\" isExecutable=\"false\">\n   \n     <extensionElements>\n        <activiti:executionListener delegateExpression=\"${workflowListener}\" event=\"end\" />\n     </extensionElements>\n     \n      <startEvent id=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C\" name=\"Start review workflow \">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <outgoing>sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4</outgoing>\n      </startEvent>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'inMlrReview\')}\" completionQuantity=\"1\" id=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change (READY FOR MLR to IN REVIEW)\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4</incoming>\n         <outgoing>sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E</outgoing>\n         <outgoing>sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39</outgoing>\n      </serviceTask>\n      <userTask activiti:assignee=\"${assignee}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\" implementation=\"webService\" isForCompensation=\"false\" name=\"Review and provide annotations\" startQuantity=\"1\">\n         <documentation>PM-MLRReview-ReviewAnnotate</documentation>	\n         <extensionElements>\n			<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E</incoming>\n         <outgoing>sid-FFDFD243-271D-458F-8DF4-854C3668FED8</outgoing>\n         <multiInstanceLoopCharacteristics activiti:collection=\"${groupService.getCandidateTaskUsers(execution,\'reviewer\')}\"  activiti:elementVariable=\"assignee\" behavior=\"All\" id=\"sid-83c385a5-2e28-45aa-a49d-8105f3aedc2d\" isSequential=\"false\"/>\n      </userTask>\n      <userTask activiti:assignee=\"${groupService.getTaskAssignee(execution,\'coordinator\')}\" activiti:dueDate=\"${documentService.getDate(execution,\'dueDate\')}\" completionQuantity=\"1\" id=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\" implementation=\"webService\" isForCompensation=\"false\" name=\"Assess outcome\" startQuantity=\"1\">\n      	 <documentation>PM-MLRReview-CompleteReview</documentation>\n         <extensionElements>\n           	<activiti:taskListener delegateExpression=\"${workflowListener}\" event=\"assignment\" />\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-FFDFD243-271D-458F-8DF4-854C3668FED8</incoming>\n         <outgoing>sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174</outgoing>\n      </userTask>\n      <exclusiveGateway gatewayDirection=\"Diverging\" id=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" name=\"Are changes required?\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffff\"/>\n         </extensionElements>\n         <incoming>sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174</incoming>\n         <outgoing>sid-9218A35A-08ED-4C84-93AA-C568CADBD664</outgoing>\n         <outgoing>sid-1498D019-EFAC-4F53-9400-715E685CA2F4</outgoing>\n      </exclusiveGateway>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'readyForMlr\')}\" completionQuantity=\"1\" id=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN REVIEW to READY FOR MLR\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-9218A35A-08ED-4C84-93AA-C568CADBD664</incoming>\n         <outgoing>sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'mlrReviewedApproved\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Review completed, changes not required\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.setStatus(execution,\'draft\')}\" completionQuantity=\"1\" id=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\" implementation=\"webService\" isForCompensation=\"false\" name=\"Make status change: IN REVIEW to DRAFT\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-1498D019-EFAC-4F53-9400-715E685CA2F4</incoming>\n         <outgoing>sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E</outgoing>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'mlrReviewedWithChanges\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Review completed, changes required\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E</incoming>\n      </serviceTask>\n      <serviceTask activiti:expression=\"#{documentService.sendNotification(execution,\'inMlrReview\',\'originator\')}\" completionQuantity=\"1\" id=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235\" implementation=\"webService\" isForCompensation=\"false\" name=\"Send notification: Piece is in review\" startQuantity=\"1\">\n         <extensionElements>\n            <signavio:signavioMetaData metaKey=\"bgcolor\" metaValue=\"#ffffcc\"/>\n         </extensionElements>\n         <incoming>sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39</incoming>\n      </serviceTask>\n      <sequenceFlow id=\"sid-FFDFD243-271D-458F-8DF4-854C3668FED8\" name=\"\" sourceRef=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\" targetRef=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\"/>\n      <sequenceFlow id=\"sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174\" name=\"\" sourceRef=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\" targetRef=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\"/>\n      <sequenceFlow id=\"sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4\" name=\"\" sourceRef=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C\" targetRef=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\"/>\n      <sequenceFlow id=\"sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E\" name=\"\" sourceRef=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" targetRef=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\"/>\n      <sequenceFlow id=\"sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39\" name=\"\" sourceRef=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" targetRef=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235\"/>\n      <sequenceFlow id=\"sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E\" name=\"\" sourceRef=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\" targetRef=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF\"/>\n      <sequenceFlow id=\"sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6\" name=\"\" sourceRef=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\" targetRef=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714\"/>\n      <sequenceFlow id=\"sid-9218A35A-08ED-4C84-93AA-C568CADBD664\" name=\"no\" sourceRef=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" targetRef=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\">\n         <conditionExpression id=\"sid-ffc0c9dc-bfc1-4cb5-b41f-6cd53e3707cb\" xsi:type=\"tFormalExpression\">${documentService.all(execution, \'finalOutcome\', \'Changes not required\')}</conditionExpression>\n      </sequenceFlow>\n      <sequenceFlow id=\"sid-1498D019-EFAC-4F53-9400-715E685CA2F4\" name=\"yes\" sourceRef=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" targetRef=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\">\n         <conditionExpression id=\"sid-933c53b2-38a4-407f-863e-999540f85fd0\" xsi:type=\"tFormalExpression\">${documentService.any(execution, \'finalOutcome\', \'Changes required\')}</conditionExpression>\n      </sequenceFlow>\n   </process>\n   <bpmndi:BPMNDiagram id=\"sid-8458580e-bbc3-4cbb-b62e-ab1badc59ee5\">\n      <bpmndi:BPMNPlane bpmnElement=\"startMlrReview\" id=\"sid-e350d9f4-f8f1-48a5-b091-f75355ef3655\">\n         <bpmndi:BPMNShape bpmnElement=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C\" id=\"sid-8E930638-BB62-41B1-A198-F3A1E30B3B3C_gui\">\n            <omgdc:Bounds height=\"30.0\" width=\"30.0\" x=\"210.0\" y=\"555.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5\" id=\"sid-BCC3D288-A085-4ACE-92F4-058EF89EFEF5_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"285.0\" y=\"645.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758\" id=\"sid-7E0A62B1-0011-49EA-975C-250C662E7758_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"456.0\" y=\"768.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4\" id=\"sid-F8849063-D0DF-4415-B84A-E8238F311BA4_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"634.0\" y=\"532.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8\" id=\"sid-EC09CF51-B7A7-4B97-BCD8-1455339698B8_gui\" isMarkerVisible=\"true\">\n            <omgdc:Bounds height=\"40.0\" width=\"40.0\" x=\"821.0\" y=\"552.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA\" id=\"sid-8FF1C933-00D1-4D59-9B9A-BF7FDC0221DA_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"1015.0\" y=\"532.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714\" id=\"sid-6A8C116B-E89A-4331-82F7-A309F3A84714_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"1015.0\" y=\"250.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67\" id=\"sid-5BAC39FB-F370-4833-A08C-10D94DBBFE67_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"791.0\" y=\"396.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF\" id=\"sid-21D509D3-1000-4DEE-AD04-F18BF9A5D7CF_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"791.0\" y=\"250.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNShape bpmnElement=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235\" id=\"sid-A7D07054-4883-41DB-9ED0-4FC987268235_gui\">\n            <omgdc:Bounds height=\"80.0\" width=\"100.0\" x=\"285.0\" y=\"250.0\"/>\n         </bpmndi:BPMNShape>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6\" id=\"sid-33C7F41A-F2AA-46AB-9B66-CD10A6F1DDC6_gui\">\n            <omgdi:waypoint x=\"1065.0\" y=\"532.0\"/>\n            <omgdi:waypoint x=\"1065.0\" y=\"330.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4\" id=\"sid-79033AB1-0CEC-4711-9317-BD1CB48A25D4_gui\">\n            <omgdi:waypoint x=\"240.0\" y=\"570.0\"/>\n            <omgdi:waypoint x=\"262.5\" y=\"570.0\"/>\n            <omgdi:waypoint x=\"262.5\" y=\"685.0\"/>\n            <omgdi:waypoint x=\"285.0\" y=\"685.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-FFDFD243-271D-458F-8DF4-854C3668FED8\" id=\"sid-FFDFD243-271D-458F-8DF4-854C3668FED8_gui\">\n            <omgdi:waypoint x=\"556.0\" y=\"808.0\"/>\n            <omgdi:waypoint x=\"605.0\" y=\"808.0\"/>\n            <omgdi:waypoint x=\"605.0\" y=\"572.0\"/>\n            <omgdi:waypoint x=\"634.0\" y=\"572.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-9218A35A-08ED-4C84-93AA-C568CADBD664\" id=\"sid-9218A35A-08ED-4C84-93AA-C568CADBD664_gui\">\n            <omgdi:waypoint x=\"861.0\" y=\"572.0\"/>\n            <omgdi:waypoint x=\"1015.0\" y=\"572.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39\" id=\"sid-25A4D1AF-28C5-4B00-8187-42DDE0FB6D39_gui\">\n            <omgdi:waypoint x=\"335.0\" y=\"645.0\"/>\n            <omgdi:waypoint x=\"335.0\" y=\"330.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-1498D019-EFAC-4F53-9400-715E685CA2F4\" id=\"sid-1498D019-EFAC-4F53-9400-715E685CA2F4_gui\">\n            <omgdi:waypoint x=\"841.0\" y=\"552.0\"/>\n            <omgdi:waypoint x=\"841.0\" y=\"476.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E\" id=\"sid-799B3A1B-CFCD-4BDE-82BA-CAE0A3AB873E_gui\">\n            <omgdi:waypoint x=\"841.0\" y=\"396.0\"/>\n            <omgdi:waypoint x=\"841.0\" y=\"330.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E\" id=\"sid-E0D9EED9-06B1-4519-A20A-5778AF40ED6E_gui\">\n            <omgdi:waypoint x=\"385.0\" y=\"685.0\"/>\n            <omgdi:waypoint x=\"420.5\" y=\"685.0\"/>\n            <omgdi:waypoint x=\"420.5\" y=\"808.0\"/>\n            <omgdi:waypoint x=\"456.0\" y=\"808.0\"/>\n         </bpmndi:BPMNEdge>\n         <bpmndi:BPMNEdge bpmnElement=\"sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174\" id=\"sid-96A8B54A-F6B5-4A22-9D9A-960F578E8174_gui\">\n            <omgdi:waypoint x=\"734.0\" y=\"572.0\"/>\n            <omgdi:waypoint x=\"821.0\" y=\"572.0\"/>\n         </bpmndi:BPMNEdge>\n      </bpmndi:BPMNPlane>\n   </bpmndi:BPMNDiagram>\n</definitions>\n'),('7',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Approval Workflow - V2.startMlrApproval.png','1','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0Y\0\00\0\0\0t��\0\09qIDATx���t��/�D造�0T�ҢG��S�bZ��Җr�L����z\\ږC˱c���Z[��L�eT�u�j�FT��Ǻ�eUNA�V.�D��~d�!f������,vv���}��}�\'���.*\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0h\"��馫�=z�����Rz��#ݭ��ڶmSq�	���s}�p±i\0� sr\"\'n�����o[��=cҤ+����(-]+�^Vڎ%%3c��qѧϱ;z�<���&?�Q`2!\'rB+5}������-�L��O�>��S�9�t����ڟ���A� sr\"\'��Оx�\'��]��_����8��.��v�0G!`2���9��H���_E�����c�������A����	-_z�dz^o:�,D�&O��]�v�8\"s�2!\'rB��L�^8�y���v��Nn�pT� eBN�,]�3]�����vcƌ����رc�0alL�;rEn�NvT� s�99�Z���\n�ҟ�w�ĉ1w���v�~�b��E���O��G����dA.++�=�G��Ϲm��Q	���A� �DNh�қ�~o���[���&m���ٿ�=�X����Ѯ]�X�hQv�ҥK�7n�(�{����W��A� sr\"\'�luL�W������Kb�������??��n������e����;���[��o����aU�\0� s�99�Zkp�k��&:t�6����ƍ���Z���f��ر#�xٲeNA.�9����	�[[�S�ӧO�:��m���9�������cĈ��o�y|����Ĵi�b��ق(�\0� sr\"\'n-QV��͋��7d��+����_=�X�j��D�0���9ApkD[�n�\ni�xȐ!�s�Ϊ���Q�FUu���}Qp�A� �DN���_��:t�G�{�d�ۼ�����0a� \n.�9����	�[ۋ)/���x�����ف�^\\�w���W]���_ɂ�f�\Z/�\\\0s�99��f���[1��/�nޒE���fd�R�{����\\�]�Ӱ-;Eݭ[�x��gQp�A� �DN����/f҇���n�%+_��e���T�j^�jUTTD<���p��زe�s|�̙#��`2!\'r���<������Ц o�V�m-;f�E߾}��˾�K.q\nZp�A� �DN�ڂ�u���4tz���ݻǫ�ގ��wT������/�G}t���һ������Aȉ� �^L��2M��ȥ��~�q����S�N�O�;^}}U|�S}bÆ\r�;A\\\0s�99��`p|�����������+ؤJ�8^VV&��`2!\'r��\n�����9HN�� ��`R� 9Qr��*�0���9Ap�����Aȉ�Wp�A�$\'JN\\�\\���Aȉ� �Jp�A� �DN@p���Ar���\\\0s�2ɉ�W	.�9����	���dBN�� ��`R� 9Qr��*��A����	���dBN�Wp�A�$\'JN\\�0)s��(9��w����g?��ѡC�8����{nl�s����=z��m۶������x<��A� sr\"\'���sL��\0�ӟ�ǆ\r��+��F��G�Mt�ءί�޽k�s7mzd��L�0���9Ap��ܹS�W��^������-��|���������3v�M7]=zt�����������O�Ci�G�3�u=N\n܅�~��Q�������U��W�|��ٲ�;�������M?\'�����!?Sp�A� �DNܪ����U�8������ݷj��ٿ)�����+�p�ۃ��>7r䗳����xᅻ��\'5}<�����t_��4$T����e�g̸���-��yd��L�0���9Ap�jɒ�V��7�[�}5�*�>�/$���Ν��{p��8���\'U]?O�[��Ͳe��i����u����.�9����	4(���\"�H��.-]������܍\rn�y�M�B�y^�z�����Aȉ��	{��א���W_��H}��ݟ�g�>�8���3�f��x���O.����#�N?/Z���v�/}���;f�]��w̘�����տ&�ם�i�Ti���3r[p�A� �DN8H�;k�\r�%4�_6�:��\0�_И�[[�Tt}!I�M��./:o�/�8��g�����*��_}fڴ�z�7���KGT5����n��:�ˊ��3�dBN��U���dBN�� ��`R� 9Qr��*�0���9Ap�����Aȉ���\n.�9H���D�	�+�\0� eBN��U�`2!\'r�+�\0� e�%\'�����9HN�� �Jp�A� �DN\\%�\0� sr���\\\0s�2ɉ�Wp0)sr\"\'�\\\0s�99�\\�0)s��(9Ap���Ar���U�`2!\'r��*�0���%\'�����9HN�� �Jps�2!\'r��*�0���9�\\\0s�2ɉ�Wp�A�$\'JN\\%�\0� sr\"\'t:th_QZ�\\h�Am���r���Q	���99�Z��O>nmq�͂���~�`n��pT� eBN���3O�;i���j��3���ɎJ���Aȉ�Ђ��խ�^���˟�[o�i���.��A����	-\\ϞG��2e���\Z6��[r�b��0)sr\"\'�\'u�|�����Bt\0�����r�`M�Nu(� eBN��cP��]����6mڬ�m�aA�d2!\'rB+o���������~���z+O;�Z\0s�99�Z��ڶ���N�[w�e_��C�f��fh��VH��LW��|�dz^���\0� sr\"\'$��jR��UiѮw�V{W����{+�K�2\r�9����	4[�+��Φ\0�r��W�����9\00��\0{g^�_O�9\00��\0{�_�_�2�/W�r��E}�b����`9\Z�����k���9�h�EE�/�Yb�\0`9\Z����x�/\0� ������fr�^�9��a\0`9\0s�	 �\0`9�\0s�	 �\0��@N\0���	.\0��@N\0��r.\0� �@p�r�\0� �@p0��\0��9�\\\00��\0��9�\\\0�A \'���9�\0�A \'��`9\0s \' �\0`9\0s�	 �\0��@N\0�\0s�	.\0��@N\0��r.\0��@N@p�r.\0� �@p0r�\0� �@p0��\0��9�\\\00�����9�\\��m0��Co��+G��|����zh����^T۶m*N8����}��N8v�#\rk5@p�����췭w�1i�Q\\|s��.���j/+mǒ��1y�����={�|n����Z\r\\�Vj�����ѣ[L�2>�˟���:���K�N�;��9�>��\0�h�\r։\'~\";Ӣ���K���#������X���J���3X\Z��hu��~s��b�.@+�����\Z��A\rρ�ɓ�ml׮�#�H��\0�h��U�E.���W׮�����J��\0�h��e��Uk_��n̘1U�%;vl�qjL�;rEn�NvTb�.@��+]���&k�ĉ1w���v�~�b��E���O��G����dMWYY�&k��Go�sn�pTb�.@��h���`Elݺu�7�-..��}��⭷ގv��ŢE����.]���q�FM���Vn��;*�V���݅��W��\Z��^�ׯ�n����.w���<F��(����߉��ߒ�~�74Y{X�R��\0�h�MVr�5�D�bÆ]g�6n�Tuf��w���۱cG��e�<]P���\Z �\0���*=�o���UOܶm{̜y{v{���1bĈ���7�<>������bڴi1{�lM�&k5@p4Y�,���7o^�_�!��.��w�u�e������ƪU�}��I���\Z �\0��\Z�-[�W5T��!C��Ν;��K�G�\ZU�T�m��I���\Z �\0��g����W����&�.�>�y�{U_?a�M�&k5@p4Y�]����/�g�Y�mޜ5�By�_}�_~���	[�f�_h��V@�U��z�Uy����[�����fd��^�{������]冀-{:a�n���g��4i��V@�U�^|��l��a��E��bkY��e%UO\\�jUTTD<���p��زe���̙�i�da�.�&����k��\rՀ�+5]����{[K����Aѷo�8�㲯��K<]P���\Z �\0��ښ��[�fOL�ս{�xu�۱y����뽭��/�G}t���}�֯_���da�.�&�Ѕ/�WLg�.���㌳��ݞ:uj|�����ⓟ�6l��i�4YX����*�d=���Ug�JJJbs��S-]�4���4Y�,��\0�0vkn4YX���&K��j �\0h��&k5@p4YJ���\Z �\0h�4Y`���&K�e.�Z\r\\\0M��da�.\0�,M�j�����d��\Z.\0�,���Z\r\\\0M��da�.\0�,MX�����R�,��\0��d)M�j�����da�.\0�,MX����\Z��ʱ=��߲�o�骪�\Z�0h�4YX��H�r��\\�����R�Ks�f�����ʯ��QM�!���q㾕}|�C�E�u�;vh���da�Fވ\\-�l�n��7ruR�:U~>��;W�r5���su�M����&k�������d��?\r�H���C����c,_>��&k�����g:^x��X�nI���{�?�諭�f�qǵѳ�QѶm���Jߓ��}��u}]m�`m?��ڲ�;��ѹs�&o$ͥX�8�qZ���*�v�����Z���su�M����[�=4k<^{�޸첯l,R��?��.i`n�yB�8�AI�wޠ�s�Vݟ�ۿ�)���|��i�#\r:KV��P�ǅ~Vͺ��e_7c���da��J��՚�]O�k�����qV��dչ�>��ٿ�F\r�{��#�ʲe��i���=�����F�)��g���橨���~������L�ڵl���\Z�d�Y�\Z��;��da��\n��V�5V����V>ޥ6-���*��_��ٿ�M�lV�F�czd�^���\'��J�^z鈏4*��������Yg�CU�S�Y��whH�U�gj��&k��odeCԻ��w��:���h�j]���QR�d�֨����=�/UzZ`Q����#G~y�hL�xY�?ʔ�U�u����O��}�3N�~��_7hP����^c���w,��jV��-�u��0���i��VkR#���7`=~���?ɦFp4Y5��]5p޼_F��]��\\\rzFՅ#j~]�i����X�ni����O,�t��5��f�Pu��i�Ruቺ~��_��+���?~X���-k��j<]���Uۅ/�Y�|cW_c���Z�yJW�r���\Z��mj@��\\�k��.]�=]E����+�A@��4YX��r�,ְ���9���h��&k�V�WѮ�R�ۏ?3�aqo���d)M�j�Qz�Դ��3\'���!�\0h��&k�bN����fzj�|���d)M�j�Qz�މ��g���;6=���R�,��Z�-�괟f�y�6=���R�,��l����-�\\\04Y�,�V+ș,�}\0�&K��jM�k�4�\0�,MX�5!Wt��\0h�4Y`�ք�O���>\0@���k�&�+Wo��~���)��4�\0�,���Z��ZR��)|�à\\=��>\0�d)M�j�و\\�T��f��Յ\Z�\0M��da������I��=^����\\\04YJ�����;Wkr5`=~���?�A�}\0��R�,��#�v]��>h���^��>\0�d)M�j�1�\rQS���[�x?r�`\0h��&k��U:���h�k����S�5Xk*7\r��&Ki��V;��.�ui�t������t����]�8�A�}\0`���h��V㯾Q�l���M+�u��sթ��*�ԈM.��Fé�\ZSԲ�\"蠱\0�di��Zm�ꕫ��jNe�UZ�ӿ��j~Ѯ��v�`\0���da�\Z�\0�}�C�����58͠�o�O�]R��Z\r\r�@v��ǭ-.�Y����~�`n��pTb����\0�;����N�t�&����g�U��u�`����\0���ի[�^�����?��9��f�6m^)j��Ck54�\0JϞG��2e�F�\0ְag�R���`����\0�8�s�÷������*.���>X��S�X���>\0h=u���=���o�ڴi�*���9�V�A�}\0�\n�����\r7|o��h���`U>Ep�k54�\0��Im�~lq�N���첯�ᡇ~���h5��`�˴��V^�\"��S�V�A�}\0p�蛫I�z<W��c�ڻJo4��+]��U�V�A�}\0@�5�rjgS��\Z\Z�\0\0�N:#T^9\r�9�Z\r�}\0\0{g^�_�����\0k54�\0�~E=��/W�k54�\0��E��D:��Y`����>\0�F��,V�F�<`����>\0��YTT�R�%6X�ᠱ\0���:��Y`����>\0�F�_T���� X�ᠱ\0�r���\0\0s�	\Z�\0�A \'࠱\0�r���>\0\0s�	\Z�\0�A \'࠱\00r���>\0\0s�	\Z�\0�A \'8h�\00�����>\0\0s�	\Z�\0\0�A \'8h�\00�����>\0\0s�	\Z�\0\0�A \'8h�\00����>\0��	\Z�\0\0�A \'8h��\0\0s�	\Z�\0�A \'࠱\0�r���>\0\0s�	\Z�\0�A \'8h�\00r���>\0\0s�	\Z�\0�A \'8h�\00�����>\0\0s�	\Z�\0\0�A \'8h�\00����>\0��	\Z�\0\0�A \'8h�\00����>\0�r\Z�\0\0�A \'8h�\00����>\0�r\Z�\0\0s \'8h�\00����>\0�r�����g��Лn���ѣ�=߿�)���>*��j/�m�6\'��k��>�w�	\';Pl0�\0�q��`p����^>p`�m�{��I����⛣�tyD�T{Yi;��̌ɓ�E�>������s��T��`�:\0���iZ��:}������-�L��O�>��S�9�t����ڟ#B�X�q�8M+\\ShO<�Y�.T��^zivyd��s�`�a0`\0�i�4�dpM���_E�����c�������`�:\0���iZ���^8��כN;с�ɓ�ml׮�#��1��u\0��Ӵ��5]�&�p��z|u�����. N�X�q�8M\\ӥ?ӕij?�j7f̘�K]�;6\n���ر#W��dq�����&k5e�6�6Zzo�t����;q�Ę;wnv�_�~�p�x���裏��{��� ���	�֣�����6\\!N4�1ࣵp��������v�_��~�G�s�X6X���q\Z�k^z����u���ޤ���8�������z;ڵk�-��[�ti��ƍwߛ!���ŉ�3�^�����9]�֑]��\\M=���ِ�1���Λ�ɱ1�:\0�Ռ�\\�{�)b���Y~xI�_�>�}���Ǻ�����=��쾷�~\'�Kv��7��=,5�kؽ�\r�3s��/r��φ�:\"�vT�p`��_g����Z�8���!�M����СClذ�7n��kɻﾛݷcǎ��e˖9-���&�ggu����$V/�E<5���>*�����+>T|X����Z�8�����S�ӧO�:��m���9�������cĈ��o�y|����Ĵi�b��ق(���&�+N�ҵ��_���oɿ���5�\\�����1���F�NW�2X���q\Z�k��-����y�������酕y�]w]v�믿o�Z�����Һ��9ל���+֭�۟�;W��ۛ��;�f���Z�8���!�ݲu{UH��C���;wVݗn�\Z5�2����+��K�i���W�\'�*��/>X9=J_������3_��\'?����cc�u\0X��1�6��#��ٿC��Hp/����s�7�W��&LD���4Y;�VČ�F��K�巷��-f�xxV�����%z}�����Z�5Z�\0�\0�V3Ncp��Ŕ�_~y<�̊xo����J/�̻��뫮R��˯d�^�f�S\n.��ɪ�Xw..�c�=��z{ܹ:b��W<qɲ��FL|4b����د>�޷8�?\\!��\0�\0�V3N�ɪ��z�U���w7oɢx�m3���)�=���vz���iؖ���֭[<�쳂(���&k�ӏĠo�\"�<�a��cK*��⊸tqE���\"�5�\"�q�Θ�(�lͮ�/��1�-�/��\0�\0�V3N�ɪ�^|���@�0����ۢd���,b񲒪SͫV�����\'�x\".\\[��z��9sQpiM�sW��X7�G��cL���daķ���97�`�gE�;cg|mZE|�W�NY\Z�:���Ty6X���q\ZMV�S������0 m\n��me�����k~qP���7�;���.�����V�d��k�6=|m��lf\\�d[\\07bԬ����kzE����3#~t�1d���y�x�G�ȳ1�:\0�Ռ�h�\nw�֭�i���ݻw�WW����\n�{[?��_|%�>�����{/�w\\��u4Y�^��q�܄���̘���⊸�Z}��N�������x��O���n�gc��dp�~lS`�f�F�ՠ�V�2M��ȥ��~�q����S�N�O�;^}}U|�S}bÆ\r�;A\\Zz��jgyIl�V��>�wg�2lN|j�ŧύ�ϊӇ�..��ű��\'���ec�AdP�6�jJ��l�Ռ�h�\Z�|��\"%%%���\n6��;����	���\n�,e��+�X�3Y�5ZX���dY`	.Ȟ1��o�����i4YJp1y+c\0�����k�0N��d)��䭌4�����rF�q\ZM��.&oe�����}��`�6N��R���[�k�|���i�4�,%����1��o������h��d	�\\���j�w����8��qZ�%�Jp1(c@�o���~�ha�6N��R���[Z5/Z�\\]T�oc��>��8m�F����2�\rְj��m���F4Z��O��q�8�&K	.&oe8h�����h��7X�\Z]��^��q�8�&K	.&oeh�\n5>�5Lu5Z�}��Z��k�0N�i�M֒\\�!���A�`p�7��\Z܈Ʃ�F��\rV�F�S��i�4-���)�Wp9(�%c�1�q_��6:�5Z�m�����E�h9^���iZT���E�Z\"0�K랼��\0�}�\rVC�B�V�=l��F��j�6N�B���-9Cp��n�6�{�`��h��\r���>��qZ���[��%�n����߲�o�骪�\Zx\n��҂ǀ�a���C�Eǎb۶\'d�В���=sT[��7\rV�F�k���j�i\Z�f]�HiQ5nܷ��/�pH��G�M��k��kf�m�E+[\Zz|N�2>z�<*��i����L��i�h��I�){��6���z����1žQ�j�,Z��lE�_��5�t��������>��\Z��@�?�С}|�+�˗Ϩ5�+W�.�>����wǺuK��M�s��W�=5�;��um۶�z��=5��~���������U[m��X����ܹS�N���(-o�7�P�N;��X�����Ǆ��S��B�o��ԬM��i�N�=j��쾺�_�8Q(�u=�ɻ��}�]Mpo�d5�\r���r�Z�Y��[3�E\r\r���C�������.�z��5�1��]>��o��=N:����7(�ܪU�g���J�??���=�����ϪY_����f̸�`����u4Y{4�멧n�&�t�_�L���=wW��)t��7�qjVjz��͟?%�t;�W_�k~�P��z|�w����\r͖�}�=i��&��j�f�F\nnI�F�`\Z���Z����?r�/[vK��t����K:�?�ٿ�5����Q���/��_g֮}�`\0��\Z�B?��`�s�3�,ZB��Wc@�.�tD�y����Y7d����_gv\n�_(��}������c7��*������澚�MC��L{mWlL��ꂎWk5k5Z���zר����\"��BR��?��ǫW/�54�/#�ߴH�y�.��秿T�u�?|��I��\Z�B?�Pp���\ZLZ�o�1 U��E�Ϳ���R�e��������iH�����4Y�y|�w���jk��kp�{���aqM���O���Z�Z�ַ���@ڱ����J~�?��*�j.�q\n:�9�˻�(s����}�}��?�c���v�/�W�B�Cͯ4����o/�1c���;�Y5+�}����i�M�l��{\\���_P��	.���e�������ǩY�i.E�O�K�,�|\nM}ٯ9N�x]���7yP��Ij�\r7���?eq�]����\ZYp�ͼy���ݻf���CϨz1bͯ˟�>��ǺuK�S��OE:��&��1=E)��Y�Ōu�5��W���ѣ[Ԣ\Z��k�Y���2����_�5Y��&+=���f�v��ϫz�u���?�%���7�qj�kj\Z�צJ���ۯ+�5ǉB���5Y�B�3Z\rm�\Z�h��k5k5��*�5�h��KD���Oݻ����U[�5����\Z,���iWi��d)��A�hU�F���1o4\\��\\��q\Z�U�k0�d)��A��SS�4���e��}{��\\X�)�4��4Yh��ɻ�4Z{���\"�q�8��*M�,e��h5��\rk5e�Fp�&M�2y�X{�?O�8m�Fp��j�4Y�@�F��g�\\��q\Z�U����d)c\0�4L���׃q�8��*M�,e��h�/W[���0�i�4��W���R�\0�5���\Z\\���k�0N�\\%��,M�2�p��hy� �i�4��j�4Y�\0c\0M�hi�0N�ApW���2h�Fk��q\ZWp5Y�,c�1��1�r��L;�i�4��j�4Y�\0c\0M��M�q�8\r�+��,M�1�\0X�)�4�+��,M�1�\0X�)�4��4Yh���0N�\\%��,M�2\0�j�8��\n�&K�e0\0�j�8��*M�,e����iWi��d)�7`�f��\\M�&�`\0�Քq\Z�\\M�&�`\0�Քq���C����˅����O�)�K�5Y��mo0\0�j�8M;�����,8͠���v�\nM�s�����2N���<����&]!8͠�?��.���bn{c�1\0�VS�i�X�^����ս���i�9��f�6m^���,��7\0k5e�f���S���XÆ�yKnW,9X��mo0\0�j�8M�;�s�÷����P�ŷܗ�kru�&����\0�ZM��u���=����mӦͪ��f�����\0c\0`������o�_n��{=�w�?����ZM�����\0�ZM��Nj��c�;u:l�e�}�=��5ޛ���[!]�3]����y�-ᴳ&�����\0c\0`�f�6N����դ\\=����ŞڻJo^��[!]��%]�F�upn{c�1\0�V3Nh�l{\0\0�B�\0\0,���\0\0}�\0\0�з�\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0,���\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0,���\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}��&\0\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0,���\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,�m{\0\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0}l{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�B�\0\0�з�\0\0}�\0\0��Ƕ\0\0,�m{\0\0�B߶\0\0,���\0�5�_����V�L�,\0\0�a�媼�&k�ͤ�\0\0\ZnQ\rVI���D�,\0\0�����d��y4Y\0\0@���ڬ�Z,g�4Y\0\0����Y^���\0\0�¼\"g�4Y\0\0@�P��\Zlsh�\0\0���m��X�,\0\0�	����d\0@�]1G�7�tՕ�G{��SJ=�}]o��\ZXm۶�8�^�?���N8�؁�,\0\08�v�O/8�߶޽{ƤIWDq��QZ�<\"V����KJf����O�cw��y��M~�&\0\0Z��ӯ��G�n1e��(/Zc��k���.]:m?���h�\0\0�6X\'����L�h��K/͎#���~Q�~/M\0\0T��\"��`i�\\�ձc��E-����,\0\0�ZG��5X�)�\Z�W�\'��خ]�G4Y\0\0�¥���\\x\rց��];�S��͕5Y\0\0�R�˴��jr|�;rEn�L�d\0@��+]����������#z���%~1�s\\��q�Mc5IMP�>z˟s�d�&\0\0Z��F�\r}��n�a����q�Ȯqs���{D�lH����q�M�4JM�>Z�]R��\0����M����cf���E�����TG��Î��\Z�묿�(5A�ІE�\0\0{�d��N�д���E����� ���G��A����q�珊˟�(i�\0\0@�����+N�ҵ��_���oɿ���5�\\�����1���F�NW(�d\0\0\rn\0�\\sNlz�X�rNl��\\��noz�����$M\0\0��&��|E�pү���⃕ӣ��yYm�?��+��~��(����&\0\04Y�-�w���.�^����oo��?�Z��������{K���#���(�k�4Y\0\0��*�诨Xw..�c�=��z{ܹ:b��W<qɲ��FL|4b����د>�޷8�?\\�a�d\0�&��Z��#1���9O}��B�ؒ����\".]\\�﯈oͪ�oܾ3&,�5[�+�#n�yK�k�4Y\0\0�ɪ����ĺ�?��̘cr��%#��@E�Ϲ��?+��;�k�*⋿��v��������U�j�4Y\0\0�ɪ�^�5Y��6F^63�[�-.�1jVE|�\\�5�\"��]Ĉ�?���2��ؼtR<�S4L�,\0\0�d�V�^��q��䒟�g�ǷW����ەu�W����s�~:�>w��I�\0\0��B���$�m+�	�~λ3N6\'>5��S���Ƀg������������j�4Y\0\0��R�,M\0\0h�4Y�,\0\0�d)M\0\0h��&K�\0\0�,M�&\0\04Y�,M\0\0h��&\0\0�di�4Y\0\0���di�\0\0@��4Y\0\0��R�,M\0\0h�4Y�,\0\0�d)M\0\0h��&K�\0\0�,M�&\0\04Y�,M\0\0h��&\0\04Y��6l�}rH��رCl����J�\0\0����)S�GϞGe�Ĵi������j�\0\0�&��N�%K~�-�O?��G��;�6?�c���3V��]�?w�=7F�^ݳ�6mz$.�pHv��C��1j������3��?��]���q�ǺuK�+_���{�?��b���d\0\0ͷ�z����?����>�[��o�/�M<�ح���~�����o�P�sRS��?JV�v�/}�뾓}���\\�}<a���_� �}�y��ϭZu�o���h�\0\0���d]z鈸��I��Y�n�\Z��~��ݚ���Fz�Vc?�*��J����LV�v�/}����e��d�׮}x���W�{4Y\0\0@�k��E.����j��\'�WZ�|�f)����l���\\]MV�9����\'g�M����Z��K��_\0\0\0ͺ�JO�7�[�ݗ��WT�T�|���s��r����\Z��>W�qG�Z�t�E�~��N��??y��t�3���?��+�?q�e�,\0\0�y7Y��M/�4{���O�0പf)=�0�QJ_�z���\Z��>W�q�E.RS��6U����E�?�i~�S���J_�^�U�i��,\0\0�Y6Y\r�tyc?�}�\0\0\0MV-�?����i�\0\0\0M��d\0�&Ki�\0\0\0M�&\0\0�di�\0\0\0M��d\0\0�,M\0\0���d\0\0�,��\0\0M��d\0\0�,M\0\0��R�,\0\0�d)M\0\0���d\0\0�,M\0\0��R�,\0\0�d5�*+{*^{��FW�>M\0\0�ɪQ�a�lB\ZU��4Y\0\0�&˙,\0\0@��4Y\0\0��R�,\0\0@���\0\04Y^���\0�֫C�����]]����O�)���;*\0�;������LV3����價]��Q	\0\0-ؙg�>wҤ+�����gޕ�%��\0\0Ђ��խ�^���˟���z�M�6��v�\0G%\0\0�p={�rʔ�\Z�XÆ�yKnW,q4\0@�pR�·o+)��5Y���o�/����T�\"\0\0���w��^sn�Z��S�զM�U��s�C\0\0Za�վ�!���ml���jeg�ެ|��\Z\r\0\0�n\'�m��ŝ:��˾����͚��>Z-�}��e��U+/r�^��)�\0\0p�蛫I�z<W�E{�4=��Jo4��+]��U\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0���?#�h/iZ�!\0\0\0\0IEND�B`�'),('8',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Submit for QC - V2.submitForMlr.png','1','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0�\0\0�\0\0\0I8�2\0\0<�IDATx�����e�(�i@.����!R��z�6�?��%�KR�z�������X���j�8F�V�W�D(����dL�76up�	r�8$������;k��a��f�������Z/k�\Z�z�����VT\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\04o!�t�:��[F��r�~���С]��E�G�6���:���}�����:}��\0\0h�z��7�wg��=�ĉ7��������B+E#D|oKKg�I�Ƈs�9}oϞ���|X\0�g�������5L�|s��\\��k��2��ᤓ:�:��vWX#\0��P�}��ӭf�����^�N9�w��e�5\0\0h��.�q���y5�;�+/�+,\0\0Мœ��c(�.�����&��ڶm�笥\0\0@���\ZO����]�t~+���[S�qN��4��/�Ess\'��5S�ߘ1cr�ʸq�Ba��#�q�F�H��I�T�c�S�;\r;�K~ќ�/cb���TN�0!̙3\'�ݷo߰`������C����eO<�D�dVTTh*0���?\'��\nk*�1ʩٝ��%�h�����_Ƅ�cǎ�b����?�.]\Z֮]ڶm.\\�.[�xq�s�֭����\'yO+��\0�h����NÎ��_�\0�|�!�_�>m�}vQؼysz�ꫯ�����a��/��֭{+������o���l��4�\0K���iؑ_���TFw�qGh߾}ز�j�֭��\\����鲽{����,Yb�WM%\0G63;5�Ӱ#��������N�>=���Ν��̙���F��޾��={����ϟ�M�f͚%_4�\0�fw\Zv����4�EՎ��;wnؼyKz;��\'��;�L��Y�&�QVv���M%\0���ٝ��%�hMM���rY�2$�۷/�,�5jT����=�R�h*8N��4��/�Ekj*��?�N:����k�I���|[���v�\\�Tp8���iؑ_���Tf�x���W�m��i���d�u�]�3�������ܰa���T�X��S����_򋖒Wk׮\r�2[!�.ߞfʃ�H�Ǔ�\\y���x\\e�;;��c�v�\Z^z�%y\"�\0h�o|���D���_���z��W����ɽ��;C��W��YR�˫����/��BX�`Aؾ���ٳg��@CO�N�n\"F~�/Zڗ5���lٿ����Mf�Ί�m���1?9(���\'�q��㮿�z_��5\0\Zcr.rjv1�K~��x�r�6~aӭ[���l](ߵ7�;�v�	/��*w\"��eM��\\S)�\0h��ש�M��/�EKj*��O�Ry×�\Z.��S��)S�����;�^S>��s-[\\�G���Eo�S����_�cQ�4dS��SO�򦴴4ɣ���������T�5\0\Z�_�f7#��G#_2�����WB�Ь�^�f7#��M�TfcaQ�-��g�@K)z���D���_4aSY}��%�J�@,z���D���_4����������ʕ?\r_��о}�p�yg�����ןZ����>в�x�����{�m��5����\Z��ש���/���\Z*Q�K�vZ����|�ɰe˯j�\Z����hя��<���������nݺ����;�5���>�i.���^�fW�\"��M�X�HbXm�ʫΝ;�[�^{m��7mZ.�����3�|_X�l���ԩ��=��O<>����ӡ_���qǵ\r�G�WS��^����������^{퐴��èQ��e�v�f���B\ru��۷/\r��}!������:�����yM�@��Nͮ�E~�/�bSY���3�~xB�����ѹ�W]5(s������,Vo���\r���ȑ�N�?��]�W~^�������3f�Mel\"��\'���F��j����U_v�u�IoϘq{޿-�;唓���r\r�*z���%�8�Mei#�4��ʫ��iv7��㿘.��u.ޯ�y�[��}�^����gl(�\rdvY���sŨ�o8�����W̒%�/<;��Z�g��\0E���%�WkRh��!�Uv�\\�q�6]�w/�w�m���쮨���Y����ؐMe�cLׯ_�*��^I|%��I�Ib{�w\'�$�&qK�q\0(z��/!��3����ԭ^=\'���9��ty<�1ޟ0al���K.��~ᅇ·�u}����=��<>{Lf�z�p���qY��N��߸���n*�[G�sWLvkkv������5�s�)s-n�^�i$L��I��D�̿ǟ�3�PL�<�$�k���Wы������G�N/��2^z���2{��xc��`�j���I�s�K�̝{�!5����ډzb_?F��=��i�̝`�p���(�pÈ\\�]���4��С��.IR�k6צ26��2��o�vu���6��:�����Wы��K^�c+�F&���jwֶG�<_�<���\0&g1�K�/y%��\\�[%�1�����d���`r6#����W�u���Lػ�W�ޙ����l\"F~	�%�D+͵���]U�7Ҋ�\'����)\0����%䗼�/��^oi�)c�k9`r6#����W�u�Z�lH<�k���B���n�\0&g1�K�/y%ZU�ŭ�Î�\n5���J\0����%䗼�&�zU�D��Q\\��5�ɀ\0L�Bы��_�+���s�v�W�IE��&��Y(z�_�y%�B��N��Gy����>)�\0L�&b䗐_�J��\\����}�W��I�%�\0L�&b䗐_�J��\\۞D���R���-�\0\Znr^����?ڷo�;���c�4�Ds�&�������{�m��7���ϧ�_�K~Q�$w��޽L��Lc׮߼�|L�G4p6Հ��pL����#��|�ɰe˯�M7}�Y�\'���бc�Zӭ[��u�y繣���^����w�KJ���5Ә7�ާ��iő|ƭqK��8��Ν;�[^{m��7mZ.�����3�|_X�l���ԩ��=��O<>����ӡ_���qǵ\r�G����yb�z��C�B3���Q��e�v�/��5�,����ۗ�q㾐����n|����������%�h\\4g�ě4p�4��HQ��T[k<���sE��O�p_�����\Z�.++{<������}�ݒ����A��9����G�+�����U��{�X���O>99�x;.+Tl֧ ����>�ޞ1���[��9唓����^������յ_�^�**+�k�_����xU�1�?�ϸ5����sEo�E�~��Mo��/��jnA���*0㖏x{߾�,k{��k��Q��N�[���?fɒ��n	*ʳ�Eы��_}={��r��5q�,�\r�@��,:�Ϸ5^����Eo����2[�<ABmf�X�Oax�Eo����,z��~�E/�K~�|�۹�	;KKgj�I��<0/�\\6$q��~����Sm��\nw����h��7n9��@�^=\'-��9��ty<�*ޟ0al��K.��~ᅇ·�u�a�����y�0���;ᄎ����=�w̘����������%1F|o�z���V��/�%�8\"��u�Mc�<\Z���Ⲣ��HĢ�|��V�$~�ؓ�|�����G�N/��\\z���7{2�x�U����*0���s�K\'̝{�a��c�_;F��=��i�̝\0�p��X��pÈ\\��]��xf��zC�^��dB]���E~�/�N?Ю�q���lu�e�C���uCC��#�x���l�����VS	аE�pqv���9�M��<ө��Ǝ���~�G\\ǲq�C/��9)OܨxAc|�%E�{�c����Qh^\r>��W(z�_��O3=��ƿ���\Z�EU��m�7�ڻ�jh�F\\a��{4&�	(z���%���odQ�I{z7B�\Z��ڣ��0�\0�^��E~�/����4�\r�ŲO���q4\'#���^���N�b�����#9�1C�!Ө���G(z���%���ջ��@񬰃�w�eCJ���=�	�v����^�����g�˸봢�ˏ��D�̿w�4���gZ�i&��K�h*E�P�\"����J�I��4��S�Ɵo%�dQ���d���Bы��_\0�J\0E���%�\04�\0�^E/�K�/@S	���^�%���P�\nE/�K~h*E�P�\"����@ѫ�E~	�h*��^䗐_��@�+��K�/@S	���^���T(z��/���P�*z�_B~�J\0E�P��/!�\0M%��W(z�_�@S	(z���%�\04�\0�^E/�K�/@S	p4�o�n������ v��͛�GRi��_B~�J�����XRr���ļy�>�|$+���K�/@S	�bpќ�oRt6�>|�#�G2�Z)���4�\0-F�^]���խ��r�³i�O��ū�����R~	�h*Z��=O]9y��\n�&�a�<�|����K�/@S	��۹�	;KKg*@� JJ��|����(���4�\0-ՠnݺlS�������,y��Y嗐_�������������Vǀ5�1^�]�6(x嗐_���59�M��<ө��Ǝ���~�G\\g�ᮓ/k�B�9iH<��.y�K~�/@S	�*�Ibb�Nbwf<G����:y��B)���4�\04Of���\n�_\0�J\0E�\"Q��{;@~h*8s�����\no�/\0M%\0�շ�o[Q��L� �\04�\0�˓E��#nMq��/\0M%\0�ʷ%#�= �\04�\0�faQ�K�z{@~h*(���(������VO�}!tg�����x\r�� \n��\Z��A\0�5�/\0�\0�k@~D0^��0�`��`�x\r�\0�(��\Z�_\0Q\0�5 �\0�\0�A~D0^��0�`���A�x\r�/\0�(\0�k�_\0Q\0�� �\0�\0�A~`0^�� \n`���A\0�5�/\0�(\0�k�_\0Q\0�� �\00���`�x\r�� \n��\Z��A\0�5�/\0�\0�k@~D�׀�0�`��`�x\r�� \n��\Z�\0Q\0�5 �\0Z� \ZBI��So�e��a/��w��څ��!� ڴ)��Y�6�c}�u����`��@�Dz��7�wg��=�ĉ7��������B+�F|KKg�I�Ƈs�9}oϞ�����X��x\r�/�V1�N�~��=��ɓo���5��S�|=�tR�]���\n�9��Т��P�}���-i\Z����6+�r�I�&�0�:���\"Ѹ�k�B��l�Ʋc�v�Ev��5 �\0Z� \ZO������j�.&M\Z��m۶�Y��x\r�/�5�Ƴ�Ɠ�8���K��o%I�<���b�xِx����N~cƌ�]&cܸq�0��ĸq#W$��$�<���b�x�xِ���	&�9s椷���,X~�|y�޽{��\'�H�̊�\nM�a���?���=\\a��5 �\0Z� ڡC�Z�C;r[%c����?�.]\Z֮]ڶm.\\�.[�xq�s�֭��ü�e��UZ��x\r�/��4���脰~���Q|��Ea��������:lJnWTV�ѣ��.[�������o���0ä	�k@~���2��;B���Ö-U[ �n}\'������N��ݻ7��d���j*�x\r�/\0M��tw��ӧ�vyݹsW�9�������È#�����gϞ�����ôi�¬Y�4��J0^��Xn*��K9w�ܰy��v<iO֝wޙ.[�fMx�����$j*�x\r�/�c��ܾcW�������ۗ[o�\Z5*�DV�[�I�T��\Z�_\0�xS��?�au�s�С5��\\sM�o���r�����4��J0^��Xn*�n�����+¶���\'�ɺ뮻rg|}��Uiӹa�\'��T��\Z�_\0�zS�v��0*�����i���C3���$=W^yez;WYuM˝��]�v\r/���&QS	�k@~�M嫯��69M�m,�JW�vT��̒��n�eeea��^xᅰ`���}{��g��$j*�x\r�/�c���ⱑ����iC���aێ��c~rP�ӧO8�3��]��v�T��\Z�_\0�ʸK�t�x��nݺ��e�B����Fsێ=��WW��ݻ���שܼy��RS	�k@~h*�gy�[*o��W�%�}*�=eʔ���z��k��>xNزeK8�&QS	�k@~h*3M�SO=��BYZZ\Z�3g���x��PQQ���T��\Z�_\0�J��� �\0��JM%���`�Tj*�5�/\04�BS	�k@~h*��P��0�j*5�����ATS��� �\0�T\nM%T�J:L�z�-�G{�_��ww��.w]\\q�ѦM������c�3���NhMC=`�Tj*��y�o�8p`ߝ�{�\'�JJ��w/3F4@����tf�4i|8�����y���[~����ATS���Va�������5L�|s��\\n\\h�2��ᤓ:�:��vWX��TD5��Jh�\r��g�?ݒf<8z��k��)���n����0�j*5��\"�]^�J\re�5�;�+/�+,�J\0�h5���DS���f?ȅ��ʸ˫q��bҤ�[۶m��5M%�A���Ls�_S����*��5���1�M]�t~��94�\0�`S���E��\\*�4�Ф�eC�Y^��E~cƌɍo�ƍ�W%ƍ�\"yO\'Y+�TD�5�շ\\^���TBs�C/RWS9a0gΜ�v߾}Â�o�/ݻwO�=��iUTTh*3���?\'��\nk%�J\0�hmMe�-���j*�������N�5���\r�BMS	��С]-סaǎ�_%%%�ϥK���kׅ�mۆ���/^��ܺu���0�c����J4�\0�omԨ��9a��/����vH�h*O>���c��\r��f�T��A+�\n�E�ׯOׁg�]6oޜ޾��æ�vEee=�K�u��\nﾻ=���oh*}����FjF�nM�Zdm�.檊���/]tNz���xP���߾}�p��\r˖���T�\\��p�e	����iӢ���w�<�}�ߩ?��wBϞ��6m�s��fS��o��q�\Z�|��/�o_\Zƍ�B�ܹS�7�&Mń�2��w�qG���Ö-U[ �n}\'�o��v�l�޽��%K����8�y\0�Fh*k6��(檊�ѣ�������0v��\n6R�Q��O9夃\Z���-}�ؐ��W]5(�����ӟ���������;�<W�����\ru�/�Z5��>�>nƌ�s�	\Z�����N�>=���Ν��̙���F��޾��={����ϟ�M�f͚�I4a\0����$F�i&��TN�����Q��c��sPc�d����Nw�-ʳU06a_����bQ�-��^�������ҍ�-���7ԧ�,�Z�\Z�}�^T�)&h䦲�81w�ܰy��v<iO֝wޙ.[�fMx���޻���\0jWh��!7���8��M�[+kk���#��~���\r]�J�pÈ�\Z��\'稊����K?�k�\n5�u�\r�i*�V��R1����4��w��5����!C¾}�r���Q�Fe�J��JM�q�\0\0MX�e���{KsMe��,./��+kZn_z����ȑ�>��?&����\'O�9�\\\'��1�<ގ˲[�\r57hP��	=�1c��7z������Xp���\Z[Ŝb�Ck*��?�N:����k�I���|[���v�&QS�y\0���T�uV׹s�\rݺuI��:��܉nj>.��W|<lڴ8ݝ��n��v���\'�y�ѻs˧M�f�D9��\r5�j�c���=ztM�Ȣ\Z���{�|\'�[]��l]���R1���CUn�����+¶��4O�{����__}U�tnذ��z4��\0hM�P�)&h�qh�ڵaTf+�����6���f���Iz�����v<��Ꚗ;��c�v�\Z^z�%�q�\0\0�J���cyz��WӼ�kroc��P�򕰣\"�g����(++�����/���۫���={�q�8�y\0\0M�b�ı=����lٿ����Mf�Ί�m���1?9(���\'�q��㮿�z��\Z�0\0��T̡�0�]Zw����kUv��-�.[�w��5��v�	/��*t��=}|�N��͛5��!�\0h*s(&�Cဳ��-�7|����>�ޞ2eJx�i���5e�<\'lٲ%ȸb�<\0��R1��ES�i*�z������P�9l�ŋ���\nM�q�\0\0�J��	�0�y\0@1\'s�	�C�!0\0(�bN1�q�8�\0s�9��!a�<\0�bN1���8$�C�\0P�	Ŝb�q�\0�9��SL`2�y\0@1\'s�	�C�8�y\0\0ŜbN1�qH�0\0��S̡�0	��\0sB1���8d�\0�bN(���C`\0@1��SL`�!�\0(�s�	�C�8�y\0\0ŜP�)&0��<\0���9��!��\0sB1���9�C��?<����S�ޚ[VϜ2��0\0��T�)&8����kƏ�bz��k�4�����:vl�`�3�y\0�Ej߾��ݻ�i�A����7����Z���T����a��s�����������dL�_�Ѱlٌ�M�ʕ?\r�]����+?�6-J��3ߗ����|\'��yjhӦ8�\\�w�ϝ]��o��q�\Z�|��/�o_\Zƍ�B�ܹS�7��\r�\0\0�����XRr���ļy�>�|$+���	M��M���C�F��e;�s��(�姜r�A\r���ߖ>Ol�����\Z��[Y����~��������w���V������z��q�u�I7c���Tb\0�i\rpќ�o��5�>|�#�G2�Z���T��L�����Q��c��sPc�d����Nw�-ʳU06a_����bQ�-��^�������ҍ�-���7ԧ�,�Z�\Z�}�^�Tb\0�i��յ_�^�**+�k�6�T\\\\�*�H�[+�ʃ��E�~����&ƭ�5��N��_�~Aކ.n��?o�a�A�Y]�\0ĭ��^��\\�W�9��o�OSY�\n5����<\0@�г�+\'O�Ycׄ1l؀��b��Q1�������[�k*�5fqyQ�]Yc��\\�j��\Zo���N�3a��Z_?����:ᄎ���v\\�ݢX�o���A����׭�ƌ~��X�jF��.�a�]rkkl5��\0hh�v�|���ҙ\Z�&����%���$.�**&4�������:w[�.�1�C�^�;�M��ew��⊏�M����V�\r����1�:�>zwn��i�̝(������V�z,�x��G��iYTc��|���D=q�k������Tb\0�h�C�4�G��,...K��aVAń�R���\0Z|cٮ�q���lu�e�C���u��R1��RS	�����6m��L�N�o\Z;�s�{��mpˆ�e�lH<�k�<�J��*&�Tj*�<\0�*�Ibb�NbwQ�c��aGeQ�u(�eC��U1��RS	�\0@1��RS	�\0@1��Rh*1\0\0�	4�BS�y\0\0@1���J0\0\0�	4��J0\0\0�	4��J0\0\0�	4�BS�y\0\0PL���J�\0\0�	4��J0\0\0�	4��J0\0\0�	4�BS�y\0\0PL���J�\0\0�	M��T�y\0\0PL���T�y\0\0PL���T�y\0\0PL���J�\0�bM��Tb\0\0PL���T�y\0\0PL���T�y\0\0PL���T�7�\0�b���}�v�w�^��k�k�o�L>�Jk%�\0@1A�q�ygl,)�_S�b޼{�J>��J�\0�b�c����L�x������I>�I�J�\0�b��W���z��VQY�\\c״����U�G��Z�y\0\0PLТ��y��ɓo��5a6���Xdm�<\0\0(&h���������35xM%%�K>�\rI\\`U�<\0\0(&h�u��e����7����e��?�*�y\0\0PL���v����we�c,���.�4��\0\0��ɹmڼ�N���4v��~���?��:�\rw�xِx���Iy�1�vy�<\0\0(&h��$11�_\'�;��#�ʢ��P�ˆ8�+�\0@1�ē������\0�bE��W�YO{;�<\0\0(&�P�-���+�`\0\0P_}����2Τ\n�\0@1��d��\'É[+[	�\0@1�ʷ�2#�=`\0\0P��E�/�Q���\0\0���Bj�J��J0\0\0�	�Փu4���\0@1�S�_\0�b�� �\0\0�XOA~\0(&���\0PL���\0��\0�)�/\0@1�S�_\0\0�	����\0XO�\0(&�z\n�\0PL���\0��\0�)�/\0\0��S@~\0�	����\0`=�\0(&�z\n�\0@1���_\0�b�) �\0\0�XOA~\0�	����\0`=�\0���z\n�/\0@1�S�_\0�b�� �\0\0�XOA~\0(&���\0PL`=�\0��\0�)�/\0@1�S�_\0�b�� �\0\0XO�\0(&�z\n�\0PL���\0��\0�)�/\0\0��S@~\0�	����\0`=�\0(&�z\n�\0PL���\0�b�) �\0\0�XOA~\0�	����\0`=�\0���z\n�/\0@1���_\0�b�� �\0\0�XOA~\0�	����\0PL`=�\0��\0�)�/\0@1�S�_\0�b�� �\0\0XO�\0(&���\0PL���\0��\0�)�/\0@1�S�_\0\0�	����\0XO�\04�\'3�Cm��ۄ��\0@>}������mB��\0����4��I�����\0PH�Z���� �\0\0����x,���(zA~\0�)߱���D��\0����J���_\0�a�_����@��\0�Pe����E/�/\0�C�W1��䗷\0\0�ˬJ:L�z�-�G{�_��ww�ЮХ;�!D�6���:���}�����:}�5M���\0\0�:=��컳w�a�ěBI��a��e!���#����3äI��9眾�g�S_N���u�J0�\0�U�>��y=zt\r�\'�*+�+\0\Z9�L�z8�N��?���>M%��\0�����~�\r�����k��\n��rһE�����y\0\0Z���S�fZ!�tEǎ�ʋ����y\0\0Z\\�J:�cg�N&���I��om۶�s�HM%��\0Тĳ�œ18v��K��o%Ik����\0h1������L�M�ƍ\\�|$����Jh>�@~cƌ�]&cܸq�0c�y\0�V/^,�.��ނ�7�����Г��w���ᱩ�\r�?�����d��RS	�g��	&�9s椷���,X~�|y�޽{��\'�H�̊�\nM�y\0�cI��u}�?���_?��Kxpd�pS�<9|wH�p��;��M� h��%I��RS	�gaǎ���1JJJҟK�.\rk׮m۶\r.L�-^�8��u�VM�y\0�c�ȭ�d7mh�03i&��4��\Z��pװS��v	����� h��th*�y�!�_�>��}vQؼysz�ꫯ�����a��/��֭{+������o���4\0��88�{i����o���~;�������p����Sg��>~j�k�o�	4����2��;B���Ö-U[ �n}\'������N��ݻ7��d����\0PL䏟�t~ؽ��/�����Wf�k����w��R�����}���@SIk���ӧO���s�0s�����Ç�#F����{aϞ=�����iӦ�Y�f��\0h*��w\\�y�i����\'�hz���~~y�pŀbM%�l(�v,�ܹs���[���=Yw�yg�l͚5፲�~Ǹn\0@S�����k�-�|\\߰g������i����_��7��v�yQA��@SI+k*��ؕk ��!C��}����ۣF��4�U��V\Z��\0h*��[�\"�xjq�5xqx}ݎ0�_>���i�ۿ����ק�����a��R1���V�Tf�X��:t�AM�5�\\��[y����o��6�y\0\0M�ʰ���gJ���^��~W���n~=����p���� �	χ�l��p�?�&<8�P��\n��JZ�<P��o/��\"l+/Oǩx➬��+w���__�6�6lp��\0\0���I\Z�?}��a�o�\Z���ƕ�7��7<�?�~|������n[�4����O��\'�]���@1���V0�]�6��l�|�|{�&>�Ќ�~<IϕW^�ގ�UV]�rg�{l׮]�K/�d\\7\0��\\~�\r���F�ь\'Ø�q�~A�������$\r��W��>3m�俅�ɋ�֧o�����bM%�`x��W�q�ɽ��;C��W��YR��͵��,���/�,X�o�:�r����u�\0\0�ʕᕤ�|��cg�;���	aԣ��~�4�����~\Z�!|�{�?�\'��q��@1���V1����lٿ����Mf�Ί�m���1?9(���\'�q��㮿�z���\0PLTŦ�f����9I�����cJ�?>�?\\W-�1g����}������H����	68���VВ��;v����kUv��-�.[�w��5��v�	/��*t��=}|�N��͛5��\0�}��a���p۴_��W�,�?lv���_����h�h�O×������ߤ�U(&8Ƞ$ʓ���9�[BKi*���5n����_\r�\\�����)S��O�V�)��9a˖-�@�u�\0\0�J����\Z�l#�Tn�X��ʧ�z*�����4�g�c��š��BSi\0@1!4bCY�r0��\0(&�b���:\Z��;��0\0���	�a���0\0���	�6����`0\0�bB(&ZyC9�0~o���0\0���	\r�&�}0�\0@1��PL�Pu���h?��\0��PL��\Zʆ�u�K��\0PL��1��Ivޓė2?E��s���\0PL��1�P��>��:�Ʋ��i,1�\0@1!�HC})���Ch,�7��]��c�y�<\0\0�	��he\n5z�\Z��\Z�|������<`\0\0ńPL43����\Z����(�k,��P�l,�\nˡ��Ĺ��\0\0h*4�z���\\����Է����<Ԇ���o.�ŒC_�����\0\0h*4|����E�o�<�]P5�Ň�Pf9ƒ#Y���>��\0\0h*4|�]}��%G�P��X��\Z�#�{��Zύ��\0h����Ӊ�����˖�H��0\'�������{�m���,pj��b�:���[.�n�G�e0_cy$\re���1�4�z^�q����ޟ:��z��-m6\0@#6�ӧ;��n���,�����o;�I�[�.����s�	��b��X����?f��ə��D�$�K�}�8>�NI�7���89��I��D�$z$�3�^I����I���YI| �U���U�wa��EI|$��I��4$\'�$$��$&�L���L��IMbxW$1\"��%��$�N�I|1�k��oEU�U��uIܐ�O�$qS���W���I|5����_I|-�[��f��ķ��=�	I|\'��Iܕ�w��^�O��$~���N�$�$15�iI�{d��I�L��M�I�G�$�h�%1\'�_$17�Ǔx2���X���I<[T�u�WI�$�|K�x!�eI�6��I�H�$~���I����I�g�3�͚$ʒ���x3�uI�O��&�!�������R�u{&���;�m?����k��,�ʓO~o�ر}�=NS	\0��T��[\ZN8�c:1�޽,]�}��p�ǧ�7mZ.����}�v��3ߗn��^P<��=�W�ny���N\\֯��l]����#9�`��|\'����Ε+��u\n����1n�B�Ν��x�D�X�D,��Vɶ���u�׺Զ��>ױ,$��2>Ͼ$��DE{�ؕ��$�\'�����4��ؘi>b�Vk3�IlR�H��d���ĬJ�Ls���%�2�>�&�4��d���$-I�L���g����-�4YO$1/�|�&,�7+Ӝ�&�gI�$Ӽ�̼Gf�����8�e���2���e���$ޓ�ݙ�16�w$�I|;�dޖ�7��z�	���?\'1>Ӥ�f��Iܘibc3{}��irc�{m�2Mpl��J��L����d֡��f��$>���b�}Y�f��،4��\'Ӥ�f��EU�=�]��??��2M~l��N��̗\0�ˀ�\'�̗�˂nI�����z�2�Ϭ�u�Kq���.:\'����<�Y�ޏ�e7k��������>^y���c��޳����漑�o��q��|��/\n��\04�uD��:>>�erʔ�������W]5(�����ӟ�)�>Y�ܚYۤ�}�x?N���u�}&�w�-��g��QX������(����s��3f�~�sz���U6�5�ɚ\r����9��p\ZK�Tr��y��`d����qi��i���?�2�����Tl��SN9�18��ybCv(cl�1��\'���\ru�/�Z5�мa\0@SYG�o����V���}0�%����~m�~]�|����Vq����4~�e˯��<��j+j������������xMe�*�㖶��|�\\}�X�lH���Jc���zw��[X;�<�={ĨQ��=Oj��K�<.���t�-ʳU06aտ�;�1�����ܳq��������?�^�P�[s�0\0��<�������	6�kl]�$�5�Ǹ⊏犘��/��x�8���q7�ښʚcvy�����5��Bm[&�j,�j��e]ױ,$�\Z�:���erL=��:ǥxB���[+k�٧��#��~���\r]�J���u��_2^z��R3߼Q��P����k��\0pMe�0�[�M�fny��\r&�m��2�i0����~�M���s£�ޝ�9��y����8f��t�?<`y�������QL�\num)���,:��2�����_��9.�=F�Me��-./��+k���kQ��_����c\'O�9�\\տ4�����BC��\r\Z�/��n�������V�(4o4�|`\0��7��Oؓ=.&��nܢX}w�#m*��xVظl�]������ML_;���.�\rt��1�?��ق���k*�y��Xַ�<��2�z\ZJ�d��>w��x��:��܉nj>.�m�eӦ��\Zc�.��1�:�������f�D9��\r5�j�c�I�z��6�E5v��Z�Nԓo��T��hfw��EA�4���5�-F�bYsW�/�u(�5��k��c(9f�ש\0��E�&;~c�=P��Z��	th,����z�X������;ԓ���<\0\0�	ńb����+llG�eBj�ޑ^��\0PL(&h���ʷk-��\0���	��QoP�<`\0\0ńb��HwY��+�a\0@1!\Z�����<��y\0\0ńPLp@�8����\0\0(&�bBc����$Fx�0�\0\0�	�������qp-��YC�y@�\0PL���bi�W��<\0�bB(&8��RC�y@�\0PL���XN�Pb�\0B1����|]6�0\0���	�I���<\0�bB(&\0�0\0�bB1`0\0�bB1`0\0�bB(&\0�y�<\0�bB(&\0�0\0���	�< �\0��PL\0��\0��PL\0��\0���	�< �\0��۷ۿ{�2y3�]�~�f�TZ+�y\0\0Z���;ccI��&�f����T򑬰V��\0\0�\\4g�ěL�� ��H�L�V��\0\0��zu�׫W�����&���?�J>���J�<`\0��g�SWN�|�	�	cذ$�\"k#`0\0@Ktn��\'�,-�ibo�()y`^�lH��\"`0\0@K5�[�.�G��(...K��aVA�<`\0�_P�kw�_��+[[����dvuڠ�\0���\0�ɹmڼ�N���4v��~���?���e\rw��x��xv����3vu��\0h��$11�_\'�;� �8���������0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0����Yp[�S�7C\0\0\0\0IEND�B`�'),('9',1,'/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/Vault-Test-Workflow.sid-3806d577-ddd6-479c-85f6-11805b79addd.png','1','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0\0\0�\0\0\0�Ɍ\n\0\0\Z�IDATx���tTս��c��!m\"��Ҵ�J���x����Kk�Dӂ�\"SᲤ%��j�Zkڦ)�,!`�AP\"�-u!\\���`$�{�\'�\'�L2��$��~������d�$�7{��0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��)�`��&M�}}ذ�u]�$�^LW�:%���~�+�l�߿�H�4\0@�Z���[G�z:#��̝;M��I]��CY�:VT,���2p`߳}��z�|ɳ��\0\0Q���u�{������중s͟�II�^ۭ[���}\0��	|��Lc�~UY�JRSS>5߂\\�B\0@D�a�Y ,t\\h��5��`x\0�t���Y�a\Z��\'_d�\0D$=\ZB\'82g��G��̷$��\0q��I=\Z��㫰p�n�-)b�\0D]gA��Q�X2S���G�0&E�I����-O-(��A����ͷd7{%\0 ��L��������O�������Ef�{���,3��,�̠��:\r�[�a�\0D����c�e�6C¼1Z������=俯�2�~�hX\0��\r��.��%�K�WK~.�sU/�9����]\"Ӿ�K>�J�O`\0\0�{`xb�`�;�|��B����˾U��C~#\'�&kn˒��iA`\0\0�}`X}��rrדrt�j�}}�Y+��\'_[!�ܛG�O`\0\0�{`8��-�����(*g��Hݛk����Wyf�P��]���3�h�	\0�x\rg�향�o���-����dկ����ʳJOo�G���E��ӥRKh 0\0\0�/0����˥o���õ�����7E����U��\"s^ٱ�V�^��<�n�x>�M�O`\0\0�S`ز�E5�aY��g��}\"��rky�L�\\/�������q���٥fpXU/W�?$k����\'0\0\0�)0������;�ѥ���7���z��5fX�k��]z^�[X/����}�[�Ħ{��;�h�	\0�x\n���p��d��er�i�i�Ȅ���\'̰PR/7�E$�ȝϜ�ѓ�K�������4��X��K\0\0������\Z(�\'ϓo��O6��-���]�}V���w���.�#{��\'0Ă����\0�\0��8祖ӧ+d�¿I�\r�ep�j�t��ri�\Z��R.�������R��+�mi�	1`�Y�f�?syI\0h�	�r]=\r�\r\04��-]\0��grZ���i\0�T�Y��Zm��j��uf2k�Y��ہ�@`@0a�� \n��|���\'��Y���4��}��̰����ە�5����@`�������i{���W�T���\ZJͪ��@b+?E�6k�Yx�	�B{Fٟ�\r�N�+AT����ͪ��9���t{;�6����?-��=���{�\01Ŭ,�R���bg��}V�艨i��W�(u\\$;D�boo\n�����wZ�����\"U��A���qDo������w�ݸg�x��v㹧�����a!T�	�4 �{�\r�~�7�-�~�^�!�m�6�U!�Y���Pew�(Cl�x�Y7�?[��~L�D�Ѷ�=��R�ܦW�M�\'B����g����n����a!���/�_�%�\r��Gh@,)��YH\r�vS�z\ZB������/�^#>�&h�	�����=\ZHhp��&��gNbA��t�BV���e4�I�[֎x�ⴗ������5�\Z��B��ۗ��=s\ZK�Eaz�y��e�աtBbb;�H��SF���	�8۟����V�\0_�!а�\Z�@����>�{��ԨGO�����y����$5+Fv�M�$��@`����-�vK���a���|�%���h��v���|V�kV�\Z��/fn;�@��c8U�\'Y�Pbi��6��BCBÂw���w�n���|Ƈ*���@{/߬]\"���N��	�ݟ��\ro�}>@G�p]����z���^=>�{;���xu1�c��ev��!;������}���;v,��kc��x~ÆG$#��t� �}������K�\"%%w�|C����~��\Z�	ޡ�9\r����Z���0?��c���E�\ZsL�\r�ncڴqM.�2%_-�t������ɓ/>*+WY�;wN��*�*�@������˃Q�{CEY�w�KC���F�o���;{�B.���t��Y��vX���l�/|��u���2��oY\r�~�מw���SIzz��F�W�6lp����?��j10�__l�4�������(,��$\'w���0D������0�fq\'z�G5���������X�P�3~l=�ӣ0�/�g?��:}�\r���|���\r���������>����ڐ��[n�NydV���5���#���]�Q���.���v�ݞ����8���Ж��D��QT�a�(���Fê]�ֳ��۷o��X�]6�:��o\\������;�3$�o8����C:�⋿h�n������|��{������jr_�\'0D����O����u�DkBGI �����(	�aNs��B\ZܓuN�{��i\\�ኖ\Z�����Ǻl�˻2�	�-���\\�}_�\'0D��ܖO�-����!C\r�a@��E�:���N�3\r�-����l�|�����s�L\rY`x���7^����=ǂ�<����?6����	��O��.�������� �D�J����������C����60��=����m	����z���N�ܞ����4�v�@C��x�D���.	��׷U�G/�v��m�QPo����.�N�d�&����{x�f���,�m��Q�m�������v�I��#�\ZU�à���\"0 �����n�[�(���q�$bI��A�o����#�ݻ��7ì*#�/�i�!��3�t�`IfC<�����!h�����v� �dؽr�=�Рa�=�Q��W�8d�~Bb��݉q�S�P�=4��炣!����x�4�ux\"˫g!�C�\n��=T=\rC����;\r5���9��4^�AO�D���_o7�k�m�x;�뜆`�<t�B��E)�!r;��0⩧�ګ��\0��nG�����a�L�>+��KM;����f\rc �m�C�O���v#~�,�|��ۆ�%����\r�ˬ:��g?yV�!�0��	���ۿ`��}r�~�F|\rA` 0���?7L�b���zZ\"���U�n3\Z������??��Lf�#����Ї��+8�����r�0�����\0��z\Z�\0@`��&0��p@X\0\0�͆�b�\0hN��~p�$\0h�\0\0\0��@`\0�\0\0��@`\0�\0���\0\0��@`\0\0E` 0\0\0��\"0��\0\0\0�Ν����v�XG@�־���x�+	\0q\r�w��|\rvԺu�}�|Kv�W\0 �qٚ�s��`G@��x�|K��+	\0q��{KOO;������:������d�W\0 \"���kOq�L\Z�����ͷ�����\0\0�,39����h�;����3߃*���	\0�F��������a!!!����\0 jBCR҅�<����i��{���@`\0�h�٩���w�vt���}ӦG�X�!t�,術z4�=�Q�,0A`\0��6Ĭ�fm7����IW�(�����Nr4�\0\0\0\0\0�\0\0��\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0\0�\0\0\0\0\0�\0\0\0\0\0�\0\0\0\0\0�\0\0\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0��\0\0\0\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0\0\0@`\0\0\0 0\0\0\0\0\0 0\0\0\0\0\0�8���\\��e\0 �\r5��B`��e\0\0�̈́�\n�y�\0\0��fC>/\0\0p��ˠs�]\0\0\0�|�e`�\0\0����\0\0�ٮ����\0\0�q�2л\0\0\0�r��\0\0\0�E��˂w̚4)��a��u��Ҫ�T\0թSB}���Ǯ�bȆ����dO\0D�%K�u�ȡ�32��ܹӤ�|�����=T���cE�2)*�!�=ۧO��͗<��\0UJJ�Y׻wO).�)�N\Z�0��������ݺ%]��\0���0`�W�O�4��W���$55�S��#\0\0�N�!�g���q��kפj��	\0@��	�:gA�!h�;���f�HLL|�=\0��h��Ȝ���=�?0\Z�\0 �術z4��F̷����C�?B@k��p�.m]�^	\0�8�΂:�R`�3g��Y��:=t�Pٸ�T^ݹS���/Y�=��sV�8w������K�?4\Z�\0�Ȣ�2�_gA�ԩSM*//�~n۶M�{�}ILL���R�-[�X?O�8A`h�:\rF÷o\0q�i�D>l��^(�cǎY�o��F9j�>��ȤI7[������5��w�y����2X�\Z\0��A�{�ҹsg9~����ĉ��=}��u�ٳg��[�neH��\0\0����C%%%���O�ʲe�[����$??�:��C��3g�X�7l� .�U�V\0\0�x�k��ڵk�ر��i�\0������8 �<��>\0\0 NCͩ��p��G�-�ϟo�LOO�0�\r��\0\0 �z������1c�|.0�t�M�u��7�~���\0\0 ���[o�]�v����V��� <�@�o���(�����H`\0\0�K`x��d��{�Qu�[��:�ǎk��y\rk6���,z��)���\Z��\0\0�����oX\r�g�#է�b�>9uNd�֊ơ��J}���/�,7n������W�&\0\0\0���Ep�Avv�4@T�>\'���n3�dȐ!ү_?�v�\'OfH��\0\0����KC밄�Ő��&��/յgC�ǧ���o�������I` 0\0\0�(0�����)?�M�_�=������_ː�ʿ]:P�?ηU\0\0����ƞ���\n����0�/�j�M�\0 NE`\0\0��@`\0\0��@`\0l�fM7k�Y̪���:���֬Y��\0��@`@��7��`��5��\0����ؗaVi+B���n�\0^J\0b�x��y5�;@L1+ˬT��)fe��Y��\'���\0bO��A���qDo����0��\0\r5��ӳ�n��۽	m����\n\r�4\0\Zj��� ��!t�Aj����2���D&/5@`��^e^=�!�n�WO�v^j��@��s�B��,��PC\0��\"0 �{�����e\0@CM`@���-�&����c�o�F=z�}�eo@`����M�48d���t��և���v=�,���@Y���R=�F� ��g|;�\0���6w���.�\n���t=��=\0�����{���`�Y�.�0Q�+���س�/��o��t�$�����z����\n��Z���0?��c���\0�	_�E�ڵsP�AO��K2f�p���E�C֐�k��m���z9~�o2mڸ��wKᇢZY�b\0Z��\'��O.���dd��>��u~�I�\rl}ڟ4i����\'�k����ر�:��G��;z�̺^��䒯X��y����_b�?y�E�8q�հ��&Lȱ.��jj�Ia�$9�{��F���)A*+W5^��y�Àv�a�m4|gD�Ѱ�\"=\0\"?0l���l���uZ������o���d߾>T\rzYjj��Ǻ�Q�����~j\0	�y�?��qxB�k@����[���2��[���/]zO�\r���i|��o����	��`����Z?��ym��o�u�b���X�mn{ޟ��|k�<��3T�lK��&����k�1/+�S��Č?&0��C�Ѱ�c���p����\ZZ�v7��u:4��0�Aw\Z�Ç7��v�z��[o=e����[�=��mН�\'pF�z����c�^m�s(-�c���W�IC��\ZY��A�E�uÇ�ο�����M\ZU���j��y\Z6���#/X��9z~Μ�m���=z�3�B�#�w��z�^WP���;6�ڣ��S_�����{?o!(�`A`@+��#���i�=Y��k�mMht7�˗ϵ��9�k�L:�I�z��ڵ�m�X����V��\r�N$��.�e��Cg���3pMrd�n���Ɇ\r�4^�����mi�i���)S������P�����\\y�Z�A/�~�t\0�K@���\\[������m�\0\"708��i�	��}��C,�P�J�ޅD^n��@�2�:�հ\Z4,��;t͇L^j��@���G�WOC[�\'��z�\0@` 0 �x�=�!{x��<�mh��K�������i��j�5\0����8�:�i�g��t�ig���\0����ؔa4�Ж�n0g\0�����0��V�U�h\0⌮\0y�Ѱ��!�_�6�O�\")��e��#\0�\0\0�\0\0�\0\0E`\0\0(\0��@\0\0��\0\0\0\r5�\0\0�\0\0�\0\0�\0\0E`\0\0(\0��@\0\0��\0\0��s����4�P����k�%�J\0@�4�ߑ��E4�P����y�-��^	\0�8#F\\�f��i4�Pyy#�4ߒ\"�J\0@�IO�9,==�ǳ�F�c�PBB�[�[��^	\0�H}���S\\<�F�+7w�b�(co\0D����NWT,���*/_��|���bW\0D�Qii=>&4�XHHH8h����\0��	\rII~����O0�!�s�a�*�\0 \Zev�t���ݻ�:��ߴ��*�i�:z�\raOp�9C\0\0���暵ݬ:�aB*��E�t�=t��!\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��!�]�]�\0\0\0\0IEND�B`�');
/*!40000 ALTER TABLE `ACT_GE_BYTEARRAY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_GE_PROPERTY`
--

DROP TABLE IF EXISTS `ACT_GE_PROPERTY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_GE_PROPERTY` (
  `NAME_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `VALUE_` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `REV_` int(11) DEFAULT NULL,
  PRIMARY KEY (`NAME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_GE_PROPERTY`
--

LOCK TABLES `ACT_GE_PROPERTY` WRITE;
/*!40000 ALTER TABLE `ACT_GE_PROPERTY` DISABLE KEYS */;
INSERT INTO `ACT_GE_PROPERTY` VALUES ('historyLevel','2',1),('next.dbid','301',4),('schema.history','create(5.5)',1),('schema.version','5.5',1);
/*!40000 ALTER TABLE `ACT_GE_PROPERTY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_ACTINST`
--

DROP TABLE IF EXISTS `ACT_HI_ACTINST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_ACTINST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8_bin NOT NULL,
  `ACT_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ACT_TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime NOT NULL,
  `END_TIME_` datetime DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_ACT_INST_START` (`START_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_END` (`END_TIME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_ACTINST`
--

LOCK TABLES `ACT_HI_ACTINST` WRITE;
/*!40000 ALTER TABLE `ACT_HI_ACTINST` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_ACTINST` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_ATTACHMENT`
--

DROP TABLE IF EXISTS `ACT_HI_ATTACHMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_ATTACHMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `URL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CONTENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_ATTACHMENT`
--

LOCK TABLES `ACT_HI_ATTACHMENT` WRITE;
/*!40000 ALTER TABLE `ACT_HI_ATTACHMENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_ATTACHMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_COMMENT`
--

DROP TABLE IF EXISTS `ACT_HI_COMMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_COMMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TIME_` datetime NOT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACTION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MESSAGE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `FULL_MSG_` longblob,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_COMMENT`
--

LOCK TABLES `ACT_HI_COMMENT` WRITE;
/*!40000 ALTER TABLE `ACT_HI_COMMENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_COMMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_DETAIL`
--

DROP TABLE IF EXISTS `ACT_HI_DETAIL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_DETAIL` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACT_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TIME_` datetime NOT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_DETAIL_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_ACT_INST` (`ACT_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_TIME` (`TIME_`),
  KEY `ACT_IDX_HI_DETAIL_NAME` (`NAME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_DETAIL`
--

LOCK TABLES `ACT_HI_DETAIL` WRITE;
/*!40000 ALTER TABLE `ACT_HI_DETAIL` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_DETAIL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_PROCINST`
--

DROP TABLE IF EXISTS `ACT_HI_PROCINST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_PROCINST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `START_TIME_` datetime NOT NULL,
  `END_TIME_` datetime DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  `START_USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `START_ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `END_ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `PROC_INST_ID_` (`PROC_INST_ID_`),
  UNIQUE KEY `ACT_UNIQ_HI_BUS_KEY` (`PROC_DEF_ID_`,`BUSINESS_KEY_`),
  KEY `ACT_IDX_HI_PRO_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_PRO_I_BUSKEY` (`BUSINESS_KEY_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_PROCINST`
--

LOCK TABLES `ACT_HI_PROCINST` WRITE;
/*!40000 ALTER TABLE `ACT_HI_PROCINST` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_PROCINST` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_HI_TASKINST`
--

DROP TABLE IF EXISTS `ACT_HI_TASKINST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_HI_TASKINST` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `OWNER_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime NOT NULL,
  `END_TIME_` datetime DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  `DELETE_REASON_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PRIORITY_` int(11) DEFAULT NULL,
  `DUE_DATE_` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_HI_TASKINST`
--

LOCK TABLES `ACT_HI_TASKINST` WRITE;
/*!40000 ALTER TABLE `ACT_HI_TASKINST` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_HI_TASKINST` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_ID_GROUP`
--

DROP TABLE IF EXISTS `ACT_ID_GROUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_ID_GROUP` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_ID_GROUP`
--

LOCK TABLES `ACT_ID_GROUP` WRITE;
/*!40000 ALTER TABLE `ACT_ID_GROUP` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_ID_GROUP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_ID_INFO`
--

DROP TABLE IF EXISTS `ACT_ID_INFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_ID_INFO` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `USER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `VALUE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PASSWORD_` longblob,
  `PARENT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_ID_INFO`
--

LOCK TABLES `ACT_ID_INFO` WRITE;
/*!40000 ALTER TABLE `ACT_ID_INFO` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_ID_INFO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_ID_MEMBERSHIP`
--

DROP TABLE IF EXISTS `ACT_ID_MEMBERSHIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_ID_MEMBERSHIP` (
  `USER_ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `GROUP_ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`USER_ID_`,`GROUP_ID_`),
  KEY `ACT_FK_MEMB_GROUP` (`GROUP_ID_`),
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `ACT_ID_GROUP` (`ID_`),
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `ACT_ID_USER` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_ID_MEMBERSHIP`
--

LOCK TABLES `ACT_ID_MEMBERSHIP` WRITE;
/*!40000 ALTER TABLE `ACT_ID_MEMBERSHIP` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_ID_MEMBERSHIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_ID_USER`
--

DROP TABLE IF EXISTS `ACT_ID_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_ID_USER` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `FIRST_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LAST_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PWD_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PICTURE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_ID_USER`
--

LOCK TABLES `ACT_ID_USER` WRITE;
/*!40000 ALTER TABLE `ACT_ID_USER` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_ID_USER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RE_DEPLOYMENT`
--

DROP TABLE IF EXISTS `ACT_RE_DEPLOYMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RE_DEPLOYMENT` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DEPLOY_TIME_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RE_DEPLOYMENT`
--

LOCK TABLES `ACT_RE_DEPLOYMENT` WRITE;
/*!40000 ALTER TABLE `ACT_RE_DEPLOYMENT` DISABLE KEYS */;
INSERT INTO `ACT_RE_DEPLOYMENT` VALUES ('1','SpringAutoDeployment','2011-06-02 01:37:48'),('101','SpringAutoDeployment','2011-06-02 01:37:49'),('117',NULL,'2011-06-02 01:37:50'),('201',NULL,'2011-07-06 02:37:20');
/*!40000 ALTER TABLE `ACT_RE_DEPLOYMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RE_PROCDEF`
--

DROP TABLE IF EXISTS `ACT_RE_PROCDEF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RE_PROCDEF` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `VERSION_` int(11) DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RE_PROCDEF`
--

LOCK TABLES `ACT_RE_PROCDEF` WRITE;
/*!40000 ALTER TABLE `ACT_RE_PROCDEF` DISABLE KEYS */;
INSERT INTO `ACT_RE_PROCDEF` VALUES ('sendForProofingCompliance:1:204','http://www.activiti.org/test','sendForProofingCompliance','sendForProofingCompliance',1,'201','Proofing-Compliance.bpmn20.xml','Proofing-Compliance.sendForProofingCompliance.png',0),('sid-3806d577-ddd6-479c-85f6-11805b79addd:1:14','http://www.signavio.com/bpmn20','TestWorkflow','sid-3806d577-ddd6-479c-85f6-11805b79addd',1,'1','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/Vault-Test-Workflow.bpmn20.xml','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/Vault-Test-Workflow.sid-3806d577-ddd6-479c-85f6-11805b79addd.png',0),('sid-3806d577-ddd6-479c-85f6-11805b79addd:2:114','http://www.signavio.com/bpmn20','TestWorkflow','sid-3806d577-ddd6-479c-85f6-11805b79addd',2,'101','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/Vault-Test-Workflow.bpmn20.xml','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/Vault-Test-Workflow.sid-3806d577-ddd6-479c-85f6-11805b79addd.png',0),('sid-3806d577-ddd6-479c-85f6-11805b79addd:3:132','http://www.signavio.com/bpmn20','TestWorkflow','sid-3806d577-ddd6-479c-85f6-11805b79addd',3,'117','Vault-Test-Workflow.bpmn20.xml','Vault-Test-Workflow.sid-3806d577-ddd6-479c-85f6-11805b79addd.png',0),('sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa:1:15','http://www.signavio.com/bpmn20','PrepareForReview','sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa',1,'1','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/PrepareForReview.bpmn20.xml','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/PrepareForReview.sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa.png',0),('sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa:2:115','http://www.signavio.com/bpmn20','PrepareForReview','sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa',2,'101','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/PrepareForReview.bpmn20.xml','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/test/PrepareForReview.sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa.png',0),('sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa:3:128','http://www.signavio.com/bpmn20','PrepareForReview','sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa',3,'117','PrepareForReview.bpmn20.xml','PrepareForReview.sid-fca9e36e-d8b9-485d-9221-dfe69c2bd3aa.png',0),('startMlrApproval:1:12','http://www.signavio.com/bpmn20','MLR Approval','startMlrApproval',1,'1','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Approval Workflow - V2.bpmn20.xml','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Approval Workflow - V2.startMlrApproval.png',0),('startMlrApproval:2:112','http://www.signavio.com/bpmn20','MLR Approval','startMlrApproval',2,'101','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Approval Workflow - V2.bpmn20.xml','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Approval Workflow - V2.startMlrApproval.png',0),('startMlrApproval:3:130','http://www.signavio.com/bpmn20','MLR Approval','startMlrApproval',3,'117','Approval Workflow - V2.bpmn20.xml','Approval Workflow - V2.startMlrApproval.png',0),('startMlrReview:1:16','http://www.signavio.com/bpmn20','MLR Review','startMlrReview',1,'1','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/MLR Review - V2.bpmn20.xml','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/MLR Review - V2.startMlrReview.png',0),('startMlrReview:2:116','http://www.signavio.com/bpmn20','MLR Review','startMlrReview',2,'101','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/MLR Review - V2.bpmn20.xml','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/MLR Review - V2.startMlrReview.png',0),('startMlrReview:3:129','http://www.signavio.com/bpmn20','MLR Review','startMlrReview',3,'117','MLR Review - V2.bpmn20.xml','MLR Review - V2.startMlrReview.png',0),('submitForMlr:1:13','http://www.signavio.com/bpmn20','Submit for MLR','submitForMlr',1,'1','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Submit for QC - V2.bpmn20.xml','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Submit for QC - V2.submitForMlr.png',0),('submitForMlr:2:113','http://www.signavio.com/bpmn20','Submit for MLR','submitForMlr',2,'101','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Submit for QC - V2.bpmn20.xml','/usr/local/apache-tomcat/6.0.32/webapps/wf/WEB-INF/classes/workflow/veeva/vault/promomats/Submit for QC - V2.submitForMlr.png',0),('submitForMlr:3:131','http://www.signavio.com/bpmn20','Submit for MLR','submitForMlr',3,'117','Submit for QC - V2.bpmn20.xml','Submit for QC - V2.submitForMlr.png',0);
/*!40000 ALTER TABLE `ACT_RE_PROCDEF` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RU_EXECUTION`
--

DROP TABLE IF EXISTS `ACT_RU_EXECUTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RU_EXECUTION` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BUSINESS_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `SUPER_EXEC_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IS_ACTIVE_` tinyint(4) DEFAULT NULL,
  `IS_CONCURRENT_` tinyint(4) DEFAULT NULL,
  `IS_SCOPE_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_UNIQ_RU_BUS_KEY` (`PROC_DEF_ID_`,`BUSINESS_KEY_`),
  KEY `ACT_IDX_EXEC_BUSKEY` (`BUSINESS_KEY_`),
  KEY `ACT_FK_EXE_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_EXE_PARENT` (`PARENT_ID_`),
  KEY `ACT_FK_EXE_SUPER` (`SUPER_EXEC_`),
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`),
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RU_EXECUTION`
--

LOCK TABLES `ACT_RU_EXECUTION` WRITE;
/*!40000 ALTER TABLE `ACT_RU_EXECUTION` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RU_EXECUTION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RU_IDENTITYLINK`
--

DROP TABLE IF EXISTS `ACT_RU_IDENTITYLINK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RU_IDENTITYLINK` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `GROUP_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_IDENT_LNK_USER` (`USER_ID_`),
  KEY `ACT_IDX_IDENT_LNK_GROUP` (`GROUP_ID_`),
  KEY `ACT_FK_TSKASS_TASK` (`TASK_ID_`),
  CONSTRAINT `ACT_FK_TSKASS_TASK` FOREIGN KEY (`TASK_ID_`) REFERENCES `ACT_RU_TASK` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RU_IDENTITYLINK`
--

LOCK TABLES `ACT_RU_IDENTITYLINK` WRITE;
/*!40000 ALTER TABLE `ACT_RU_IDENTITYLINK` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RU_IDENTITYLINK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RU_JOB`
--

DROP TABLE IF EXISTS `ACT_RU_JOB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RU_JOB` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LOCK_OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RETRIES_` int(11) DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_JOB_EXCEPTION` (`EXCEPTION_STACK_ID_`),
  CONSTRAINT `ACT_FK_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RU_JOB`
--

LOCK TABLES `ACT_RU_JOB` WRITE;
/*!40000 ALTER TABLE `ACT_RU_JOB` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RU_JOB` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RU_TASK`
--

DROP TABLE IF EXISTS `ACT_RU_TASK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RU_TASK` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `REV_` int(11) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `OWNER_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DELEGATION_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PRIORITY_` int(11) DEFAULT NULL,
  `CREATE_TIME_` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DUE_DATE_` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_TASK_CREATE` (`CREATE_TIME_`),
  KEY `ACT_FK_TASK_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_TASK_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_TASK_PROCDEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `ACT_RE_PROCDEF` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RU_TASK`
--

LOCK TABLES `ACT_RU_TASK` WRITE;
/*!40000 ALTER TABLE `ACT_RU_TASK` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RU_TASK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACT_RU_VARIABLE`
--

DROP TABLE IF EXISTS `ACT_RU_VARIABLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACT_RU_VARIABLE` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_VAR_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_VAR_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_VAR_BYTEARRAY` (`BYTEARRAY_ID_`),
  CONSTRAINT `ACT_FK_VAR_BYTEARRAY` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `ACT_GE_BYTEARRAY` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `ACT_RU_EXECUTION` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACT_RU_VARIABLE`
--

LOCK TABLES `ACT_RU_VARIABLE` WRITE;
/*!40000 ALTER TABLE `ACT_RU_VARIABLE` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACT_RU_VARIABLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_template`
--

DROP TABLE IF EXISTS `email_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `version` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `master_template` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_template`
--

LOCK TABLES `email_template` WRITE;
/*!40000 ALTER TABLE `email_template` DISABLE KEYS */;
INSERT INTO `email_template` VALUES (1,1,'2011-06-01 21:37:51',1,1,'2011-06-01 21:37:51','newUser','<html>\r\n    <head>\r\n        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\r\n        \r\n        <title>Veeva Vault Notification</title>\r\n		<style type=\"text/css\">\r\n			/* Client-specific Styles */\r\n			#outlook a{padding:0;} /* Force Outlook to provide a \"view in browser\" button. */\r\n			body{width:100% !important;} /* Force Hotmail to display emails at full width */\r\n			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */\r\n			\r\n			/* Reset Styles */\r\n			body{margin:0; padding:0;}\r\n			img{border:none; font-size:14px; font-weight:bold; height:auto; line-height:100%; outline:none; text-decoration:none; text-transform:capitalize;}\r\n			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}\r\n			\r\n			body, #backgroundTable{\r\n				background-color:#b0c9da;\r\n			}\r\n\r\n\r\n			#templatePreheader{\r\n				background-color:#b0c9da;\r\n			}\r\n\r\n			.preheaderContent div{\r\n				color:#505050;\r\n				font-family:Arial;\r\n				font-size:10px;\r\n				line-height:100%;\r\n				text-align:left;\r\n			}\r\n			.preheaderContent div a:link, .preheaderContent div a:visited{\r\n				color:#336699;\r\n				font-weight:normal;\r\n				text-decoration:underline;\r\n			}\r\n\r\n			#templateHeader{\r\n				background-color:#B0C9DA;\r\n				border-bottom:0;\r\n			}\r\n\r\n			.headerContent{\r\n				color:#202020;\r\n				font-family:Arial;\r\n				font-size:34px;\r\n				font-weight:bold;\r\n				line-height:100%;\r\n				padding:0;\r\n				text-align:center;\r\n				vertical-align:middle;\r\n			}\r\n\r\n\r\n			.headerContent a:link, .headerContent a:visited{\r\n				color:#336699;\r\n				font-weight:normal;\r\n				text-decoration:underline;\r\n			}\r\n\r\n			#headerImage{\r\n				height:auto;\r\n				max-width:600px;\r\n			}\r\n\r\n			#footerImage{\r\n				height:auto;\r\n				max-width:600px;\r\n			}\r\n\r\n			#templateContainer, .bodyContent{\r\n				background-color:#FDFDFD;\r\n			}\r\n			\r\n			.bodyContent {\r\n                                background-color:#FFFFFF;\r\n                        }\r\n\r\n			.bodyContent div{\r\n				color:#505050;\r\n				font-family:Arial;\r\n				font-size:14px;\r\n				line-height:150%;\r\n				text-align:left;\r\n			}\r\n			\r\n			.bodyContent div a:link, .bodyContent div a:visited{\r\n				color:#336699;\r\n				font-weight:normal;\r\n				text-decoration:underline;\r\n			}\r\n			\r\n			.bodyContent img{\r\n				display:inline;\r\n				height:auto;\r\n			}\r\n						\r\n			#templateFooter{\r\n				background-color:#FDFDFD;\r\n				border-top:0;\r\n			}\r\n			\r\n			.footerContent div{\r\n				color:#707070;\r\n				font-family:Arial;\r\n				font-size:12px;\r\n				line-height:125%;\r\n				text-align:left;\r\n			}\r\n			\r\n			.footerContent div a:link, .footerContent div a:visited{\r\n				color:#336699;\r\n				font-weight:normal;\r\n				text-decoration:underline;\r\n			}\r\n			\r\n			.footerContent img{\r\n				display:inline;\r\n			}\r\n\r\n		</style>\r\n	</head>\r\n    <body leftmargin=\"0\" marginwidth=\"0\" topmargin=\"0\" marginheight=\"0\" offset=\"0\">\r\n    	<center>\r\n        	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" id=\"backgroundTable\" style=\"margin: 20;background-color: #b0c9da;\">\r\n            	<tr>\r\n                	<td align=\"center\" valign=\"top\">\r\n                        <!-- // Begin Template Preheader \\\\ -->\r\n                        <table border=\"0\" cellpadding=\"10\" cellspacing=\"0\" width=\"600\" height=\"20\" id=\"templatePreheader\">\r\n                        </table>\r\n                        <!-- // End Template Preheader \\\\ -->\r\n                    	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" id=\"templateContainer\">\r\n                        	<tr>\r\n                            	<td align=\"center\" valign=\"top\">\r\n                                    <!-- // Begin Template Header \\\\ -->\r\n                                	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" height=\"130\" id=\"templateHeader\">\r\n                                        <tr>\r\n                                            <td class=\"headerContent\" style=\"background-color:#FFFFFF\">\r\n                                            \r\n                                                <div id=\"login-link\">\r\n                                                    <a href=\"http://veevavault.com\"></a>\r\n                                                </div>\r\n                                            	<!-- // Begin Module: Standard Header Image \\\\ -->\r\n                                            	<img src=\"${staticContentBaseUrl}/images/mail_header.png\" style=\"max-width:600px;\" id=\"headerImage campaign-icon\" mc:label=\"header_image\" mc:edit=\"header_image\" mc:allowdesigner mc:allowtext />\r\n                                            	<!-- // End Module: Standard Header Image \\\\ -->\r\n                                            \r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    <!-- // End Template Header \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        	<tr>\r\n                            	<td valign=\"top\">\r\n                                    <!-- // Begin Template Body \\\\ -->\r\n                                	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" id=\"templateBody\">\r\n                                    	<tr>\r\n                                        	<td valign=\"top\">\r\n                                                 <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n                                                    <tr>\r\n                                                        <td valign=\"top\" class=\"bodyContent\">\r\n                                            \r\n                                                            <!-- // Begin Module: Standard Content \\\\ -->\r\n                                                            <table border=\"0\" cellpadding=\"20\" cellspacing=\"0\" width=\"100%\" style=\"background-color:#ffffff\">\r\n                                                                <tr>\r\n                                                                    <td valign=\"top\">\r\n			                                                            <div mc:edit=\"std_content00\">\r\nDear ${firstName} ${lastName}\r\n\r\n<p>Welcome to Veeva Vault!  Your user name and temporary password are given below:</p>\r\n\r\n<p>\r\nUser Name: <b>${userName}</b><br>\r\nPassword: <b>${userPassword}</b>\r\n</p>\r\n\r\n<#setting url_escaping_charset=\'utf-8\'>\r\n<a href=\"${uiServiceBaseExtUrl}/changepassword?userName=${userName?url}&userPassword=${userPassword?url}\">Click here to change your password</a>\r\n\r\n<p>If you experience problems with the above link, please contact your administrator for assistance.</p>\r\n\r\n<p>You\'ll be asked to change your password when you first log in.  Note that passwords are case-sensitive.\r\nMake sure that you choose a password that you can remember, but complex enough not to be guessed by others.\r\nIf you experience problems, contact your system administrator to reset the password.</p>\r\n\r\nOnce again, welcome to Veeva Vault!\r\n							                                            </div>\r\n                                                                    </td>\r\n                                                                </tr>\r\n                                                            </table>\r\n                                                            <!-- // End Module: Standard Content \\\\ -->\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                </table>\r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                    <!-- // End Template Body \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        	<tr>\r\n                            	<td align=\"center\" valign=\"top\">\r\n		                          <!-- // Begin Footer Image \\\\ -->\r\n                            	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" height=\"34\" id=\"templateHeader\">\r\n                                    <tr>\r\n                                        <td class=\"footerContent\">\r\n                                        	<img src=\"${staticContentBaseUrl}/images/mail_footer.png\" style=\"max-width:600px;\" id=\"footerImage campaign-icon\" mc:label=\"header_image\" mc:edit=\"footer_image\" mc:allowdesigner mc:allowtext />\r\n                                        </td>\r\n                                    </tr>\r\n                                </table>\r\n                                <!-- // End Footer Image \\\\ -->\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                        <br />\r\n                    </td>\r\n                </tr>\r\n            </table>\r\n        </center>\r\n    </body>\r\n</html>\r\n\r\n\r\n',0,'Welcome to Veeva Vault!','\0'),(2,1,'2011-06-01 21:37:51',1,1,'2011-06-01 21:37:51','resetPassword','<html>\n    <head>\n        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n        \n        <title>Veeva Vault Notification</title>\n		<style type=\"text/css\">\n			/* Client-specific Styles */\n			#outlook a{padding:0;} /* Force Outlook to provide a \"view in browser\" button. */\n			body{width:100% !important;} /* Force Hotmail to display emails at full width */\n			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */\n			\n			/* Reset Styles */\n			body{margin:0; padding:0;}\n			img{border:none; font-size:14px; font-weight:bold; height:auto; line-height:100%; outline:none; text-decoration:none; text-transform:capitalize;}\n			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}\n			\n			body, #backgroundTable{\n				background-color:#b0c9da;\n			}\n\n\n			#templatePreheader{\n				background-color:#b0c9da;\n			}\n\n			.preheaderContent div{\n				color:#505050;\n				font-family:Arial;\n				font-size:10px;\n				line-height:100%;\n				text-align:left;\n			}\n			.preheaderContent div a:link, .preheaderContent div a:visited{\n				color:#336699;\n				font-weight:normal;\n				text-decoration:underline;\n			}\n\n			#templateHeader{\n				background-color:#B0C9DA;\n				border-bottom:0;\n			}\n\n			.headerContent{\n				color:#202020;\n				font-family:Arial;\n				font-size:34px;\n				font-weight:bold;\n				line-height:100%;\n				padding:0;\n				text-align:center;\n				vertical-align:middle;\n			}\n\n\n			.headerContent a:link, .headerContent a:visited{\n				color:#336699;\n				font-weight:normal;\n				text-decoration:underline;\n			}\n\n			#headerImage{\n				height:auto;\n				max-width:600px;\n			}\n\n			#footerImage{\n				height:auto;\n				max-width:600px;\n			}\n\n			#templateContainer, .bodyContent{\n				background-color:#FDFDFD;\n			}\n			\n			.bodyContent {\n                                background-color:#FFFFFF;\n                        }\n\n			.bodyContent div{\n				color:#505050;\n				font-family:Arial;\n				font-size:14px;\n				line-height:150%;\n				text-align:left;\n			}\n			\n			.bodyContent div a:link, .bodyContent div a:visited{\n				color:#336699;\n				font-weight:normal;\n				text-decoration:underline;\n			}\n			\n			.bodyContent img{\n				display:inline;\n				height:auto;\n			}\n						\n			#templateFooter{\n				background-color:#FDFDFD;\n				border-top:0;\n			}\n			\n			.footerContent div{\n				color:#707070;\n				font-family:Arial;\n				font-size:12px;\n				line-height:125%;\n				text-align:left;\n			}\n			\n			.footerContent div a:link, .footerContent div a:visited{\n				color:#336699;\n				font-weight:normal;\n				text-decoration:underline;\n			}\n			\n			.footerContent img{\n				display:inline;\n			}\n\n		</style>\n	</head>\n    <body leftmargin=\"0\" marginwidth=\"0\" topmargin=\"0\" marginheight=\"0\" offset=\"0\">\n    	<center>\n        	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" id=\"backgroundTable\" style=\"margin: 20;background-color: #b0c9da;\">\n            	<tr>\n                	<td align=\"center\" valign=\"top\">\n                        <!-- // Begin Template Preheader \\\\ -->\n                        <table border=\"0\" cellpadding=\"10\" cellspacing=\"0\" width=\"600\" height=\"20\" id=\"templatePreheader\">\n                        </table>\n                        <!-- // End Template Preheader \\\\ -->\n                    	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" id=\"templateContainer\">\n                        	<tr>\n                            	<td align=\"center\" valign=\"top\">\n                                    <!-- // Begin Template Header \\\\ -->\n                                	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" height=\"130\" id=\"templateHeader\">\n                                        <tr>\n                                            <td class=\"headerContent\" style=\"background-color:#FFFFFF\">\n                                            \n                                                <div id=\"login-link\">\n                                                    <a href=\"http://veevavault.com\"></a>\n                                                </div>\n                                            	<!-- // Begin Module: Standard Header Image \\\\ -->\n                                            	<img src=\"${staticContentBaseUrl}/images/mail_header.png\" style=\"max-width:600px;\" id=\"headerImage campaign-icon\" mc:label=\"header_image\" mc:edit=\"header_image\" mc:allowdesigner mc:allowtext />\n                                            	<!-- // End Module: Standard Header Image \\\\ -->\n                                            \n                                            </td>\n                                        </tr>\n                                    </table>\n                                    <!-- // End Template Header \\\\ -->\n                                </td>\n                            </tr>\n                        	<tr>\n                            	<td valign=\"top\">\n                                    <!-- // Begin Template Body \\\\ -->\n                                	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" id=\"templateBody\">\n                                    	<tr>\n                                        	<td valign=\"top\">\n                                                 <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n                                                    <tr>\n                                                        <td valign=\"top\" class=\"bodyContent\">\n                                            \n                                                            <!-- // Begin Module: Standard Content \\\\ -->\n                                                            <table border=\"0\" cellpadding=\"20\" cellspacing=\"0\" width=\"100%\" style=\"background-color:#ffffff\">\n                                                                <tr>\n                                                                    <td valign=\"top\">\n			                                                            <div mc:edit=\"std_content00\">\nA password reset has been requested for the Veeva Vault account associated with this email address. If you did \nnot request this reset contact your system administrator.\n\n<p>Your user name and temporary password are given below:</p>\n\n<p>\nUser Name: <b>${userName}</b><br>\nPassword: <b>${userPassword}</b>\n</p>\n\n<p><#setting url_escaping_charset=\'utf-8\'>\n<a href=\"${uiServiceBaseExtUrl}/changepassword?userName=${userName?url}&userPassword=${userPassword?url}\">Click here to log in</a></p>\n\n<p>If you experience problems with the above link, please contact your veevasystems.com administrator for assistance.</p>\n\nYou\'ll be asked to change your password when you first log in.  Note that passwords are case-sensitive.\nMake sure that you choose a password that you can remember, but complex enough not to be guessed by others.\n							                                            </div>\n                                                                    </td>\n                                                                </tr>\n                                                            </table>\n                                                            <!-- // End Module: Standard Content \\\\ -->\n                                                        </td>\n                                                    </tr>\n                                                </table>\n                                            </td>\n                                        </tr>\n                                    </table>\n                                    <!-- // End Template Body \\\\ -->\n                                </td>\n                            </tr>\n                        	<tr>\n                            	<td align=\"center\" valign=\"top\">\n		                          <!-- // Begin Footer Image \\\\ -->\n                            	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" height=\"34\" id=\"templateHeader\">\n                                    <tr>\n                                        <td class=\"footerContent\">\n                                        	<img src=\"${staticContentBaseUrl}/images/mail_footer.png\" style=\"max-width:600px;\" id=\"footerImage campaign-icon\" mc:label=\"header_image\" mc:edit=\"footer_image\" mc:allowdesigner mc:allowtext />\n                                        </td>\n                                    </tr>\n                                </table>\n                                <!-- // End Footer Image \\\\ -->\n                                </td>\n                            </tr>\n                        </table>\n                        <br />\n                    </td>\n                </tr>\n            </table>\n        </center>\n    </body>\n</html>\n\n\n',0,'Your Veeva Vault password has been reset','\0');
/*!40000 ALTER TABLE `email_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_access_token`
--

DROP TABLE IF EXISTS `veeva_access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_token` varchar(255) COLLATE utf8_bin NOT NULL,
  `access_identifier` varchar(255) COLLATE utf8_bin NOT NULL,
  `contact_user_id` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `status` varchar(255) COLLATE utf8_bin NOT NULL,
  `target_user_id` bigint(20) NOT NULL,
  `target_user_email` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `allow_rendition_download` bit(1) DEFAULT b'0',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_access_token`
--

LOCK TABLES `veeva_access_token` WRITE;
/*!40000 ALTER TABLE `veeva_access_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_access_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_admin_audit`
--

DROP TABLE IF EXISTS `veeva_admin_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_admin_audit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `event_time` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `new_value` varchar(4000) DEFAULT NULL,
  `object_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `object_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `old_value` varchar(4000) DEFAULT NULL,
  `property` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `vmc_user_on_behalf_of` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_admin_audit`
--

LOCK TABLES `veeva_admin_audit` WRITE;
/*!40000 ALTER TABLE `veeva_admin_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_admin_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_auto_number`
--

DROP TABLE IF EXISTS `veeva_auto_number`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_auto_number` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `pattern` varchar(100) COLLATE utf8_bin NOT NULL,
  `last_used_value` int(11) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `search_pattern` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `search_pattern` (`instance_id`,`search_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_auto_number`
--

LOCK TABLES `veeva_auto_number` WRITE;
/*!40000 ALTER TABLE `veeva_auto_number` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_auto_number` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_auto_number_format`
--

DROP TABLE IF EXISTS `veeva_auto_number_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_auto_number_format` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `pattern` varchar(100) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `format_key` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `format_key` (`instance_id`,`format_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_auto_number_format`
--

LOCK TABLES `veeva_auto_number_format` WRITE;
/*!40000 ALTER TABLE `veeva_auto_number_format` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_auto_number_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_catalog`
--

DROP TABLE IF EXISTS `veeva_catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_catalog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `catalog_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `public_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `active` bit(1) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `version` int(11) DEFAULT NULL,
  `system` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `catalog_key` (`instance_id`,`catalog_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_catalog`
--

LOCK TABLES `veeva_catalog` WRITE;
/*!40000 ALTER TABLE `veeva_catalog` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_catalog_value`
--

DROP TABLE IF EXISTS `veeva_catalog_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_catalog_value` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `catalog_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `catalog_value_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `active` bit(1) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `version` int(11) DEFAULT NULL,
  `public_key` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `catalog_value_key` (`instance_id`,`catalog_key`,`catalog_value_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_catalog_value`
--

LOCK TABLES `veeva_catalog_value` WRITE;
/*!40000 ALTER TABLE `veeva_catalog_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_catalog_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_compound_document_node`
--

DROP TABLE IF EXISTS `veeva_compound_document_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_compound_document_node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `node_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `node_type` varchar(25) COLLATE utf8_bin NOT NULL,
  `parent_node_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `position_in_parent` int(11) NOT NULL,
  `section_number` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `section_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `referenced_document_id` bigint(20) DEFAULT NULL,
  `referenced_major_version` int(11) DEFAULT NULL,
  `referenced_minor_version` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `node_key` (`instance_id`,`document_id`,`major_version`,`minor_version`,`node_key`),
  KEY `doc_id` (`instance_id`,`document_id`,`major_version`,`minor_version`),
  KEY `ref_doc_id` (`instance_id`,`referenced_document_id`),
  KEY `parent_node` (`instance_id`,`document_id`,`major_version`,`minor_version`,`parent_node_key`,`position_in_parent`),
  KEY `ref_doc_version` (`instance_id`,`referenced_document_id`,`referenced_major_version`,`referenced_minor_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_compound_document_node`
--

LOCK TABLES `veeva_compound_document_node` WRITE;
/*!40000 ALTER TABLE `veeva_compound_document_node` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_compound_document_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_compound_document_template`
--

DROP TABLE IF EXISTS `veeva_compound_document_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_compound_document_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `public_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `disabled` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`template_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_compound_document_template`
--

LOCK TABLES `veeva_compound_document_template` WRITE;
/*!40000 ALTER TABLE `veeva_compound_document_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_compound_document_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_compound_document_template_base`
--

DROP TABLE IF EXISTS `veeva_compound_document_template_base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_compound_document_template_base` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `doc_type_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `doc_sub_type_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `doc_classification_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `standard` tinyint(1) DEFAULT '0',
  `disabled` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_key` (`instance_id`,`template_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_compound_document_template_base`
--

LOCK TABLES `veeva_compound_document_template_base` WRITE;
/*!40000 ALTER TABLE `veeva_compound_document_template_base` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_compound_document_template_base` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_compound_document_template_node`
--

DROP TABLE IF EXISTS `veeva_compound_document_template_node`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_compound_document_template_node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `parent_node_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `node_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `node_type` varchar(25) COLLATE utf8_bin NOT NULL,
  `section_number` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `position_in_parent` int(11) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `root` tinyint(1) DEFAULT '0',
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `node_key` (`instance_id`,`template_key`,`node_key`),
  KEY `parent_node` (`instance_id`,`template_key`,`parent_node_key`,`position_in_parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_compound_document_template_node`
--

LOCK TABLES `veeva_compound_document_template_node` WRITE;
/*!40000 ALTER TABLE `veeva_compound_document_template_node` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_compound_document_template_node` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_country`
--

DROP TABLE IF EXISTS `veeva_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `country_key` bigint(20) NOT NULL,
  `active` bit(1) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `country_key` (`instance_id`,`country_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_country`
--

LOCK TABLES `veeva_country` WRITE;
/*!40000 ALTER TABLE `veeva_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_dashboard`
--

DROP TABLE IF EXISTS `veeva_dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_dashboard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `layout_key` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_dashboard`
--

LOCK TABLES `veeva_dashboard` WRITE;
/*!40000 ALTER TABLE `veeva_dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_dashboard_component`
--

DROP TABLE IF EXISTS `veeva_dashboard_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_dashboard_component` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `data_source_key` bigint(20) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_dashboard_component`
--

LOCK TABLES `veeva_dashboard_component` WRITE;
/*!40000 ALTER TABLE `veeva_dashboard_component` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_dashboard_component` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_dashboard_data_source`
--

DROP TABLE IF EXISTS `veeva_dashboard_data_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_dashboard_data_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `report_key` bigint(20) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_dashboard_data_source`
--

LOCK TABLES `veeva_dashboard_data_source` WRITE;
/*!40000 ALTER TABLE `veeva_dashboard_data_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_dashboard_data_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_dashboard_layout`
--

DROP TABLE IF EXISTS `veeva_dashboard_layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_dashboard_layout` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `component_key` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_dashboard_layout`
--

LOCK TABLES `veeva_dashboard_layout` WRITE;
/*!40000 ALTER TABLE `veeva_dashboard_layout` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_dashboard_layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc`
--

DROP TABLE IF EXISTS `veeva_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `currently_checked_out_by` bigint(20) DEFAULT NULL,
  `currently_checked_out_date` datetime DEFAULT NULL,
  `document_number` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `deleted` char(1) NOT NULL DEFAULT 'N',
  `compound_document` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `document_number` (`instance_id`,`document_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc`
--

LOCK TABLES `veeva_doc` WRITE;
/*!40000 ALTER TABLE `veeva_doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_attribute`
--

DROP TABLE IF EXISTS `veeva_doc_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_attribute` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `category_key` varchar(255) DEFAULT NULL,
  `category_type` varchar(255) DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `position_in_section` int(11) NOT NULL,
  `section_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `system_attribute` bit(1) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `public_key` varchar(100) DEFAULT NULL,
  `is_shared` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`attribute_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`),
  KEY `section_key` (`instance_id`,`section_key`,`position_in_section`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_attribute`
--

LOCK TABLES `veeva_doc_attribute` WRITE;
/*!40000 ALTER TABLE `veeva_doc_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_attribute_category_key`
--

DROP TABLE IF EXISTS `veeva_doc_attribute_category_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_attribute_category_key` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `attribute_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `category_key` varchar(1024) COLLATE utf8_bin NOT NULL,
  `category_type` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_attribute_category_key`
--

LOCK TABLES `veeva_doc_attribute_category_key` WRITE;
/*!40000 ALTER TABLE `veeva_doc_attribute_category_key` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_attribute_category_key` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_attribute_dependency_rule`
--

DROP TABLE IF EXISTS `veeva_doc_attribute_dependency_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_attribute_dependency_rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `rule_key` varchar(50) COLLATE utf8_bin NOT NULL,
  `category_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `controlling_attribute_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `controlling_attribute_value` varchar(250) COLLATE utf8_bin NOT NULL,
  `controlled_attribute_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `rule_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `rule_key` (`instance_id`,`rule_key`),
  KEY `controlling_attribute` (`instance_id`,`controlling_attribute_key`),
  KEY `controlled_attribute` (`instance_id`,`controlled_attribute_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_attribute_dependency_rule`
--

LOCK TABLES `veeva_doc_attribute_dependency_rule` WRITE;
/*!40000 ALTER TABLE `veeva_doc_attribute_dependency_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_attribute_dependency_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_audit`
--

DROP TABLE IF EXISTS `veeva_doc_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_audit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `doc_id` bigint(20) NOT NULL,
  `event` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `event_time` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `new_value` varchar(1500) DEFAULT NULL,
  `old_value` varchar(1500) DEFAULT NULL,
  `property` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `signature_meaning` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `task_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `workflow_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `vmc_user_on_behalf_of` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `DOC_ID_PROPERTY_IDX` (`doc_id`,`property`),
  KEY `event` (`instance_id`,`event`,`event_time`),
  KEY `event_time` (`doc_id`,`event`,`event_time`),
  KEY `event_user` (`instance_id`,`user_id`,`event`,`event_time`),
  KEY `event_time2` (`event_time`,`doc_id`,`event`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_audit`
--

LOCK TABLES `veeva_doc_audit` WRITE;
/*!40000 ALTER TABLE `veeva_doc_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_comment`
--

DROP TABLE IF EXISTS `veeva_doc_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `comment_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `reply_to` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `comment_text` longtext COLLATE utf8_bin NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `comment_key` (`instance_id`,`comment_key`),
  KEY `reply_to` (`instance_id`,`reply_to`),
  KEY `document_id` (`instance_id`,`document_id`,`major_version`,`minor_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_comment`
--

LOCK TABLES `veeva_doc_comment` WRITE;
/*!40000 ALTER TABLE `veeva_doc_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_favorites`
--

DROP TABLE IF EXISTS `veeva_doc_favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_favorites` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `document_id` bigint(20) NOT NULL,
  `when_added` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`instance_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_favorites`
--

LOCK TABLES `veeva_doc_favorites` WRITE;
/*!40000 ALTER TABLE `veeva_doc_favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_object_reference`
--

DROP TABLE IF EXISTS `veeva_doc_object_reference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_object_reference` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `referenced_object_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `referenced_object_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `referenced_parent_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `referencing_object_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `referencing_object_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `referencing_parent_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `referenced_object` (`instance_id`,`referenced_object_type`,`referenced_object_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_object_reference`
--

LOCK TABLES `veeva_doc_object_reference` WRITE;
/*!40000 ALTER TABLE `veeva_doc_object_reference` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_object_reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_property_level_security`
--

DROP TABLE IF EXISTS `veeva_doc_property_level_security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_property_level_security` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `rule_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `rule_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `doc_property_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `deleted` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `user_group_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `user_group_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rule_key` (`instance_id`,`rule_key`),
  KEY `doc_property` (`instance_id`,`doc_property_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_property_level_security`
--

LOCK TABLES `veeva_doc_property_level_security` WRITE;
/*!40000 ALTER TABLE `veeva_doc_property_level_security` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_property_level_security` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_recent`
--

DROP TABLE IF EXISTS `veeva_doc_recent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_recent` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `document_id` bigint(20) NOT NULL,
  `action` varchar(100) NOT NULL,
  `when_added` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`instance_id`,`user_id`),
  KEY `document_id` (`instance_id`,`user_id`,`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_recent`
--

LOCK TABLES `veeva_doc_recent` WRITE;
/*!40000 ALTER TABLE `veeva_doc_recent` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_recent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_rel`
--

DROP TABLE IF EXISTS `veeva_doc_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_rel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `relationship_type_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `source_document_id` bigint(20) NOT NULL,
  `target_document_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `source_major_version` int(11) DEFAULT NULL,
  `source_minor_version` int(11) DEFAULT NULL,
  `target_major_version` int(11) DEFAULT NULL,
  `target_minor_version` int(11) DEFAULT NULL,
  `source_third_party_linkage` varchar(255) DEFAULT NULL,
  `target_third_party_linkage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `third_party_linkage` (`target_third_party_linkage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_rel`
--

LOCK TABLES `veeva_doc_rel` WRITE;
/*!40000 ALTER TABLE `veeva_doc_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_rel_type`
--

DROP TABLE IF EXISTS `veeva_doc_rel_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_rel_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `relationship_type_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) DEFAULT NULL,
  `deleted` bit(1) DEFAULT b'0',
  `disabled` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`relationship_type_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_rel_type`
--

LOCK TABLES `veeva_doc_rel_type` WRITE;
/*!40000 ALTER TABLE `veeva_doc_rel_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_rel_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_rendition`
--

DROP TABLE IF EXISTS `veeva_doc_rendition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_rendition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `rendition_type` varchar(200) COLLATE utf8_bin NOT NULL,
  `rendition_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `content_size` bigint(20) DEFAULT NULL,
  `mime_type` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `upload_date` datetime DEFAULT NULL,
  `upload_user` bigint(20) DEFAULT NULL,
  `file_name` varchar(1000) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `document_id` (`instance_id`,`document_id`,`major_version`,`minor_version`),
  KEY `rendition_type` (`instance_id`,`rendition_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_rendition`
--

LOCK TABLES `veeva_doc_rendition` WRITE;
/*!40000 ALTER TABLE `veeva_doc_rendition` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_rendition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_rendition_type`
--

DROP TABLE IF EXISTS `veeva_doc_rendition_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_rendition_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `rendition_type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `deleted` bit(1) DEFAULT b'0',
  `disabled` bit(1) DEFAULT b'0',
  `removable_from_doc` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`rendition_type_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_rendition_type`
--

LOCK TABLES `veeva_doc_rendition_type` WRITE;
/*!40000 ALTER TABLE `veeva_doc_rendition_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_rendition_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_section`
--

DROP TABLE IF EXISTS `veeva_doc_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_section` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `properties_can_be_added` bit(1) NOT NULL,
  `section_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `section_order` int(11) NOT NULL,
  `system_defined` bit(1) NOT NULL,
  `user_modifiable` bit(1) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `country_flag_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`section_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_section`
--

LOCK TABLES `veeva_doc_section` WRITE;
/*!40000 ALTER TABLE `veeva_doc_section` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_status_role_permission`
--

DROP TABLE IF EXISTS `veeva_doc_status_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_status_role_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `document_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `permission` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`instance_id`,`lifecycle`,`document_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_status_role_permission`
--

LOCK TABLES `veeva_doc_status_role_permission` WRITE;
/*!40000 ALTER TABLE `veeva_doc_status_role_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_status_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_template`
--

DROP TABLE IF EXISTS `veeva_doc_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `doc_type_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `doc_sub_type_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `doc_classification_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `template_mime_type` varchar(200) COLLATE utf8_bin NOT NULL,
  `template_file_path` varchar(500) COLLATE utf8_bin NOT NULL,
  `template_file_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `template_size` bigint(20) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `file_upload_date` datetime DEFAULT NULL,
  `file_upload_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_key` (`instance_id`,`template_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_template`
--

LOCK TABLES `veeva_doc_template` WRITE;
/*!40000 ALTER TABLE `veeva_doc_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_type`
--

DROP TABLE IF EXISTS `veeva_doc_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `type_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `public_key` varchar(100) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `system` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`type_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_type`
--

LOCK TABLES `veeva_doc_type` WRITE;
/*!40000 ALTER TABLE `veeva_doc_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_type_filter`
--

DROP TABLE IF EXISTS `veeva_doc_type_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_type_filter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `sub_type_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `classification_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `attribute_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_type` (`instance_id`,`type_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_type_filter`
--

LOCK TABLES `veeva_doc_type_filter` WRITE;
/*!40000 ALTER TABLE `veeva_doc_type_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_type_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_type_lifecycle`
--

DROP TABLE IF EXISTS `veeva_doc_type_lifecycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_type_lifecycle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `sub_type_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `classification_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `lifecycle_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_type` (`instance_id`,`type_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_type_lifecycle`
--

LOCK TABLES `veeva_doc_type_lifecycle` WRITE;
/*!40000 ALTER TABLE `veeva_doc_type_lifecycle` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_type_lifecycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_type_rendition_type`
--

DROP TABLE IF EXISTS `veeva_doc_type_rendition_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_type_rendition_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `sub_type_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `classification_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `rendition_type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_type` (`instance_id`,`type_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_type_rendition_type`
--

LOCK TABLES `veeva_doc_type_rendition_type` WRITE;
/*!40000 ALTER TABLE `veeva_doc_type_rendition_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_type_rendition_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_type_role`
--

DROP TABLE IF EXISTS `veeva_doc_type_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_type_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `type_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `sub_type_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `classification_key` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `role` varchar(20) COLLATE utf8_bin NOT NULL,
  `role_type` varchar(20) COLLATE utf8_bin NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_type` (`instance_id`,`type_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_type_role`
--

LOCK TABLES `veeva_doc_type_role` WRITE;
/*!40000 ALTER TABLE `veeva_doc_type_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_type_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_user_role`
--

DROP TABLE IF EXISTS `veeva_doc_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `instance_user_id` bigint(20) NOT NULL,
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_type` (`instance_id`,`user_type`,`instance_user_id`),
  KEY `document_id` (`instance_id`,`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_user_role`
--

LOCK TABLES `veeva_doc_user_role` WRITE;
/*!40000 ALTER TABLE `veeva_doc_user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_version`
--

DROP TABLE IF EXISTS `veeva_doc_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_version` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `annotate_doc_key` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `classification` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `content_file_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `content_mime_type` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `content_size` bigint(20) DEFAULT NULL,
  `content_thumbnail_file_name` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `description` varchar(1500) DEFAULT NULL,
  `document_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `latest_version_for_doc` bit(1) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title` varchar(1500) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `steady_state` bit(1) NOT NULL DEFAULT b'0',
  `checkout_file_name` varchar(1500) DEFAULT NULL,
  `document_type` varchar(255) DEFAULT NULL,
  `document_sub_type` varchar(255) DEFAULT NULL,
  `deleted` char(1) NOT NULL DEFAULT 'N',
  `lifecycle` varchar(255) DEFAULT NULL,
  `product` varchar(1000) DEFAULT NULL,
  `country` varchar(1000) DEFAULT NULL,
  `study` varchar(1000) DEFAULT NULL,
  `location` varchar(1000) DEFAULT NULL,
  `site` varchar(1000) DEFAULT NULL,
  `owning_facility` varchar(1000) DEFAULT NULL,
  `impacted_facilities` varchar(1000) DEFAULT NULL,
  `owning_department` varchar(1000) DEFAULT NULL,
  `impacted_departments` varchar(1000) DEFAULT NULL,
  `clm_content` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `version_id` (`instance_id`,`document_id`,`major_version`,`minor_version`),
  KEY `doc_id` (`instance_id`,`document_id`),
  KEY `doc_type` (`instance_id`,`document_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_version`
--

LOCK TABLES `veeva_doc_version` WRITE;
/*!40000 ALTER TABLE `veeva_doc_version` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_doc_version_standard_catalog_values`
--

DROP TABLE IF EXISTS `veeva_doc_version_standard_catalog_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_doc_version_standard_catalog_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `major_version` int(11) NOT NULL,
  `minor_version` int(11) NOT NULL,
  `doc_property_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `catalog_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `catalog_value_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `version_id` (`instance_id`,`document_id`,`major_version`,`minor_version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_doc_version_standard_catalog_values`
--

LOCK TABLES `veeva_doc_version_standard_catalog_values` WRITE;
/*!40000 ALTER TABLE `veeva_doc_version_standard_catalog_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_doc_version_standard_catalog_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_file_type_setting`
--

DROP TABLE IF EXISTS `veeva_file_type_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_file_type_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `file_type_setting_key` bigint(20) NOT NULL,
  `checkout_operation` varchar(100) COLLATE utf8_bin NOT NULL,
  `deleted` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `file_type_setting_key` (`instance_id`,`file_type_setting_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_file_type_setting`
--

LOCK TABLES `veeva_file_type_setting` WRITE;
/*!40000 ALTER TABLE `veeva_file_type_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_file_type_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_group`
--

DROP TABLE IF EXISTS `veeva_flow_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `process_groups` (`process_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_group`
--

LOCK TABLES `veeva_flow_group` WRITE;
/*!40000 ALTER TABLE `veeva_flow_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_group_membership`
--

DROP TABLE IF EXISTS `veeva_flow_group_membership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_group_membership` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `group_member_type` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT 'user',
  PRIMARY KEY (`id`),
  KEY `user_groups` (`user_id`),
  KEY `flow_group_membership_idx` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_group_membership`
--

LOCK TABLES `veeva_flow_group_membership` WRITE;
/*!40000 ALTER TABLE `veeva_flow_group_membership` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_group_membership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_process_definition`
--

DROP TABLE IF EXISTS `veeva_flow_process_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_process_definition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_version` int(11) NOT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT 'active',
  `lifecycle_key` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `flow_def_status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_process_definition`
--

LOCK TABLES `veeva_flow_process_definition` WRITE;
/*!40000 ALTER TABLE `veeva_flow_process_definition` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_process_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_process_history`
--

DROP TABLE IF EXISTS `veeva_flow_process_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_process_history` (
  `history_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `process_definition_id` bigint(20) NOT NULL,
  `status` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `document_id` bigint(20) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_process_history`
--

LOCK TABLES `veeva_flow_process_history` WRITE;
/*!40000 ALTER TABLE `veeva_flow_process_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_process_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_process_instance`
--

DROP TABLE IF EXISTS `veeva_flow_process_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_process_instance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `process_definition_id` bigint(20) NOT NULL,
  `status` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `document_id` bigint(20) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `document_id` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_process_instance`
--

LOCK TABLES `veeva_flow_process_instance` WRITE;
/*!40000 ALTER TABLE `veeva_flow_process_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_process_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_task`
--

DROP TABLE IF EXISTS `veeva_flow_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assignee` bigint(20) DEFAULT NULL,
  `assignee_source_group` varchar(255) COLLATE utf8_bin NOT NULL,
  `assign_date` datetime DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `status` varchar(20) COLLATE utf8_bin NOT NULL,
  `task_meta_data_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `process_tasks` (`process_instance_id`),
  KEY `flow_task_assignee_idx` (`assignee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_task`
--

LOCK TABLES `veeva_flow_task` WRITE;
/*!40000 ALTER TABLE `veeva_flow_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_task_history`
--

DROP TABLE IF EXISTS `veeva_flow_task_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_task_history` (
  `history_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assignee` bigint(20) DEFAULT NULL,
  `assignee_source_group` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `status` varchar(20) COLLATE utf8_bin NOT NULL,
  `task_meta_data_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `assign_date` datetime DEFAULT NULL,
  PRIMARY KEY (`history_id`),
  KEY `process_tasks_hist` (`process_instance_id`),
  KEY `flow_task_assignee_hist_idx` (`assignee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_task_history`
--

LOCK TABLES `veeva_flow_task_history` WRITE;
/*!40000 ALTER TABLE `veeva_flow_task_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_task_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_timer`
--

DROP TABLE IF EXISTS `veeva_flow_timer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_timer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `status` varchar(20) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_timer`
--

LOCK TABLES `veeva_flow_timer` WRITE;
/*!40000 ALTER TABLE `veeva_flow_timer` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_timer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_variable`
--

DROP TABLE IF EXISTS `veeva_flow_variable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_variable` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `value` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `value_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `step_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `process_variables` (`process_instance_id`),
  KEY `process_variable_name_value` (`name`,`value`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_variable`
--

LOCK TABLES `veeva_flow_variable` WRITE;
/*!40000 ALTER TABLE `veeva_flow_variable` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_variable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_flow_variable_history`
--

DROP TABLE IF EXISTS `veeva_flow_variable_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_flow_variable_history` (
  `history_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `process_instance_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `value` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `value_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `step_key` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`history_id`),
  KEY `process_var_hist` (`process_instance_id`),
  KEY `process_var_name_value_hist` (`name`,`value`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_flow_variable_history`
--

LOCK TABLES `veeva_flow_variable_history` WRITE;
/*!40000 ALTER TABLE `veeva_flow_variable_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_flow_variable_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_group`
--

DROP TABLE IF EXISTS `veeva_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `group_description` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `group_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `system_group` bit(1) DEFAULT NULL,
  `user_type_id` bigint(20) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `group_key` bigint(20) NOT NULL,
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  `active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_key` (`instance_id`,`group_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_group`
--

LOCK TABLES `veeva_group` WRITE;
/*!40000 ALTER TABLE `veeva_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_group_member`
--

DROP TABLE IF EXISTS `veeva_group_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_group_member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL,
  `group_member_id` bigint(20) NOT NULL,
  `group_member_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_member` (`instance_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_group_member`
--

LOCK TABLES `veeva_group_member` WRITE;
/*!40000 ALTER TABLE `veeva_group_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_group_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_instance_user`
--

DROP TABLE IF EXISTS `veeva_instance_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_instance_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `receive_emails_for_notifications_and_tasks` bit(1) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_type_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `image_file_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`instance_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_instance_user`
--

LOCK TABLES `veeva_instance_user` WRITE;
/*!40000 ALTER TABLE `veeva_instance_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_instance_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_lifecycle`
--

DROP TABLE IF EXISTS `veeva_lifecycle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_lifecycle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `starting_state_key` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `superseded_state_key` varchar(255) DEFAULT NULL,
  `public_key` varchar(100) DEFAULT NULL,
  `steady_state_key` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`lifecycle_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_lifecycle`
--

LOCK TABLES `veeva_lifecycle` WRITE;
/*!40000 ALTER TABLE `veeva_lifecycle` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_lifecycle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_lifecycle_role`
--

DROP TABLE IF EXISTS `veeva_lifecycle_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_lifecycle_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `role_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `system_role` bit(1) NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `description` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`lifecycle_key`,`role_key`),
  UNIQUE KEY `public_key` (`instance_id`,`lifecycle_key`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_lifecycle_role`
--

LOCK TABLES `veeva_lifecycle_role` WRITE;
/*!40000 ALTER TABLE `veeva_lifecycle_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_lifecycle_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_lifecycle_role_default_user`
--

DROP TABLE IF EXISTS `veeva_lifecycle_role_default_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_lifecycle_role_default_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `role_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `rule_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `user_type` varchar(20) COLLATE utf8_bin NOT NULL,
  `user_group_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `default_user_group` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `default_user` (`instance_id`,`lifecycle_key`,`role_key`,`rule_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_lifecycle_role_default_user`
--

LOCK TABLES `veeva_lifecycle_role_default_user` WRITE;
/*!40000 ALTER TABLE `veeva_lifecycle_role_default_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_lifecycle_role_default_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_lifecycle_role_rule`
--

DROP TABLE IF EXISTS `veeva_lifecycle_role_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_lifecycle_role_rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `role_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `rule_key` varchar(200) COLLATE utf8_bin NOT NULL,
  `allow_all_users_and_groups` bit(1) DEFAULT b'1',
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rule` (`instance_id`,`lifecycle_key`,`role_key`,`rule_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_lifecycle_role_rule`
--

LOCK TABLES `veeva_lifecycle_role_rule` WRITE;
/*!40000 ALTER TABLE `veeva_lifecycle_role_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_lifecycle_role_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_lifecycle_state`
--

DROP TABLE IF EXISTS `veeva_lifecycle_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_lifecycle_state` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `lifecycle_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `state_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `public_key` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `system` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`lifecycle_key`,`state_key`),
  UNIQUE KEY `public_key` (`instance_id`,`lifecycle_key`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_lifecycle_state`
--

LOCK TABLES `veeva_lifecycle_state` WRITE;
/*!40000 ALTER TABLE `veeva_lifecycle_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_lifecycle_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_notification`
--

DROP TABLE IF EXISTS `veeva_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `document_id` bigint(20) NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `message` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `notification` varchar(4000) DEFAULT NULL,
  `recipient_id` bigint(20) NOT NULL,
  `sender_id` bigint(20) NOT NULL,
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `task_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `template_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `workflow_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `major_version` int(11) DEFAULT NULL,
  `minor_version` int(11) DEFAULT NULL,
  `comment` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recipient_id` (`instance_id`,`recipient_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_notification`
--

LOCK TABLES `veeva_notification` WRITE;
/*!40000 ALTER TABLE `veeva_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_notification_template`
--

DROP TABLE IF EXISTS `veeva_notification_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_notification_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `message_template` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `notification_template` varchar(1023) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `subject_template` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `xml` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_notification_template`
--

LOCK TABLES `veeva_notification_template` WRITE;
/*!40000 ALTER TABLE `veeva_notification_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_notification_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_overlay`
--

DROP TABLE IF EXISTS `veeva_overlay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_overlay` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `overlay_key` varchar(255) NOT NULL,
  `xml` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  `created_by` bigint(20) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) unsigned NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `overlay_key` (`instance_id`,`overlay_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_overlay`
--

LOCK TABLES `veeva_overlay` WRITE;
/*!40000 ALTER TABLE `veeva_overlay` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_overlay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_pdf_template`
--

DROP TABLE IF EXISTS `veeva_pdf_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_pdf_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) COLLATE utf8_bin NOT NULL,
  `template_file_path` varchar(500) COLLATE utf8_bin NOT NULL,
  `template_file_name` varchar(200) COLLATE utf8_bin NOT NULL,
  `template_size` bigint(20) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_date` datetime NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `file_upload_date` datetime DEFAULT NULL,
  `file_upload_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_key` (`instance_id`,`template_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_pdf_template`
--

LOCK TABLES `veeva_pdf_template` WRITE;
/*!40000 ALTER TABLE `veeva_pdf_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_pdf_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_picklist`
--

DROP TABLE IF EXISTS `veeva_picklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_picklist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `picklist_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `public_key` varchar(100) DEFAULT NULL,
  `catalog_picklist` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `private_key` (`instance_id`,`picklist_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_picklist`
--

LOCK TABLES `veeva_picklist` WRITE;
/*!40000 ALTER TABLE `veeva_picklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_picklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_product`
--

DROP TABLE IF EXISTS `veeva_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `deleted` bit(1) DEFAULT b'0',
  `product_key` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_key` (`instance_id`,`product_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_product`
--

LOCK TABLES `veeva_product` WRITE;
/*!40000 ALTER TABLE `veeva_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_report_format`
--

DROP TABLE IF EXISTS `veeva_report_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_report_format` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `query_id` bigint(20) NOT NULL,
  `shared` bit(1) NOT NULL DEFAULT b'0',
  `parent_report_id` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `report_format_key` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `format_key` (`instance_id`,`report_format_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_report_format`
--

LOCK TABLES `veeva_report_format` WRITE;
/*!40000 ALTER TABLE `veeva_report_format` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_report_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_report_query`
--

DROP TABLE IF EXISTS `veeva_report_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_report_query` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `xml` longtext COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `report_type` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `report_object_type` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `report_query_key` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `query_key` (`instance_id`,`report_query_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_report_query`
--

LOCK TABLES `veeva_report_query` WRITE;
/*!40000 ALTER TABLE `veeva_report_query` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_report_query` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_report_sharing`
--

DROP TABLE IF EXISTS `veeva_report_sharing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_report_sharing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `report_id` bigint(20) NOT NULL,
  `share_user_id` bigint(20) NOT NULL,
  `share_type` varchar(20) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `share_user_type` varchar(20) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_report_sharing`
--

LOCK TABLES `veeva_report_sharing` WRITE;
/*!40000 ALTER TABLE `veeva_report_sharing` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_report_sharing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_report_usage`
--

DROP TABLE IF EXISTS `veeva_report_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_report_usage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `report_id` bigint(20) NOT NULL,
  `usage_type` varchar(20) COLLATE utf8_bin NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_report_usage`
--

LOCK TABLES `veeva_report_usage` WRITE;
/*!40000 ALTER TABLE `veeva_report_usage` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_report_usage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_signature_template`
--

DROP TABLE IF EXISTS `veeva_signature_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_signature_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `template_key` varchar(100) NOT NULL,
  `public_key` varchar(100) NOT NULL,
  `xml` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  `created_by` bigint(20) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) unsigned NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `template_key` (`instance_id`,`template_key`),
  UNIQUE KEY `public_key` (`instance_id`,`public_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_signature_template`
--

LOCK TABLES `veeva_signature_template` WRITE;
/*!40000 ALTER TABLE `veeva_signature_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_signature_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_user_license_type`
--

DROP TABLE IF EXISTS `veeva_user_license_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_user_license_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `vault_license_id` bigint(20) NOT NULL,
  `user_license_type_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `licensed_user_count` int(11) NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_user_license_type`
--

LOCK TABLES `veeva_user_license_type` WRITE;
/*!40000 ALTER TABLE `veeva_user_license_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_user_license_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_user_preferences`
--

DROP TABLE IF EXISTS `veeva_user_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_user_preferences` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_preference_type` varchar(50) COLLATE utf8_bin NOT NULL,
  `user_preference_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `user_preference_xml` longtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_preference` (`instance_id`,`user_id`,`user_preference_type`,`user_preference_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_user_preferences`
--

LOCK TABLES `veeva_user_preferences` WRITE;
/*!40000 ALTER TABLE `veeva_user_preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_user_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_user_type`
--

DROP TABLE IF EXISTS `veeva_user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_user_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `system_privileges` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `user_type_id` bigint(20) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_user_type`
--

LOCK TABLES `veeva_user_type` WRITE;
/*!40000 ALTER TABLE `veeva_user_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_user_type_definition`
--

DROP TABLE IF EXISTS `veeva_user_type_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_user_type_definition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `user_type_id` bigint(20) NOT NULL,
  `user_license_type_id` bigint(20) NOT NULL,
  `xml` longtext COLLATE utf8_bin,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_user_type_definition`
--

LOCK TABLES `veeva_user_type_definition` WRITE;
/*!40000 ALTER TABLE `veeva_user_type_definition` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_user_type_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_vault`
--

DROP TABLE IF EXISTS `veeva_vault`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_vault` (
  `id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `database_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `doc_auto_number` varchar(250) COLLATE utf8_bin NOT NULL,
  `instance_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `promo_mats_app` bit(1) DEFAULT NULL,
  `solr_core_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `default_last_used_autonumber` int(11) NOT NULL DEFAULT '0',
  `domain_id` bigint(20) DEFAULT NULL,
  `advanced_checkin_checkout` bit(1) DEFAULT NULL,
  `admin_access_classic_mode` bit(1) DEFAULT b'1',
  `admin_role_access_all` bit(1) DEFAULT b'0',
  `admin_add_viewer_role` bit(1) DEFAULT b'0',
  `admin_send_to_external` bit(1) DEFAULT b'0',
  `admin_allow_rendition_download` bit(1) DEFAULT b'0',
  `admin_allow_source_download` bit(1) DEFAULT b'1',
  `default_timezone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `tmf_app` bit(1) DEFAULT NULL,
  `quality_app` bit(1) DEFAULT NULL,
  `r_d_app` bit(1) DEFAULT NULL,
  `med_info_app` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_vault`
--

LOCK TABLES `veeva_vault` WRITE;
/*!40000 ALTER TABLE `veeva_vault` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_vault` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_vault_configuration`
--

DROP TABLE IF EXISTS `veeva_vault_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_vault_configuration` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) unsigned NOT NULL,
  `created_by` bigint(20) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) unsigned NOT NULL,
  `modified_date` datetime NOT NULL,
  `edition` varchar(50) COLLATE utf8_bin NOT NULL,
  `application` varchar(50) COLLATE utf8_bin NOT NULL,
  `feature_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `group_by` varchar(255) COLLATE utf8_bin NOT NULL,
  `data_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `value` varchar(2000) COLLATE utf8_bin NOT NULL,
  `default_enabled_value` varchar(2000) COLLATE utf8_bin NOT NULL,
  `scope` int(10) unsigned NOT NULL DEFAULT '0',
  `visible_in_ui` bit(1) NOT NULL,
  `enable_action` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `disable_action` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `run_once_on_enable` bit(1) NOT NULL DEFAULT b'0',
  `enable_action_executed` bit(1) NOT NULL DEFAULT b'0',
  `confirmation_message` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `help_text` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `feature_idx` (`instance_id`,`edition`,`application`,`feature_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Contains feature information about a vault instance';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_vault_configuration`
--

LOCK TABLES `veeva_vault_configuration` WRITE;
/*!40000 ALTER TABLE `veeva_vault_configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_vault_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_vault_license`
--

DROP TABLE IF EXISTS `veeva_vault_license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_vault_license` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `instance_id` bigint(20) NOT NULL,
  `license_model` varchar(255) COLLATE utf8_bin NOT NULL,
  `external_domain` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `licensed_document_count` int(11) DEFAULT NULL,
  `xml` longtext COLLATE utf8_bin,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_vault_license`
--

LOCK TABLES `veeva_vault_license` WRITE;
/*!40000 ALTER TABLE `veeva_vault_license` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_vault_license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veeva_workflow_task`
--

DROP TABLE IF EXISTS `veeva_workflow_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veeva_workflow_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` bigint(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `instance_id` bigint(20) NOT NULL,
  `modified_by` bigint(20) NOT NULL,
  `modified_date` datetime NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `version` int(11) DEFAULT NULL,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veeva_workflow_task`
--

LOCK TABLES `veeva_workflow_task` WRITE;
/*!40000 ALTER TABLE `veeva_workflow_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `veeva_workflow_task` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-12-05  1:31:53
