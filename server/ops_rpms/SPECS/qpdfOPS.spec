Name: qpdf-4.0.1
Version: ops_veeva_1.0
Release: 6 
Summary: Tool for content preservering transformation of PDF file, for Vault POD  
Group: Applications/Productivity
License: Distributable
Source0: qpdf-4.0.1.tar.gz 
BuildArch: x86_64 
BuildRoot: %{_tmppath}/ops_veeva_qpdf-root
Requires:gcc gcc-c++ zlib-devel pcre-devel
%description
From JIRA DEV-17361  

%prep
%setup -q -n %{name}

%build
./configure
make
make install

%install
mkdir -p $RPM_BUILD_ROOT/usr/local/bin
mkdir -p $RPM_BUILD_ROOT/usr/local/lib/pkgconfig
mkdir -p $RPM_BUILD_ROOT/usr/local/share/doc/qpdf
mkdir -p $RPM_BUILD_ROOT/usr/local/include/qpdf
mkdir -p $RPM_BUILD_ROOT/usr/local/share/man/man1
cp include/qpdf/*.h $RPM_BUILD_ROOT/usr/local/include/qpdf
cp include/qpdf/*.hh $RPM_BUILD_ROOT/usr/local/include/qpdf
cp doc/*.1 $RPM_BUILD_ROOT/usr/local/share/man/man1
cp qpdf/build/.libs/qpdf $RPM_BUILD_ROOT/usr/local/bin
#cp /usr/local/bin/qdf $RPM_BUILD_ROOT/usr/local/bin
cp zlib-flate/build/.libs/zlib-flate $RPM_BUILD_ROOT/usr/local/bin
#cp /usr/local/bin/zlib-flate $RPM_BUILD_ROOT/usr/local/bin
cp doc/stylesheet.css $RPM_BUILD_ROOT/usr/local/share/doc/qpdf
cp doc/qpdf-manual.html $RPM_BUILD_ROOT/usr/local/share/doc/qpdf
cp doc/qpdf-manual.pdf $RPM_BUILD_ROOT/usr/local/share/doc/qpdf
cp libqpdf.pc $RPM_BUILD_ROOT/usr/local/lib/pkgconfig
cp libqpdf/build/.libs/libqpdf.so.10.0.1 $RPM_BUILD_ROOT/usr/local/lib/libqpdf.so.10
cp qpdf/fix-qdf $RPM_BUILD_ROOT/usr/local/bin

%clean
make clean
rm -rf $RPM_BUILD_ROOT

%post

%files
%defattr(-,root,root)
/usr/local/bin/fix-qdf
/usr/local/include/qpdf
/usr/local/share/doc/qpdf/stylesheet.css
/usr/local/share/doc/qpdf/qpdf-manual.html
/usr/local/lib/pkgconfig/libqpdf.pc
/usr/local/share/doc/qpdf/qpdf-manual.pdf
/usr/local/share/man/man1/fix-qdf.1
/usr/local/share/man/man1/qpdf.1
/usr/local/share/man/man1/zlib-flate.1
/usr/local/bin/qpdf
/usr/local/bin/zlib-flate
/usr/local/lib/libqpdf.so.10

%changelog
* Mon Jun 24 2013 Mike Pereira <mike.pereira@veevasystems.com>
- Missed libqpdf.so.10
* Fri Jun 21 2013 Mike Pereira <mike.pereira@veevasystems.com>
- Add packaged files, and dependencies
* Tue Jun 17 2013 Mike Pereira <mike.pereira@veevasystems.com>
- Create spec file to install qdpf for Vault POD
