Name: 5xReleasePost 
Version: 1.0
Release: 1 
Summary: For General Release 4.x changes from 5.x 
Group: Applications/Productivity
License: Distributable
Source0: 5xReleasePost.tar.gz 
BuildArch: noarch
BuildRoot: %{_tmppath}/5xReleasePost-root
%description
https://veevavault.atlassian.net/wiki/display/DEV/4.0.X-5.0.1++Full+Upgrade

%prep
%setup -q -n %{name}

%build
#make

%install
mkdir -p $RPM_BUILD_ROOT/data2/buildimage
install 5xReleasePost.txt $RPM_BUILD_ROOT/data2/buildimage


%clean
#make clean
rm -rf $RPM_BUILD_ROOT

%post
##Build 
# ~/.ssh/config
#Change :
#Host = build
#To:
#Host = build2

##ADD :
#Host = build
#Hostname = 128.242.247.124
#IdentityFile = ~/.ssh/vault.pem
#User = build

rm -rf /usr/local/apache-tomcat/6.0.32/webapps/users*
rm -rf /usr/local/apache-tomcat/6.0.32/webapps/flow*
rm -rf /usr/local/apache-tomcat/6.0.32/webapps/docs*
 

cd /data2/
mkdir reports
chown tomcat6:tomcat6 reports




%files
%defattr(-,root,root)
/data2/buildimage/5xReleasePost.txt


%changelog
*Tue Jun 9 2013 Mike Pereira <mike.pereira@veevasystems.com>
- Create spec file with 5.x general release changes

