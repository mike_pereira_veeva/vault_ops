Name: ops_veeva_fontsTEX170 
Version: 1.0
Release: 1
Summary: Config fix for PDF Rendering for Vault POD for certain fonts
Group: Applications/Productivity
License: Distributable
Source0: ops_veeva_fontsTEX170.tar.gz
BuildArch: noarch
BuildRoot: %{_tmppath}/ops_veeva_fontsTEX170-root
Requires:msttcorefonts
%description
From JIRA TEX-170 to copy .xpdrc file with specific font mapping and setup link for fonts folder for all Vault PODS

%prep
%setup -q -n %{name}

%build
#make

%install
mkdir -p $RPM_BUILD_ROOT/usr/local/apache-tomcat/6.0.32/temp
install .xpdfrc $RPM_BUILD_ROOT/usr/local/apache-tomcat/6.0.32/temp


%clean
#make clean
rm -rf $RPM_BUILD_ROOT

%post
chown tomcat6:tomcat6 /usr/local/apache-tomcat/6.0.32/temp/.xpdfrc
cd /usr/share/fonts
ln -s /usr/share/fonts/msttcorefonts msttcore

%files
%defattr(-,root,root)
/usr/local/apache-tomcat/6.0.32/temp/.xpdfrc

%changelog
* Tue Dec 7 2004 Mike Pereira <mike.pereira@veevasystems.com>
- Create spec file to copy .xpdrc file with specific font mapping

