Name: 4xReleasePost 
Version: 1.0
Release: 2 
Summary: For General Release 4.x changes from 3.x and specific 3.5.7 change for mysql config 
Group: Applications/Productivity
License: Distributable
Source0: 4xReleasePost.tar.gz 
BuildArch: noarch
BuildRoot: %{_tmppath}/4xReleasePost-root
%description
From https://sites.google.com/a/veevasystems.com/veeva-content-management/development/engineering/release-notes/release-notes-3-0-1-x---4-0-0 

%prep
%setup -q -n %{name}

%build
#make

%install
mkdir -p $RPM_BUILD_ROOT/data2/buildimage
mkdir -p $RPM_BUILD_ROOT/usr/local/ffmpeg
mkdir -p $RPM_BUILD_ROOT/usr/local/ImageMagick
install 4xReleasePost.txt $RPM_BUILD_ROOT/data2/buildimage
install ffmpegFiles.tar.gz $RPM_BUILD_ROOT/data2/buildimage
install ImageMagick.tar.gz $RPM_BUILD_ROOT/data2/buildimage


%clean
#make clean
rm -rf $RPM_BUILD_ROOT

%post
# ********************************************************************************************************************************************
#9/21/2012 3.5.7 FT Release
#------------------------------------------------
#Release Notes - Mysql Config Change
#set global thread_cache_size=40
#set  global table_definition_cache=12000
#set  global table_open_cache =5000
#mysql --defaults-file=~/.conf/.vba.cnf -e "set global thread_cache_size=40"
#mysql --defaults-file=~/.conf/.vba.cnf -e "set  global table_definition_cache=12000"
#mysql --defaults-file=~/.conf/.vba.cnf -e "set  global table_open_cache =5000"
echo "y" | cp /etc/my.cnf /etc/my.cnf.bak1
sed 's/thread_cache_size\               = 8/thread_cache_size\               = 40/' /etc/my.cnf > /tmp/mycnf1
echo "y" | cp /tmp/mycnf1 /etc/my.cnf
sed 's/table_open_cache\                = 4000/table_open_cache                = 5000/' /etc/my.cnf > /tmp/mycnf1
echo "y" | cp /tmp/mycnf1 /etc/my.cnf
sed '/table_open_cache/ i\table_definition_cache\          = 12000' /etc/my.cnf  > /tmp/mycnf1
echo "y" | cp /tmp/mycnf1 /etc/my.cnf

echo "Verify New MYSQL Cache values are set"
diff -b /etc/my.cnf /etc/my.cnf.bak1

#Release Notes 3.0.1.X - 4.0.1
#10/05/2012 4.0.0 FT Release
#9/21/2012 3.5.7 FT Release

#svn export https://veevavault.atlassian.net/svn/ops/trunk/server/ffmpeg/ffmpegFiles.tar.gz
# Downloaded from init_vault_servers.sh
mkdir /usr/local/ffmpeg
cd /data2/buildimage
tar -xvf ffmpegFiles.tar.gz -C /usr/local/ffmpeg/
cd /usr/local/ffmpeg/qt
python setup.py install
cd ..

echo "Verify QTFASTSTART Version"
echo "------------------------"
qtfaststart --version
echo "------------------------"
#(should display "qtfaststart  1.6")

#svn export https://veevavault.atlassian.net/svn/ops/trunk/server/ImageMagick/ImageMagick.tar.gz
# Downloaded from init_vault_servers.sh
cd /data2/buildimage
mkdir /usr/local/ImageMagick
tar xvf ImageMagick.tar.gz -C /usr/local/ImageMagick/
echo "Verify ImageMagick"
ls -l /usr/local/ImageMagick/


#For /etc/init.d/tomcat6: (updated in SVN)
# check for the dynamic library path
#LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/ImageMagick
#export LD_LIBRARY_PATH

grep LD_LIBRARY_PATH /etc/init.d/tomcat6
grep "Djmagick.systemclassloader=no" /etc/init.d/tomcat6


%files
%defattr(-,root,root)
/data2/buildimage/4xReleasePost.txt
/data2/buildimage/ImageMagick.tar.gz
/data2/buildimage/ffmpegFiles.tar.gz
/usr/local/ImageMagick/
/usr/local/ffmpeg/

%changelog
*Wed Jun 26 2013 Mike Pereira <mike.pereira@veevasystems.com>
- Create fix tar extract and compile for post for imagetrick and ffmpeg
*Tue Jun 10 2013 Mike Pereira <mike.pereira@veevasystems.com>
- Create spec file with 4.x general release changes

