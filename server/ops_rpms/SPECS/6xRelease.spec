Name: 6xReleasePost 
Version: 1.0
Release: 2 
Summary: For General Release 5.x changes from 6.x 
Group: Applications/Productivity
License: Distributable
Source0: 6xReleasePost.tar.gz 
BuildArch: noarch
BuildRoot: %{_tmppath}/6xReleasePost-root
%description
From https://veevavault.atlassian.net/wiki/display/DEV/5.0.X+-+6.0.X++Full+Upgrade 

%prep
%setup -q -n %{name}

%build
#make

%install
mkdir -p $RPM_BUILD_ROOT/data2/buildimage
install 6xReleasePost.txt $RPM_BUILD_ROOT/data2/buildimage


%clean
#make clean
rm -rf $RPM_BUILD_ROOT

%post
mkdir /data2/public
chown tomcat6:tomcat6 /data2/public/
chmod 740 /data2/public/

cp /usr/local/apache-tomcat/6.0.32/webapps/ui/JMVC/veeva_vault/resources/styles/images/vault_logo_plain.png /data2/public/vaultEmailFooterLogo.png
chown tomcat6:tomcat6 /data2/public/vaultEmailFooterLogo.png

cd /data2
mkdir vaultTmpFiles
chown tomcat6:tomcat6 -R vaultTmpFiles
cd /usr/local/apache-tomcat/6.0.32/
if [ -d "tempFileDirectory" ]
then
    echo "Directory tempFileDirectory exits."
	rm -rf tempFileDirectory/
else
    echo "Directory tempFileDirectory does not exits."
fi

mkdir /data2/vault/\$\{activemq.data\}/
chown tomcat6:tomcat6 /data3/vault/\$\{activemq.data\}/
rmdir /usr/local/apache-tomcat/6.0.32/\$\{activemq.data\}/
cd /usr/local/apache-tomcat/6.0.32/
ln -s /data2/vault/\$\{activemq.data\}/ /usr/local/apache-tomcat/6.0.32/\$\{activemq.data\}


%files
%defattr(-,root,root)
/data2/buildimage/6xReleasePost.txt


%changelog
*Tue Jun 25 2013 Mike Pereira <mike.pereira@veevasystems.com>
- Add active queue folder change to /data2 
*Tue Jun 10 2013 Mike Pereira <mike.pereira@veevasystems.com>
- Create spec file with 6.x general release changes

