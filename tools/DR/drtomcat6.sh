#!/bin/bash
#set -x
SERV=`ps -ef |grep jav[a] | awk '{print $1}' | head -1 | wc -l`
if [ "$SERV" == "1" ];
then
#echo "Application running on DR system $PODD"
echo "Application running on DR system $PODD" > /tmp/drmessage.txt
mail -s "DR tomcat is running, need to turn off unelss doing DR deployment or testing" techops@veevasystems.com < /tmp/drmessage.txt
#mail -s "DR tomcat is running, need to turn off unelss doing DR testing" mike.pereira@veevasystems.com < /tmp/drmessage.txt
else
echo "No services running" > /tmp/drmessage.txt
fi
