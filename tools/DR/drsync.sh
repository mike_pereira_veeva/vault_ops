#!/bin/bash
#set -x
export REPLYTO="vault_error@veevasystems.com"
#MAILTOME="mike.pereira@veevasystems.com"
MAILTOME="mike.pereira@veevasystems.com"
#MAILTO="sysadmin@veevasystems.com"
MAILTO="drvaultemail@integration1.vaultdev.com"
SERVERNAME=`hostname`
##Main DR Sync - accumlated data for vault filesystem and DB backups to DR system
###########################################################################
#/usr/bin/ionice -c2 -n7 rsync -tavz -e "ssh -q -c arcfour" /data2/mysql/binlog /data2/vault /data3/backup /data2/public root2@209.207.193.52:/data2 > /tmp/$$.drsync.log
/bin/nice -n 19 rsync -tavz -e "ssh -q -c arcfour" /data2/mysql/binlog /data2/vault /data3/backup /data2/public root2@209.207.193.52:/data2 > /tmp/$$.drsync.log
#/usr/bin/ionice -c2 -n7 rsync -tavz -e "ssh -q -c arcfour" --exclude '*.png'  --exclude '*.rendition.pdf' /data2/annotate root2@209.207.193.52:/data2 > /tmp/$$.drsync.log
/bin/nice -n 19 rsync -tavz -e "ssh -q -c arcfour" --exclude '*.png'  --exclude '*rendition.pdf' /data2/annotate root2@209.207.193.52:/data2 > /tmp/$$.drsync.log
if [ "$?" = "0" ]; then
    echo "OK" 2>&1 > /dev/null	
else
        CHECK_VAN=`grep "rsync warning: some files vanished" /tmp/$$.drsync.log | awk '{print $5}'` 
       if [ "$CHECK_VAN" == "vanished" ];
	then
	echo "error" 2>&1 > /dev/null 
       else
	echo "error" 2>&1 > /dev/null 
	fi
fi
##########################################################################
## Disable transfer for conf folder to test DR PDF rendering smoketests
#rsync -tavz -e "ssh -q -c arcfour" /data2/mysql/binlog /data2/vault /data2/annotate /data2/backups root2@209.207.193.52:/data2 > /tmp/$$.drsync.log
###########################################################################

####
rsync -tavz --delete -e "ssh -q -c arcfour" /data2/Solr root2@209.207.193.52:/data2 > /tmp/$$.solrsync.log
if [ "$?" = "0" ]; then
    echo "OK" 2>&1 > /dev/null 
else
        CHECK_SVAN=`grep "rsync warning: some files vanished" /tmp/$$.solrsync.log | awk '{print $5}'` 
        if [ "$CHECK_SVAN" == "vanished" ];
	then
	echo "error" 2>&1 > /dev/null 
       else
	echo "error" 2>&1 > /dev/null 
	fi
	exit 1
fi

# for auth1 backups to DR
rsync -tavz --delete -e "ssh -q -c arcfour" /data3/backups/auth1 root2@209.207.193.52:/data2/authbackup  > /tmp/$$.authsync_app.log
rsync -tavz --delete -e "ssh -q -c arcfour" /data3/backups/vmc1 root2@209.207.193.52:/data2/vmcbackup  > /tmp/$$.vmc1sync_db.log
## Keep init script up to data with any changes for JMX
scp -q -c arcfour /etc/init.d/tomcat6* root2@209.207.193.52:/etc/init.d/

## Capture mysql binlog stats
mysql -e "show master status" > /tmp/binlogstatus.txt
mysql -e "show binary logs" >> /tmp/binlogstatus.txt
scp -q /tmp/binlogstatus.txt root2@209.207.193.52:/data2/backups

### Cleanup tmp files after 3 days old ###
find /tmp -name "*sync*" -mtime +3 -exec rm {} \;
