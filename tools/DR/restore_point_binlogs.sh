echo "Current binlog postition for POF"
mysql --defaults-file=~/.conf/.vba.cnf -e"show master status"
service mysql stop
rm -rf /data2/mysql/binlog/*
cp -pr /data2/binlog/* /data2/mysql/binlog
chown -R mysql:mysql /data2/mysql/binlog/
service mysql start
STARTFILE=`cat /data2/mysql/data/xtrabackup_binlog_pos_innodb | awk '{print $1}'`
STARTPOS=`cat /data2/mysql/data/xtrabackup_binlog_pos_innodb | awk '{print $2}'`
find /data2/binlog/ -newer $STARTFILE | sed /index/d | grep mysql-bin | sort > /tmp/restore_binlog.lst
for i in `cat /tmp/restore_binlog.lst`; do echo -n "$i "; done > /tmp/binloglist
BINARRAY=`cat /tmp/binloglist`

echo "Starting POF"
echo "-------------"
echo "mysqlbinlog $STARTFILE $BINARRAY --start-position=$STARTPOS | mysql --defaults-file=~/.conf/.vba.cnf"
mysqlbinlog $STARTFILE $BINARRAY --start-position=$STARTPOS | mysql --defaults-file=~/.conf/.vba.cnf
echo "Check again master postition after POF"
mysql --defaults-file=~/.conf/.vba.cnf -e"show master status"
#sleep 5
#echo "Reset Master"
#mysql --defaults-file=~/.conf/.vba.cnf -e"reset master"
#sleep 5
#echo " New Postition for master after Reset"
#mysql --defaults-file=~/.conf/.vba.cnf -e"show master status"
