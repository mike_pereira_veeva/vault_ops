#!/bin/sh
#set -x
LOGPATH=/var/lib/mysql/log
NEWLOGPATH=/data/dump
DATE=`date --date "yesterday" +\%Y-\%m-\%d`
LOG="$NEWLOGPATH/slow.log"
PLOG="/tmp/plog"
NOWDATE=`date --date "now" +\%H\:\%M:`
# perl -e "print scalar localtime (1351897276)" | awk '{print $4}'
# SET timestamp=1351840543;
# grep -oE "\/\*[^\*]*\*/" /var/lib/mysql/log/slow.log | wc -l
# grep SET slow.log | awk -F"=" '{print $2}' | sed -e 's/;//' > /tmp/tslist
# grep SET slow.log | awk -F"=" '{print $2}' | sed -e 's/;//' | sort | uniq > /tmp/tslist
# CONVERT=`perl -e "print scalar localtime (1351903568)" | awk '{print $4}'` 
# sed s/1351903568/$CONVERT/ slow.log
# for i in `cat tslist`; do CONVERT=`perl -e "print scalar localtime ($i)" | awk '{print $4}'` | sed s/$i/$CONVERT/ slow.log > /tmp/slotmp; cp /tmp/slotmp slow.log ; done
#AUCOUNT=`grep "$NDATE" $LOGPATH/ui.log $LOGPATH/api.log  | awk '/maxTime/ {print $5, " "}' | wc -l`

PCHECK=`cat $PLOG` 
NCHECK=`grep -oE "\/\*[^\*]*\*/" $LOG | sed 's/\/\*//g' | sed 's/\*\///g' | wc -l` 
CHECK=`expr $NCHECK - $PCHECK`

#For Nagios NRPE client
OK=0
WARNING=1
CRITICAL=2

echo "slowcount:$CHECK"

# "Pipe data to for future diff"
grep -oE "\/\*[^\*]*\*/" $LOG | sed 's/\/\*//g' | sed 's/\*\///g' | wc -l > $PLOG 
