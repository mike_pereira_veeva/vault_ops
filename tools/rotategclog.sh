#!/bin/sh
#set -x
DATE=`date --date "yesterday" +\%Y-\%m-\%d`
PATH=/usr/local/apache-tomcat/6.0.32/logs
DEST="/data3/logbackup/gc"

#Copy gc log file with yesterday date to /data3/logbackups area
cd $PATH
/bin/cp gc.log $DEST/gc.log.$DATE

#truncate current .log file
echo " " > $PATH/gc.log
