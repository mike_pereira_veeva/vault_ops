#!/bin/bash

##
## sub usage()
## Display usage info
##
usage(){

  cat <<EOF
  Usage: $0 options
  
  Print   a daily vault usage report"
  Options:
     -h                Show this message
EOF

}



#######################################################
# Main                                                #
#######################################################


export TZ="/usr/share/zoneinfo/US/Pacific"
DATE=`date --date "yesterday" +\%Y-\%m-\%d`
PRINTDT=`date --date "yesterday" +\%m/\%d/\%Y`

#DATE='2011-11-04'
#PRINTDT='11/04/2011'
MYSQL="mysql --defaults-file=/home/root2/.conf/.vba.cnf  --skip-column-names  -hlocalhost  "

echo "
****************************
Vault Cust Activity: $PRINTDT
      CONFIDENTIAL!
     Do not forward!
****************************
     SUMMARY TOTALS
"




sql="select count(*) , sum(active_user_count), sum(unique_login_count), sum(doc_count),  sum(doc_version_count) , sum(doc_upload_count),sum(doc_view_count) ,SUM(ifnull(annotation_count,0))  ,  sum(new_workflow) , sum(created_reports), sum(rep_exe_calls), round(sum(db_size_kbytes)/1024) ,  round(sum(vault_size_kbytes) / 1024 ) ,  round(sum(annotate_size_kbytes) / 1024 ), sum(new_api_calls) from directory.vault_stats  s join vmc.vmc_veeva_vault i on i.id = s.instance_id  join vmc.vmc_veeva_domain vd on vd.id = i.domain_id  where s.instance_id > 1  and metric_date=\"$DATE\" and vd.domain_type = \"$1\" "

values=`echo $sql | $MYSQL `

labels=( "Vaults    " "Active   Users" "Unique   Logins" "Total Documents" "Total Doc Versions" "New  Uploads  " "Doc   Views  " "New Notes  " "New WorkFlows  " "Total Reports  " "Report Views  " "db   size (mb)  " "doc   size (mb)  " "annotate   size (mb)" "New API Calls       ")

i=0
for value in  $values ; do

    printf "%-14s%3s\t%'12d\n"  "${labels[$i]}" "=" "$value"
    let i++
done
######sql="select vi.id from vmc.vmc_veeva_vault vi where pod = 'VV1' and vault_name not like '%PMO%' and status <> 'Delete' order by 1";
#sql="select vvv.id from  vmc.vmc_veeva_vault  vvv join vmc.vmc_veeva_domain vvd  on vvd.id =  vvv.domain_id join vmc.vmc_veeva_customer c on c.id = domain_customer_id where   vvd.domain_type  = \"$1\" order by customer_name, domain_name, vault_name, vvv.id";
sql="select vvv.id from  vmc.vmc_veeva_vault  vvv join vmc.vmc_veeva_domain vvd  on vvd.id =  vvv.domain_id join vmc.vmc_veeva_customer c on c.id = domain_customer_id where   vvd.domain_type  = \"$1\" and vvv.vault_name not like '%PMO%' and vvv.status <> 'Delete' order by customer_name, domain_name, vault_name, vvv.id;";
dbs=`echo $sql | $MYSQL `


echo ""
echo ""
for db in $dbs ; do 

sql="select concat_ws('|',concat(customer_name, ' - ' , if(ifnull(vault_name,'')='',domain_name,concat(domain_name,'-',vault_name)) , '.',domain_suffix), active_user_count, unique_login_count, doc_version_count , doc_view_count , doc_upload_count, ifnull(annotation_count,0) , active_workflow_count , task_count, round(db_size_kbytes / 1024 ) , round(vault_size_kbytes/1024) , round(annotate_size_kbytes/1024)) , doc_count, new_workflow, new_api_calls, created_reports, rep_exe_calls  from directory.vault_stats s join vmc.vmc_veeva_vault i on i.id = s.instance_id  join vmc.vmc_veeva_domain vd on vd.id = i.domain_id  join vmc.vmc_veeva_customer c on c.id = domain_customer_id  and  metric_date=\"$DATE\"  and  i.id = $db"

#    sql="select concat_ws('|',concat(customer_name, ' - ' , if(ifnull(vault_name,'')='',domain_name,concat(vault_name,'.',domain_name)) , '.',domain_suffix,' (',domain_type,')'), active_user_count, unique_login_count, doc_version_count , doc_view_count , doc_upload_count, active_workflow_count , task_count, round(db_size_kbytes / 1024 ) , round(vault_size_kbytes/1024) , round(annotate_size_kbytes/1024))  from directory.vault_stats s join vmc.vmc_veeva_vault i on i.id = s.instance_id  join vmc.vmc_veeva_domain vd on vd.id = i.domain_id  join vmc.vmc_veeva_customer c on c.id = domain_customer_id  and  metric_date=\"$DATE\"  and  i.id = $db"

    podSql="select pod from vmc.vmc_veeva_vault where id = $db"
 
    pod=`echo $podSql | $MYSQL`
    
    echo "**********************************"
    echo "Vault Details"
    values=`echo $sql | $MYSQL `
    name=${values%%|*}
    values=${values#*|}
    values=(${values//|/ });
    printf "  Vault                                         =  %-50s\n"  "$name : vault $db ($pod)" 
    printf "  Users / Logins                         =  %'6d       /  %'6d\n"  ${values[0]} ${values[1]} 
    printf "  Documents / Doc Versions / Views / New Uploads   =  %'6d / %'6d /  %'6d /  %'6d\n"   ${values[11]} ${values[2]} ${values[3]} ${values[4]}
    printf "  New Notes/  New WorkFlows / Tasks /New Api Calls =  %'6d   / %'6d       /  %'6d  / %'6d\n"   ${values[5]} ${values[12]} ${values[7]} ${values[13]} 
    #printf "  Reports / Report Views  =   %'6d\n"   ${values[14]}  
    printf "  Reports / Report Views  =  %'6d  / %'6d\n"   ${values[14]} ${values[15]} 
    printf "  db size   / filesystem size / annotate size      =  %'6d mb /  %'6d mb /  %'6d mb \n"   ${values[8]} ${values[9]} ${values[10]}
    echo ""
done
