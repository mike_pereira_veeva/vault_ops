#!/bin/sh
#set -x
LOGPATH=/usr/local/apache-tomcat/6.0.32/logs
NDATE=`date --date "1 hour ago" +\%Y-\%m-\%d\ \%H\:`
LOG="gc.log"
TEMP_LOG_DATA="/tmp/$LOG.parsed.txt"
#PDATE=`date --date "10 minutes ago" +\%H\:\%M:`
PDATE=`date --date "5 minutes ago" +\%H\:\%M:`
NOWDATE=`date --date "now" +\%H\:\%M:`

## Changed from checking hourly with grep to every 10 minutes using sed
#PPCOUNT=`grep "$NDATE" $LOGPATH/docs.log $LOGPATH/flow.log $LOGPATH/reports.log $LOGPATH/users.log | awk '/maxTime/ {print $5, " "}' | wc -l`   
PCOUNTD=`sed -n "/$PDATE/,/$NOWDATE/p" $LOGPATH/docs.log | awk '/maxTime/ {print $5, " "}' | wc -l`   
PCOUNTF=`sed -n "/$PDATE/,/$NOWDATE/p" $LOGPATH/flow.log | awk '/maxTime/ {print $5, " "}' | wc -l`   
PCOUNTR=`sed -n "/$PDATE/,/$NOWDATE/p" $LOGPATH/reports.log | awk '/maxTime/ {print $5, " "}' | wc -l`   
PCOUNTU=`sed -n "/$PDATE/,/$NOWDATE/p" $LOGPATH/users.log | awk '/maxTime/ {print $5, " "}' | wc -l`   
PCOUNT=`expr $PCOUNTD + $PCOUNTF + $PCOUNTR + $PCOUNTU` 
#ACOUNT=`grep "$NDATE" $LOGPATH/api.log | awk '/maxTime/ {print $5, " "}' | wc -l`
ACOUNT=`sed -n "/$PDATE/,/$NOWDATE/p" $LOGPATH/api.log | awk '/maxTime/ {print $5, " "}' | wc -l`
#UCOUNT=`grep "$NDATE" $LOGPATH/ui.log | awk '/maxTime/ {print $5, " "}' | wc -l`
UCOUNT=`sed -n "/$PDATE/,/$NOWDATE/p" $LOGPATH/ui.log | awk '/maxTime/ {print $5, " "}' | wc -l`
#AUCOUNT=`grep "$NDATE" $LOGPATH/ui.log $LOGPATH/api.log  | awk '/maxTime/ {print $5, " "}' | wc -l`
AUCOUNT=`expr $ACOUNT + $UCOUNT`
log_check=$PCOUNT 

#For Nagios NRPE client
OK=0
WARNING=1
CRITICAL=2

SERVERNAME=`hostname`

#if [ $log_check -ge 10 ];
#then
echo "ApiUi_count:$AUCOUNT Apicount:$ACOUNT Uicount:$UCOUNT Privatecounts:$PCOUNT" 
#echo "ApiUi_count:$AUCOUNT Apicount:$ACOUNT Uicount:$UCOUNT Privatecounts:$PCOUNT HourPrivate:$PPCOUNT" 
exit $OK
#else
#echo "something wrong, not able to access logs"
#exit $WARNING
#fi
