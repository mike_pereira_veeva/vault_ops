#set -x
DATE=`date --date "yesterday" +\%Y-\%m-\%d`
#LOGPATH=/data3/backups/vv1/logs
LOGPATH=/data3/vault/logs
APILOG="$LOGPATH/api.log.$DATE"
UILOG="$LOGPATH/ui.log.$DATE"
UICONTROLLER=/tmp/controllerui.list
APICONTROLLER=/tmp/controllerapi.list

if [ -f "$UILOG" ]
then
ls -l $UILOG > /tmp/UI_perfcheck
else
touch $UILOG
fi

if [ -f "$APILOG" ]
then
ls -l $APILOG > /tmp/API_perfcheck
else
touch $APILOG
fi

#grep currentTime $UILOG | awk '{print $19}' | sort | uniq | sed -e 's/(..)//' |  sed -e 's/()//' > $UICONTROLLER
grep currentTime  $UILOG | egrep -E -v 'Download|post|createDocument|uploadNewVersion|update|checkout|checkin|IRepController.publish|ProxyController.getDocRendition|ProxyController.getVideoDownload' | awk '{print $19}' | sort | uniq | sed -e 's/(..)//' |  sed -e 's/()//' > $UICONTROLLER

echo " " > /tmp/comp.list
echo " " > /tmp/comp5.list
echo " " > /tmp/comp10.list
echo " " > /tmp/count

for control in `cat $UICONTROLLER`;
do
# List array of highest currentTime for eaxh controller
COUNT=`grep $control $UILOG | awk -F"currentTime=" '{print $2}' | awk '{if($1 > 2000 && $1 < 4999) print $1}' | wc -l`
COUNT5=`grep $control $UILOG | awk -F"currentTime=" '{print $2}' | awk '{if($1 > 5000 && $1 < 9999) print $1}' | wc -l`
COUNT10=`grep $control $UILOG | awk -F"currentTime=" '{print $2}' | awk '{if($1 > 10000) print $1}' | wc -l`
HIGHEST=`grep $control $UILOG | awk -F"currentTime=" '{print $2}' | awk '{if($1 > 2000 && $1 < 4999) print $1}' | sort -nr | head -1`
HIGHEST5=`grep $control $UILOG | awk -F"currentTime=" '{print $2}' | awk '{if($1 > 5000 && $1 < 9999) print $1}' | sort -nr | head -1`
HIGHEST10=`grep $control $UILOG | awk -F"currentTime=" '{print $2}' | awk '{if($1 > 10000) print $1}' | sort -nr | head -1`
echo "$COUNT    | $HIGHEST   $control" >> /tmp/comp.list
echo "$COUNT5    | $HIGHEST5   $control" >> /tmp/comp5.list
echo "$COUNT10    | $HIGHEST10   $control" >> /tmp/comp10.list

done

cat /tmp/comp.list | sed -e 's/currentTime=//' | sort -nr | uniq > /tmp/comp2.list
cat /tmp/comp5.list | sed -e 's/currentTime=//' | sort -nr | uniq > /tmp/comp2_5.list
cat /tmp/comp10.list | sed -e 's/currentTime=//' | sort -nr | uniq > /tmp/comp2_10.list

echo "From server VV1"
echo "-------------------------------------"
echo "Total Calls  Vault ID "
awk '/maxTime/ {print $5, " "}' $UILOG | sort | uniq -c | sort -rn
echo " "
echo " The following Controllers are filtered out - (Download|post|createDocument|uploadNewVersion|update|checkout|checkin|ProxyController.getVideoDownload|IRepController.publish|ProxyController.getDocRendition)"
echo " "
echo "List of UI Controller Calls with currentTime greater than 10000 for $DATE"
echo "-------------------------------------"
echo "Count    Time       Controller"
echo "-------------------------------------"
#head -50 /tmp/comp2.list 
head -25 /tmp/comp2_10.list | sed -e '/0\    |/d'

echo " "
echo "List of UI Controller Calls with currentTime between 5000-9999 for $DATE"
echo "-------------------------------------"
echo "Count    Time       Controller"
echo "-------------------------------------"
cat /tmp/comp.list | sed -e 's/currentTime=//' | sort -nr | uniq > /tmp/comp2.list

head -25 /tmp/comp2_5.list | sed -e '/0\    |/d'

echo " "
echo "List of UI Controller Calls with currentTime between 2000-4999 for $DATE"
echo "-------------------------------------"
echo "Count    Time       Controller"
echo "-------------------------------------"
cat /tmp/comp.list | sed -e 's/currentTime=//' | sort -nr | uniq > /tmp/comp2.list

head -25 /tmp/comp2.list | sed -e '/0\    |/d'

echo " "

SLOWREPORT=`grep maxTime $LOGPATH/reports.log|grep ReportExecutionController.executeSummaryReport |awk {'print $13 " " $20'} | cut -d'=' -f2 | awk {'if ($1 > 30000) print $0'}`
echo "$SLOWREPORT" > /tmp/slowreport.list
echo "Slow Report over 30000"
echo "-------------------------------------"
cat /tmp/slowreport.list | sed -e 's/currentTime=//' | sort -nr | uniq > /tmp/slowreport_10.list
head -25 /tmp/slowreport_10.list | sed -e '/0\    |/d'
echo ""

#grep currentTime $APILOG | awk '{print $19}' | sort | uniq | sed -e 's/(..)//' |  sed -e 's/()//' > $APICONTROLLER
grep currentTime $APILOG | egrep -E -v 'Download|post|createDocument|uploadNewVersion|update|checkout|checkin|IRepController.publish|ProxyController.getDocRendition|ProxyController.getVideoDownload' | awk '{print $19}' | sort | uniq | sed -e 's/(..)//' |  sed -e 's/()//' > $APICONTROLLER
echo " " > /tmp/comp_api.list
echo " " > /tmp/comp_api_10.list
echo " " > /tmp/comp_api_5.list
echo " " > /tmp/count
for control in `cat $APICONTROLLER`;
do

COUNT=`grep $control $APILOG | sed 's/=/ /g' | egrep -v 'from|document_number__v' | awk '{if($16 > 2000 && $16 < 4999) print $16}' | wc -l`
COUNT5=`grep $control $APILOG | sed 's/=/ /g' | egrep -v 'from|document_number__v' | awk '{if($16 > 5000 && $16 < 9999) print $16}' | wc -l`
COUNT10=`grep $control $APILOG | sed 's/=/ /g' | egrep -v 'from|document_number__v' | awk '{if($16 > 10000) print $16}' | wc -l`
HIGHEST=`grep $control $APILOG | sed 's/=/ /g' | egrep -v 'from|document_number__v' | awk '{if($16 > 2000 && $16 < 4999) print $16}' | sort -nr | head -1`
HIGHEST5=`grep $control $APILOG | sed 's/=/ /g' | egrep -v 'from|document_number__v' | awk '{if($16 > 5000 && $16 < 9999) print $16}' | sort -nr | head -1`
HIGHEST10=`grep $control $APILOG | sed 's/=/ /g' | egrep -v 'from|document_number__v' | awk '{if($16 > 10000) print $16}' | sort -nr | head -1`
echo "$COUNT    | $HIGHEST   $control" >> /tmp/comp_api.list
echo "$COUNT10    | $HIGHEST10   $control" >> /tmp/comp_api_10.list
echo "$COUNT5    | $HIGHEST5   $control" >> /tmp/comp_api_5.list
 done

echo " "
echo "Total API Calls by Vault for $DATE"
echo "-------------------------------------"
echo "Total Calls   Vault ID "
echo " "
awk '/maxTime/ {print $5, " "}' $APILOG | sort | uniq -c | sort -rn
echo " "
echo "List of API Controller Calls with currentTime greater than 10000 for $DATE"
echo "-------------------------------------"
echo "Count    Time       Controller"
echo "-------------------------------------"
cat /tmp/comp_api.list | sed -e 's/currentTime=//' | sort -nr | uniq > /tmp/comp2api.list
cat /tmp/comp_api_10.list | sed -e 's/currentTime=//' | sort -nr | uniq > /tmp/comp2api_10.list
cat /tmp/comp_api_5.list | sed -e 's/currentTime=//' | sort -nr | uniq > /tmp/comp2api_5.list
#head -50 /tmp/comp2api.list | sed -e '/0\    |/d'
head -25 /tmp/comp2api_10.list | sed -e '/0\    |/d'
echo " "
echo "List of API Controller Calls with currentTime between 5000-9999 for $DATE"
echo "-------------------------------------"
echo "Count    Time       Controller"
echo "-------------------------------------"

head -25 /tmp/comp2api_5.list | sed -e '/0\    |/d'
echo " "
echo "List of API Controller Calls with currentTime between 2000-4999 for $DATE"
echo "-------------------------------------"
echo "Count    Time       Controller"
echo "-------------------------------------"

head -25 /tmp/comp2api.list | sed -e '/0\    |/d'

echo " "
echo " "
echo "Total Slow Queries for $DATE"
echo "-------------------------------------"
echo "Count   Slow Query "
echo "-------------------------------------"
grep -oE "\/\*[^\*]*\*/" /data3/logbackup/mysqlslow/slow.log.$DATE | sed 's/\/\*//g' | sed 's/\*\///g' | sort | uniq -c | sort -rn

echo " "
echo "Overall controller counts for $DATE"
echo "-------------------------------------"
awk '/maxTime/ {print $19}' $LOGPATH/*.log.$DATE | sort | uniq -c | sort -rn


