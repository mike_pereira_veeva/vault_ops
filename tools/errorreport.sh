#!/bin/sh
#set -x
#VAULTID=`echo select id from vmc.vmc_veeva_vault where pod = "'$POD'" \; | mysql --skip-column-names | sed '12d'`
VAULTID=`echo "select id from vmc.vmc_veeva_vault where domain_id in (select id from vmc.vmc_veeva_domain where domain_type in ('Production','Sandbox')) and pod='VV1'" | mysql --skip-column-names`
LOGPATH=/usr/local/apache-tomcat/6.0.32/logs
SOLRLOGPATH=/usr/local/apache-tomcat/6.0.32-Solr/logs
DATE=`date --date "yesterday" +\%Y-\%m-\%d`
LOG="solr ui api users docs reports flow"
TEMP_LOG_DATA="/tmp/$LOG.parsed.txt"


#Get Totals First
for file in $LOG;
do
#Avoid cron errors, fake it
if [ -e "$file" ]
then
ls -l $file
else
touch $LOGPATH/$file
fi

echo " " > /tmp/$file.vaulterrors.txt
if [ $file == solr ];
then
#NCOUNT=`awk '/ERROR/ {print $5}' $SOLRLOGPATH/$file\.log\.$DATE | sed -e 's/vaultId://g' | wc -l`
awk '/ERROR/ {print $5}' $SOLRLOGPATH/$file\.log\.$DATE |  sed -e 's/vaultId://g' >> /tmp/$file.vaulterrors.txt 
NCOUNT=`cat /tmp/$file.vaulterrors.txt | sed -e '/^$/d' | wc -l`
else
#NCOUNT=`awk '/ERROR/ {print $5}' $LOGPATH/$file\.log\.$DATE | sed -e 's/vaultId://g' | wc -l`
awk '/ERROR/ {print $5}' $LOGPATH/$file\.log\.$DATE |  sed -e 's/vaultId://g'  >> /tmp/$file.vaulterrors.txt
NCOUNT=`cat /tmp/$file.vaulterrors.txt | sed -e '/^$/d' | wc -l`
fi

#/tmp/docs.vaulterrors.txt | grep vaultId: | sed -e 's/vaultId://g'

for logfile in $file
do
	# Take extension available in a filename
        case "$logfile" in
        solr) solr_result=$NCOUNT 
           ;;
        ui) ui_result=$NCOUNT 
           ;;
        api) api_result=$NCOUNT 
            ;;
        users) users_result=$NCOUNT 
             ;;
        docs) docs_result=$NCOUNT 
             ;;
        reports) reports_result=$NCOUNT 
             ;;
        flow) flow_result=$NCOUNT 
             ;;
        *) echo "Not processed"
           ;;
esac
done



done
echo "Total Errors for all Vaults on $DATE for VV1"
echo "-------------------------------------------------------------------------------------------------------------------------------------"
echo "solr=$solr_result ui=$ui_result api=$api_result users=$users_result docs=$docs_result reports=$reports_result flow=$flow_result"
echo "-------------------------------------------------------------------------------------------------------------------------------------"
echo "-------------------------------------------------------------------------------------------------------------------------------------"
echo " "
echo " "
echo " "
echo "--------------------------"
echo "Total Errors for Classes"
echo "--------------------------"

awk '/ERROR/ {print $9}' $LOGPATH/*.log\.$DATE | sort | uniq -c | sort -rn


#Get Totals for each vault 
	for vault in $VAULTID;
	do

        solr_vresult=`cat /tmp/solr.vaulterrors.txt | grep $vault | wc -l` 
        ui_vresult=`cat /tmp/ui.vaulterrors.txt | grep $vault | wc -l`
        api_vresult=`cat /tmp/api.vaulterrors.txt | grep $vault | wc -l`
        users_vresult=`cat /tmp/users.vaulterrors.txt | grep $vault | wc -l`
        docs_vresult=`cat /tmp/docs.vaulterrors.txt | grep $vault | wc -l`
        reports_vresult=`cat /tmp/reports.vaulterrors.txt | grep $vault | wc -l`
        flow_vresult=`cat /tmp/flow.vaulterrors.txt | grep $vault | wc -l`

echo " "
echo "Total Errors for Prod and SandBox Vault ID: $vault on $DATE"
echo "--------------------------------------------------------------------------------------------------------------------------------------"
echo "solr=$solr_vresult ui=$ui_vresult api=$api_vresult users=$users_vresult docs=$docs_vresult reports=$reports_vresult flow=$flow_vresult"
echo "--------------------------------------------------------------------------------------------------------------------------------------"
echo " "
done

#awk '/maxTime/ {print $5, " "}' $UILOG | sort | uniq -c | sort -rn
#awk '/ERROR/ {print $5}' $LOG.log.$DATE | sort | uniq -c > $TEMP_LOG_DATA
 


