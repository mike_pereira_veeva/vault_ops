/*
        lib_mysqludf_veevavault  - a library with miscellaneous Veeva vault functions
*/

DROP FUNCTION IF EXISTS lib_mysqludf_veevavault_info;
DROP FUNCTION IF EXISTS provision_vault;

CREATE FUNCTION lib_mysqludf_veevavault_info RETURNS string SONAME 'lib_mysqludf_veevavault.so';
CREATE FUNCTION provision_vault RETURNS int SONAME 'lib_mysqludf_veevavault.so';

