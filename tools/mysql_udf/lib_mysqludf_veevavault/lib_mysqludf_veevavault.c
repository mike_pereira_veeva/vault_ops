/* 
	lib_mysqludf_sys - a library with miscellaneous (operating) system level functions
	Copyright (C) 2007  Roland Bouman 
	Copyright (C) 2008-2009  Roland Bouman and Bernardo Damele A. G.
	web: http://www.mysqludf.org/
	email: mysqludfs@gmail.com, bernardo.damele@gmail.com
	
	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.
	
	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.
	
	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/
#if defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(WIN32)
#define DLLEXP __declspec(dllexport) 
#else
#define DLLEXP
#endif

#ifdef STANDARD
#include <string.h>
#include <stdlib.h>
#include <time.h>
#ifdef __WIN__
typedef unsigned __int64 ulonglong;
typedef __int64 longlong;
#else
typedef unsigned long long ulonglong;
typedef long long longlong;
#endif /*__WIN__*/
#else
#include <my_global.h>
#include <my_sys.h>
#endif
#include <mysql.h>
#include <m_ctype.h>
#include <m_string.h>
#include <stdlib.h>

#include <ctype.h>

#ifdef HAVE_DLOPEN
#ifdef	__cplusplus
extern "C" {
#endif

#define LIBVERSION "lib_mysqludf_sys version 0.0.3"

#ifdef __WIN__
#define SETENV(name,value)		SetEnvironmentVariable(name,value);
#else
#define SETENV(name,value)		setenv(name,value,1);		
#endif

DLLEXP 
my_bool lib_mysqludf_veevavault_info_init(
	UDF_INIT *initid
,	UDF_ARGS *args
,	char *message
);

DLLEXP 
void lib_mysqludf_veevavault_info_deinit(
	UDF_INIT *initid
);

DLLEXP 
char* lib_mysqludf_veevavault_info(
	UDF_INIT *initid
,	UDF_ARGS *args
,	char* result
,	unsigned long* length
,	char *is_null
,	char *error
);

/**
 * provision_vault
 * 
 * executes a script to provision a new vault
 * Beware that this can be a security hazard.
 */
DLLEXP 
my_bool provision_vault_init(
	UDF_INIT *initid
,	UDF_ARGS *args
,	char *message
);

DLLEXP 
void provision_vault_deinit(
	UDF_INIT *initid
);

DLLEXP 
my_ulonglong provision_vault(
	UDF_INIT *initid
,	UDF_ARGS *args
,	char *is_null
,	char *error
);


#ifdef	__cplusplus
}
#endif

/**
 * lib_mysqludf_veevavault_info
 */
my_bool lib_mysqludf_veevavault_info_init(
	UDF_INIT *initid
,	UDF_ARGS *args
,	char *message
){
	my_bool status;
	if(args->arg_count!=0){
		strcpy(
			message
		,	"No arguments allowed (udf: lib_mysqludf_veevavault_info)"
		);
		status = 1;
	} else {
		status = 0;
	}
	return status;
}
void lib_mysqludf_veevavault_info_deinit(
	UDF_INIT *initid
){
}
char* lib_mysqludf_veevavault_info(
	UDF_INIT *initid
,	UDF_ARGS *args
,	char* result
,	unsigned long* length
,	char *is_null
,	char *error
){
	strcpy(result,LIBVERSION);
	*length = strlen(LIBVERSION);
	return result;
}

my_bool provision_vault_init(
	UDF_INIT *initid
,	UDF_ARGS *args
,	char *message
){
	if (args->arg_count > 0
	    && args->arg_type[0] != INT_RESULT) {
	    strcpy(message, "instance-num must be an integer value");
	    return 1;
	}

	if (args->arg_count > 1
        && args->arg_type[1] != INT_RESULT) {
        strcpy(message, "instance-num must be an integer value");
        return 1;
	}

	if (args->arg_count > 2
        && args->arg_type[2] != STRING_RESULT) {
        strcpy(message, "template-db must be an string value");
        return 1;
    }

    if (args->arg_count > 3
        && args->arg_type[3] != STRING_RESULT) {
        strcpy(message, "target-db must be an string value");
        return 1;
    }

    if (args->arg_count > 4) {
        strcpy(message, "too many arguments");
        return 1;
    }

	return 0;
}
void provision_vault_deinit(
	UDF_INIT *initid
){
}
my_ulonglong provision_vault(
	UDF_INIT *initid
,	UDF_ARGS *args
,	char *is_null
,	char *error
){
    char cmd[512];
    int rc;
    long long instance_val;
    long long port_val;
    char *template_val;
    char *target_val;
    int string_len;
  
    instance_val = *((long long*) args->args[0]);
    sprintf(cmd, "/data2/mysql/provision_vault %llu", instance_val);

    if (args->arg_count > 1) {
        port_val = *((long long*) args->args[1]);
        strcat(cmd, " ");
        sprintf(&cmd[strlen(cmd)], "%llu", port_val);
    }

    if (args->arg_count > 2) {
        template_val = (char *)args->args[2];
        string_len = args->lengths[2];
        strcat(cmd, " ");
        strncat(cmd, template_val, string_len);
    }

    if (args->arg_count > 3) {
        target_val = (char *)args->args[3];
        string_len = args->lengths[3];
        strcat(cmd, " ");
        strncat(cmd, target_val, string_len);
    }

    rc = system(cmd);
    if ( rc == -1 )  {
        perror("Could not fork: ");
    }
    return rc;
}

#endif /* HAVE_DLOPEN */
