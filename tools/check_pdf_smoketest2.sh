HOSTNAME=$1

# Set return STATUS codes for Nagios

OK=0
CRITICAL=2

curl -F "file=@/etc/nagios/msft-files/excel.xlsx" -F "submit=Send" https://$HOSTNAME/pdf/pdfgenerator/convert/all > /tmp/excel.pdf

EXCEL=`head -5 /tmp/excel.pdf  | grep "%PDF-"|wc -l`
if [ $EXCEL -lt 1 ]; then
   echo "SmokeTest Excel PDF Rendering FAILED"
   exit $CRITICAL;
 fi

curl -F "file=@/etc/nagios/msft-files/powerpoint.pptx" -F "submit=Send" https://$HOSTNAME/pdf/pdfgenerator/convert/all > /tmp/powerpoint.pdf

POWERPOINT=`head -5 /tmp/powerpoint.pdf  | grep "%PDF-"|wc -l`
if [ $POWERPOINT -lt 1 ]; then
   echo "SmokeTest PowerPoint PDF Rendering FAILED"
   exit $CRITICAL;
fi

curl -F "file=@/etc/nagios/msft-files/word.docx" -F "submit=Send" https://$HOSTNAME/pdf/pdfgenerator/convert/all > /tmp/word.pdf
WORD=`head -5 /tmp/word.pdf  | grep "%PDF-"|wc -l`
if [ $WORD -lt 1 ]; then
   echo "SmokeTest Word PDF Rendering FAILED"
   exit $CRITICAL;
 fi

curl -F "file=@/etc/nagios/msft-files/2253.pdf" -F "submit=Send" https://$HOSTNAME/pdf/pdfgenerator/convert/all > /tmp/adobe.pdf
ADOBE=`head -5 /tmp/adobe.pdf  | grep "%PDF-"|wc -l`
if [ $ADOBE -lt 1 ]; then
   echo "SmokeTest for Secure Adobe PDF Rendering FAILED"
   exit $CRITICAL;
 fi

curl -F "file=@/etc/nagios/msft-files/HeartStartFR3AED.pdf" -F "submit=Send" https://$HOSTNAME/pdf/pdfgenerator/convert/all > /tmp/adobe2.pdf
ADOBE2=`head -5 /tmp/adobe2.pdf  | grep "%PDF-"|wc -l`
if [ $ADOBE2 -lt 1 ]; then
   echo "SmokeTest for Secure Adobe PDF Rendering FAILED"
   exit $CRITICAL;
 fi

curl -F "file=@/etc/nagios/msft-files/test.gif" -F "submit=Send" https://$HOSTNAME/pdf/pdfgenerator/convert/all > /tmp/test.gif
GIF=`head -5 /tmp/test.gif  | grep "%PDF-"|wc -l`
if [ $GIF -lt 1 ]; then
   echo "SmokeTest for Secure Adobe PDF Rendering FAILED"
   exit $CRITICAL;
 fi



echo "SmokeTest PDF Rendering OK"
exit $OK
