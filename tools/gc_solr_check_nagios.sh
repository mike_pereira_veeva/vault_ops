#!/bin/sh
#set -x
LOGPATH=/usr/local/apache-tomcat/6.0.32-Solr/logs
DATE=`date --date "yesterday" +\%Y-\%m-\%d`
LOG="gc.log"
TEMP_LOG_DATA="/tmp/$LOG.parsed.txt"

DATEN=`date --date "today" +\%Y-\%m-\%dT\%H`
NCOUNT=`grep "Full GC" $LOGPATH/$LOG | grep $DATEN | wc -l`   
#echo "Count is $NCOUNT"

gc_check=$NCOUNT 

#For Nagios NRPE client
OK=0
WARNING=1
CRITICAL=2

SERVERNAME=`hostname`

if [ $gc_check -ge 10 ];
then
echo "Count is $gc_check" > /tmp/loggc_nagios.txt
grep "Full GC" $LOGPATH/$LOG | grep `date --date "today" +\%Y-\%m-\%dT\%H` >> /tmp/loggc_nagios.txt
echo "alert"
exit $CRITICAL
elseif [ $gc_check -ge 5 ];
echo "Count is $gc_check" > /tmp/loggc_nagios.txt
grep "Full GC" $LOGPATH/$LOG | grep `date --date "today" +\%Y-\%m-\%dT\%H` >> /tmp/loggc_nagios.txt
echo "warn"
exit $WARNING
else
echo "Count is $gc_check and is OK"
exit $OK
fi
