#!/bin/sh
#set -x
DATE=`date --date "yesterday" +\%Y-\%m-\%d`
PATH=/var/lib/mysql/log
#PATH="/home/root2"
DEST="/data3/logbackup/mysqlslow"

#Copy slow log file with yesterday date to /data3/logbackups area
cd $PATH
/bin/cp slow.log $DEST/slow.log.$DATE

#truncate current slow.log file
echo " " > $PATH/slow.log
