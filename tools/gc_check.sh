#!/bin/sh
#set -x
LOGPATH=/usr/local/apache-tomcat/6.0.32/logs
DATE=`date --date "yesterday" +\%Y-\%m-\%d`
LOG="gc.log"
TEMP_LOG_DATA="/tmp/$LOG.parsed.txt"

DATEN=`date --date "today" +\%Y-\%m-\%dT\%H`
NCOUNT=`grep "Full GC" $LOGPATH/$LOG | grep $DATEN | wc -l`   
echo "Count is $NCOUNT"

gc_check=$NCOUNT 


MAILTO="vaultoutage@veevasystems.com mike.pereira@veevasystems.com derek.allwardt@veevasystems.com jon.stone@veevasystems.com"
#MAILTO="mike.pereira@veevasystems.com"
export REPLYTO="sysadmin@veevasystems.com"
SERVERNAME=`hostname`

if [ $gc_check -ge 10 ];
then
echo "Count is $gc_check" > /tmp/loggc.txt
grep "Full GC" $LOGPATH/$LOG | grep `date --date "today" +\%Y-\%m-\%dT\%H` >> /tmp/loggc.txt
mail -s"The gc log is over ten Full GC counts `date +\%m/\%d/\%Y` for $SERVERNAME"  "$MAILTO"   -- -f $REPLYTO < /tmp/loggc.txt
echo "alert"
else
echo "$gc_check"
echo "ok"
fi

