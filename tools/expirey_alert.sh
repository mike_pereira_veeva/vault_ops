#!/bin/bash
#set -x
LOG="/usr/local/apache-tomcat/6.0.32/logs/docs.log"
LOGLOAD="/usr/local/apache-tomcat/6.0.32/logs/loader.log"
FILE=/tmp/errorExpiry
#THRESH="-91115000"
#THRESH="-4000"
THRESH="-1000"
THRESHLOAD="-1000"
CHECKGNT=`tail $THRESH $LOG | grep "getNotificationTemplate(instanceId, userId, templateName)" | head -1 | awk '{print $8}'`
CHECKGN=`tail $THRESH $LOG | grep "getNotifications(instance, recipientId, documentId, majorVersion, minorVersion, templateId)" | head -1 | awk '{print $8}'`
CHECKSENPD=`tail $THRESH $LOG | grep "sendExpiryNotificationsPerDoc(instance, docVersionId)" | head -1 | awk '{print $8}'`
CHECKSENPDU=`tail $THRESH $LOG | grep "sendExpiryNotificationsPerDoc()" | head -1 | awk '{print $8}'`
CHECKSENFAILED=`tail $THRESH $LOG | grep "notification process failed for instance_" | head -1 | awk '{print $8}'`
CHECKSENUOED=`tail $THRESH $LOG | grep "notifyUserOfExpiredDocs(instanceId) documentAttribute is null for instance_" | head -1 | awk '{print $8}'`
CHECKSENEX=`tail $THRESH $LOG | grep "Exception processing expired notification for instance_" | head -1 | awk '{print $8}'`
CHECKSENUEX=`tail $THRESH $LOG | grep "notifyUserOfApproachingExpiry()" | head -1 | awk '{print $8}'`
#MAILTO="mike.pereira@veevasystems.com"
MAILTO="mike.pereira@veevasystems.com kasim.tuman@veevasystems.com"
export REPLYTO="vault_sysadmin@veevasystems.com"
#Nagios return status codes when get Nagios installed on prod systems
OK=0
WARNING=1
CRITICAL=2
SERVERNAME=`hostname`;

# Clean File
echo " " > $FILE

#Error log checks
if [ "$CHECKSENFAILED" == "ERROR" ];
then
echo "alert for -expired or failed-"
tail $THRESH $LOG | grep "notification process failed for instance_" > $FILE
mail   -s"alert for -expired or failed-: `date +\%m/\%d/\%Y` for $SERVERNAME "  "$MAILTO"   -- -f $REPLYTO < $FILE
echo $CRITICAL
else
# For testing email delivery
#grep "WebApplicationContext" $LOG | head -3 > $FILE
#mail   -s"alert for -expired or failed-: `date +\%m/\%d/\%Y` for $SERVERNAME "  "$MAILTO"   -- -f $REPLYTO < $FILE
echo $OK
fi

if [ "$CHECKSENUOED" == "ERROR" ];
then
echo "alert for -document attribute is null-"
tail $THRESH $LOG | grep "notifyUserOfExpiredDocs(instanceId) documentAttribute is null for instance_" > $FILE
mail   -s"alert for -document attribute is null-: `date +\%m/\%d/\%Y` for $SERVERNAME "  "$MAILTO"   -- -f $REPLYTO < $FILE
echo $CRITICAL
else
echo $OK
fi

if [ "$CHECKSENEX" == "ERROR" ];
then
echo "alert for -expired notification-"
tail $THRESH $LOG | grep "Exception processing expired notification for instance_" > $FILE
mail   -s"alert for -expired notification-: `date +\%m/\%d/\%Y` for $SERVERNAME "  "$MAILTO"   -- -f $REPLYTO < $FILE
echo $CRITICAL
else
echo $OK
fi

if [ "$CHECKSENUEX" == "ERROR" ];
then
echo "alert for -unable to create-"
tail $THRESH $LOG | grep "notifyUserOfApproachingExpiry()" > $FILE
mail   -s"alert for -unable to create-: `date +\%m/\%d/\%Y` for $SERVERNAME"  "$MAILTO"   -- -f $REPLYTO < $FILE
echo $CRITICAL
else
echo $OK
fi

if [ "$CHECKGN" == "ERROR" ];
then
echo "alert for -exception while finding notification for document-"
tail $THRESH $LOG | grep "getNotifications(instance, recipientId, documentId, majorVersion, minorVersion, templateId)" > $FILE
mail   -s"alert for -exception while finding notification for document-: `date +\%m/\%d/\%Y` for $SERVERNAME"  "$MAILTO"   -- -f $REPLYTO < $FILE
echo $CRITICAL
else
echo $OK
fi

if [ "$CHECKGNT" == "ERROR" ];
then
echo "alert for -exception retrieving notification template for-"
tail $THRESH $LOG | grep "getNotificationTemplate(instanceId, userId, templateName)" > $FILE
mail   -s"alert for -exception retrieving notification template for-: `date +\%m/\%d/\%Y` for $SERVERNAME" "$MAILTO"   -- -f $REPLYTO < $FILE
echo $CRITICAL
else
echo $OK
fi

if [ "$CHECKSENPD" == "ERROR" ];
then
echo "alert for -exception for document-"
tail $THRESH $LOG | grep "sendExpiryNotificationsPerDoc(instance, docVersionId)" > $FILE
mail   -s"alert for -exception for document-: `date +\%m/\%d/\%Y` for $SERVERNAME"  "$MAILTO"   -- -f $REPLYTO < $FILE
echo $CRITICAL
else
echo $OK
fi

if [ "$CHECKSENPDU" == "ERROR" ];
then
echo "alert for unable to expire document"
tail $THRESH $LOG | grep "sendExpiryNotificationsPerDoc()" > $FILE
mail   -s"alert for unable to expire document: `date +\%m/\%d/\%Y` for $SERVERNAME"  "$MAILTO"   -- -f $REPLYTO < $FILE
echo $CRITICAL
else
echo $OK
fi



TDATE=`date | awk '{print $1 $2 $3}'`
MDATE=`cat /tmp/DATE`
if [ "$TDATE" == "$MDATE" ];
then
echo "already checked"
exit 1; 
else
LOADSUCCESS=`tail $THRESHLOAD $LOGLOAD | grep "notifyUserOfApproachingExpiry() Notifications for all instances completed successfully" /usr/local/apache-tomcat/6.0.32/logs/loader.* | head -1 | awk '{print $13}'`
tail $THRESHLOAD $LOGLOAD | grep "notifyUserOfApproachingExpiry() Notifications for all instances completed successfully" /usr/local/apache-tomcat/6.0.32/logs/loader.* | head -1 | awk '{print $2}' > /tmp/doublcheck.txt
CHECKAGAIN=`tail $THRESHLOAD $LOGLOAD | grep "notifyUserOfApproachingExpiry() Notifications for all instances completed successfully" /usr/local/apache-tomcat/6.0.32/logs/loader.* | head -1 | awk '{print $2}'` 
DOUBLECHECK=`cat /tmp/doublcheck.txt`
if [ "$LOADSUCCESS" == "successfully." ];
then
echo ""
tail $THRESH $LOGLOAD | grep -A 4 "notifyUserOfApproachingExpiry() Notifications for all instances completed successfully" > $FILE	
mail   -s"Notifications for all instances completed successfully `date +\%m/\%d/\%Y` for $SERVERNAME"  "$MAILTO"   -- -f $REPLYTO < $FILE
else
echo "checked"
fi
fi

date | awk '{print $1 $2 $3}' > /tmp/DATE
