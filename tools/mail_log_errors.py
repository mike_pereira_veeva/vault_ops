#!/usr/local/bin/python
import os
import time
import sys
from stat import *
import re
from collections import deque
import smtplib
import MySQLdb
import string
from datetime import datetime,tzinfo,timedelta
from email.mime.text import MIMEText

def ignore_error(line):

    for error in fixed:        
        if re.match('^\s*$',error) and not re.match ('^\#',error):
            continue
        if string.find(line,error) >= 0 : 
            return True        
    return False
def lookup_user(vault_id,user_id):
    if user_id == "1":
        return "< System User >"
    db=MySQLdb.connect(read_default_file="~/.conf/.vba.cnf" , host='v1.auth.com', db='vaultauth')
    c=db.cursor()
    c.execute("""select concat('<user_name=',ifnull(user_name,'NULL'),' vault_name=',ifnull(if(vault_name='','NULL',vault_name),'NULL'),'>') from  auth_veeva_user u join auth_veeva_user_has_veeva_vault m   on u.id = m.veeva_user_id join  auth_veeva_vault v on v.id = m.veeva_vault_id  where  u.id = %s and v.id = %s """ % (user_id, vault_id,) )
    result=c.fetchone()
    if result is None:
        return "<User not found>"
    return result[0]

def tail( f, oldest_str,window=10000):
    BUFSIZ = 1024
    f.seek(0, 2)
    bytes = f.tell()
    size = window
    block = -1
    data = deque('')
    ts=oldest_str
    found=0
    linesFound=0
    while size > 0 and bytes > 0 and ts >= oldest_str :
        if (bytes - BUFSIZ > 0):
            # Seek back one whole BUFSIZ
            f.seek(block*BUFSIZ, 2)
            # read BUFFER
            read_buf=(f.read(BUFSIZ))
            line_tokens=re.match(".+?([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}),[0-9]{3}.*",read_buf,re.DOTALL)
            if line_tokens :
                ts=line_tokens.group(1)
                found=1
                data.appendleft(read_buf)
            elif found == 1:
                data.appendleft(read_buf)
        else:
            # file too small, start from begining
            f.seek(0,0)
            # only read what was not read
            read_buf=f.read(bytes)
            data.appendleft(read_buf)

        linesFound = read_buf.count('\n')
        size -= linesFound
        bytes -= BUFSIZ
        block -= 1
        
    if found==0:
        return None
    else:
        return '\n'.join(''.join(data).splitlines()[-window:])

def get_latest_logs(top):


    now=time.time()
    # look back a certain number of seconds in log file
    oldest=now - window_secs
    oldest_str=time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime(oldest))
    now_str=time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime(now))

    # for email lsubject line,   get times in  Pacific timezone
    os.environ['TZ'] = 'US/Pacific'
    time.tzset()
    pacific_from=time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(oldest))
    os.environ['TZ'] = 'US/Pacific'
    time.tzset()
    pacific_to=time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(now))




    # look  through log files in  the dir that  match our list
    for f in os.listdir(top):
        try:
            logs.index(f)
        except ValueError:
            continue

        output=""
        errors=0
        in_error=0
        pathname = os.path.join(top, f)
        statinfo = os.stat(pathname)
        if  statinfo.st_mtime > oldest :
            found=0
            f2 = open(pathname, 'r')
            text=tail(f2,oldest_str)
            if text  is not None:
                for line in text.split('\n'):
                    if  re.match('.+? ERROR .*',line) and not ignore_error(line) :
                        errors += 1
                        in_error = 1
                        tokens=re.match('.+?vaultId:([0-9]+)\s+userId:([0-9]+).*',line)
                        info="<>"
                        if tokens:
                            info=lookup_user(tokens.group(1), tokens.group(2))
                        output =  output + "\n" + "=" * 20 +  info + "=" * 20 + "\n" + line
                    elif in_error > 0 and in_error < after_context :
                        output =  output + '\n'  + line
                        in_error += 1
                    else:
                        in_error = 0
        if errors > 0 :
            msg = MIMEText(output)
            msg['Subject'] = 'VV_POD %i ERROR(S) in %s: %s to %s ' %  (errors , f, pacific_from, pacific_to)
            msg['From'] = MAILFROM
            msg['To'] = MAILTO

            # Send the message via our own SMTP server, but don't include the

            s = smtplib.SMTP('localhost')
            s.sendmail(MAILFROM, MAILTO.split(",") , msg.as_string())
            s.quit()            

if __name__ == '__main__' :

    logs=[ "docs.log", "annotate-java.log", "ui.log", "workflow.log","users.log","api.log","flow.log","reports.log" ]
    ignore_file="/data2/conf/ignore_errors.txt"
    dir='/usr/local/apache-tomcat/6.0.32/logs'
    MAILTO="sita.b@veevasystems.com,eric.bezar@veevasystems.com,vault_engineering@veevasystems.com,steve.harper@veevasystems.com,kate.wilber@veevasystems.com,jason.harlander@veevasystems.com,lexi.viripaeff@veevasystems.com,george.lee@veevasystems.com,mike.pereira@veevasystems.com,nick.goldammer@veevasystems.com,patrick.mcmajors@veevasystems.com"
    #    MAILTO="andrew.platts@veevasystems.com"
    MAILFROM='vault_error@veevasystems.com'
    # how far back to look in log files
    window_secs=301
    # how many lines of context after error
    after_context=10

    #  read lines to ignore
    try:
        ignore = open(ignore_file,'r')
    except:
        if IOError: 
            sys.exit("/data2/conf/ignore_errors.txt is required - even if empty")

    fixed = ignore.read().split('\n')


    get_latest_logs(dir)

