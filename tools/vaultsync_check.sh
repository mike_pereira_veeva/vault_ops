#!/bin/sh
#set -x
LOGPATH=/tmp
PDATE=`date --date "30 minutes ago" +\%H\:\%M`
NOWDATE=`date --date "now" +\%H\:\%M:`

#For Nagios NRPE client
OK=0
WARNING=1
CRITICAL=2

CHECK=`find /tmp/ -name "*drsync.log" -mmin -30 | wc -l`
#CHECK=`find /tmp/ -name "*drsync.log" -mmin -0 | wc -l`
if [ "$CHECK" == "0" ];
then
echo "Problem, verify drsync cron is running"
exit $CRITICAL;
else
echo -n "Rsync script is running, "
echo "File check count is $CHECK"
exit $OK;
fi

