#!/bin/sh
#set -x
#VAULTID=`echo select id from vmc.vmc_veeva_vault where pod = "'$POD'" \; | mysql --skip-column-names | sed '12d'`
LOGPATH=/usr/local/apache-tomcat/6.0.32/logs
SOLRLOGPATH=/usr/local/apache-tomcat/6.0.32-Solr/logs
DATE=`date --date "yesterday" +\%Y-\%m-\%d`
LOG="solr ui api users docs reports flow"
TEMP_LOG_DATA="/tmp/$LOG.parsed.txt"


for file in $LOG;
do
echo " " > /tmp/$file.logsize.txt
if [ $file == solr ];
then
#NCOUNT=`awk '/ERROR/ {print $5}' $SOLRLOGPATH/$file\.log\.$DATE | sed -e 's/vaultId://g' | wc -l`
NCOUNT=`du -s $SOLRLOGPATH/$file\.log |  awk '{print $1}'`  
else
NCOUNT=`du -s $LOGPATH/$file\.log |  awk '{print $1}'`   
fi


for logfile in $file
do
	# Take extension available in a filename
        case "$logfile" in
        solr) solr_sizelg=$NCOUNT 
           ;;
        ui) ui_sizelg=$NCOUNT 
           ;;
        api) api_sizelg=$NCOUNT 
            ;;
        users) users_sizelg=$NCOUNT 
             ;;
        docs) docs_sizelg=$NCOUNT 
             ;;
        reports) reports_sizelg=$NCOUNT 
             ;;
        flow) flow_sizelg=$NCOUNT 
             ;;
        *) echo "Not processed"
           ;;
esac
done



done
echo "Total Size for all Vault Logs "
echo "-------------------------------------------------------------------------------------------------------------------------------------"
echo "solr=$solr_sizelg ui=$ui_sizelg api=$api_sizelg users=$users_sizelg docs=$docs_sizelg reports=$reports_sizelg flow=$flow_sizelg"
echo "-------------------------------------------------------------------------------------------------------------------------------------"
echo " "

MAILTO="vaultoutage@veevasystems.com mike.pereira@veevasystems.com jon.stone@veevasystems.com"
export REPLYTO="sysadmin@veevasystems.com"
SERVERNAME=`hostname`

#if [ $solr_sizelg -ge 50 ];
if [ $solr_sizelg -ge 5103980 ];
then
echo "Size is $solr_sizelg" > /tmp/logsize.txt
mail -s"The solr log is over 5G `date +\%m/\%d/\%Y` for $SERVERNAME"  "$MAILTO"   -- -f $REPLYTO < /tmp/logsize.txt
echo "alert"
else
echo "ok"
fi

if [ $docs_sizelg -ge 5103980 ];
then
echo "Size is $docs_sizelg" > /tmp/logsize.txt
mail -s"The docs log is over 5G `date +\%m/\%d/\%Y` for $SERVERNAME"  "$MAILTO"   -- -f $REPLYTO < /tmp/logsize.txt 
else
echo "ok"
fi

if [ $ui_sizelg -ge 5103980 ];
then
echo "Size is $ui_sizelg" > /tmp/logsize.txt
mail -s"The ui log is over 5G `date +\%m/\%d/\%Y` for $SERVERNAME"  "$MAILTO"   -- -f $REPLYTO < /tmp/logsize.txt 
else
echo "ok"
fi

if [ $api_sizelg -ge 5103980 ];
then
echo "Size is $api_sizelg" > /tmp/logsize.txt
mail -s"The api log is over 5G `date +\%m/\%d/\%Y` for $SERVERNAME"  "$MAILTO"   -- -f $REPLYTO < /tmp/logsize.txt 
else
echo "ok"
fi
