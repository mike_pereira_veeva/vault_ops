#!/bin/bash
target=10.209.128.160 # sjcm001
rsync -tavz /data3/vault/logs stage:/data3/backups/vv1    > /dev/null
rsync -tavz /data3/vault/Solr-logs/ stage:/data3/backups/vv1/logs-Solr    > /dev/null
#rsync -tavz /usr/local/apache-tomcat/6.0.32-Solr/logs/ stage:/data3/backups/vv1/logs-Solr    > /dev/null
rsync -tavz /usr/local/apache-tomcat/6.0.32/activity stage:/data3/backups/vv1 > /dev/null
chown -R tomcat6:tomcat6 /data3/logbackup/mysqlslow
chmod -R 777 /data3/logbackup/mysqlslow
rsync -tavz /data3/logbackup/mysqlslow stage:/data3/backups/vv1 > /dev/null
LOGPATH=/var/lib/mysql/log
NEWLOGPATH=/data/dump
LOG="$NEWLOGPATH/slow.log"
/bin/cp $LOGPATH/slow.log $NEWLOGPATH
chmod 666 $NEWLOGPATH/slow.log

