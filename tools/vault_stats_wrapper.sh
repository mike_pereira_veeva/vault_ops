#!/bin/bash 
function run_report() {
    TYPE=$1
    /home/root2/vault_stats_rpt.sh $TYPE > /tmp/$$.$TYPE.rpt
    mail   -s"Vault Cust Activity (${TYPE} domains): `date --date "yesterday" +\%m/\%d/\%Y` "  "$MAILTO"   -- -f $REPLYTO < /tmp/$$.$TYPE.rpt
}


/home/root2/vault_stats.sh 2>&1 > /dev/null
ssh vv2.veevavault.com "/home/root2/vault_stats.sh" 2>&1 > /dev/null 
ssh -n -p 56922 vv3.veevavault.com "/home/root2/vault_stats.sh" 2>&1 > /dev/null
ssh -n -p 56922 vv4.veevavault.com "/home/root2/vault_stats.sh" 2>&1 > /dev/null

host=`grep  '^#hostname' /home/root2/.conf/.vba.cnf|cut -f2 -d'='` 
export REPLYTO="vault_sysadmin@veevasystems.com"


MAILTO="vault_perf_stats@veevasystems.com"

run_report "Production"

MAILTO="eric.bezar@veevasystems.com mich.wallace@veevasystems.com mike.pereira@veevasystems.com"

run_report "Sandbox"

export TZ="/usr/share/zoneinfo/US/Pacific"
DATE=`date --date "yesterday" +\%m/\%d/\%Y`





