#!/bin/bash 
#set -x
function run_report() {
    sh /home/root2/performance.sh > /tmp/$$.per.rpt
    mail   -s"VV1 Performance report for `date --date "yesterday" +\%m/\%d/\%Y` "  "$MAILTO"   -- -f $REPLYTO < /tmp/$$.per.rpt
}


host=`grep  '^#hostname' /home/root2/.conf/.vba.cnf|cut -f2 -d'='` 
export REPLYTO="vault_sysadmin@veevasystems.com"


MAILTO="mike.pereira@veevasystems.com vault_engineering@veevasystems.com igor.tsives@veevasystems.com"

run_report 

export TZ="/usr/share/zoneinfo/US/Pacific"
DATE=`date --date "yesterday" +\%m/\%d/\%Y`





