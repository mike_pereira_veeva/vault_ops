#!/bin/bash 
#set -x
function run_report() {
    sh /home/root2/errorreport.sh > /tmp/$$.err.rpt
    mail   -s"VV1 Vault error report for `date --date "yesterday" +\%m/\%d/\%Y` "  "$MAILTO"   -- -f $REPLYTO < /tmp/$$.err.rpt
}


host=`grep  '^#hostname' /home/root2/.conf/.vba.cnf|cut -f2 -d'='` 
export REPLYTO="vault_sysadmin@veevasystems.com"


MAILTO="mike.pereira@veevasystems.com vault_engineering@veevasystems.com"
#MAILTO="mike.pereira@veevasystems.com derek.allwardt@veevasystems.com"

run_report 

export TZ="/usr/share/zoneinfo/US/Pacific"
DATE=`date --date "yesterday" +\%m/\%d/\%Y`





