#!/bin/sh
#set -x
LOGPATH=/usr/local/apache-tomcat/6.0.32/logs
LOG="gc.log"
TEMP_LOG_DATA="/tmp/$LOG.10parsed.txt"
PDATE=`date --date "5 minutes ago" +\%H\:\%M:`

#DATEN=`date --date "today" +\%Y-\%m-\%dT\%H`
NCOUNT=`grep "Full GC" $LOGPATH/$LOG | grep $PDATE | wc -l`   
#echo "Count is $NCOUNT"

gc_check=$NCOUNT 

#For Nagios NRPE client
OK=0
WARNING=1
CRITICAL=2

SERVERNAME=`hostname`

if [ $gc_check -ge 10 ];
then
echo "Count is $gc_check" > /tmp/loggc10_nagios.txt
grep "Full GC" $LOGPATH/$LOG | grep `date --date "today" +\%Y-\%m-\%dT\%H` >> /tmp/loggc10_nagios.txt
echo "alert"
exit $CRITICAL
fi

if [ $gc_check -ge 5 ];
then
echo "Count is $gc_check" > /tmp/loggc10_nagios.txt
grep "Full GC" $LOGPATH/$LOG | grep `date --date "today" +\%Y-\%m-\%dT\%H` >> /tmp/loggc10_nagios.txt
echo "warn"
exit $WARNING
fi

echo "Count is $gc_check and is OK"
exit $OK

