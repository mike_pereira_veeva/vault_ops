# Flush
iptables -F
# Setup logging (for debugging)
#iptables -A INPUT -j LOG
#Define default policy for Filter tables, reject connections
iptables -P FORWARD DROP
iptables -P INPUT DROP
iptables -P OUTPUT DROP
# Setup to allow all connections for loopback
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT
# Setup to allow all connections for internal network connections in softlayer 
iptables -A INPUT  -i eth0 -j ACCEPT
iptables -A OUTPUT -o eth0 -j ACCEPT
# Allow pinging for external network
iptables -A INPUT -i eth1 -p icmp -j ACCEPT
#Allow all connections to go out for external network
iptables -A OUTPUT -o eth1 -p icmp -j ACCEPT
iptables -A OUTPUT -o eth1 -p all -j ACCEPT
#Allow ssh from external network (TCPWrappers will deny certain networks)
iptables -A INPUT -i eth1 -p tcp --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
# Setup for Filter table to allow certain networks to go through ports http, https, 8080 and 8443 for Tomcat after PREROUTE table did the redirects (see below)
iptables -A INPUT -i eth1 -p tcp -s 206.169.98.0/24 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT 
iptables -A INPUT -i eth1 -p tcp -s 206.169.98.0/23 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT  
iptables -A INPUT -i eth1 -p tcp -s 203.100.78.0/24 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT     
iptables -A INPUT -i eth1 -p tcp -s 203.100.78.0/23 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT        
iptables -A INPUT -i eth1 -p tcp -s 209.119.63.0/24 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT         
iptables -A INPUT -i eth1 -p tcp -s 50.17.0.0/16 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.18.0.0/16 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.18.0.0/15 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.23.0.0/16 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT 
iptables -A INPUT -i eth1 -p tcp -s 70.60.142.0/23 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 70.60.142.0/24 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 206.169.98.0/24 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 206.169.98.0/23 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 203.100.78.0/23 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 209.119.63.0/24 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 209.207.0.0/16 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.17.0.0/16 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.18.0.0/16 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.18.0.0/15 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.23.0.0/16 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 70.60.142.0/23 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 70.60.142.0/24 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.97.0.0/16 --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.97.0.0/16 --dport 8443 -m state --state NEW,ESTABLISHED -j ACCEPT
#Setup exceptions for mysql port traffic
iptables -A INPUT -i eth1 -p tcp -s 50.97.0.0/16 --dport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.23.94.66/32 --dport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.23.94.67/32 --dport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.97.218.53/32 --dport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.97.218.54/32 --dport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.97.218.54/31 --dport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.97.249.178/31 --dport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.97.249.178/32 --dport 3306 -m state --state NEW,ESTABLISHED -j ACCEPT
#Open range of ports from 8000 to 8999 (were orginally defined in Security group for Amazon)
iptables -A INPUT -i eth1 -p tcp -s 50.97.0.0/16 --dport 8000:8999 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.23.0.0/16 --dport 8000:8999 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth1 -p tcp -s 50.18.0.0/15 --dport 8000:8999 -m state --state NEW,ESTABLISHED -j ACCEPT
# Set to redirect ports http and https to tomcat ports 8080 and 8443 for SSL, then go to filter table to block connections
iptables -A PREROUTING -t nat  -p tcp --dport 443 -j REDIRECT --to-port 8443
iptables -A PREROUTING -t nat  -p tcp --dport 80 -j REDIRECT --to-port 8080
service iptables save

