#!/bin/sh
#set -x
TEMP_DATA="/tmp/ffmpeg.txt"

#DATEN=`date --date "today" +\%Y-\%m-\%dT\%H`
FTCOUNT=`ps -eLf | grep  ffmpe[g] | wc -l`   
FCOUNT=`ps -ef | grep  ffmpe[g] | awk '{print $8}' | wc -l`   

echo -n "Thread $FTCOUNT"
echo -n " Process $FCOUNT"

threadcount_check=$FTCOUNT 
processcount_check=$FCOUNT 

#For Nagios NRPE client
OK=0
WARNING=1
CRITICAL=2

SERVERNAME=`hostname`

#if [ $threadcount_check -ge 10 ] && [ $processcount_check -eq 4 ];
if [ $threadcount_check -ge 20 ]; 
then
echo "Video Transcoding Count Thread count is $threadcount_check" > $TEMP_DATA 
echo "alert"

	CHECK_NORMAL=`cat /etc/nagios/nrpe.cfg | grep "32,30,28" | wc -l`
	if [ $CHECK_NORMAL -eq 0 ];
	then
	echo "Update Nagios Config to new settings"
	cat /etc/nagios/nrpe.cfg | sed s/18,16,14/32,30,28/ > /tmp/nagffmeg.tmp
	cp /tmp/nagffmeg.tmp /etc/nagios/nrpe.cfg
	/sbin/service nrpe reload 
	fi
exit $WARNING
fi


if [ $threadcount_check -le 10 ];
#if [ $threadcount_check -le 10 ] && [ $processcount_check -eq 0 ];
then
	echo -n "Count is $threadcount_check and is OK - "
	CHECK_NORMAL=`cat /etc/nagios/nrpe.cfg | grep "18,16,14" | wc -l`
	if [ $CHECK_NORMAL -eq 1 ];
	then
	echo "Config is normal load"
	else
	echo "Update Nagios Config to original settings"
	cat /etc/nagios/nrpe.cfg | sed s/32,30,28/18,16,14/ > /tmp/nagffmeg.tmp
	cp /tmp/nagffmeg.tmp /etc/nagios/nrpe.cfg
	/sbin/service nrpe reload
	fi
exit $OK
fi

