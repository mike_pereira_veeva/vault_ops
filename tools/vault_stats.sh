#!/bin/bash
#set -x
##
## sub usage()
## Display usage info
##
usage(){

  cat <<EOF
  Usage: $0 options
  
  Insert  vault stats for yesterday to a table in the directory db
  Options:
     -h                Show this message
EOF

}



#######################################################
# Main                                                #
#######################################################


export TZ="/usr/share/zoneinfo/US/Pacific"
DATE=`date --date "yesterday" +\%Y-\%m-\%d`

HR=`date +%z|tr -d 0`
MYSQL="mysql --defaults-file=/home/root2/.conf/.vba.cnf  --skip-column-names  -hlocalhost "
MYSQL_AUTH="mysql --defaults-file=/home/root2/.conf/.vba.cnf  --skip-column-names  -hv1.auth.com "
SOLR="/data2/Solr/woozle/solr"
APILOG="/data3/vault/logs/api.log.$DATE"
UILOG="/data3/vault/logs/ui.log.$DATE"
ANNOTATE="/data2/annotate/data/public"
DB="/data2/mysql/data"
TEMP_LOG_DATA="/tmp/apiparsed.txt"
TEMP_UILOG_DATA="/tmp/uiparsed.txt"

if [ -f $APILOG ] ; then
awk '/maxTime/ {print $5}' $APILOG | sort | uniq -c > $TEMP_LOG_DATA
else
echo "0" > $TEMP_LOG_DATA
fi

if [ -f $UILOG ] ; then
#grep maxTime $UILOG | grep ReportingCTController.viewerBody  | cut -d':' -f 5 | cut -d' ' -f1 | sort -n | uniq -c > $TEMP_UILOG_DATA   
grep maxTime $UILOG | grep ReportingCTController.viewerBody  | awk '{print $5}' | sort -n | uniq -c > $TEMP_UILOG_DATA   
else
echo "0" > $TEMP_UILOG_DATA
fi

#sql="select vi.id from vmc.vmc_veeva_vault vi where pod = 'VV1' and status <> 'Delete' order by 1";
#sql="select vi.id from vmc.vmc_veeva_vault vi where pod = 'VV1' and vault_name not like '%PMO%' and status <> 'Delete' order by 1";
sql="select vi.id from vmc.vmc_veeva_vault vi where pod = 'VV1' and vault_name not like '%PMO%' and status <> 'Delete' order by 1";
dbs=`echo $sql | $MYSQL `

metric[0]="active_user_count"
metric[1]="doc_count"
metric[2]="doc_version_count"
metric[3]="unique_login_count"
metric[4]="doc_upload_count"
metric[5]="doc_view_count"
metric[6]="active_workflow_count"
metric[7]="task_count"
metric[8]="new_workflow"
metric[9]="created_reports"
### from filesystem
metric[10]="vault_size_kbytes"
metric[11]="db_size_kbytes"
metric[12]="rendition_size_kbytes"
metric[13]="thumbnail_size_kbytes"
metric[14]="solr_size_kbytes"
metric[15]="annotate_size_kbytes"
metric[16]="new_api_calls"
metric[17]="rep_exe_calls"

for db in $dbs ; do 

 #   echo "db instance_${db}"
 #   echo "==============================="
    query[0]="select   count(*)   from vaultauth.auth_veeva_vault vi join vaultauth.auth_veeva_user_has_veeva_vault  vm on vm.veeva_vault_id = vi.id join vaultauth.auth_veeva_user vu on vu.id =  vm.veeva_user_id where user_name != 'system'  and active  and vi.id=$db"
    query[1]="select   count(*)   from instance_${db}.veeva_doc where deleted <> 'Y'"
    query[2]="select   count(*)   from instance_${db}.veeva_doc_version v join instance_${db}.veeva_doc d on d.id = v.document_id where v.deleted != 'Y' and d.deleted != 'Y'"
    query[3]="select   count(distinct user_id)   from vaultauth.auth_veeva_auth_audit where login_type in ('SSO Login','User Login','ui')  and login_result like '%Succes%' and vault_id = $db and login_time between \"$DATE\" - interval $HR  hour and  \"$DATE\" - interval $HR hour  + interval 86400 second" 
    query[4]="select   count(*)   from instance_${db}.veeva_doc_audit where event = 'UploadDoc'  and old_value is null and property is null and event_time between \"$DATE\" - interval $HR hour  and  \"$DATE\" - interval $HR hour + interval 86400 second" 
    query[5]="select   count(*)   from instance_${db}.veeva_doc_audit where event = 'GetDocumentVersion'  and event_time between \"$DATE\" - interval $HR hour  and  \"$DATE\" - interval $HR hour + interval 86400 second" 
    query[6]="SELECT sum(count) FROM ( SELECT COUNT(a.id) as count  FROM instance_${db}.veeva_flow_process_instance a union SELECT COUNT(DISTINCT PROC_INST_ID_)  as count FROM instance_${db}.ACT_RU_EXECUTION) AS T"
    query[7]="SELECT sum(count) FROM (SELECT COUNT(*)  as count FROM instance_${db}.veeva_flow_task  union select count(*)  as count from instance_${db}.ACT_RU_TASK) AS T"
 #  query[8]="SELECT count(*) FROM instance_${db}.veeva_flow_process_instance where created_date like \"$DATE%\""
    query[8]="select t1.r + t2.r as total_c from (SELECT count(*) as r FROM instance_${db}.veeva_flow_process_instance where created_date like \"$DATE%\") as t1 cross join (SELECT COUNT(*) as r FROM instance_${db}.veeva_flow_process_history WHERE created_date like  \"$DATE%\" AND status NOT IN ('cancelled', 'error')) as t2"
    query[9]="select count(*) from instance_${db}.veeva_report_format"
    
    len=${#query[*]} #Num elements in aarray

    values=""

    i=0
    while [ $i -lt $len ]; do
	CONNECTION=$MYSQL
	if  (( $i == 0 || $i == 3 )) ; then  CONNECTION=$MYSQL_AUTH; fi
	value=`echo ${query[$i]} | $CONNECTION`
	values[$i]=$value
	let i++
    done
    
    # sql="select base_file_location  from instance_${db}.veeva_vault"
    # filesystem=`echo $sql | $MYSQL `

    filesystem="/data2/vault/veevaVault/fileSystem/*/instance_${db}"

    if [ -d $filesystem ] ; then 
	vault_size_kbytes=`du  -ks $filesystem|cut -f1 `
	rendition_size_kbytes=$(( vault_size_kbytes - `du  -ks --exclude renditions $filesystem|cut -f1 ` ))
	thumbnail_size_kbytes=$(( vault_size_kbytes - `du  -ks --exclude thumbnails $filesystem|cut -f1 ` ))
    else
	vault_size_kbytes=0
	rendition_size_kbytes=0
	thumbnail_size_kbytes=0
    fi

    values[$i]=$vault_size_kbytes
    let i++

    db_size_kbytes=`du -ks ${DB}/instance_${db}|cut -f1`
    #echo "db_size_kbytes=$db_size_kbytes"

    values[$i]=$db_size_kbytes
    let i++

    values[$i]=$rendition_size_kbytes
    let i++
    values[$i]=$thumbnail_size_kbytes
    let i++

    if [ -d $SOLR/instance_$db ] ; then 
	solr_size_kbytes=`du  -ks $SOLR/instance_$db|cut -f1 `
    else
	solr_size_kbytes=0
    fi

    values[$i]=$solr_size_kbytes
    let i++

    if [ -d $ANNOTATE/repo-${db} ] ; then 
	annotate_size_kbytes=`du  -ks $ANNOTATE/repo-${db}|cut -f1 `
    else
	echo "WARNING: no annotations"
	annotate_size_kbytes=0
    fi

    values[$i]=$annotate_size_kbytes
    let i++

# Parse through API log and cache to temp file
       new_api_calls=`grep -w vaultId:${db} $TEMP_LOG_DATA | awk '{print $1}'`

     if [ -z "$new_api_calls" ] ; then
       new_api_calls=0
     fi

        values[$i]=$new_api_calls
        let i++

# Parse through UI log and cache report executes to temp file
       rep_exe_calls=`grep -w vaultId:${db} $TEMP_UILOG_DATA | awk '{print $1}'`

     if [ -z "$rep_exe_calls" ] ; then
       rep_exe_calls=0
     fi

        values[$i]=$rep_exe_calls
        let i++

    annotation_count=0
    tot_annotation_count=0
    for  note_date in `find /data2/annotate/data/private/repo-$db/derek.allwardt@veevasystems.com/notes/*/*/notes.txt -exec grep -o "date.>[^<]*" {} \; 2>/dev/null |cut -f2 -d '>'|tr "." ":"|tr " " "T" ` ; do
	date_time=`echo $note_date|tr "T"  " "` 
        pacific_time=`date --date "$date_time  UTC  + $HR hours" +%Y-%m-%d`
	echo "Vault $db:  $date_time : $pacific_time"
        if [ "$pacific_time" == "$DATE" ] ; then 
            let annotation_count=annotation_count+1
	else
	    let tot_annotation_count=tot_annotation_count+1
        fi                                      
    done
    echo "Vault $db:  annotations for $DATE : $annotation_count"
    echo "Vault $db:  annotations total : $tot_annotation_count"
    values[$i]=$annotation_count

    #echo "db instance_${db}"
    #echo "==============================="

    sql=`cat <<EOF
    insert into  directory.vault_stats (
     instance_id, metric_date, active_user_count, doc_count  , doc_version_count, unique_login_count, doc_upload_count, doc_view_count, active_workflow_count, task_count, new_workflow, created_reports, vault_size_kbytes, db_size_kbytes, rendition_size_kbytes, thumbnail_size_kbytes, solr_size_kbytes, annotate_size_kbytes, new_api_calls, rep_exe_calls, annotation_count
 ) VALUES ( 
     $db,        "$DATE"     ,    ${values[0]}  ,${values[1]},   ${values[2]}   , ${values[3]}      ,    ${values[4]} ,   ${values[5]},          ${values[6]},${values[7]},      ${values[8]},   ${values[9]},         ${values[10]},         ${values[11]},    ${values[12]}, ${values[13]} , ${values[14]}, ${values[15]}, ${values[16]}, ${values[17]}, ${values[18]}
 );
EOF`

    echo $sql | $MYSQL 
    if (($?)) ; then 
	echo "ERROR: Inserting  stats to vault_stats"
	exit 1
    fi

done 
