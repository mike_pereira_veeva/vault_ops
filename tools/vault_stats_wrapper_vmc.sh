#!/bin/bash
function run_report() {
    TYPE=$1
    /home/root2/vault_stats_rpt.sh $TYPE > /tmp/$$.$TYPE.rpt
    mail   -s"Vault Cust Activity (${TYPE} domains): `date --date "yesterday" +\%m/\%d/\%Y` "  "$MAILTO"   -- -f $REPLYTO < /tmp/$$.$TYPE.rpt
}


ssh -n -p 56922 root2@vv1.veevavault.com "/home/root2/vault_stats_vmc.sh" 2>&1 > /dev/null
ssh -n -p 56922 root2@vv2.veevavault.com "/home/root2/vault_stats_vmc.sh" 2>&1 > /dev/null
ssh -n -p 56922 root2@vv4.veevavault.com "/home/root2/vault_stats_vmc.sh" 2>&1 > /dev/null
ssh -n -p 56922 root2@vv3.veevavault.com "/home/root2/vault_stats_vmc.sh" 2>&1 > /dev/null
DATE=`date --date "yesterday" +\%Y-\%m-\%d`
cd /data/vaultstats
scp -P 56922 root2@vv1.veevavault.com:/data/vaultstats/vaultstat_$DATE.sql ./vv1_vaultstat_$DATE.sql
scp -P 56922 root2@vv2.veevavault.com:/data/vaultstats/vaultstat_$DATE.sql ./vv2_vaultstat_$DATE.sql
scp -P 56922 root2@vv3.veevavault.com:/data/vaultstats/vaultstat_$DATE.sql ./vv3_vaultstat_$DATE.sql
scp -P 56922 root2@vv4.veevavault.com:/data/vaultstats/vaultstat_$DATE.sql ./vv4_vaultstat_$DATE.sql
mysql --defaults-file=~/.conf/.vba.cnf directory < ./vv1_vaultstat_$DATE.sql
mysql --defaults-file=~/.conf/.vba.cnf directory < ./vv2_vaultstat_$DATE.sql
mysql --defaults-file=~/.conf/.vba.cnf directory < ./vv3_vaultstat_$DATE.sql
mysql --defaults-file=~/.conf/.vba.cnf directory < ./vv4_vaultstat_$DATE.sql


host=`grep  '^#hostname' /home/root2/.conf/.vba.cnf|cut -f2 -d'='`
export REPLYTO="vault_sysadmin@veevasystems.com"


MAILTO="vault_perf_stats@veevasystems.com"

run_report "Production"

MAILTO="eric.bezar@veevasystems.com mich.wallace@veevasystems.com mike.pereira@veevasystems.com"
#MAILTO="mike.pereira@veevasystems.com"

run_report "Sandbox"

export TZ="/usr/share/zoneinfo/US/Pacific"
DATE=`date --date "yesterday" +\%m/\%d/\%Y`
